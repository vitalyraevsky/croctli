package ru.baccasoft.croctli.android.specialcondition.parser.test;

import junit.framework.TestCase;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.gen.core.ArrayOfTaskActor;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.TaskActor;
import ru.baccasoft.croctli.android.specialcondition.IExpression;
import ru.baccasoft.croctli.android.specialcondition.parser.ParseException;
import ru.baccasoft.croctli.android.specialcondition.parser.Parser;


public class ParserTest extends TestCase {


    public void testParseHasDeadLine() throws ParseException {

        Parser parser = new Parser("Deadline.HasValue");
        IExpression<Boolean> expression = parser.parse();

        assertNotNull(expression);

        Task task = new Task();

        assertFalse( expression.compute(task) );

        task.setDeadlineDateTime(DateTime.now());

        assertTrue(expression.compute(task));

    }

    public void testParseDeadLineConditions() throws ParseException {

//      !IsCompleted && Deadline.HasValue && Deadline.Value.Date >= DateTime.Now.Date && Deadline.Value.Date <= DateTime.Now.AddDays(7).Date

        Parser parser = new Parser("Deadline.HasValue && Deadline.Value.Date >= DateTime.Now.Date && Deadline.Value.Date <= DateTime.Now.AddDays(7).Date");

        IExpression<Boolean> expression = parser.parse();

        assertNotNull(expression);

        Task task = new Task();

        assertFalse(expression.compute(task));

        task.setDeadlineDateTime(DateTime.now().plusDays(1));

        assertTrue(expression.compute(task));

    }

    public void testParseComplexConditions() throws ParseException  {

        Parser parser = new Parser("IsCompleted");

        IExpression<Boolean> expression = parser.parse();

        assertNotNull(expression);

        Task task = new Task();
        task.setExecutants(getExecutantsWithCompleteTask(false));
        task.setIsFavourite(false);
        task.setDeadlineDateTime(DateTime.now().plusDays(1));

        assertFalse(expression.compute(task));
        task.setIsFavourite(true);

        assertFalse(expression.compute(task));

    }

    public void testIsCompleted() throws ParseException  {

        Parser parser = new Parser("IsCompleted");

        IExpression<Boolean> expression = parser.parse();

        assertNotNull(expression);

        Task task = new Task();
        task.setExecutants( getExecutantsWithCompleteTask(true) );

        assertTrue(expression.compute(task));

    }


    public ArrayOfTaskActor getExecutantsWithCompleteTask(boolean isComplete) {

        ArrayOfTaskActor executants = new ArrayOfTaskActor();

        List<TaskActor> taskActors = new ArrayList<TaskActor>();

        TaskActor actor1 = new TaskActor();
        actor1.setIsComlete(isComplete);

        TaskActor actor2 = new TaskActor();
        actor2.setIsComlete(false);

        taskActors.add(actor1);
        taskActors.add(actor2);

        executants.setItem(taskActors);

        return executants;
    }

}
