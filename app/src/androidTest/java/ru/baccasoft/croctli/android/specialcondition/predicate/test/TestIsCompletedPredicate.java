package ru.baccasoft.croctli.android.specialcondition.predicate.test;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.gen.core.ArrayOfTaskActor;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.TaskActor;
import ru.baccasoft.croctli.android.specialcondition.predicate.IsCompletedPredicate;

public class TestIsCompletedPredicate extends InstrumentationTestCase {

    private IsCompletedPredicate isCompletedPredicate;

    public void setUp(){
        isCompletedPredicate = new IsCompletedPredicate();
    }

    public void testParseIsCompletedTask(){

        assertNotNull(isCompletedPredicate);

        Task task = new Task();

        List<TaskActor> taskActors = new ArrayList<TaskActor>();

        TaskActor actor1 = new TaskActor();
        actor1.setIsComlete(true);

        TaskActor actor2 = new TaskActor();
        actor2.setIsComlete(false);

        taskActors.add(actor1);
        taskActors.add(actor2);

        ArrayOfTaskActor arrayOfTaskActors = new ArrayOfTaskActor();
        arrayOfTaskActors.setItem(taskActors);

        task.setExecutants(arrayOfTaskActors);

        assertTrue( isCompletedPredicate.compute(task) );

    }

    public void testParseIsNotCompletedTask(){

        assertNotNull(isCompletedPredicate);

        Task task = new Task();

        List<TaskActor> taskActors = new ArrayList<TaskActor>();

        TaskActor actor1 = new TaskActor();
        actor1.setIsComlete(false);

        TaskActor actor2 = new TaskActor();
        actor2.setIsComlete(false);

        taskActors.add(actor1);
        taskActors.add(actor2);

        ArrayOfTaskActor arrayOfTaskActors = new ArrayOfTaskActor();
        arrayOfTaskActors.setItem(taskActors);

        task.setExecutants(arrayOfTaskActors);

        assertFalse( isCompletedPredicate.compute(task) );

    }

}
