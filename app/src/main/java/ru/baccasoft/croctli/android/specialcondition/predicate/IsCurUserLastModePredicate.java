package ru.baccasoft.croctli.android.specialcondition.predicate;


import android.util.Log;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.SystemUser;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

import java.sql.SQLException;


public class IsCurUserLastModePredicate implements IBooleanAtom {

    private static final String TAG = IsCurUserLastModePredicate.class.getSimpleName();

    public static final String ATOM_NAME = "iscuruserlastmode";


    @Override
    public Boolean compute(Task task) {

        try {

            String userLogin = App.prefs.getUserLogin();

            SystemUser user = App.getDatabaseHelper().getSystemUserDao().getBylogin(userLogin);

            if( task.getChangedById().equals( App.prefs.getUserLogin() ) )
                return true;

        } catch (SQLException e) {
            Log.e(TAG,"could not queary user by login" + e.getMessage());
        }

        return false;

    }

    @Override
    public IBooleanAtom create() {
        return new IsCurUserLastModePredicate();
    }

    @Override
    public String getAtomName() {
        return ATOM_NAME;
    }
}
