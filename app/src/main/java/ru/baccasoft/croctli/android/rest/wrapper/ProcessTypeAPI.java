package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfProcessType;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.ProcessTypeSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 * GD, GM
 */
public class ProcessTypeAPI implements IRestApiCall<ProcessTypeSet, ArrayOfProcessType, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = ProcessTypeAPI.class.getSimpleName();

    @Override
    public ProcessTypeSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/ProcessType/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(ProcessTypeSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/ProcessType/Delete/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfProcessType objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
