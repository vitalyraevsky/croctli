package ru.baccasoft.croctli.android.sync;

import android.util.Log;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ConditionOperator;
import ru.baccasoft.croctli.android.gen.core.ConditionOperatorSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.ConditionOperatorAPI;

import java.sql.SQLException;

/**
 * получить список (один раз?)
 */
public class ConditionOperatorSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        ConditionOperatorAPI conditionOperatorAPI = new ConditionOperatorAPI();
        ConditionOperatorSet operators = conditionOperatorAPI.modifyGet(null, null, 0);
        for (ConditionOperator op : operators.getArrayOf().getItem()) {
            op.setServerTime(operators.getServerTimeString());
            try {
                App.getDatabaseHelper().getConditionOperatorDao().createOrUpdate(op);
            } catch (SQLException e) {
                TableUtils.createLogMessage(TableUtils.GENERAL_WRITE_MESSAGE, e);
                Log.e(TAG, TableUtils.GENERAL_WRITE_MESSAGE, e);
            }
        }
    }


    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }


//    @Override
//    public void getList() {
//
//    }
}
