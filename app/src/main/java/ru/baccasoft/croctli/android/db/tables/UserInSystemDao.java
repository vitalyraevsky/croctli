package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.UserInSystem;
import ru.baccasoft.croctli.android.gen.core.UserNSI;

import java.sql.SQLException;
import java.util.ArrayList;

public class UserInSystemDao extends BaseDaoImpl<UserInSystem, String> {
    public UserInSystemDao(ConnectionSource connectionSource, Class<UserInSystem> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(UserInSystem data) throws SQLException {
        int ret = super.create(data);

        if(data.getNSI().getItem() == null) {
            data.setNsiList(new ArrayList<UserNSI>());
        } else {
            data.setNsiList(new ArrayList<UserNSI>(data.getNSI().getItem()));
        }

        for (UserNSI nsi : data.getNsiList()) {
            nsi.setUserInSystem(data);
            ret += App.getDatabaseHelper().getUserNsiDao().create(nsi);
        }
        return ret;
    }
}
