package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.ItemField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemFieldDAO extends BaseDaoImpl<ItemField, String> {
    @SuppressWarnings("unused")
    private static final String TAG = ItemFieldDAO.class.getSimpleName();

    public ItemFieldDAO(ConnectionSource connectionSource, Class<ItemField> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<ItemField> getByClassifierItem(ClassifierItem ci) throws SQLException {
        QueryBuilder<ItemField, String> q = queryBuilder();
        q.where().eq(ItemField.CLASSIFIER_ITEM_ID_FIELD, ci.getId());
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ItemField> preparedQuery = q.prepare();

        return query(preparedQuery);

    }

    public List<String> getValues(ClassifierItem ci) throws SQLException {
        QueryBuilder<ItemField, String> q = queryBuilder();
        q.where().eq(ItemField.CLASSIFIER_ITEM_ID_FIELD, ci.getId());
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ItemField> preparedQuery = q.prepare();

        List<ItemField> list = query(preparedQuery);
        List<String> ret = new ArrayList<String>();
        for (ItemField i: list)
            ret.add(i.getValue());

        return ret;
    }

//    public List<ItemField> getWhenClassifierDisplayFieldFilled(EntityProperty p, ) throws SQLException {
//        QueryBuilder<ItemField, String> q = queryBuilder();
//        q.where().eq(ItemField.CLASSIFIER_ITEM_ID_FIELD, ci.getId());
////        Log.d(TAG, q.prepareStatementString());
//        PreparedQuery<ItemField> preparedQuery = q.prepare();
//
//        return query(preparedQuery);
//
//    }
    public List<ItemField> getByName(String name) throws SQLException {
        QueryBuilder<ItemField, String> q = queryBuilder();
        q.where().eq("name", name);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ItemField> preparedQuery = q.prepare();

        return query(preparedQuery);
    }
}
