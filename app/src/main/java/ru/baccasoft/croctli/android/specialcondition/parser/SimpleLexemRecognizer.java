package ru.baccasoft.croctli.android.specialcondition.parser;

public class SimpleLexemRecognizer implements ILexemRecognizer {
	
	private final String sample;
	private final LexemType type;
	
	public SimpleLexemRecognizer( String sample, LexemType type ) {
		this.sample = sample;
		this.type = type;
	}

	@Override
	public boolean isApplicable(LexerSource state) {
		return state.getUnparsedTail().startsWith(sample);
	}

	@Override
	public ILexem parse(LexerSource source) throws ParseException {
		ILexem result = new SimpleLexem(type, source, sample);
		source.moveAhead(sample.length());
		return result;
	}

}
