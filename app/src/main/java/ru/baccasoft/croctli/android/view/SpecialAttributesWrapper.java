package ru.baccasoft.croctli.android.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.ArrayScalarWrapper;
import ru.baccasoft.croctli.android.view.wrapper.ClassifierWrapper;
import ru.baccasoft.croctli.android.view.wrapper.ScalarWrapper;
import ru.baccasoft.croctli.android.view.wrapper.SystemUserWrapper;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.ExpanderView;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test.ClassifierArrayWrapper;

/**
 * Создает Layout, содержащий поля специальных атрибутов задачи.
 * Работает с базой, поэтому запускать надо в отдельном потоке
 *
 */

public class SpecialAttributesWrapper {
    private static final String TAG = SpecialAttributesWrapper.class.getSimpleName();

    // во сколько экспандеров уже вложена вьюшка, которую мы сейчас формируем
    private final int nestLevel;
    Context mContext;
    Activity mActivity;

    List<EntityProperty> props;
    private Boolean globalReadOnly;


    /**
     *
     * @param props набор свойств для отображения. все они будут отображены в пределах одного экспандера.
     *              массивные свойства пойдут во вложенный экспандер
     * @param nestLevel во сколько экспандеров вложена эта вьюшка. должен быть не меньше 0
     * @param readOnly false - все вьюшки будут в режиме редактирования, true - все вьюшки в режиме просмотра,
     *                 null - редактирование или просмотр определяется по флагу EntityProperty.isReadOnly
     *                  для каждой вьюшки отдельно
     */
    public SpecialAttributesWrapper(List<EntityProperty> props, Activity activity, int nestLevel, Boolean readOnly) {
        if(nestLevel < 0) {
            throw new IllegalArgumentException("starting nesting level must be at least 0");
        }

        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        this.props = props;
        this.nestLevel = nestLevel;
        this.globalReadOnly = readOnly;

        Log.d(TAG, "current view expander level: " + nestLevel);
    }

    public SpecialAttributesWrapper(List<EntityProperty> props, Activity activity) {
        this(props, activity, 0, null);
    }

    /**
     * Получить вюьшку, наполненную всеми подлежащими заполнению атрибутами:
     * скалярными и сложными
     *
     * @return Корневую вьюшку, содержащую все размещенные EntityProperties
     */
    public View getLayout() {
        LinearLayout root = ViewUtils.makeVerticalContainer(mContext);
        Log.d(TAG, "### SpecialAttributesWrapper");
        for(EntityProperty p : props) {
            // пропускаем невидимые свойства
            if(!p.isIsVisible())
                continue;
            Preload.startLoadingBaseByChildsId(p);
            View vp = getPropertyLayout(p);
            if(vp != null)
                root.addView(vp);
        }
        return root;
    }

    /**
     * Создает нужную View в зависимости от типа свойства (скаляр, справочник и т.д.)
     *
     * @param p свойство EntityProperty, для которого нужно сделать вьюшку
     * @return готовую заполненную вьюшку
     */
    private View getPropertyLayout(EntityProperty p) {
        Log.d(TAG, "EntityPropertyId: \'" + p.getId() + "\'");
        // Формальное описание логики
        //root=вертикальный контейнер
        // берем EntityProperty
        // [Скалярные свойства]
        // если скаляр
        //      создаю и добавляю нужную вьюшку в root (все вьюшки скаляров - горизонтальные контейнеры)
        //
        // [Справочные свойства]
        //
        // если справочное свойство (SpecifierTypeId != null)
        //      запрашиваю DisplayMode
        //
        //      [(DisplayMode = 0)]
        //      если 0
        //          рисую справочные свойства
        //          по условиям из ФС:
        //              обычный справочник
        //              или свободный справочник
        //              или справочник со свободной датой
        //
        //      [(DisplayMode = 1) и пользователи]
        //      [Массивные справочные свойства с DisplayMode = 1]
        //      если 1 (метка*)
        //          отрисовываю элемент (или массив элементов) справочника в виде кнопок
        //
        // [(DisplayMode = 1) и пользователи]
        // если EntityProperty.isSystemUserType
        //      goto метка*  (в качестве элементов справочника используется список пользователей)
        //
        //
        // [Сложные (объектные) свойства]
        // если есть дочерние EntityProperty (поиск в базе EntityPropertyDao.getChildsById(id_текущего_свойства)
        //      создаем экспандер
        //      ресурсивно вызываем этот метод
        //      и добавляем возвращенную вьюшку в этот эспандер
        //      эспандер добавляем в основную вьюшку

        // 2.8.2.1	Скалярные свойства
        if(ViewUtils.isNotEmpty(p.getScalarType()) && !p.isIsArrayProperty()) {
            ScalarWrapper scalarWrapper = new ScalarWrapper(mActivity);
            return scalarWrapper.getLayout(p, globalReadOnly);
        }

        // 2.8.2.3	Массивные свойства
        // Это может быть
        //      либо массивное необъектное свойство (string, date etc)
        //      либо массивное справочное свойство
        //      либо массивное сложное свойство
        if(p.isIsArrayProperty() ) {
            Log.d(TAG, "arrayProperty with title:\'" + p.getDisplayGroup() + "\'");
            if(ViewUtils.isNotEmpty(p.getScalarType())) {
                // массив скаляров
                Log.d(TAG, "scalar array");
                return new ArrayScalarWrapper(mActivity).getLayout(p, globalReadOnly);
            }
            if(ViewUtils.isNotEmpty(p.getClassifierTypeId())) {
                // массив на основе справочника
                //TODO: новый врапер
                Log.d(TAG, "classifier array");
                return new ClassifierArrayWrapper(mContext).getLayout(p, globalReadOnly);
//                return new ArrayPropertyWrapper(mActivity, nestLevel).getLayout(p, globalReadOnly);
            }

            // 2.8.2.5	Сложные (объектные) свойства
            List<EntityProperty> childProps = TableUtils.getEntityPropertyChildsById(p.getId());
            if(childProps.size() > 0) {
                return makeComplexObjectView(p, childProps);
            } else {
                // не рисуем сложные свойства без содержимого
                // т.е. не рисуем экспандеры с одним только заголовком без контента
                return null;
            }
        }

        // 2.8.2.2	Справочные свойства
        // (DisplayMode = 0 или DisplayMode = 1)
        // Это может быть:
        // либо справочник (_одно_ значение можно выбрать из справочника)
        // либо справочник со свободным вводом (можно выбрать _одно_ значение из справочника или ввести произвольный текст
        // либо справочник пользователей системы (IsSystemUserType==true)
        // Также бывают справочники со свободным вводом и scalarType=[date|dateTime], они обрабатываются не здесь
        // Также бывают справочники с полем isArray=true, это массивы сложных объектов, обрабатываются не здесь
        if(ViewUtils.isNotEmpty(p.getClassifierTypeId()) && !p.isIsArrayProperty() && ViewUtils.isEmpty(p.getScalarType())) {
            return new ClassifierWrapper(mContext).getLayout(p, globalReadOnly);
        }

        if(p.isIsSystemUserType()) {
            return new SystemUserWrapper(mContext).getLayout(p, globalReadOnly);
        }

        // 2.8.2.2	Свойства с аттачментами
        if(p.getBinaryValue() != null) {
            return null;    //TODO:
        }

        // 2.8.2.5	Сложные (объектные) свойства
        List<EntityProperty> childProps = TableUtils.getEntityPropertyChildsById(p.getId());
        if(childProps.size() > 0) {
            Log.d(TAG, "EntityProperty with id:\'" + p.getId() + "\'" + " has "
                    + childProps.size() + " childs");
            for(EntityProperty chp : childProps) {
                Log.d(TAG, chp.getDisplayGroup());
            }
            return makeComplexObjectView(p, childProps);
        }
        Log.w(TAG, ReflectionToStringBuilder.toString(p));
        throw new IllegalStateException("can't make view. unrecognized EntityProperty with id:\'" + p.getId() + "\'");
    }

    private View makeComplexObjectView(EntityProperty p, List<EntityProperty> childProps) {
        int newNestLevel = nestLevel + 1;

        SpecialAttributesWrapper childViewWrapper = new SpecialAttributesWrapper(
                childProps, this.mActivity, newNestLevel, this.globalReadOnly);
        View childView = childViewWrapper.getLayout();

        String expanderTitle;
        if(nestLevel % 2 == 0 ) { // эспандер второго уровня
            expanderTitle= (p.getDisplayGroup() == null)
                    ? ""
                    : p.getDisplayGroup();
        } else {
            expanderTitle = (p.getClassifierDisplayField() == null)
                    ? ""
                    : p.getClassifierDisplayField();
        }
        Log.d(TAG, "expander title:\'" + expanderTitle + "\'");
        expanderTitle = ViewUtils.textFromPattern(expanderTitle, p.getId());
        Log.d(TAG, "parsed expanderTitle:\'" + expanderTitle + "\'");

        ExpanderView expanderView = new ExpanderView(mContext, expanderTitle, newNestLevel, globalReadOnly, p.getId());
        expanderView.addSubView(childView, false);

        return expanderView;
    }
}
