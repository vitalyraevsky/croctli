package ru.baccasoft.croctli.android.view.wrapper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import org.joda.time.DateTime;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.utils.DateTimeWrapper;
import ru.baccasoft.croctli.android.utils.DateWrapper;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

import static ru.baccasoft.croctli.android.view.util.ViewUtils.SCALAR_TYPE;
import static ru.baccasoft.croctli.android.view.util.ViewUtils.isEmpty;

public class ScalarWrapper implements IWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = ScalarWrapper.class.getSimpleName();

    private Context mContext;
    private Activity mActivity;

    private boolean readOnly;

    public ScalarWrapper(Activity mActivity) {
        this.mActivity = mActivity;
        this.mContext = mActivity.getApplicationContext();
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        Log.d(TAG, "making view for scalar type \'" + p.getScalarType() + "\' [\'" + p.getDisplayGroup() + "\', \'" + p.getValue() + "\']");
        validate(p);
        SCALAR_TYPE scalarType = SCALAR_TYPE.forValue(p.getScalarType());

        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        String name = (p.getDisplayGroup() == null) ? "" : p.getDisplayGroup();
        String value = (p.getValue() == null) ? "" : p.getValue();


        switch (scalarType) {
            case STRING:
                return ViewUtils.makeStringScalarWithTitle(mContext, name, value, this.readOnly, p.getId());
            case TEXT:
                return ViewUtils.makeTextScalarWithTitle(mContext, name, value, this.readOnly);
            case INT:
                return ViewUtils.makeIntScalarWithTitle(mContext, name, value, this.readOnly);
            case FLOAT:
                return ViewUtils.makeFloatScalarWithTitle(mContext, name, value, this.readOnly);
            case BOOLEAN:
                boolean isEnabled;
                String isEnabledString = value.trim();

                if (isEnabledString.compareToIgnoreCase("false") == 0) {
                    isEnabled = false;
                } else if (isEnabledString.compareToIgnoreCase("true") == 0) {
                    isEnabled = true;
                } else {
                    throw new IllegalStateException(
                            "entityProperty with scalar type \'" + scalarType + " must have field \"value\" true or false " +
                                    "but we got:\'" + value + "" );
                }
                return ViewUtils.makeBooleanScalarWithTitle(mContext, name, isEnabled, this.readOnly);
            case DATE:
                // TODO: сюда может прийти дата с #UserValueLabel

                return ViewUtils.makeDateScalarWithTitle(mActivity, p.getDisplayGroup(), new DateWrapper(value),this.readOnly, p.getId());
            case DATE_TIME:
                return ViewUtils.makeDateTimeScalarWithTitle(mActivity, p.getDisplayGroup(), new DateTimeWrapper(value),this.readOnly, p.getId());
            default:
                throw new IllegalStateException("cant recognize scalarType:\'" + scalarType);
        }
    }

    private void validate(EntityProperty p) {
        final String scalarType = p.getScalarType();
        if(isEmpty(scalarType))
            throw new IllegalStateException("scalarType cant be empty. scalarType:\'" + scalarType + "\'");
        if(!SCALAR_TYPE.isValid(scalarType))
            throw new IllegalStateException("invalid scalarType \'" + scalarType + "\'");
    }
}
