package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import de.greenrobot.event.EventBus;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Date;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.utils.DateWrapper;
import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.event.ScalarChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class DateScalar extends TextView {
    @SuppressWarnings("unused")
    private static final String TAG = DateScalar.class.getSimpleName();

    private Activity mActivity;
    private Context mContext;

    private DatePickerDialog datePickerDialog;
    private DatePickerDialog.OnDateSetListener dateDialogCallback;

    public static final int textSize = R.dimen.px_16;

    public DateScalar(final Activity activity, IDateField date, boolean isReadonly, final String entityPropertyId) {
//        super(activity.getApplicationContext(), date.toViewDate(), false, entityPropertyId);
        super(activity.getApplicationContext());
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        initThis(isReadonly);
        updateThis(date);

        this.dateDialogCallback = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                DateTime joda = new LocalDate(year, monthOfYear+1, dayOfMonth).toDateTimeAtStartOfDay();
                DateWrapper selDate = new DateWrapper( new Date( joda.getMillis() ) );
                updateThis(selDate);
                String selDateString = selDate.toGuiString();

                EventBus.getDefault().post(new ScalarChangeEvent(entityPropertyId, selDateString, activity.getApplicationContext()));
//                EventBus.getDefault().post(new ValueModifiedEvent(
//                    entityPropertyId, selDateString, ValueModifiedEvent.VIEW_TYPE.SCALAR));
            }
        };

        if (!isReadonly) {
            DateTime joda = new DateTime(date.smartGetTime());
            datePickerDialog = new DatePickerDialog(mActivity, dateDialogCallback,
                    joda.getYear(), joda.getMonthOfYear()-1,
                    joda.getDayOfMonth());
            datePickerDialog.getDatePicker().setCalendarViewShown(false);

            setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker();
                }
            });
        }
    }

    private void initThis(boolean isReadonly) {
//        setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
        setTextColor(mContext.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, textSize, mContext);

        Drawable drawable;
        if(isReadonly) {
            drawable = getResources().getDrawable(R.drawable.spinner_ab_disabled_blue);
        } else {
            drawable = getResources().getDrawable(R.drawable.spinner_ab_focused_blue);
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {
            setBackground(drawable);
        } else {
            setBackgroundDrawable(drawable);
        }

    }

    private void showDatePicker() {
        datePickerDialog.show();
    }

    private void updateThis(IDateField date) {
        setText(date.toGuiString());
        setText(date.toGuiString());
    }
}