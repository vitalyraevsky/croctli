package ru.baccasoft.croctli.android.view.util;

import android.util.Log;

import org.apache.commons.lang.StringUtils;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.utils.DateTimeWrapper;

import java.util.ArrayList;
import java.util.List;


public class CrocUtils {
    @SuppressWarnings("unused")
    private static final String TAG = CrocUtils.class.getSimpleName();

    public static final List<String> templateAttributeCodes = new ArrayList<String>();
    static {
        templateAttributeCodes.add("TaskCreateDate");
        templateAttributeCodes.add("TaskCreator");
        templateAttributeCodes.add("TaskDescription");
        templateAttributeCodes.add("ProcessTypeName");
        templateAttributeCodes.add("ProcessTypeDescription");
        templateAttributeCodes.add("ProcessDescription");
        templateAttributeCodes.add("ProcessInitiator");
        templateAttributeCodes.add("TaskStatus");
        templateAttributeCodes.add("TaskDeadline");
        templateAttributeCodes.add("IsTaskSingleExecutor");
        templateAttributeCodes.add("TaskChangeDate");
        templateAttributeCodes.add("SourceSystem");
    }

    // инфа, откуда брать данные для каждого атрибута, получена реверс-инжинирингом
    // при помощи скринов с iOS-клиента, полурандомных селектов по базе и полусуток моей жизни
    // может оказаться неверной. beware!, прохожий.

    /**
     * Получить значение для отображения на основе кода атрибута.
     *
     * @param code
     * @param task
     * @return
     */
    public static String getValueByAttributeCode(String code, Task task) {
        if(code.equals("TaskName"))
            return task.getName();
        if(code.equals("TaskCreateDate")) {
            final DateTimeWrapper date = new DateTimeWrapper(task.getCreateDateString());
            return date.toGuiString();
        }
        if(code.equals("ProcessName")) {
            String processId = task.getProcessId();
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);
            return process.getName();
        }
        if(code.equals("ProcessCreateDate")) {
            String processId = task.getProcessId();
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);

            String dateString = process.getCreateDateString();
            final DateTimeWrapper date = new DateTimeWrapper(dateString);
            return date.toGuiString();
        }
        if(code.equals("TaskCreator")) {
            String creatorId = task.getCreatedById();
            Log.d(TAG, "Task.create_by_id:\'" + creatorId + "\'");
            if(ViewUtils.isEmpty(creatorId))
                return "";
            SystemUser systemUser = TableUtils.getSystemUserById(creatorId);
//            UserInSystem userInSystem = TableUtils.getUserInSystemById(creatorId);
            if(systemUser == null)
                return "";
            Log.d(TAG, "SystemUser.localname:\'" + systemUser.getLocalName()+"\'");
            return systemUser.getLocalName();
        }
        if(code.equals("TaskDescription"))
            return task.getDescription();
        if(code.equals("ProcessTypeName")) {
            String processId = task.getProcessId();
            if (ViewUtils.isEmpty(processId))
                return "";
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);
            if (process == null)
                return "";
            String processTypeId = process.getProcessTypeId();
            if (ViewUtils.isEmpty(processTypeId))
                return "";
            ProcessType processType = TableUtils.getProcessTypeById(processTypeId);
            if (processType == null)
                return "";
            return processType.getName();
        }
        if(code.equals("ProcessTypeDescription")) {
            String processId = task.getProcessId();
            if (ViewUtils.isEmpty(processId))
                return "";
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);
            if (process == null)
                return "";
            String processTypeId = process.getProcessTypeId();
            if (ViewUtils.isEmpty(processTypeId))
                return "";
            ProcessType processType = TableUtils.getProcessTypeById(processTypeId);
            if (processType == null)
                return "";
            return processType.getDescription();
        }
        if(code.equals("ProcessDescription")) {
            String processId = task.getProcessId();
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);
            return process.getDescription();
        }
        if(code.equals("ProcessInitiator")) {
            String processId = task.getProcessId();
            ru.baccasoft.croctli.android.gen.core.Process process = TableUtils.getProcessById(processId);
            String userId = process.getInitiatorId();
            SystemUser systemUser = TableUtils.getSystemUserById(userId);
//            UserInSystem userInSystem = TableUtils.getUserInSystemById(userId);
            return systemUser.getLocalName();
        }
        if(code.equals("TaskStatus")) {
            String stateId = task.getTaskStateId();
            if(StringUtils.isEmpty(stateId) ) {
                return "";
            }
            TaskStateInSystem taskState = TableUtils.getTaskStateById(stateId);
            return taskState.getLocalNameInSystem();
        }
        if(code.equals("TaskDeadline")) {
            return formatDateString( task.getDeadlineDateString() );
        }
        if(code.equals("IsTaskSingleExecutor")) {
            String isSingleExecutor = "Единственный исполнитель: ";
            isSingleExecutor += (task.executantsCollection.size() == 1) ? " да" : " нет";
            return isSingleExecutor;
        }
        if(code.equals("TaskChangeDate")) {
            return formatDateString(task.getChangeDateString());
        }
        if(code.equals("SourceSystem")) {
            String adapterId = task.getAdapterId();
            Adapter adapter = TableUtils.getAdapterById(adapterId);
            String sourceSystemId = adapter.getSourceSystemId();
            SourceSystem sourceSystem = TableUtils.getSourceSystemById(sourceSystemId);
            return sourceSystem.getName();
        }

        throw new IllegalArgumentException("cant process attribute code:\'" + code + "\'");
    }

    private static String formatDateString( String value ) {
        final DateTimeWrapper date = new DateTimeWrapper(value);
        return date.toGuiString();

    }

    /**
     * Вытащить из справочника отображаемое значение на основе кода.
     * Пояснение: EP в поле Ep.value может хранить как код из справочника, так и произвольный текст.
     *
     * @param p
     * @return если найден по коду в справочнике, то отображаемое значение (ClassifierItem.displayValue)
     *          если не найден - оригинальную строку (Ep.value)
     *          если Ep.value пустое или null - возвращаем пустую строку
     */
    public static String getValueFromClassifier(EntityProperty p) {
        String value = p.getValue();
        if(ViewUtils.isEmpty(value))
            return "";
        // проверяем, сохранен ли в EP.value код из справочника или произвольное значение
        List<ClassifierItem> classifierItems = TableUtils.getClassifierItemsByCodes(p.getClassifierTypeId(), value);
        if(classifierItems.size() > 0)
            value = classifierItems.get(0).getDisplayValue();

        return value;
    }


}
