package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;


//import static ru.baccasoft.croctli.android.view.event.ClassifierArrayChangeEvent.OPERATION;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.MultiAutoCompleteTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.view.event.ClassifierArrayChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;


/**
 * Массивные справочные свойства с DisplayMode=1
 *
 */

/**
 * Вьюшка для множественного ввода текста из выпадающего списка.
 * Курсор всегда установлен после последнего элемента.
 * Запрещен ввод с клавиатуры, кнопка удаления (backspace) удаляет последний токен.
 * Элементы раздеяются запятыми.
 */

public class ClassifierArrayNew2 extends MultiAutoCompleteTextView implements View.OnClickListener, AdapterView.OnItemClickListener {
    @SuppressWarnings("unused")
    private static final String TAG = "ClassifierArrayNew2";

    Context mContext;
    List<String> classifierItemValues;
    Map<String, String> classifierItemsByValue;
    List<ClassifierItem> initItems;
    String entityPropertyId;

//    final String[] str={"Andoid","Jelly Bean","Froyo",
//            "Ginger Bread","Eclipse Indigo","Eclipse Juno"};

    final static String tokenSeparator = ",";
    final static String tokenSeparatorWithSpace = ", ";

    private boolean mWatcherBlocked = false;

    public ClassifierArrayNew2(List<ClassifierItem> allItems, List<ClassifierItem> initItems,
                               String entityPropertyId, Context context) {
        super(context);
        this.entityPropertyId = entityPropertyId;
        this.mContext = context;

        this.initItems = new ArrayList<ClassifierItem>(initItems);


        classifierItemValues = new ArrayList<String>();
        classifierItemsByValue = new HashMap<String, String>();
        for(ClassifierItem ci : allItems) {
            classifierItemValues.add(ci.getDisplayValue());
            classifierItemsByValue.put(ci.getDisplayValue(), ci.getCode());
        }

        init(context, initItems);
    }

    private void init(Context context, List<ClassifierItem> initItems) {
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
        setThreshold(1);

        setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        setOnItemClickListener(this);
        setOnClickListener(this);
        addTextChangedListener(textWatcher);

        ArrayAdapter<String> adp=new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line,classifierItemValues);
        setAdapter(adp);

        setInitialState(initItems);
        setFocusableInTouchMode(false);
        setCursorVisible(false);
        setBackgroundColor(context.getResources().getColor(R.color.text_F2F2F2));
    }

    /**
     * Вставляем начальные значения
     *
     * @param initItems
     */
    private void setInitialState(List<ClassifierItem> initItems) {
        if(initItems.size() == 0)
            return;

        List<CharSequence> tokens = new ArrayList<CharSequence>(initItems.size());

        for(ClassifierItem ci : initItems) tokens.add(ci.getDisplayValue());
        Log.d(TAG, "initial values:\'" + tokens.toString() + "\'");
        placeText(tokens.toArray(new CharSequence[tokens.size()]));
    }

    TextWatcher textWatcher = new TextWatcher() {
        int startPos;   // начальная позиция последнего изменения
        int beforeTextChangedLength;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // значит не ввод с клавиатуры
            if(after > 1) mWatcherBlocked = true;

            if(mWatcherBlocked) return;

            startPos = start;
            beforeTextChangedLength = s.length();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s.length() == 0) mWatcherBlocked = true;

            if(mWatcherBlocked) return;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(mWatcherBlocked) {
                mWatcherBlocked = false;
                return;
            }

            mWatcherBlocked = true;

            CharSequence[] tokens;
            if(beforeTextChangedLength > s.length()) {  // значит удалили символ(-ы)
                tokens = getOnlyValidValues(null, s.toString().split(tokenSeparator), true);
            } else {    // значит добавили символы
                tokens = getFixedStringAsArray(s.toString(), startPos);
            }

            placeText(tokens);

            mWatcherBlocked = false;
        }
    };

    @Override
    public void onClick(View v) {
        showDropDown();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mWatcherBlocked = true;
        performCompletion();    // позволяем внутренней магии выставить выбранное значение
        // форсируем выставление токенов с нужным форматированием
        placeText(getOnlyValidValues(null, getText().toString().split(tokenSeparator), false));
        mWatcherBlocked = false;
    }

    /**
     * Запрещаем выделение. Каждый раз выставляем курсор в конец строки.
     */
    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        setSelection(getText().length());
    }

    /**
     * Возвращает список только валидных токенов из переданного списка.<br>
     *
     * Прим.: поскольку сейчас курсор всегда выставляется в конец строки,
     * достаточно просто удалить последний элемент <br>
     *
     * @param allTokens *не используется* список валидных значений
     * @param parsedTokens список значений, введенных во вьюшке
     * @param removeLastElement если true, возращаем оригинальный (без пустых значений) список без последнего элемента
     * @return список только валидных значений
     */
    private CharSequence[] getOnlyValidValues(CharSequence[] allTokens, CharSequence[] parsedTokens,
                                              boolean removeLastElement) {
        if(removeLastElement) {
            return trimCharSequence(Arrays.copyOf(parsedTokens, parsedTokens.length - 1));
        } else {
            return trimCharSequence(Arrays.copyOf(parsedTokens, parsedTokens.length));
        }
    }

    /**
     * Применяет trim() к каждому элементу массива
     * и удаляет пустые элементы.
     *
     */
    private CharSequence[] trimCharSequence(CharSequence[] items) {
        List<CharSequence> result = new ArrayList<CharSequence>(items.length);
        for(CharSequence i : items) {
            i = i.toString().trim();
            if(!TextUtils.isEmpty(i))
                result.add(i);
        }
        return result.toArray(new CharSequence[result.size()]);
    }

    /**
     * Располагает текст во вьюшке.
     * Все особенности (типа применение спанов к тексту и прочих кастомизаций) идут сюда.
     * Прим.: вызывает setText(), поэтому сразу заставит сработать наш TextWatcher()
     */
    private void placeText(CharSequence[] tokens) {
        Log.d(TAG, "placeText()");
        Log.d(TAG, "tokens:" + Arrays.toString(tokens));

        StringBuilder sb = new StringBuilder("");
        for(CharSequence t : tokens) sb.append(t).append(tokenSeparatorWithSpace);

        SpannableStringBuilder ssb = new SpannableStringBuilder(sb);

        List<String> selectedItemsCodes = new ArrayList<String>();
        if(tokens.length > 0) {
            int x = 0;
            for (CharSequence t : tokens) {
                Log.d(TAG, "token:\'" + t + "\'");

                // сохраняем _код_ выбранного элемента
                String code = classifierItemsByValue.get(String.valueOf(t));
                selectedItemsCodes.add(code);

                // применяем форматирование
//                ssb.setSpan(new ClassifierItemSpan(code), x, x + t.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                ssb.setSpan(new UnderlineSpan(), x, x + t.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                x = x + t.length() + tokenSeparatorWithSpace.length();
                sb.append(t).append(tokenSeparator + " ");
            }
        }

        setTextColor(getResources().getColor(R.color.text_000000));
        setText(ssb);
        //setSelection(getText().length());   // двигаем курсор в конец

        sendSelectedValues(selectedItemsCodes);
    }

    /**
     * Удаляет символ из строки и переводит строку в массив токенов.
     * В качестве разделителя используется запятая.
     * Каждый токен проходит через trim().
     *
     * @return массив токенов. пустые токены выкинуты, каждый токен прошел через trim()
     */
    private CharSequence[] getFixedStringAsArray(String text, int charPos) {
        CharSequence s1 = "";
        CharSequence s2 = "";

        if(text.length() > 0)
            s1 = text.subSequence(0, charPos);

        if(charPos < text.length() -1)
            s2 = text.subSequence(charPos + 1, text.length());

        CharSequence fixedString = TextUtils.concat(s1, s2);
        CharSequence[] fixedArray = fixedString.toString().split(tokenSeparator);
        return trimCharSequence(fixedArray);
    }

    /**
     * Отправляем, какие значения были выбраны
     * @param selectedItemsCodes
     */
    private void sendSelectedValues(List<String> selectedItemsCodes) {
        for(String s :selectedItemsCodes) {
            Log.d("", "sending code:\'"+s+"\'");
            EventBus.getDefault().post(new ClassifierArrayChangeEvent(
                    entityPropertyId, s, ClassifierArrayChangeEvent.OPERATION.ADD));
//            EventBus.getDefault().post(new ValueModifiedEvent(entityPropertyId, s, VIEW_TYPE.CLASSIFIER_ARRAY));
        }
    }

}
