package ru.baccasoft.croctli.android;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.util.Log;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import ru.baccasoft.croctli.android.db.DatabaseHelper;
import ru.baccasoft.croctli.android.updater.StepOneResult;


public class App extends Application {
    @SuppressWarnings("unused")
    private static final String TAG = Application.class.getSimpleName();

    private static App instance_ = null;
    private static DatabaseHelper databaseHelper;

    public static Preferences prefs;

    public static DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    private static StepOneResult result;

    public static StepOneResult getStepOneResult() {
        return result;
    }

    public static void setStepOneResult(StepOneResult result) {
        App.result = result;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        
        instance_ = this;

        // заставляем настройки по умолчанию сохраниться
        PreferenceManager.setDefaultValues(this, R.xml.prefs, false);
    }

    public void initSingletons(String user_postfix) {
        Log.d(TAG, "initSingletons : " + user_postfix);

        prefs = new Preferences(this,user_postfix);
        setDeviceId();
        initUserDatabase();
    }

    public void initUserDatabase(){
        databaseHelper = OpenHelperManager.getHelper( getApplicationContext(), DatabaseHelper.class );
        SQLiteDatabase sb = getDatabaseHelper().getWritableDatabase();// this line responsible to call onCreate()
    }

    @Override
    public void onTerminate() {
        OpenHelperManager.releaseHelper();
        super.onTerminate();
    }


    private void setDeviceId() {
        final String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d(TAG, "device id: " + android_id);
        if(android_id == null)
            prefs.setDeviceId(android_id);
    }


    public static Context getContext() {
        return instance_.getApplicationContext();
    }

    public static App getInstance(){
        return instance_;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
