package ru.baccasoft.croctli.android.updater;

import android.os.AsyncTask;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Tab;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.TypeActivity;
import ru.baccasoft.croctli.android.gen.core.UserActivity;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.AppVersionAPI;
import ru.baccasoft.croctli.android.rest.wrapper.UserActivityAPI;
import ru.baccasoft.croctli.android.sync.AdapterSync;
import ru.baccasoft.croctli.android.sync.ClassifierSync;
import ru.baccasoft.croctli.android.sync.ConditionOperatorSync;
import ru.baccasoft.croctli.android.sync.SourceSystemSync;
import ru.baccasoft.croctli.android.sync.SpecialConditionSync;
import ru.baccasoft.croctli.android.sync.SystemUserSync;
import ru.baccasoft.croctli.android.sync.TabSync;
import ru.baccasoft.croctli.android.sync.TaskStateInSystemSync;
import ru.baccasoft.croctli.android.sync.ViewCustomizationSync;

//import com.j256.ormlite.table.TableUtils;

/**
 * Предназначен для выполнения первичной синхронизации клиента.<br>
 * Используется в случае, если синхронизация ни разу не производилась,<br>
 * т.е. предполагается, что в локальной бд данных нет. <br><br>
 *
 * При запуске проверяет, выполнялась ли до этого синхронизация.<br>
 * Если не выполнялась, запускает синхронизацию, иначе завершается.<br>
 * Если синхронизация запускается и нормально завершается, сохраняет <br>
 * дату последней синхронизации.
 *
 * FIXME: на данный момент здесь объединены два этапа синхроназиции:
 * первый должен запускаться после прохождения аутентификации на сплешскрине (see activity "SplashScreen")
 * второй должен запускаться при открытии главного экрана (экран задач, see activity TasksActivity)
 */

public class FirstSyncStepOneTask extends AsyncTask<Void, Integer, StepOneResult> {

    private static final String TAG = FirstSyncStepOneTask.class.getSimpleName();

    private final int steps = 9;   // число вызываемых методов
    private final int maxProgress = 100;

    private static final String FIRST_SYNC_START_REST_DATE = "0001-01-01.00:00:00.000";
    //    private static final int SYNC_MAX_COUNT = 100;
    private int progress;


    @Override
    protected StepOneResult doInBackground(Void... params) {
        final long lastSyncDateMillis;
        String lastUser = App.prefs.getLastUser();
        String thisUser = App.prefs.getUserLogin();
        if (thisUser.equals(lastUser)) {
            lastSyncDateMillis = App.prefs.getLastSyncDate();
        }else {
            lastSyncDateMillis = 0;
        }
        StepOneResult result = new StepOneResult();

        if(lastSyncDateMillis != 0) {
            publishProgress(maxProgress);
            result.status = StepOneResult.FIRST_SYNC_STATUS.SKIP;
            App.prefs.setLastUser(App.prefs.getUserLogin());
            return result;
        }

        UserActivityAPI userActivityAPI = new UserActivityAPI();
        UserActivity userActivity = userActivityAPI.modifyGet(null, null, 0);

        DateTime serverDate = RestApiDate.parseDateTimeParsable(userActivity.getServerTimeString());
        DateTime activeDate = RestApiDate.parseDateTimeParsable(userActivity.getActiveTimeString());
        List<TypeActivity> types = userActivity.getTypes().getItem();

        final RestApiDate dateFrom = new RestApiDate(FIRST_SYNC_START_REST_DATE, true);
        final RestApiDate dateTo = new RestApiDate(serverDate);

        progress = 0;

        Log.d(TAG, "[SYNC] starting first sync for period: [" + dateFrom + " | " + dateTo + "]");

        AppVersionAPI appVersionAPI = new AppVersionAPI();
        appVersionAPI.modifyGet(null, null, 0);
        updateProgress();

        List <Task> taskList = new ArrayList<Task>(TableUtils.getTasksExceptClosed());
        List<String> idsTasks = new ArrayList<String>();
        if (taskList != null)
            for (Task t: taskList)
                idsTasks.add(t.getId());
        TableUtils.deleteTasks(idsTasks);

        List<Tab> tabList = null;
        try {
            tabList = new ArrayList<Tab>(App.getDatabaseHelper().getTabDao().queryForAll());
        }catch (Exception e) {  // SQLException, InterruptedException, BrokenBarrierException
            String message = "cannot read tasksView from db";
            TableUtils.createLogMessage(message, e);
            Log.e(TAG, message, e);
        }
        List<String> idsTabs = new ArrayList<String>();
        if (tabList != null)
            for (Tab t : tabList)
                idsTabs.add(t.getId());
        TableUtils.deleteTabs(idsTabs);

        new ConditionOperatorSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new SpecialConditionSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new SourceSystemSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new AdapterSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new TaskStateInSystemSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new SystemUserSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new ClassifierSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new ViewCustomizationSync().atFirst(dateFrom, dateTo);
        updateProgress();

        new TabSync().atFirst(dateFrom, dateTo);
        updateProgress();

        result.dateFrom = dateFrom;
        result.dateTo = dateTo;
        result.status = StepOneResult.FIRST_SYNC_STATUS.SUCCESS;
        App.prefs.setLastUser(App.prefs.getUserLogin());
        return result;
    }

    private void updateProgress() {
        progress += maxProgress/steps;
        publishProgress(progress);
    }
}
