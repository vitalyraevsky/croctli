package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Process;
import ru.baccasoft.croctli.android.gen.core.ProcessSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.ProcessAPI;

import java.util.List;

/**
 * SD, SM, GD, GM
 */
public class ProcessSync implements ISync {
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        //TODO: нет алгоритма отправки
//        sd(dateFrom, dateTo);
//        sm(dateFrom, dateTo);
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        ProcessAPI processAPI = new ProcessAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> processes = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, processAPI);

        TableUtils.deleteProcesses(processes);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        ProcessAPI processAPI = new ProcessAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<ProcessSet, Process>();

        @SuppressWarnings("unchecked")
        List<Process> processes = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, processAPI);

        for(Process p : processes) {
            TableUtils.createOrUpdateProcess(p);
        }
    }
}
