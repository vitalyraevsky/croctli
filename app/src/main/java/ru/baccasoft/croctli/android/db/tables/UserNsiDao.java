package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.UserNSI;

import java.sql.SQLException;

public class UserNsiDao extends BaseDaoImpl<UserNSI, String> {
    public UserNsiDao(ConnectionSource connectionSource, Class<UserNSI> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
