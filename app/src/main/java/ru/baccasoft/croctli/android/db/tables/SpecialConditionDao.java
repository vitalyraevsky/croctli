package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.SpecialCondition;

import java.sql.SQLException;

public class SpecialConditionDao extends BaseDaoImpl<SpecialCondition, String> {
    public SpecialConditionDao(ConnectionSource connectionSource, Class<SpecialCondition> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
