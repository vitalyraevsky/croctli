package ru.baccasoft.croctli.android.view.util;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.*;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.rest.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Попап, в котором показывается дополнительная информация для элементов справочника
 * с DisplayGroup=1 и пользователей
 *
 */
public class ClassifierPopup extends PopupWindow {

    final private String firstItemTag = "first";
    final private String secondItemTag = "second";

    LayoutInflater inflater;

    public ClassifierPopup(Context context, String title, Map<String, String> data) {
        super(context);

        inflater = LayoutInflater.from(context);

        initThis();

        View v = inflater.inflate(R.layout.property_popup, null);
        TextView titleView = (TextView) v.findViewById(R.id.title);
        ListView listView = (ListView) v.findViewById(R.id.list);

        List<HashMap<String, String>> dataForList = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> itemForList;
        for(Map.Entry<String, String> d : data.entrySet()) {
            itemForList = new HashMap<String, String>();
            itemForList.put(firstItemTag, d.getKey());
            itemForList.put(secondItemTag, d.getValue());
            dataForList.add(itemForList);
        }

        SimpleAdapter adapter = new SimpleAdapter(context, dataForList, android.R.layout.simple_list_item_2,
                new String[] {firstItemTag, secondItemTag},
                new int[] { android.R.id.text1, android.R.id.text2});

        titleView.setText(title);
        listView.setAdapter(adapter);

        setContentView(v);
        setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


//        EventBus.getDefault().register(this);
    }

    private void initThis() {
        setFocusable(true);
        setOutsideTouchable(true);
        setTouchable(true);
        setBackgroundDrawable(new ColorDrawable());
        setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismiss();
                return true;
            }
        });
    }

    public void showUnder(View parent) {
        Rect loc = Utils.locateView(parent);
        showAtLocation(parent, Gravity.TOP | Gravity.LEFT, loc.left, loc.bottom);
    }

//    public void onEvent(DismissPopupEvent event) {
//        this.dismiss();
//    }


}
