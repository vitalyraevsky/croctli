package ru.baccasoft.croctli.android.specialcondition.parser;

public interface ILexem {
	
	LexemType getType();
	String getParserTail();
	String getLexemString();

}
