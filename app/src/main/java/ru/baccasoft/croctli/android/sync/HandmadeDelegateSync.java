package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.HandmadeDelegate;
import ru.baccasoft.croctli.android.gen.core.HandmadeDelegateSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.HandmadeDelegateAPI;

import java.util.List;

public class HandmadeDelegateSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        //TODO: нет алгоритма отправки
//        sd(dateFrom, dateTo);
//        sm(dateFrom, dateTo);
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        HandmadeDelegateAPI handmadeDelegateAPI = new HandmadeDelegateAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> handmadeDelegatesIds = recursiveCall.getRestApiSetAsList(
                dateFrom, dateTo, SYNC_MAX_COUNT, handmadeDelegateAPI);

        TableUtils.deleteHandmadeDelegates(handmadeDelegatesIds);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        HandmadeDelegateAPI handmadeDelegateAPI = new HandmadeDelegateAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<HandmadeDelegateSet, HandmadeDelegate>();

        @SuppressWarnings("unchecked")
        List<HandmadeDelegate> handmadeDelegates = recursiveCall.getRestApiSetAsList(
                dateFrom, dateTo, SYNC_MAX_COUNT, handmadeDelegateAPI);

        for(HandmadeDelegate d : handmadeDelegates) {
            TableUtils.createOrUpdateHandmadeDelegate(d);
        }
    }
}
