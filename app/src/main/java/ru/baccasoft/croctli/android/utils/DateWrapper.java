package ru.baccasoft.croctli.android.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateWrapper extends DateWrapperBase implements IDateField {

    public DateWrapper( String value ) { super(value, DATE_PATTERNS); }

    public DateWrapper( Date value ) { super(value); }

    @Override
    public String toGuiString() {
        return getValue(DateFormat.getDateInstance(), "");
    }

    @Override
    public String toRestString() {
        return getValue(new SimpleDateFormat(DEFAULT_DATE), "0000-00-00");
    }

}
