package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfHandmadeDelegate;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.HandmadeDelegateSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class HandmadeDelegateAPI implements IRestApiCall<HandmadeDelegateSet, ArrayOfHandmadeDelegate, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = HandmadeDelegateAPI.class.getSimpleName();

    @Override
    public HandmadeDelegateSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/HandmadeDelegate/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(HandmadeDelegateSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfHandmadeDelegate objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
