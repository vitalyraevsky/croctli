package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;


public class DeadLineHasValuePredicate implements IBooleanAtom {

    public static final String ATOM_NAME = "deadline.hasvalue";

    @Override
    public Boolean compute(Task task) {
        return task.getDeadlineDateTime() != null;
    }

    @Override
    public IBooleanAtom create() {
        return new DeadLineHasValuePredicate();
    }

    @Override
    public String getAtomName() {
        return ATOM_NAME;
    }
}
