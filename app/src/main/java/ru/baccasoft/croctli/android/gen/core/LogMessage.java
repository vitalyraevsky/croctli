
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.rest.RestApiDate;

import java.util.Collection;


/**
 * <p>Java class for LogMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LogMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DateTimeOf" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MachineName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Severity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Categories" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LogMessage", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "message",
    "dateTimeOf",
    "machineName",
    "severity",
    "priority",
    "additionalInfo",
    "subSystem",
    "categories"
})
@DatabaseTable(tableName = "log_message")
@JsonIgnoreProperties({"id", "wasSynchronized"})
public class LogMessage {
    @SuppressWarnings("unused")
    private static final String TAG = LogMessage.class.getSimpleName();

    /**
     * Создает новую запись об ошибке.<br>
     * Заполняет остальные поля следующим образом:<br>
     *  String dateTimeOfString;    - дата-время по UTC<br>
     *  String machineName;         - App.prefs.getDeviceId(); [идентификатор устройства]<br>
     *          (уходит на сервер в каждом запросе, требующем авторизацию, в заголовке X-TLI-DEVICE)<br>
     *  int severity; (1..3)        - 3 (на данный момент для всех, потому что только ошибки отправляем)<br>
     *  int priority; (1..3)        - 2 (на данный момент для всех, потому что отправляем только ошибки. которые как дети, важны все одинаково)<br>
     *  String subSystem;           - null<br>
     *  ArrayOfString categories;   - null<br>
     *
     * @param message краткое содержание ошибки
     * @param additionalInfo stackTrace
     */
    public LogMessage(String message, String additionalInfo) {
        this.message = message;
        this.additionalInfo = additionalInfo;

        this.dateTimeOf = RestApiDate.now();
        this.machineName = App.prefs.getDeviceId();
        this.severity = 3;
        this.priority = 2;
        this.subSystem = null;
        this.categories = null;
    }

    public LogMessage() { }

    @DatabaseField(columnName = "id", dataType = DataType.INTEGER, generatedId = true)
    public int id;

    @XmlElement(name = "Message", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(columnName = "message", dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("Message")
    protected String message;

//    @XmlElement(name = "DateTimeOf", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
//    @XmlSchemaType(name = "dateTime")
//    protected XMLGregorianCalendar dateTimeOf;

    @DatabaseField(columnName = "date_time_of", dataType = DataType.DATE_TIME, canBeNull = false)
    @JsonProperty("DateTimeOf")
    protected DateTime dateTimeOf;

    @XmlElement(name = "MachineName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(columnName = "machine_name", dataType = DataType.STRING)
    @JsonProperty("MachineName")
    protected String machineName;

    @XmlElement(name = "Severity", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "severity", dataType = DataType.INTEGER)
    @JsonProperty("Severity")
    protected int severity;

    @XmlElement(name = "Priority", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "priority", dataType = DataType.INTEGER)
    @JsonProperty("Priority")
    protected int priority;

    @XmlElement(name = "AdditionalInfo", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(columnName = "additional_info", dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("AdditionalInfo")
    protected String additionalInfo;

    @XmlElement(name = "SubSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(columnName = "sub_system", dataType = DataType.STRING)
    @JsonProperty("SubSystem")
    protected String subSystem;

    @XmlElement(name = "Categories", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("Categories")
    protected ArrayOfString categories;

    @ForeignCollectionField
    protected Collection<LogMessageCategory> categoryCollection;

    @DatabaseField(columnName = "synchronized", dataType = DataType.BOOLEAN, defaultValue = "false")
    private boolean wasSynchronized;

    public boolean isWasSynchronized() {
        return wasSynchronized;
    }

    public void setWasSynchronized(boolean wasSynchronized) {
        this.wasSynchronized = wasSynchronized;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    public DateTime getDateTimeOf() {
        return dateTimeOf;
    }

    public void setDateTimeOf(DateTime dateTimeOf) {
        this.dateTimeOf = dateTimeOf;
    }

    /**
     * Gets the value of the machineName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMachineName() {
        return machineName;
    }

    /**
     * Sets the value of the machineName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMachineName(String value) {
        this.machineName = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     */
    public int getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     */
    public void setSeverity(int value) {
        this.severity = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     */
    public void setPriority(int value) {
        this.priority = value;
    }

    /**
     * Gets the value of the additionalInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * Sets the value of the additionalInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalInfo(String value) {
        this.additionalInfo = value;
    }

    /**
     * Gets the value of the subSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSystem() {
        return subSystem;
    }

    /**
     * Sets the value of the subSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSystem(String value) {
        this.subSystem = value;
    }

    /**
     * Gets the value of the categories property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getCategories() {
        return categories;
    }

    /**
     * Sets the value of the categories property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setCategories(ArrayOfString value) {
        this.categories = value;
    }

}
