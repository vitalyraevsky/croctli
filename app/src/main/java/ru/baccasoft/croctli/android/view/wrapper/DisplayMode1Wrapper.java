package ru.baccasoft.croctli.android.view.wrapper;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.Classifier;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Создает вьюшку для EntityProperty,
 * у которого флаг isSystemUser == true
 * или у которого в связанном Classifier указан displayMode = 1.
 *
 * Вьюшка: слева заголовок, справа подряд справочные свойства, на которые можно тыкнуть,
 * чтобы отобразить попап с подробной инфой.
 *
 */
public class DisplayMode1Wrapper implements IWrapper {
    private boolean readOnly;
    private Context mContext;

    public DisplayMode1Wrapper(Context context) {
        this.mContext = context;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        if(this.readOnly) {
            return makeReadView(p);
        } else {
            return makeEditableView(p);
        }
    }

    private View makeReadView(EntityProperty p) {
        String classifierCode = p.getValue();
        String classifierDisplayValue = "";
        classifierDisplayValue = Preload.getClassifierDisplayValue(classifierCode);
        return ViewUtils.makeStringScalarWithTitle(mContext,p.getDisplayGroup(), classifierDisplayValue, true, p.getId());
    }

    private View makeEditableView(EntityProperty p) {
        List<ClassifierItem> classifierItems = Preload.getClassifierItemsByClassiferId(p);
        String value = Preload.getValueFromClassifier(p);
        // вида [код:значение]
        Map<String, String> items = new HashMap<String, String>();
        for(ClassifierItem ci : classifierItems) {
            items.put(ci.getCode(), ci.getDisplayValue());
        }
        LinearLayout view = ViewUtils.makeEditableViewWithTitle(mContext, p.getDisplayGroup(), items, p.getId(), value, p);
        return view;
    }

    private void validate(EntityProperty p) {
        if(!p.isIsSystemUserType()) {
            String classifierId = p.getClassifierTypeId();
            if(ViewUtils.isEmpty(classifierId)) {
                throw new IllegalStateException("neither systemUser nor classifier. classifierId:\'" + classifierId + "\'");
            }
            Classifier classifier = TableUtils.getClassifierById(classifierId);
            if(classifier == null) {
                throw new IllegalStateException("neither systemUser nor classifier. classifierId:\'" + classifierId + "\'");
            }
            if(classifier.getDisplayMode() != 1) {
                throw new IllegalStateException("neither systemUser nor classifier with displayMode=1");
            }
        }
    }

//    /**
//     * Вспомогательный метод. Создает вьюшку для EntityProperty,
//     * у которого флаг isSystemUser == true
//     * или у которого в связанном Classifier указан displayMode = 1.
//     * Также создаем массивные справочные свойства.
//     *
//     * @param p
//     * @return
//     */
//    private View makeViewDisplayMode1(EntityProperty p) {
//        //      [(DisplayMode = 1) и пользователи]
//        //      [Массивные справочные свойства с DisplayMode = 1]
//        // 2.8.2.3	Массивные свойства
//        // 2.8.2.4	Массивные справочные свойства с DisplayMode = 1
//
//        String classifierCode = p.getValue();
////        final String value; // что будем отображать в этой вьюшке в правой части
//        PropertyWithInfoArrayWithTitle.PropertiesHolder
//                propHolder = new PropertyWithInfoArrayWithTitle.PropertiesHolder();
//
//        if(ViewUtils.isEmpty(classifierCode)) {   // значит в EntityProperty не сохранено значение из справочника
//            propHolder.name = "";
//            propHolder.popupTitle = "";
//            propHolder.popupInfo = new HashMap<String, String>();
//        } else {    // значит classifierCode является ключом к значению ClassifierItem.displayValue (по полю ClassifierItem.code)
//            try {
//                List<ClassifierItem> classifierItems = App.getDatabaseHelper().getClassifierItemDAO()
//                        .getByCode(p.getClassifierTypeId(), classifierCode);
//                if(classifierItems.size() > 1) {
//                    throw new IllegalStateException("cant be more than one classifier item by code");
//                } else if(classifierItems.size() == 0) {
//                    propHolder.name = "";
//                    propHolder.popupTitle = "";
//                    propHolder.popupInfo = new HashMap<String, String>();
//                } else {
//                    ClassifierItem classifierItem = classifierItems.get(0);
//                    propHolder.name = classifierItem.getShortDisplayValue();
//                    propHolder.popupTitle = classifierItem.getDisplayValue();
//
//                    List<ItemField> itemFields = App.getDatabaseHelper().getItemFieldDAO()
//                            .getByClassifierItem(classifierItem);
//                    if (itemFields.size() == 0) {
//                        propHolder.popupInfo = new HashMap<String, String>();
//                    } else {
//                        for (ItemField i : itemFields) {
//                            propHolder.popupInfo.put(i.getName(), i.getValue());
//                        }
//                    }
//                }
//            } catch (SQLException ex) {
//                throw new RuntimeException("cant read from db", ex);
//            }
//        }
//
////        List<ClassifierItem> classifierItems;
////        try {
////            classifierItems = App.getDatabaseHelper().getClassifierItemDAO()
////                    .getByClassifierId(p.getClassifierTypeId());
////        } catch (SQLException ex) {
////            throw new RuntimeException("cant read classifierItem db", ex);
////        }
////
////        List<PropertyWithInfoArrayWithTitle.PropertiesHolder>
////                items = new ArrayList<PropertyWithInfoArrayWithTitle.PropertiesHolder>();
////
////        for(ClassifierItem ci : classifierItems) {
////            Map<String, String> popupInfo = new HashMap<String, String>();
////            List<ItemField> itemFields;
////            try {
////                itemFields = App.getDatabaseHelper().getItemFieldDAO()
////                        .getByClassifierItem(ci);
////            } catch (SQLException ex) {
////                throw new RuntimeException("cant read from itemField db", ex);
////            }
////
////            for(ItemField fi : itemFields)
////                popupInfo.put(fi.getName(), fi.getValue());
////
////            items.add(new PropertyWithInfoArrayWithTitle.PropertiesHolder(
////                    ci.getShortDisplayValue(), ci.getDisplayValue(), popupInfo));
////        }
//
//        List<PropertyWithInfoArrayWithTitle.PropertiesHolder>
//                items = new ArrayList<PropertyWithInfoArrayWithTitle.PropertiesHolder>();
//        items.add(propHolder);
//        return new PropertyWithInfoArrayWithTitle(
//                mContext, p.getDisplayGroup(), items);
//    }

}
