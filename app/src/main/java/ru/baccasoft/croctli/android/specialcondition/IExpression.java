package ru.baccasoft.croctli.android.specialcondition;

import ru.baccasoft.croctli.android.gen.core.Task;

/**
 * Общий интерфейс *любых* выражений при разборе (атрибут задачи, сравнение, предикат, логический оператор).
 */
public interface IExpression<ResultType> {
	ResultType compute( Task task );
}
