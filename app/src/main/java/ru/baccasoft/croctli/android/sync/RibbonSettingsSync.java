package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.RibbonSettingSet;
import ru.baccasoft.croctli.android.gen.core.RibbonSettings;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.RibbonSettingsAPI;

import java.util.List;

/**
 * SD, SM, GD, GM
 */
public class RibbonSettingsSync implements ISync {
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        //TODO: нет алгоритма отправки
//        sd(dateFrom, dateTo);
//        sm(dateFrom, dateTo);
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        RibbonSettingsAPI ribbonSettingsAPI = new RibbonSettingsAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> ribbonSettingIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, ribbonSettingsAPI);

        TableUtils.deleteRibbonSettings(ribbonSettingIds);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        RibbonSettingsAPI ribbonSettingsAPI = new RibbonSettingsAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<RibbonSettingSet, RibbonSettings>();

        @SuppressWarnings("unchecked")
        List<RibbonSettings> ribbonSettings = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, ribbonSettingsAPI);

        for(RibbonSettings r : ribbonSettings) {
            TableUtils.createOrUpdateRibbonSetting(r);
        }
    }
}
