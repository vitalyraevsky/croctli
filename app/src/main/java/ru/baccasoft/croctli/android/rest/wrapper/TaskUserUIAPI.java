package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.ArrayOfTaskUserUI;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskUserUISet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 * Created by developer on 17.10.14.
 */
public class TaskUserUIAPI implements IRestApiCall<TaskUserUISet, ArrayOfTaskUserUI, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = TaskUserUIAPI.class.getSimpleName();

    @Override
    public TaskUserUISet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/TaskUserUI/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(TaskUserUISet.class, url, needAuth);
//        HttpResponse r = RestClient.callSomeRestApi(api, true);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfTaskUserUI objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
