package ru.baccasoft.croctli.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.PropertyWithInfo;

/**
 * Адаптер для наполнения "массивных справочных свойств с DisplayMode = 1 "
 * Например, для отображения списка Исполнителей.
 *
 */

public class ArrayDictionaryEntityPropertyAdapter extends BaseAdapter {

    Context context;
    String[] executors;

    private static LayoutInflater inflater = null;

    public ArrayDictionaryEntityPropertyAdapter(Context context, String[] executors) {
        this.context = context;
        this.executors = executors;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return executors.length;
    }

    @Override
    public Object getItem(int position) {
        return executors[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if( vi == null)
            vi = inflater.inflate(R.layout.task_errand_list_item, null); //TODO: убрать null из параметров
        PropertyWithInfo textView = (PropertyWithInfo) vi.findViewById(R.id.root);
        textView.setText(executors[position]);
        return vi;
    }
}
