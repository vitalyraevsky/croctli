package ru.baccasoft.croctli.android.specialcondition.parser;

public interface ILexemRecognizer {

	boolean isApplicable( LexerSource state );
	ILexem parse( LexerSource state ) throws ParseException; 
}
