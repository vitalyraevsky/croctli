package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class BooleanTrue implements IBooleanAtom {
	
	private static final String ATOM_NAME = "true";

	@Override
	public Boolean compute(Task task) {
		return true;
	}

	@Override
	public IBooleanAtom create() {
		return new BooleanTrue();
	}

	@Override
	public String getAtomName() {
		return ATOM_NAME;
	}
}
