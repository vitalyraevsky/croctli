package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystemSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.TaskStateInSystemAPI;

import java.util.List;

/**
 * GD, GM
 */
public class TaskStateInSystemSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        TaskStateInSystemAPI taskStateInSystemAPI = new TaskStateInSystemAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> taskStateInSystemIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskStateInSystemAPI);

        TableUtils.deleteTaskStateInSystems(taskStateInSystemIds);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        TaskStateInSystemAPI taskStateInSystemAPI = new TaskStateInSystemAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TaskStateInSystemSet, TaskStateInSystem>();

        @SuppressWarnings("unchecked")
        List<TaskStateInSystem> taskStateInSystems = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskStateInSystemAPI);

        for(TaskStateInSystem ts : taskStateInSystems) {
            TableUtils.createOrUpdateTaskStateInSystem(ts);
        }
    }
}
