
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Adapter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Adapter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WebAccessUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsAnonymousAuth" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AuthCertSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthBasicUserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthBasicPassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NextSyncTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SyncInterval" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SourceSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsIWA" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UseGZIP" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UseMTOM" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AutoAddDelegateAction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AutoAddCompleteAction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UseUserStateUpdate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StrategyVersion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MaxTasksPerRequest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MaxProcessesPerRequest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AllowLinkProcessStart" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HttpTimeoutSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Adapter", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "url",
    "webAccessUrl",
    "isAnonymousAuth",
    "authCertSerialNumber",
    "authBasicUserName",
    "authBasicPassword",
    "nextSyncTime",
    "syncInterval",
    "sourceSystemId",
    "isIWA",
    "useGZIP",
    "useMTOM",
    "autoAddDelegateAction",
    "autoAddCompleteAction",
    "useUserStateUpdate",
    "strategyVersion",
    "maxTasksPerRequest",
    "maxProcessesPerRequest",
    "allowLinkProcessStart",
    "httpTimeoutSeconds"
})
@JsonIgnoreProperties({"nextSyncTime"})
@DatabaseTable(tableName = "adapter")
public class Adapter {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(columnName = "id", dataType = DataType.STRING, id = true)
    protected String id;

    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Name")
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;

    @XmlElement(name = "URL", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("URL")
    @DatabaseField(columnName = "url", dataType = DataType.STRING)
    protected String url;

    @XmlElement(name = "WebAccessUrl", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("WebAccessUrl")
    @DatabaseField(columnName = "web_access_url", dataType = DataType.STRING)
    protected String webAccessUrl;

    @XmlElement(name = "IsAnonymousAuth", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsAnonymousAuth")
    @DatabaseField(columnName = "is_anonymous", dataType = DataType.BOOLEAN)
    protected boolean isAnonymousAuth;

    @XmlElement(name = "AuthCertSerialNumber", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("AuthCertSerialNumber")
    @DatabaseField(columnName = "auth_cert_serial_number", dataType = DataType.STRING)
    protected String authCertSerialNumber;

    @XmlElement(name = "AuthBasicUserName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("AuthBasicUserName")
    @DatabaseField(columnName = "auth_basic_user_name", dataType = DataType.STRING)
    protected String authBasicUserName;

    @XmlElement(name = "AuthBasicPassword", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("AuthBasicPassword")
    @DatabaseField(columnName = "auth_basic_password", dataType = DataType.STRING)
    protected String authBasicPassword;

//---
    @XmlElement(name = "NextSyncTime", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar nextSyncTime;

    @JsonProperty("NextSyncTime")
    @DatabaseField(columnName = "next_sync_time", dataType = DataType.STRING)
    protected String nextSyncTimeString;
//---

    @XmlElement(name = "SyncInterval", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("SyncInterval")
    @DatabaseField(columnName = "sync_internal", dataType = DataType.INTEGER)
    protected int syncInterval;

    @XmlElement(name = "SourceSystemId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("SourceSystemId")
    @DatabaseField(columnName = "source_system_id", dataType = DataType.STRING)
    protected String sourceSystemId;

    @XmlElement(name = "IsIWA", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsIWA")
    @DatabaseField(columnName = "is_iwa", dataType = DataType.BOOLEAN)
    protected boolean isIWA;

    @XmlElement(name = "UseGZIP", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("UseGZIP")
    @DatabaseField(columnName = "use_gzip", dataType = DataType.BOOLEAN)
    protected boolean useGZIP;

    @XmlElement(name = "UseMTOM", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("UseMTOM")
    @DatabaseField(columnName = "use_mtom", dataType = DataType.BOOLEAN)
    protected boolean useMTOM;

    @XmlElement(name = "AutoAddDelegateAction", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("AutoAddDelegateAction")
    @DatabaseField(columnName = "auto_add_delegate_action", dataType = DataType.BOOLEAN)
    protected boolean autoAddDelegateAction;

    @XmlElement(name = "AutoAddCompleteAction", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("AutoAddCompleteAction")
    @DatabaseField(columnName = "auto_add_complete_action", dataType = DataType.BOOLEAN)
    protected boolean autoAddCompleteAction;

    @XmlElement(name = "UseUserStateUpdate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("UseUserStateUpdate")
    @DatabaseField(columnName = "use_user_state_update", dataType = DataType.BOOLEAN)
    protected boolean useUserStateUpdate;

    @XmlElement(name = "StrategyVersion", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("StrategyVersion")
    @DatabaseField(columnName = "strategy_version", dataType = DataType.INTEGER)
    protected int strategyVersion;

    @XmlElement(name = "MaxTasksPerRequest", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("MaxTasksPerRequest")
    @DatabaseField(columnName = "max_task_per_request", dataType = DataType.INTEGER)
    protected int maxTasksPerRequest;

    @XmlElement(name = "MaxProcessesPerRequest", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("MaxProcessesPerRequest")
    @DatabaseField(columnName = "max_process_per_count", dataType = DataType.INTEGER)
    protected int maxProcessesPerRequest;

    @XmlElement(name = "AllowLinkProcessStart", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("AllowLinkProcessStart")
    @DatabaseField(columnName = "allow_link_process_start", dataType = DataType.BOOLEAN)
    protected boolean allowLinkProcessStart;

    @XmlElement(name = "HttpTimeoutSeconds", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("HttpTimeoutSeconds")
    @DatabaseField(columnName = "http_timeout_seconds", dataType = DataType.INTEGER)
    protected int httpTimeoutSeconds;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURL() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURL(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the webAccessUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebAccessUrl() {
        return webAccessUrl;
    }

    /**
     * Sets the value of the webAccessUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebAccessUrl(String value) {
        this.webAccessUrl = value;
    }

    /**
     * Gets the value of the isAnonymousAuth property.
     * 
     */
    public boolean isIsAnonymousAuth() {
        return isAnonymousAuth;
    }

    /**
     * Sets the value of the isAnonymousAuth property.
     * 
     */
    public void setIsAnonymousAuth(boolean value) {
        this.isAnonymousAuth = value;
    }

    /**
     * Gets the value of the authCertSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCertSerialNumber() {
        return authCertSerialNumber;
    }

    /**
     * Sets the value of the authCertSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCertSerialNumber(String value) {
        this.authCertSerialNumber = value;
    }

    /**
     * Gets the value of the authBasicUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthBasicUserName() {
        return authBasicUserName;
    }

    /**
     * Sets the value of the authBasicUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthBasicUserName(String value) {
        this.authBasicUserName = value;
    }

    /**
     * Gets the value of the authBasicPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthBasicPassword() {
        return authBasicPassword;
    }

    /**
     * Sets the value of the authBasicPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthBasicPassword(String value) {
        this.authBasicPassword = value;
    }

    /**
     * Gets the value of the nextSyncTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextSyncTime() {
        return nextSyncTime;
    }

    /**
     * Sets the value of the nextSyncTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextSyncTime(XMLGregorianCalendar value) {
        this.nextSyncTime = value;
    }

    /**
     * Gets the value of the syncInterval property.
     * 
     */
    public int getSyncInterval() {
        return syncInterval;
    }

    /**
     * Sets the value of the syncInterval property.
     * 
     */
    public void setSyncInterval(int value) {
        this.syncInterval = value;
    }

    /**
     * Gets the value of the sourceSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemId() {
        return sourceSystemId;
    }

    /**
     * Sets the value of the sourceSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemId(String value) {
        this.sourceSystemId = value;
    }

    /**
     * Gets the value of the isIWA property.
     * 
     */
    public boolean isIsIWA() {
        return isIWA;
    }

    /**
     * Sets the value of the isIWA property.
     * 
     */
    public void setIsIWA(boolean value) {
        this.isIWA = value;
    }

    /**
     * Gets the value of the useGZIP property.
     * 
     */
    public boolean isUseGZIP() {
        return useGZIP;
    }

    /**
     * Sets the value of the useGZIP property.
     * 
     */
    public void setUseGZIP(boolean value) {
        this.useGZIP = value;
    }

    /**
     * Gets the value of the useMTOM property.
     * 
     */
    public boolean isUseMTOM() {
        return useMTOM;
    }

    /**
     * Sets the value of the useMTOM property.
     * 
     */
    public void setUseMTOM(boolean value) {
        this.useMTOM = value;
    }

    /**
     * Gets the value of the autoAddDelegateAction property.
     * 
     */
    public boolean isAutoAddDelegateAction() {
        return autoAddDelegateAction;
    }

    /**
     * Sets the value of the autoAddDelegateAction property.
     * 
     */
    public void setAutoAddDelegateAction(boolean value) {
        this.autoAddDelegateAction = value;
    }

    /**
     * Gets the value of the autoAddCompleteAction property.
     * 
     */
    public boolean isAutoAddCompleteAction() {
        return autoAddCompleteAction;
    }

    /**
     * Sets the value of the autoAddCompleteAction property.
     * 
     */
    public void setAutoAddCompleteAction(boolean value) {
        this.autoAddCompleteAction = value;
    }

    /**
     * Gets the value of the useUserStateUpdate property.
     * 
     */
    public boolean isUseUserStateUpdate() {
        return useUserStateUpdate;
    }

    /**
     * Sets the value of the useUserStateUpdate property.
     * 
     */
    public void setUseUserStateUpdate(boolean value) {
        this.useUserStateUpdate = value;
    }

    /**
     * Gets the value of the strategyVersion property.
     * 
     */
    public int getStrategyVersion() {
        return strategyVersion;
    }

    /**
     * Sets the value of the strategyVersion property.
     * 
     */
    public void setStrategyVersion(int value) {
        this.strategyVersion = value;
    }

    /**
     * Gets the value of the maxTasksPerRequest property.
     * 
     */
    public int getMaxTasksPerRequest() {
        return maxTasksPerRequest;
    }

    /**
     * Sets the value of the maxTasksPerRequest property.
     * 
     */
    public void setMaxTasksPerRequest(int value) {
        this.maxTasksPerRequest = value;
    }

    /**
     * Gets the value of the maxProcessesPerRequest property.
     * 
     */
    public int getMaxProcessesPerRequest() {
        return maxProcessesPerRequest;
    }

    /**
     * Sets the value of the maxProcessesPerRequest property.
     * 
     */
    public void setMaxProcessesPerRequest(int value) {
        this.maxProcessesPerRequest = value;
    }

    /**
     * Gets the value of the allowLinkProcessStart property.
     * 
     */
    public boolean isAllowLinkProcessStart() {
        return allowLinkProcessStart;
    }

    /**
     * Sets the value of the allowLinkProcessStart property.
     * 
     */
    public void setAllowLinkProcessStart(boolean value) {
        this.allowLinkProcessStart = value;
    }

    /**
     * Gets the value of the httpTimeoutSeconds property.
     * 
     */
    public int getHttpTimeoutSeconds() {
        return httpTimeoutSeconds;
    }

    /**
     * Sets the value of the httpTimeoutSeconds property.
     * 
     */
    public void setHttpTimeoutSeconds(int value) {
        this.httpTimeoutSeconds = value;
    }

}
