package ru.baccasoft.croctli.android.specialcondition.bool;

import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.specialcondition.parser.ILexem;
import ru.baccasoft.croctli.android.specialcondition.parser.ParseException;
import ru.baccasoft.croctli.android.specialcondition.predicate.*;

import java.util.HashMap;
import java.util.Map;

public class BooleanAtomFactory {
    @SuppressWarnings("unused")
    private static final String TAG = BooleanAtomFactory.class.getSimpleName();

	private static final Map<String, IBooleanAtomCreator> CREATORS = new HashMap<String, IBooleanAtomCreator>() {
		private void put( IBooleanAtomCreator creator ) {
			put(creator.getAtomName(), creator);
		}
		
		{
			put( new BooleanTrue() );
			put( new BooleanFalse() );
            put( new IsCompletedPredicate() );
            put( new HasConflictsPredicate() );
			put( new IsFavoritePredicate() );
            put( new IsSentPredicate() );
            put( new IsCurUserLastModePredicate() );
            put( new IsDeletedPredicate() );
            put( new DeadLineHasValuePredicate() );
		}
	};
	
	public static IBooleanAtom createAtom( ILexem lexem ) throws ParseException {
		IBooleanAtomCreator creator = CREATORS.get( lexem.getLexemString() );
		if( creator == null ) {
            String message = "Unrecognized 'boolean atom' lexem at "+lexem.getParserTail();
            TableUtils.createLogMessage(message);
            Log.e(TAG, message);
			throw new ParseException(message);
		}
		return creator.create();
	}
}
