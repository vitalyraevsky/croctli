package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.Action;

import java.sql.SQLException;

public class ActionDAO extends BaseDaoImpl<Action, String> {
    public ActionDAO(ConnectionSource connectionSource, Class<Action> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}

