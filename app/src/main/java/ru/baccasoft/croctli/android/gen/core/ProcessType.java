
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for ProcessType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcessType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdapterId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsStartable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SourceSystemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessTypeViewUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisplayInitialForm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsFastStartable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessType", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "description",
    "adapterId",
    "isStartable",
    "sourceSystemName",
    "processTypeViewUrl",
    "displayInitialForm",
    "isFastStartable"
})
@DatabaseTable(tableName = "process_type")
public class ProcessType {

    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @JsonProperty("Name")
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;

    @JsonProperty("Description")
    @XmlElement(name = "Description", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "description", dataType = DataType.STRING)
    protected String description;

    @JsonProperty("AdapterId")
    @XmlElement(name = "AdapterId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "adapter_id", dataType = DataType.STRING)
    protected String adapterId;

    @JsonProperty("IsStartable")
    @XmlElement(name = "IsStartable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_startable", dataType = DataType.BOOLEAN)
    protected boolean isStartable;

    @JsonProperty("SourceSystemName")
    @XmlElement(name = "SourceSystemName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "source_system_name", dataType = DataType.STRING)
    protected String sourceSystemName;

    @JsonProperty("ProcessTypeViewUrl")
    @XmlElement(name = "ProcessTypeViewUrl", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_view_url", dataType = DataType.STRING)
    protected String processTypeViewUrl;

    @JsonProperty("DisplayInitialForm")
    @XmlElement(name = "DisplayInitialForm", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "display_initial_form", dataType = DataType.INTEGER)
    protected int displayInitialForm;

    @JsonProperty("IsFastStartable")
    @XmlElement(name = "IsFastStartable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_fast_startable", dataType = DataType.BOOLEAN)
    protected boolean isFastStartable;

    @DatabaseField(columnName = "server_time", dataType = DataType.STRING)
    protected String serverTime;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the adapterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdapterId() {
        return adapterId;
    }

    /**
     * Sets the value of the adapterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdapterId(String value) {
        this.adapterId = value;
    }

    /**
     * Gets the value of the isStartable property.
     * 
     */
    public boolean isIsStartable() {
        return isStartable;
    }

    /**
     * Sets the value of the isStartable property.
     * 
     */
    public void setIsStartable(boolean value) {
        this.isStartable = value;
    }

    /**
     * Gets the value of the sourceSystemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemName() {
        return sourceSystemName;
    }

    /**
     * Sets the value of the sourceSystemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemName(String value) {
        this.sourceSystemName = value;
    }

    /**
     * Gets the value of the processTypeViewUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeViewUrl() {
        return processTypeViewUrl;
    }

    /**
     * Sets the value of the processTypeViewUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeViewUrl(String value) {
        this.processTypeViewUrl = value;
    }

    /**
     * Gets the value of the displayInitialForm property.
     * 
     */
    public int getDisplayInitialForm() {
        return displayInitialForm;
    }

    /**
     * Sets the value of the displayInitialForm property.
     * 
     */
    public void setDisplayInitialForm(int value) {
        this.displayInitialForm = value;
    }

    /**
     * Gets the value of the isFastStartable property.
     * 
     */
    public boolean isIsFastStartable() {
        return isFastStartable;
    }

    /**
     * Sets the value of the isFastStartable property.
     * 
     */
    public void setIsFastStartable(boolean value) {
        this.isFastStartable = value;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}
