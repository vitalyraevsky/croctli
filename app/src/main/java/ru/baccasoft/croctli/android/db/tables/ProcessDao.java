package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Comment;
import ru.baccasoft.croctli.android.gen.core.Process;

import java.sql.SQLException;
import java.util.ArrayList;

public class ProcessDao extends BaseDaoImpl<Process, String> {
    public ProcessDao(ConnectionSource connectionSource, Class<Process> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(Process data) throws SQLException {
        int ret = super.create(data);

        data.commentsCollection = (data.getComments().getItem() == null)
                ? new ArrayList<Comment>()
                : new ArrayList<Comment>(data.getComments().getItem());

        for(Comment c : data.commentsCollection) {
            c.setProcess(data);
            ret += App.getDatabaseHelper().getCommentsDao().create(c);
        }
        return ret;
    }
}
