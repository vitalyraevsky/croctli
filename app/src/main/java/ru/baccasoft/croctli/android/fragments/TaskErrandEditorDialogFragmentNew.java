package ru.baccasoft.croctli.android.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.AsyncTask;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.view.ErrandViewWrapper;
import ru.baccasoft.croctli.android.view.SpecialAttributesWrapper;
import ru.baccasoft.croctli.android.view.event.AddObjectArrayElement;
import ru.baccasoft.croctli.android.view.event.EditorOnNegativeEvent;
import ru.baccasoft.croctli.android.view.event.EditorOnPositiveEvent;
import ru.baccasoft.croctli.android.view.event.EmptyFieldEvent;
import ru.baccasoft.croctli.android.view.event.IChangeEvent;
import ru.baccasoft.croctli.android.view.event.UpdateYourselfEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.validator.BaseValidator;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.ExpanderView;

/**
 * Редактор поручений.
 *
 * Прежде, чем вызвать диалог, проверяйте, что существует корневое EP для поручений
 *
 * Иерархия вьюшек:
 * (в качестве ViewGroup чаще всего используется LinearLayout, но гарантировать нельзя)
 * LinearLayout - корень вьюшки диалога
 *      ...
 *      ScrollView
 *          ViewGroup - корневая вьюшка, куда добавляются все поручения
 *              ViewGroup   - поручение
 *              ViewGroup   - поручение
 *              ViewGroup   - поручение
 *              ...         - "поручения до самого низа"
  */

public class TaskErrandEditorDialogFragmentNew extends DialogFragment {
    @SuppressWarnings("unused")
    private static final String TAG = TaskErrandEditorDialogFragmentNew.class.getSimpleName();

    public static final String ERRAND_EDITOR_DIALOG_ARGS_KEY = "ERRAND_EDITOR_DIALOG_ARGS_KEY";

    private Context mContext;
    private Activity mActivity;

//    private ScrollView scrollView;
    private LinearLayout rootErrandsLayout;  // сюда добавляем ViewGroup, содержащий СУЩЕСТВУЮЩИЕ в бд, поручения
    private ViewGroup rootForAddingNewErrands;  // сюда добавлять НОВЫЕ поручения при создании

    private LinearLayout progressLayout;
    private TextView loadingMessageView;

    private LinearLayout buttonsLayout;
    private Button positiveButton;
    private Button negativeButton;
    private ProgressBar loadTempErrand;

    // кнопка добавления новой Резолюци
    private ImageButton addButton;

    private String dialogTitle;
    private String taskId;
    private Task currentTask;
    private EntityProperty errandsRootEp;

    private Boolean readOnly = true;

    // NOTE: можно избавиться, теперь в списке задач всегда отображаются только незавершенные задачи
    private boolean isTaskCompleted;

//    private boolean hasErrandsRoot = false; // связаны ли с задачей какие-либо резолюции
    private boolean hasErrandsEPs = false;  // есть ли связанное(-ые) EP, содержащие резолюции
    private boolean hasErrandsTemplateEP = false;   // если шаблонные EP для создания резолюций

    List<EntityProperty> childEps;
    EntityProperty templateEp;

    private Handler h;

    public static TaskErrandEditorDialogFragmentNew newInstance(String taskId, String title) {
        TaskErrandEditorDialogFragmentNew f = new TaskErrandEditorDialogFragmentNew();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        args.putString(TaskErrandEditorDialogFragmentNew.ERRAND_EDITOR_DIALOG_ARGS_KEY, title);
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = mActivity.getApplicationContext();

        Bundle args = getArguments();
        validate(args);

        taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);
        dialogTitle = args.getString(TaskErrandEditorDialogFragmentNew.ERRAND_EDITOR_DIALOG_ARGS_KEY);

        //FIXME: всякая работа с базой должна идти из отдельного потока

        currentTask = TableUtils.getTaskById(taskId);
        isTaskCompleted = TableUtils.isTaskCompleted(currentTask);

        // получаем свойство, указывающее на дочерние резолюции
        // раз попали сюда корневой EP для резолюций должен существовать
        errandsRootEp = TableUtils.getRootEpForErrands(taskId);
        Log.d(TAG, "errandsRootEp id=\'" + errandsRootEp.getId() + "\'");
        if(errandsRootEp == null) {  // он уже должен быть != null, раз вызван этот диалог
            String message = "root EP for errands of task with id:\'" + taskId + "\' cant be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }

        childEps = TableUtils.getEntityPropertyChildsById(errandsRootEp.getId());
        hasErrandsEPs = (ViewUtils.isNotEmpty(childEps));
        templateEp = TableUtils.getEPObjectArrayAddingTemplate(errandsRootEp.getId());
        hasErrandsTemplateEP = (templateEp != null);

        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawableResource(R.color.text_FFFFFF);

        View view = inflater.inflate(R.layout.errand_editor_dialog, container, false);

        TextView titleView = (TextView) view.findViewById(R.id.title);
        titleView.setText(dialogTitle);

        rootErrandsLayout = (LinearLayout) view.findViewById(R.id.root_errand_layout);
        loadingMessageView = (TextView) view.findViewById(R.id.loading_message);
        progressLayout = (LinearLayout) view.findViewById(R.id.progress_save);
        buttonsLayout = (LinearLayout) view.findViewById(R.id.buttons);
        positiveButton = (Button) view.findViewById(R.id.positive);
        negativeButton = (Button) view.findViewById(R.id.negative);
        loadTempErrand = (ProgressBar) view.findViewById(R.id.add_progress_bar);
        addButton = (ImageButton) view.findViewById(R.id.edit);

        // логика, в каком случае какие кнопки доступны на UI
        if(hasErrandsTemplateEP) {
            if(isTaskCompleted) {
                addButton.setVisibility(View.GONE);
                buttonsLayout.setVisibility(View.GONE);
                readOnly = true;
            } else {
                addButton.setVisibility(View.VISIBLE);
                buttonsLayout.setVisibility(View.VISIBLE);
                readOnly = false;
            }
        }
        if(!hasErrandsTemplateEP) {
            addButton.setVisibility(View.GONE);
            if(isTaskCompleted) {
                buttonsLayout.setVisibility(View.GONE);
                readOnly = true;
            } else {
                buttonsLayout.setVisibility(View.VISIBLE);
                readOnly = false;
            }
        }

        if(!readOnly) {
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EditorOnNegativeEvent());
                    dismiss();
                }
            });
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EditorOnPositiveEvent());
                }
            });

            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "addButton on click");
                    new AddErrnad().execute();
                }
            });
        }
        return view;
    }

    private void addNewErrand() {
        EntityProperty epTemplate = TableUtils.getEPObjectArrayAddingTemplate(errandsRootEp.getId());
        Log.d(TAG, "template EP id:\'" + epTemplate.getId() + "\'");
        String origEpTemplateId = epTemplate.getId();
        epTemplate.setOrderInArray(0);

        List<String> createdEps = ViewUtils.createEpsBasedOnTemplate(epTemplate, origEpTemplateId, null);   //todo: передавать и обрабатывать дату создания
        for(String id:createdEps) {
            Log.d(TAG, "created temp EP with id:\'" + id + "\'");
        }

        epIdsToDeleteOnNegative.addAll(createdEps);
        //prepareToSave.add(createdEps);

        List<EntityProperty> epList = TableUtils.getEntityPropertyChildsById(epTemplate.getId());

        ErrandViewWrapper errandWrapper = new ErrandViewWrapper(
                epList, mActivity, false);
        ViewGroup errandView = errandWrapper.getLayout();

        errandView.addView(createDeleteButton(errandView, createdEps.get(0)));
        ViewUtils.makeErrandViewPretty(errandView, mActivity.getResources());
        rootForAddingNewErrands.addView(errandView, ViewUtils.errandViewLP);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        //if(hasErrandsEPs) {//Если все резолюции удаленные, тогда при открытии списка резолюций нельзя добавить новую, не создана главная вьюшка для резолюций
            // оповещаем, что идет загрузка, пока вьюшки создаются
            updateView(false);
            startLoading();
        //}
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    private void startLoading() {
        h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                updateViewInBackground();
            }
        });
    }


    /**
     * Показывает "загрузка" и скрывает остальное.
     * Или наоборот, в зависимости от флага
     */
    private void updateView(boolean isViewLoadingCompleted) {
        if(isViewLoadingCompleted) {
            rootErrandsLayout.setVisibility(View.VISIBLE);
            buttonsLayout.setVisibility(View.VISIBLE);
            loadingMessageView.setVisibility(View.GONE);
        } else {
            loadingMessageView.setVisibility(View.VISIBLE);
            buttonsLayout.setVisibility(View.GONE);
            rootErrandsLayout.setVisibility(View.GONE);
        }
    }

    /**
     * В фоне создает вьюшки, которые собираемся показать.
     * И в главном треде показывает эти вьюшки.
     */
    private void updateViewInBackground() {
        //ErrandListWrapper errandListWrapper = new ErrandListWrapper(mActivity);
        Log.d(TAG, "errand editor readOnly:"+readOnly);
        //rootForAddingNewErrands = errandListWrapper.getLayout(errandsRootEp, readOnly);

        final LinearLayout errandsContainer = ViewUtils.makeVerticalContainer(mActivity.getApplicationContext());
        errandsContainer.setBackgroundColor(mActivity.getResources().getColor(R.color.text_F2F2F2));

        List<EntityProperty> childRootEps = TableUtils.getEntityPropertyChildsById(errandsRootEp.getId());
        if(childRootEps.size() > 0) {
            for(EntityProperty p : childRootEps) {
                List<EntityProperty> cps = TableUtils.getEntityPropertyChildsById(p.getId());
                ErrandViewWrapper wrapper = new ErrandViewWrapper(cps, mActivity, readOnly);
                final ViewGroup errandView = wrapper.getLayout();
                if (!readOnly)
                    errandView.addView(createDeleteButton(errandView, cps.get(0).getEntityPropertyId()));
                ViewUtils.makeErrandViewPretty(errandView, mActivity.getResources());
                errandsContainer.addView(errandView, ViewUtils.errandViewLP);
            }
        }
        rootForAddingNewErrands = errandsContainer;


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rootErrandsLayout.addView(rootForAddingNewErrands);
                updateView(true);
            }
        });
        changeEvents.clear();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        //EventBus.getDefault().post(new EditorOnNegativeEvent());
        EventBus.getDefault().post(new UpdateYourselfEvent());
        super.onDismiss(dialog);
    }

    private ImageButton createDeleteButton(final ViewGroup errandView, String propertyId){
        final String entityPropertyId = propertyId;
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageButton removeButton = (ImageButton)inflater.inflate(R.layout.delete_errand_image, null);
        LinearLayout.LayoutParams para=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        para.gravity = Gravity.LEFT;
        removeButton.setLayoutParams(para);

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> epIdsToDelete = ViewUtils.getChildsEpsBasedOnTemplate(entityPropertyId, null);
                //TableUtils.deleteEntityProperties(epIdsToDelete);
                Log.d(TAG, "ClickOnDeleteButton");
                errandView.removeAllViews();
                rootForAddingNewErrands.removeView(errandView);//TODO если тут удаляем (старую резолюцию), приложение падало при сохранении списка в Event (в TaskErrandEditorDialogFramentNew) - обращается к ключу, который мы тут удалили
                for(String child:epIdsToDelete) {
                    if(!epIdsToDeleteOnPositive.contains(child)){
                        epIdsToDeleteOnPositive.add(child);
                    }

                    if(changeEvents.containsKey(child)){
                        changeEvents.remove(child);
                    }
                }
            }
        });
        return removeButton;
    }

    private void validate(Bundle args) {
        if (args == null)
            throw new IllegalStateException("args cant be null");
        if (!args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY))
            throw new IllegalStateException("taskId cannot be null");
        if(!args.containsKey(TaskErrandEditorDialogFragmentNew.ERRAND_EDITOR_DIALOG_ARGS_KEY))
            throw new IllegalStateException("dialog title cant be null");
    }
    //}}}

    //--------------------------------------------------------

    //{{{ Обработка изменений во вьюшках

    // список EP, которые нужно будет удалить при сохранении
    private List<String> epIdsToDeleteOnPositive = new ArrayList<String>();
    // список EP, которые были созданы в базе, но которые надо удалить,
    // если не была нажата кнопка Сохранить
    private List<String> epIdsToDeleteOnNegative = new ArrayList<String>();
    // хранит все изменения в редакторе, о которых получили оповещение через EventBus
    Map<String, IChangeEvent> changeEvents = new LinkedHashMap<String, IChangeEvent>();
    // хранит претендентов на сохранение
    private List<List<String>> prepareToSave = new ArrayList<List<String>>();

    // Составляем список всех изменений в Редакторе реквизитов
    public void onEvent(IChangeEvent event) {
        Log.d(TAG, "got IChangeEvent event");
        changeEvents.put(event.getKey(), event);
    }

//    // для массивов объектов
//    public void onEvent(RemoveObjectArrayElement event) {
//        epIdsToDeleteOnPositive.add(event.getEntityPropertyId());
//        ViewGroup viewGroup = event.getViewGroup();
//        viewGroup.setVisibility(View.GONE);
//    }
//
    // для массивов объектов
    public void onEvent (AddObjectArrayElement event) {
        ExpanderView expanderView = (ExpanderView) event.getViewGroup();
        int expanderNestLevel = event.getNestLevel();

        EntityProperty epTemplate = TableUtils.getEPObjectArrayAddingTemplate(event.getEntityPropertyId());
        String origEpTemplateId = epTemplate.getId();
        epTemplate.setOrderInArray(0);

        List<String> createdEps = ViewUtils.createEpsBasedOnTemplate(epTemplate, origEpTemplateId, null);   //todo: передавать и обрабатывать дату создания

        for(String id:createdEps) {
            Log.d(TAG, "created temp EP with id:\'" + id + "\'");
        }

        epIdsToDeleteOnNegative.addAll(createdEps);

        List<EntityProperty> epList = new ArrayList<EntityProperty>();
        epList.add(epTemplate);

        SpecialAttributesWrapper wrapper = new SpecialAttributesWrapper(
                epList, mActivity, expanderNestLevel, false);

        View newView = wrapper.getLayout();

        expanderView.addSubView(newView, false);
    }

    // если нажали Сохранить
    public void onEvent(EditorOnPositiveEvent event) {
//        ChangeEventHandler.saveChanges(modifyEvents);
        boolean canBeSaved = true;
        
        BaseValidator baseValidator = null;
        valid: for(Map.Entry<String, IChangeEvent> entry : changeEvents.entrySet()) {
            for(BaseValidator validator : entry.getValue().getValidators()) {
                baseValidator = validator;
                if(!baseValidator.isValid()) {
                    canBeSaved = false;
                    EventBus.getDefault().post(new EmptyFieldEvent());
                    break  valid;
                }
            }
        }

        if(!canBeSaved) {
            baseValidator.alertIfInvalid(getActivity());
            return;
        }
        rootErrandsLayout.setVisibility(View.GONE);
        progressLayout.setVisibility(View.VISIBLE);
        positiveButton.setEnabled(false);
        negativeButton.setEnabled(false);
        addButton.setEnabled(false);
        new SaveToDb().execute();
    }

    // если не нажали Сохранить
    public void onEvent(EditorOnNegativeEvent event) {
        Log.d(TAG, "EditorOnNegativeEvent");
        TableUtils.deleteEntityProperties(epIdsToDeleteOnNegative);
    }
    //}}}

    public class SaveToDb extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            for (Map.Entry<String, IChangeEvent> entry : changeEvents.entrySet()) {
                IChangeEvent changeEvent = entry.getValue();
                if (epIdsToDeleteOnPositive.contains(entry.getKey()))
                    continue;//Пропускаем те ключи, которые удалили
                changeEvent.save();
            }

            TableUtils.deleteEntityProperties(epIdsToDeleteOnPositive);
            epIdsToDeleteOnPositive.clear();
            epIdsToDeleteOnNegative.clear();
            Preload.loadBase(taskId, Preload.loading.half);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(mContext, "Сохранено", Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }


    public class AddErrnad extends AsyncTask<Void, Void, List<EntityProperty>>{
        List<String> createdEps;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            addButton.setClickable(false);
            addButton.setAlpha(0.4f);
            positiveButton.setEnabled(false);
            negativeButton.setEnabled(false);
            loadTempErrand.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<EntityProperty> doInBackground(Void... voids) {
            EntityProperty epTemplate = TableUtils.getEPObjectArrayAddingTemplate(errandsRootEp.getId());
            Log.d(TAG, "template EP id:\'" + epTemplate.getId() + "\'");
            String origEpTemplateId = epTemplate.getId();
            epTemplate.setOrderInArray(0);

            createdEps = ViewUtils.createEpsBasedOnTemplate(epTemplate, origEpTemplateId, null);   //todo: передавать и обрабатывать дату создания
            for(String id:createdEps) {
                Log.d(TAG, "created temp EP with id:\'" + id + "\'");
            }

            epIdsToDeleteOnNegative.addAll(createdEps);
            //prepareToSave.add(createdEps);

            List<EntityProperty> epList = TableUtils.getEntityPropertyChildsById(epTemplate.getId());
            return epList;
        }

        @Override
        protected void onPostExecute(List<EntityProperty> epList) {
            super.onPostExecute(epList);
            ErrandViewWrapper errandWrapper = new ErrandViewWrapper(
                    epList, mActivity, false);
            ViewGroup errandView = errandWrapper.getLayout();

            errandView.addView(createDeleteButton(errandView, createdEps.get(0)));
            ViewUtils.makeErrandViewPretty(errandView, mActivity.getResources());
            rootForAddingNewErrands.addView(errandView, ViewUtils.errandViewLP);
            addButton.setClickable(true);
            addButton.setAlpha(1f);
            positiveButton.setEnabled(true);
            negativeButton.setEnabled(true);
            loadTempErrand.setVisibility(View.GONE);
        }
    }
}
