package ru.baccasoft.croctli.android.specialcondition.parser;

public class LexerSource {
	private final String expression;
	private String unparsedTail;
	
	public LexerSource( String lowercaseExpression ) {
		this.expression = lowercaseExpression;
		this.unparsedTail = lowercaseExpression;
	}
	
	public String getExpression() { return expression; }
	public String getUnparsedTail() { return unparsedTail; }
	
	void moveAhead( int numberOfSkippedCharacters ) {
		unparsedTail = unparsedTail.substring(numberOfSkippedCharacters);
	}
}
