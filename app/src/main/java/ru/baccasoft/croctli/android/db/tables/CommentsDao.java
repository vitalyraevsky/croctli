package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.Comment;

import java.sql.SQLException;

public class CommentsDao extends BaseDaoImpl<Comment, String> {
    public CommentsDao(ConnectionSource connectionSource, Class<Comment> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
