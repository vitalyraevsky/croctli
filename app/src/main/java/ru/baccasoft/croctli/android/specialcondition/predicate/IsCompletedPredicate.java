package ru.baccasoft.croctli.android.specialcondition.predicate;


import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.TaskActor;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class IsCompletedPredicate implements IBooleanAtom {
	
	public static final String ATOM_NAME = "iscompleted";

	@Override
	public Boolean compute(Task task) {

		if(task.getPerformedActionId() != null){
			return true;
		}

        /*if( task.getExecutants() == null)
            return false;

        // Возвращаем true - если у одного из исполнителей стоит флаг isComplete
        for ( TaskActor actor : task.getExecutants().getItem()){
            if( actor.isIsComlete() )
                return true;
        }*/
        return false;
	}

	@Override
	public IBooleanAtom create() {
		return new IsCompletedPredicate();
	}

	@Override
	public String getAtomName() {
		return ATOM_NAME;
	}
}
