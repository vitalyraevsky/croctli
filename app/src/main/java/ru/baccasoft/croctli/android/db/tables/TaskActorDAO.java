package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.TaskActor;

import java.sql.SQLException;
import java.util.List;

public class TaskActorDAO extends BaseDaoImpl<TaskActor, String> {
    public TaskActorDAO(ConnectionSource connectionSource, Class<TaskActor> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<TaskActor> getByTask(String taskId) throws SQLException {
        QueryBuilder<TaskActor, String> q = queryBuilder();
        q.where().eq(TaskActor.TASK_ID_FIELD, taskId);

//        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<TaskActor> preparedQuery = q.prepare();
        return query(preparedQuery);
    }
}
