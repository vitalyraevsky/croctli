package ru.baccasoft.croctli.android.rest;

import android.util.Log;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

import java.text.DecimalFormat;

/**
 * Предназначен для работы со служебными датами синхронизации и изменения объектов на уровне API
 * (в противоположность бизнес-модели данных, представляемой кучей IDateValue). Фактически обертка для DateTime.
 * Содержим over9000 костыльных атавизмов с тех времён, когда данный класс работал с датами обоих типов.
 */

public class RestApiDate {
    @SuppressWarnings("unused")
    private static final String TAG = RestApiDate.class.getSimpleName();

    private DateTime dateTime;
    //прим.: реальность не совпадает с ФС, в конце еще милисекунды нужны
    private static final String restDatePattern = "YYYY-dd-MM.HH:mm:ss.SSS";

    public RestApiDate() {
        this.dateTime = new DateTime(DateTimeZone.UTC);
    }

    /**
     * Дата формируется для таймзоны UTC.
     *
     * @param dateTime
     */
    public RestApiDate(DateTime dateTime) {
        this.dateTime = new DateTime(dateTime, DateTimeZone.UTC);
    }

    public RestApiDate(String dateString, boolean isRestDate) {
        if(dateString == null ) {
            String message = "arguments cant be null";
            TableUtils.createLogMessage(message);
            throw new IllegalArgumentException(message);
        }

        this.dateTime = isRestDate ? parseRestFormatted(dateString) : parseDateTimeParsable(dateString);
    }

    public long getMillis() {
        return this.dateTime.getMillis();
    }

    /**
     * Сформировать строковую дату, пригодную для rest-вызовов.
     * Таймзона используется та же, что и у внутренней DateTime,
     * т.е. UTC.
     *
     * @return Пример: "2014-20-10.22:00:00.000"
     */
    public String toRestDate() {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(restDatePattern);
        return fmt.print(this.dateTime);
    }


    /**
     * Синоним для toRestDate().
     * Во-первых, меньше рефакторить; во-вторых, чаще RestDate используется для этой цели
     *
     * @return
     */
    public String toString() {
        return toRestDate();
    }

    public DateTime toDateTime() {
        return this.dateTime;
    }

    //}}}

    //{{{ Статические хелперы
    /**
     * Перевести строковую дату, которая нужна для rest-запросов, в DateTime
     *
     * @param date  Пример: "2014-20-10.22:00:00.000"
     * @return
     */
    private static DateTime parseRestFormatted(String date) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(restDatePattern).withZone(DateTimeZone.UTC);
        return fmt.parseDateTime(date);
    }

    /**
     * Привести строковую дату-время к приемлемому виду.<br>
     * Ожидаемые форматы строки:<br>
     * (ScalarType = date) YYYY-MM-DD.<br>
     * (ScalarType = dateTime) YYYY-MM-DDTHH:MM(:SS(.mmm))(zzz).<br>
     *
     * Парсим не вручную, а полагаемся на парсер joda-time: ISODateTimeFormat.dateTimeParser()<br>
     *
     * @param date дата или дата-время
     * @return
     */
    public static DateTime parseDateTimeParsable(String date) {
        return new DateTime(date, DateTimeZone.UTC);
    }

    /**
     *  Хелпер для получения текущей даты. <br>
     *
     * @return текущее время и дату в таймзоне UTC
     */
    public static DateTime now() {
        return DateTime.now(DateTimeZone.UTC);
    }

    // костыль для дат, которые представлены пустой строкой
    public static DateTime empty() {
        return new DateTime(0, DateTimeZone.UTC);
    }
}
