package ru.baccasoft.croctli.android.fragments.documents;


import android.util.Log;
import android.webkit.WebView;

import java.io.File;
import java.io.IOException;

import ru.baccasoft.croctli.android.FileUtils;

public class ImageViewFragment extends WebViewBasedAttachmentViewFragment {

    private static final String TAG = ImageViewFragment.class.getSimpleName();

    @Override
    public void loadContent(WebView webView) {

        String content = "<html><table><tr><td> <img src=\"" + getAttachmentName() + "\" ></td></tr></table></html>";

        try {

            FileUtils.cacheFile( getActivity(), getAttachmentPath() , getAttachmentName() , true );
            File html = FileUtils.writeStringToFile( getActivity().getCacheDir(), getAttachmentName()+".html" , content );

            Log.d(TAG,"try open file : " + html.getAbsolutePath() );
            webView.loadUrl("file:///" + html.getAbsolutePath() );

        } catch (IOException e) {
            Log.e(TAG,"not able ti cache file: " + getAttachmentPath() );
        }
    }
}
