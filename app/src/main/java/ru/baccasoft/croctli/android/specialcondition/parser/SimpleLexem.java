package ru.baccasoft.croctli.android.specialcondition.parser;

public class SimpleLexem implements ILexem {
	
	private final LexemType type;
	private final String parserTail;
	private final String lexemString;
	
	public SimpleLexem( LexemType type, LexerSource lexerSource, String lexemString ) {
		this.type = type;
		this.parserTail = lexerSource.getUnparsedTail();
		this.lexemString = lexemString;
	}

	@Override
	public LexemType getType() {
		return type;
	}

	@Override
	public String getParserTail() {
		return parserTail;
	}

	@Override
	public String getLexemString() {
		return lexemString;
	}
}
