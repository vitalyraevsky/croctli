
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * <p>Java class for ClassifierItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClassifierItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisplayValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Fields" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfItemField"/>
 *         &lt;element name="ShortDisplayValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassifierItem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "code",
    "displayValue",
    "fields",
    "shortDisplayValue",
    "isDeleted"
})
@DatabaseTable(tableName = "classifier_item")
public class ClassifierItem {

    public static final String CLASSIFIER_ID_FIELD = "classifier_fk";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CLASSIFIER_ID_FIELD)
    private Classifier classifier;

    public Classifier getClassifier() {
        return classifier;
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    @DatabaseField(id = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("Code")
    @XmlElement(name = "Code", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String code;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("DisplayValue")
    @XmlElement(name = "DisplayValue", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String displayValue;

//--
    @JsonProperty("Fields")
    @XmlElement(name = "Fields", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfItemField fields;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    public Collection<ItemField> fieldsCollection;
//--

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("ShortDisplayValue")
    @XmlElement(name = "ShortDisplayValue", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected String shortDisplayValue;

    @DatabaseField(dataType = DataType.BOOLEAN_OBJ)
    @JsonProperty("IsDeleted")
    @XmlElement(name = "IsDeleted", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected Boolean isDeleted;


    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the displayValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayValue() {
        return displayValue;
    }

    /**
     * Sets the value of the displayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayValue(String value) {
        this.displayValue = value;
    }

    /**
     * Gets the value of the fields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItemField }
     *     
     */
    public ArrayOfItemField getFields() {
        return fields;
    }

    /**
     * Sets the value of the fields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItemField }
     *     
     */
    public void setFields(ArrayOfItemField value) {
        this.fields = value;
    }

    /**
     * Gets the value of the shortDisplayValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDisplayValue() {
        return shortDisplayValue;
    }

    /**
     * Sets the value of the shortDisplayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDisplayValue(String value) {
        this.shortDisplayValue = value;
    }

    /**
     * Gets the value of the isDeleted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDeleted() {
        return isDeleted;
    }

    /**
     * Sets the value of the isDeleted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDeleted(Boolean value) {
        this.isDeleted = value;
    }

}
