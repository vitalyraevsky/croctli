package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateTimeScalar;

public class DateTimeScalarWithTitle extends LinearLayout {
    @SuppressWarnings("unused")
    private static final String TAG = DateTimeScalarWithTitle.class.getSimpleName();

    public DateTimeScalarWithTitle(Activity activity, String title, IDateField date,
                                   boolean isReadonly, String entityPropertyId) {
        super(activity.getApplicationContext());
        Context mContext = activity.getApplicationContext();

        TextView titleView = ViewUtils.makeTitleTextView(mContext, title, null);

        DateTimeScalar dateTimeScalar = new DateTimeScalar(activity, date, isReadonly, entityPropertyId);
        dateTimeScalar.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        addView(titleView);
        addView(dateTimeScalar);
    }
}
