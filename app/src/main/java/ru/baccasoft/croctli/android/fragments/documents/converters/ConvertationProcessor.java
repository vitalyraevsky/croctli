package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;

import java.io.IOException;

import ae.javax.xml.bind.JAXBException;


public interface ConvertationProcessor {

    /**
     * Инициализируем процессор из имени файла, должно выполняться в main thread
     * @param officeFileName
     * @throws Exception
     */
    public void init(String officeFileName) throws Exception ;

    /**
     * Конвертируем в html
     * @param imageDirPath
     * @param targetUri
     * @param activity
     * @return
     */
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException;

}
