
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for HandmadeDelegate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HandmadeDelegate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HandmadeToSystemUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HandmadeFromSystemUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaskActor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HandmadeDelegate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "comment",
    "handmadeToSystemUserId",
    "handmadeFromSystemUserId",
    "taskActor",
    "createDate"
})
@JsonIgnoreProperties({"createDate"})
public class HandmadeDelegate {

    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "id", dataType = DataType.STRING, id = true)
    protected String id;

    @JsonProperty("Comment")
    @XmlElement(name = "Comment", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(dataType = DataType.STRING, columnName = "comment")
    protected String comment;

    @JsonProperty("HandmadeToSystemUserId")
    @XmlElement(name = "HandmadeToSystemUserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "handmade_to_system_user_id", dataType = DataType.STRING)
    protected String handmadeToSystemUserId;

    @JsonProperty("HandmadeFromSystemUserId")
    @XmlElement(name = "HandmadeFromSystemUserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "handmade_from_system_user_id", dataType = DataType.STRING)
    protected String handmadeFromSystemUserId;

    @JsonProperty("TaskActor")
    @XmlElement(name = "TaskActor", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(dataType = DataType.STRING, columnName = "task_actor")
    protected String taskActor;


    @XmlElement(name = "CreateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;

    @JsonProperty("CreateDate")
    @DatabaseField(dataType = DataType.STRING, columnName = "create_date")
    protected String createDateString;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the handmadeToSystemUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandmadeToSystemUserId() {
        return handmadeToSystemUserId;
    }

    /**
     * Sets the value of the handmadeToSystemUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandmadeToSystemUserId(String value) {
        this.handmadeToSystemUserId = value;
    }

    /**
     * Gets the value of the handmadeFromSystemUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandmadeFromSystemUserId() {
        return handmadeFromSystemUserId;
    }

    /**
     * Sets the value of the handmadeFromSystemUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandmadeFromSystemUserId(String value) {
        this.handmadeFromSystemUserId = value;
    }

    /**
     * Gets the value of the taskActor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskActor() {
        return taskActor;
    }

    /**
     * Sets the value of the taskActor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskActor(String value) {
        this.taskActor = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    public String getCreateDateString() {
        return createDateString;
    }

    public void setCreateDateString(String createDateString) {
        this.createDateString = createDateString;
    }
}
