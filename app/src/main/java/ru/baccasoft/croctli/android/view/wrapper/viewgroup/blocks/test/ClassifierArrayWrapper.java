package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import java.util.List;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.IWrapper;

/**
 *
 */
public class ClassifierArrayWrapper implements IWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierArrayWrapper.class.getSimpleName();

    private Context mContext;
    private boolean readOnly;


    public ClassifierArrayWrapper(Context context) {
        this.mContext = context;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
//        validate(); // TODO:
        readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        List<ClassifierItem> allItems = Preload.getClassifierItemsByClassiferId(p);
        List<ClassifierItem> selectedItems = Preload.getSelectedItems(p);

        Log.d(TAG, "selected items id:");
        for(ClassifierItem ci : selectedItems) {
            Log.d(TAG, ci.getId());
        }

        if(readOnly) {
            return makeReadView(selectedItems, p);
        } else {
            return makeEditableView(allItems, selectedItems, p);
//            return new ClassifierArrayNew(allItems, selectedItems, p.getId(), mContext);
        }
    }

    private View makeEditableView(List<ClassifierItem> allItems, List<ClassifierItem> selectedItems, EntityProperty p) {
        LinearLayout ll = ViewUtils.makeHorizontalContainer(mContext);

        TextView titleView = ViewUtils.makeTitleTextView(mContext, p.getDisplayGroup(), null);

        MultiAutoCompleteTextView classifierArrayNew2 = ViewUtils.makeClassifierArrayNew2(allItems, selectedItems, p.getId(), mContext);
        ImageButton addClassifier = ViewUtils.makeImageButtonAddClassifier(mContext, classifierArrayNew2);

        ll.addView(titleView);
        ll.addView(classifierArrayNew2);
        ll.addView(addClassifier);
        return ll;
    }

    //{{{
    private View makeReadView(List<ClassifierItem> selectedItems, EntityProperty p) {
        LinearLayout view = ViewUtils.makeHorizontalContainer(mContext);

        TextView titleView = ViewUtils.makeTitleTextView(mContext, p.getDisplayGroup(), null);

        TextView arrayReadOnly = ViewUtils.makeTextView(mContext);

        view.addView(titleView);
        view.addView(arrayReadOnly);

        Log.d("ARRAY_WRAPPER", "items size:" + selectedItems.size());

        if(selectedItems.size() == 0)
            return view;

        StringBuilder sb = new StringBuilder();
        sb.append(selectedItems.get(0).getDisplayValue());

        for(int i = 1;i< selectedItems.size(); i++) {
            sb.append(",")
              .append(selectedItems.get(i).getDisplayValue());
        }

        Log.d("", "selected items: " + sb);
        placeBubbles(sb.toString(), arrayReadOnly);

        return view;
    }

    private void placeBubbles(String text, TextView textView) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(text);

        String chips[] = text.trim().split(",");
        int x = 0;
        for (String c : chips) {
            ssb.setSpan(new ClassifierItemSpan(null), x, x + c.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(R.color.text_0099CC), x, x + c.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            x = x + c.length() + 1;
        }
        textView.setText(ssb);
    }
    //}}}
}