
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Delegat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Delegat">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UserTo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Delegat", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "userFrom",
    "userTo",
    "sourceSystem"
})
public class Delegat {

    @XmlElement(name = "UserFrom", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String userFrom;
    @XmlElement(name = "UserTo", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String userTo;
    @XmlElement(name = "SourceSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String sourceSystem;

    /**
     * Gets the value of the userFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserFrom() {
        return userFrom;
    }

    /**
     * Sets the value of the userFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserFrom(String value) {
        this.userFrom = value;
    }

    /**
     * Gets the value of the userTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserTo() {
        return userTo;
    }

    /**
     * Sets the value of the userTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserTo(String value) {
        this.userTo = value;
    }

    /**
     * Gets the value of the sourceSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystem() {
        return sourceSystem;
    }

    /**
     * Sets the value of the sourceSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystem(String value) {
        this.sourceSystem = value;
    }

}
