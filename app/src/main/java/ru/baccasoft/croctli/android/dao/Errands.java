package ru.baccasoft.croctli.android.dao;

import java.util.List;

/**
 * Created by developer on 31.10.14.
 */
public class Errands {

    // Ответственный исполнитель
    private UserShort responsible;
    // Исполнители
    private List<UserShort> performers;
    // Контрольный срок
    //TODO:
    // Текст резолюции
    private String croc_content;

    public UserShort getResponsible() {
        return responsible;
    }

    public void setResponsible(UserShort responsible) {
        this.responsible = responsible;
    }

    public List<UserShort> getPerformers() {
        return performers;
    }

    public void setPerformers(List<UserShort> performers) {
        this.performers = performers;
    }

    public String getCroc_content() {
        return croc_content;
    }

    public void setCroc_content(String croc_content) {
        this.croc_content = croc_content;
    }

    // заполняется из SystemUser
    public class UserShort {
        public String userId;
        public String nameShort;
    }

}
