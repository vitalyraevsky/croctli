
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Comment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Comment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedByName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Comment", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "createdBy",
    "createDate",
    "text",
    "processId",
    "createdByName"
})
@JsonIgnoreProperties({"createDate"})
@DatabaseTable(tableName = "comment")
public class Comment {

    public static final String PROCESS_FK_FIELD = "fk_process";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = PROCESS_FK_FIELD)
    private Process process;

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    @JsonProperty("Id")
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @JsonProperty("CreatedBy")
    @DatabaseField(columnName = "created_by", dataType = DataType.STRING)
    @XmlElement(name = "CreatedBy", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected String createdBy;

    @XmlElement(name = "CreateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected XMLGregorianCalendar createDate;

    @JsonProperty("CreateDate")
    @DatabaseField(columnName = "create_date", dataType = DataType.STRING)
    protected String createDateString;

    @JsonProperty("Text")
    @DatabaseField(columnName = "text", dataType = DataType.STRING)
    @XmlElement(name = "Text", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected String text;

    @JsonProperty("ProcessId")
    @XmlElement(name = "ProcessId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String processId;

    @JsonProperty("CreatedByName")
    @XmlElement(name = "CreatedByName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "created_by_name", dataType = DataType.STRING)
    protected String createdByName;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the processId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Gets the value of the createdByName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByName() {
        return createdByName;
    }

    /**
     * Sets the value of the createdByName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByName(String value) {
        this.createdByName = value;
    }

}
