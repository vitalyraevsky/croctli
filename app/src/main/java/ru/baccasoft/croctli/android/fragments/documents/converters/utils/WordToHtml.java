package ru.baccasoft.croctli.android.fragments.documents.converters.utils;

import android.util.Log;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.model.PicturesTable;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Element;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class WordToHtml {

    private static final String TAG = WordToHtml.class.getSimpleName();

    private HWPFDocument wordDocument;
    private Writer writer;
    private PicturesManager pictureManager;

    private WordToHtml(HWPFDocument wordDocument, Writer writer){
        this.wordDocument = wordDocument;
        this.writer = writer;
    }

    public static WordToHtml create(String path, Writer writer) throws IOException {
        return create(new FileInputStream(path),writer);
    }

    private static WordToHtml create(InputStream in, Writer output) {
        try {
            HWPFDocument wordDocument = new HWPFDocument(in);
            return create(wordDocument,output);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot create workbook from stream", e);
        }
    }

    private static WordToHtml create(HWPFDocument wordDocument, Writer writer) {
        return new WordToHtml(wordDocument,writer);
    }

    public void setPictureManager(PicturesManager pictureManager){
        this.pictureManager = pictureManager;
    }

    public void printPage() throws IOException {

        try {

            WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());

            if( pictureManager != null )
                wordToHtmlConverter.setPicturesManager(pictureManager);

            wordToHtmlConverter.processDocument(wordDocument);

            org.w3c.dom.Document htmlDocument = wordToHtmlConverter.getDocument();
            DOMSource domSource = new DOMSource(htmlDocument);
            StreamResult streamResult = new StreamResult(writer);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.METHOD, "html");
            serializer.transform( domSource, streamResult );

            Log.d(TAG,"parsed doc : " + streamResult.getWriter().toString());
        } catch (TransformerException e) {
            Log.e(TAG,"TransformerException",e);
        } catch (ParserConfigurationException e) {
            Log.e(TAG, "ParserConfigurationException", e);
        }

    }
}
