package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Adapter;
import ru.baccasoft.croctli.android.gen.core.AdapterSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.AdapterAPI;

import java.util.List;

/**
 * GD, GM
 */
public class AdapterSync implements ISync {

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        AdapterAPI adapterAPI = new AdapterAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<AdapterSet, Adapter>();

        @SuppressWarnings("unchecked")
        List<Adapter> adapters = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, adapterAPI);

        for(Adapter a : adapters) {
            TableUtils.createOrUpdateAdapter(a);
        }
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        AdapterAPI adapterAPI = new AdapterAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> adapterDeleteIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, adapterAPI);
        TableUtils.deleteAdapters(adapterDeleteIds);
    }

//    @Override
//    public void getList() {
//        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
//    }
}
