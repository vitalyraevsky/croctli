package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Created by usermane on 04.12.2014.
 */
public class ClassifierArrayWithTitle extends LinearLayout {
    public ClassifierArrayWithTitle(Context context, String title,
                        List<ClassifierItem> classifierItems, String entityPropertyId) {
        super(context);

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView titleView = ViewUtils.makeTitleTextView(context, title, null);

//        ClassifierArrayNew classifierArray = new ClassifierArrayNew(classifierItems, entityPropertyId, context);
//        classifierArray.setLayoutParams(new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        addView(titleView);
//        addView(classifierArray);
    }
}
