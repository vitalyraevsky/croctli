
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for FilterCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilterCondition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConditionOperatorId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntityPropertyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConditionOperatorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilterCondition", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "conditionOperatorId",
    "entityPropertyName",
    "value",
    "conditionOperatorCode"
})
@DatabaseTable(tableName = "filter_condition")
public class FilterCondition {
    @SuppressWarnings("unused")
    private static final String TAG = FilterCondition.class.getSimpleName();

    public static final String FILTER_FK_ID_FIELD = "filter_fk";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FILTER_FK_ID_FIELD)
    private Filter filter;

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    protected String id;

    @XmlElement(name = "ConditionOperatorId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("ConditionOperatorId")
    @DatabaseField(columnName = "ConditionOperatorId", dataType = DataType.STRING)
    protected String conditionOperatorId;

    @XmlElement(name = "EntityPropertyName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("EntityPropertyName")
    @DatabaseField(columnName = "EntityPropertyName", dataType = DataType.STRING)
    protected String entityPropertyName;

    @XmlElement(name = "Value", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Value")
    @DatabaseField(columnName = "Value", dataType = DataType.STRING)
    protected String value;

    @XmlElement(name = "ConditionOperatorCode", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("ConditionOperatorCode")
    @DatabaseField(columnName = "ConditionOperatorCode", dataType = DataType.STRING)
    protected String conditionOperatorCode;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the conditionOperatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionOperatorId() {
        return conditionOperatorId;
    }

    /**
     * Sets the value of the conditionOperatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionOperatorId(String value) {
        this.conditionOperatorId = value;
    }

    /**
     * Gets the value of the entityPropertyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityPropertyName() {
        return entityPropertyName;
    }

    /**
     * Sets the value of the entityPropertyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityPropertyName(String value) {
        this.entityPropertyName = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the conditionOperatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionOperatorCode() {
        return conditionOperatorCode;
    }

    /**
     * Sets the value of the conditionOperatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionOperatorCode(String value) {
        this.conditionOperatorCode = value;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}
