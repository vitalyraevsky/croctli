package ru.baccasoft.croctli.android.view.event;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.validator.BaseValidator;

public class ClassifierArrayChangeEvent implements IChangeEvent {
    private static final String TAG = ClassifierArrayChangeEvent.class.getSimpleName();

    private final String entityPropertyId;
    private final String classifierCode;
    private final OPERATION operation;
    private final List<BaseValidator> validators;

    public enum OPERATION {
        ADD, REMOVE
    }

    public ClassifierArrayChangeEvent(String entityPropertyId, String classifierCode, OPERATION operation) {
        this.entityPropertyId = entityPropertyId;
        this.classifierCode = classifierCode;
        this.operation = operation;
        validators = new ArrayList<BaseValidator>(0);
    }

    @Override
    public void save() {
        final EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
//        final String classifierId = ep.getClassifierTypeId();
        if (ep == null)
            return;//TODO  из-за реализации удаления резолюций, приложение тут падает, т.к. обращается к айдишнику, который мы удалили - пока закрыл так (не вникал сильно в Event)
        String epValue = ep.getValue();
        if(epValue == null) { epValue = ""; }

        List<String> existingCodes = restoreContent(epValue);

        switch (operation) {
            case ADD:
                if(!existingCodes.contains(classifierCode)) {
                    if (ViewUtils.isNotEmpty(epValue))
                        epValue += "@";
                    epValue = epValue + classifierCode;
                }
                break;
            case REMOVE:
                epValue = epValue.replace(classifierCode, "");
                epValue = epValue.replace("@@", "@");
                break;
        }

        ep.setValue(epValue);
        TableUtils.updateEntityProperty(ep, true);
    }

    @Override
    public String getKey() {
        return entityPropertyId;
    }

    @Override
    public boolean isNotEmpty() {
        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        String epValue = ep.getValue();
        Log.d(TAG, "is empty value:\'" + epValue + "\'");
        if(epValue.isEmpty()){
            return false;
        }
            return true;
    }

    @Override
    public List<BaseValidator> getValidators() {
        return validators;
    }

    /**
     * Распарсить данные, сохраненные в строку:
     * все значения разделены символом @
     * @param value
     * @return
     */
    private List<String> restoreContent(String value) {
        Log.d(TAG, "initial value:\'" + value + "\'");
        String values[] = value.split("@");
        Log.d(TAG, "parsed values:\'" + values);
        return Arrays.asList(values);
    }
}
