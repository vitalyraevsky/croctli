package ru.baccasoft.croctli.android.view.event;

import java.util.List;

import ru.baccasoft.croctli.android.view.validator.BaseValidator;

/**
 * Интерфейс для событий, публикуемых через EventBus, при изменении различных свойства через гуй.
 * Каждая реализация предназначена для разного по способу представления (как в гуе, так и в базе) данных
 * Каждая реализация должна уметь сохранять инкапсулированные данные об изменениях.
 */
public interface IChangeEvent {
    void save();
    String getKey();
    boolean isNotEmpty();
    List<BaseValidator> getValidators();
}
