package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class ViewCustomizationAPI implements IRestApiCall<ViewCustomizationSet, ArrayOfViewCustomization, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = ViewCustomizationAPI.class.getSimpleName();

    private static final String CALL_PREFIX = ViewCustomization.class.getSimpleName();

    @Override
    public ViewCustomizationSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = Utils.getApiString(CALL_PREFIX, Utils.apiCallMethods.MODIFY, dateFrom, dateTo, max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(ViewCustomizationSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = Utils.getApiString(CALL_PREFIX, Utils.apiCallMethods.DELETE, dateFrom, dateTo, max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfViewCustomization objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
