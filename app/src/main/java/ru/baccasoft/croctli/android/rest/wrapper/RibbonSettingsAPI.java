package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfRibbonSetting;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.RibbonSettingSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class RibbonSettingsAPI implements IRestApiCall<RibbonSettingSet, ArrayOfRibbonSetting, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = RibbonSettingsAPI.class.getSimpleName();

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfRibbonSetting objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public RibbonSettingSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/RibbonSettings/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(RibbonSettingSet.class, url, needAuth);
    }

}
