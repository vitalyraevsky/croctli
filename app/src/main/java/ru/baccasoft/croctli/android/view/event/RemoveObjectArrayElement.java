package ru.baccasoft.croctli.android.view.event;

import android.view.ViewGroup;

public class RemoveObjectArrayElement {
    private String entityPropertyId;
    private ViewGroup viewGroup;

    public RemoveObjectArrayElement(String entityPropertyId, ViewGroup viewGroup) {
        this.entityPropertyId = entityPropertyId;
        this.viewGroup = viewGroup;
    }

    public String getEntityPropertyId() {
        return entityPropertyId;
    }

    public ViewGroup getViewGroup() {
        return viewGroup;
    }
}
