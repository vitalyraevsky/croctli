package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Classifier;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.ItemField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClassifierItemDAO extends BaseDaoImpl<ClassifierItem, String> {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierItemDAO.class.getSimpleName();

    public ClassifierItemDAO(ConnectionSource connectionSource, Class<ClassifierItem> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(ClassifierItem data) throws SQLException {
        int ret = super.create(data);

        //перекладываем
        if(data.fieldsCollection == null) {
            data.fieldsCollection = (data.getFields().getItem() == null)
                    ? new ArrayList<ItemField>()
                    : new ArrayList<ItemField>(data.getFields().getItem());
        }

        // с каждым ClassifierItem связан List<ItemField>
        for(ItemField itemField : data.fieldsCollection) {
            itemField.setClassifierItem(data);
            App.getDatabaseHelper().getItemFieldDAO().create(itemField);
        }

        return ret;
    }

    public List<ClassifierItem> getByClassifier(Classifier classifier) throws SQLException {
        QueryBuilder<ClassifierItem, String> q = queryBuilder();
        q.where().eq(ClassifierItem.CLASSIFIER_ID_FIELD, classifier.getId());
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ClassifierItem> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    public List<ClassifierItem> getByClassifierId(String classifierId) throws SQLException {
        QueryBuilder<ClassifierItem, String> q = queryBuilder();
        q.where().eq(ClassifierItem.CLASSIFIER_ID_FIELD, classifierId);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ClassifierItem> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    public List<ClassifierItem> getByCode(String classifierId, String codeField) throws SQLException {
        QueryBuilder<ClassifierItem, String> q = queryBuilder();
        q.where().eq(ClassifierItem.CLASSIFIER_ID_FIELD, classifierId).and()
                .eq("code", codeField);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ClassifierItem> preparedQuery = q.prepare();
        return query(preparedQuery);
    }

    public List<ClassifierItem> getForDisplayMode0(EntityProperty p) throws SQLException {
        QueryBuilder<ClassifierItem, String> q = queryBuilder();
        q.where().eq(ClassifierItem.CLASSIFIER_ID_FIELD, p.getClassifierTypeId())
                .eq("code", p.getClassifierDisplayField());
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ClassifierItem> preparedQuery = q.prepare();
        return query(preparedQuery);

    }
}
