
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for MetaAuth complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetaAuth">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="methods" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfString"/>
 *         &lt;element name="FacadeInternal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FacadeExternal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CasInternal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CasExternal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UseDirectAuth" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasTaskFlow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasProcessFlow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasCommentFlow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasOutOfOfficeFlow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaAuth", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "methods",
    "facadeInternal",
    "facadeExternal",
    "casInternal",
    "casExternal",
    "useDirectAuth",
    "hasTaskFlow",
    "hasProcessFlow",
    "hasCommentFlow",
    "hasOutOfOfficeFlow"
})
@DatabaseTable(tableName = "meta_auth")
public class MetaAuth {
    @DatabaseField(generatedId = true)
    public int id;

    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("methods")
    protected ArrayOfString methods;

    @ForeignCollectionField
    protected ForeignCollection<StringJson> fMethods;

    @XmlElement(name = "FacadeInternal", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("FacadeInternal")
    protected String facadeInternal;

    @XmlElement(name = "FacadeExternal", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("FacadeExternal")
    protected String facadeExternal;

    @XmlElement(name = "CasInternal", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("CasInternal")
    protected String casInternal;

    @XmlElement(name = "CasExternal", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    @JsonProperty("CasExternal")
    protected String casExternal;

    @XmlElement(name = "UseDirectAuth", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    @JsonProperty("UseDirectAuth")
    protected boolean useDirectAuth;

    @XmlElement(name = "HasTaskFlow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    @JsonProperty("HasTaskFlow")
    protected boolean hasTaskFlow;

    @XmlElement(name = "HasProcessFlow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    @JsonProperty("HasProcessFlow")
    protected boolean hasProcessFlow;

    @XmlElement(name = "HasCommentFlow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    @JsonProperty("HasCommentFlow")
    protected boolean hasCommentFlow;

    @XmlElement(name = "HasOutOfOfficeFlow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    @JsonProperty("HasOutOfOfficeFlow")
    protected boolean hasOutOfOfficeFlow;

    /**
     * Gets the value of the methods property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getMethods() {
        return methods;
    }

    /**
     * Sets the value of the methods property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setMethods(ArrayOfString value) {
        this.methods = value;
    }

    /**
     * Gets the value of the facadeInternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacadeInternal() {
        return facadeInternal;
    }

    /**
     * Sets the value of the facadeInternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacadeInternal(String value) {
        this.facadeInternal = value;
    }

    /**
     * Gets the value of the facadeExternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacadeExternal() {
        return facadeExternal;
    }

    /**
     * Sets the value of the facadeExternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacadeExternal(String value) {
        this.facadeExternal = value;
    }

    /**
     * Gets the value of the casInternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasInternal() {
        return casInternal;
    }

    /**
     * Sets the value of the casInternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasInternal(String value) {
        this.casInternal = value;
    }

    /**
     * Gets the value of the casExternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasExternal() {
        return casExternal;
    }

    /**
     * Sets the value of the casExternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasExternal(String value) {
        this.casExternal = value;
    }

    /**
     * Gets the value of the useDirectAuth property.
     * 
     */
    public boolean isUseDirectAuth() {
        return useDirectAuth;
    }

    /**
     * Sets the value of the useDirectAuth property.
     * 
     */
    public void setUseDirectAuth(boolean value) {
        this.useDirectAuth = value;
    }

    /**
     * Gets the value of the hasTaskFlow property.
     * 
     */
    public boolean isHasTaskFlow() {
        return hasTaskFlow;
    }

    /**
     * Sets the value of the hasTaskFlow property.
     * 
     */
    public void setHasTaskFlow(boolean value) {
        this.hasTaskFlow = value;
    }

    /**
     * Gets the value of the hasProcessFlow property.
     * 
     */
    public boolean isHasProcessFlow() {
        return hasProcessFlow;
    }

    /**
     * Sets the value of the hasProcessFlow property.
     * 
     */
    public void setHasProcessFlow(boolean value) {
        this.hasProcessFlow = value;
    }

    /**
     * Gets the value of the hasCommentFlow property.
     * 
     */
    public boolean isHasCommentFlow() {
        return hasCommentFlow;
    }

    /**
     * Sets the value of the hasCommentFlow property.
     * 
     */
    public void setHasCommentFlow(boolean value) {
        this.hasCommentFlow = value;
    }

    /**
     * Gets the value of the hasOutOfOfficeFlow property.
     * 
     */
    public boolean isHasOutOfOfficeFlow() {
        return hasOutOfOfficeFlow;
    }

    /**
     * Sets the value of the hasOutOfOfficeFlow property.
     * 
     */
    public void setHasOutOfOfficeFlow(boolean value) {
        this.hasOutOfOfficeFlow = value;
    }

}
