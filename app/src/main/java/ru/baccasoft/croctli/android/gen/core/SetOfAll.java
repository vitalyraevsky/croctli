
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SetOfAll complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetOfAll">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="handmadeDelegateSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}HandmadeDelegateSet"/>
 *         &lt;element name="taskStateInSystemSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}TaskStateInSystemSet"/>
 *         &lt;element name="entityPropertySet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}EntityPropertySet"/>
 *         &lt;element name="classifierSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ClassifierSet"/>
 *         &lt;element name="processTypeSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ProcessTypeSet"/>
 *         &lt;element name="processSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ProcessSet"/>
 *         &lt;element name="taskSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}TaskSet"/>
 *         &lt;element name="sourceSystemSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}SourceSystemSet"/>
 *         &lt;element name="systemUserSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}SystemUserSet"/>
 *         &lt;element name="adapterSet" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}AdapterSet"/>
 *         &lt;element name="arrayOfUIForm" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfUIForm"/>
 *         &lt;element name="arrayOfFavoriteValue" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfFavoriteValue"/>
 *         &lt;element name="arrayOfFavoriteUserProcess" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfFavoriteUserProcess"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetOfAll", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "handmadeDelegateSet",
    "taskStateInSystemSet",
    "entityPropertySet",
    "classifierSet",
    "processTypeSet",
    "processSet",
    "taskSet",
    "sourceSystemSet",
    "systemUserSet",
    "adapterSet",
    "arrayOfUIForm",
    "arrayOfFavoriteValue",
    "arrayOfFavoriteUserProcess"
})
public class SetOfAll {

    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected HandmadeDelegateSet handmadeDelegateSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected TaskStateInSystemSet taskStateInSystemSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected EntityPropertySet entityPropertySet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ClassifierSet classifierSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ProcessTypeSet processTypeSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ProcessSet processSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected TaskSet taskSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected SourceSystemSet sourceSystemSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected SystemUserSet systemUserSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected AdapterSet adapterSet;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfUIForm arrayOfUIForm;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfFavoriteValue arrayOfFavoriteValue;
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfFavoriteUserProcess arrayOfFavoriteUserProcess;

    /**
     * Gets the value of the handmadeDelegateSet property.
     * 
     * @return
     *     possible object is
     *     {@link HandmadeDelegateSet }
     *     
     */
    public HandmadeDelegateSet getHandmadeDelegateSet() {
        return handmadeDelegateSet;
    }

    /**
     * Sets the value of the handmadeDelegateSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link HandmadeDelegateSet }
     *     
     */
    public void setHandmadeDelegateSet(HandmadeDelegateSet value) {
        this.handmadeDelegateSet = value;
    }

    /**
     * Gets the value of the taskStateInSystemSet property.
     * 
     * @return
     *     possible object is
     *     {@link TaskStateInSystemSet }
     *     
     */
    public TaskStateInSystemSet getTaskStateInSystemSet() {
        return taskStateInSystemSet;
    }

    /**
     * Sets the value of the taskStateInSystemSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskStateInSystemSet }
     *     
     */
    public void setTaskStateInSystemSet(TaskStateInSystemSet value) {
        this.taskStateInSystemSet = value;
    }

    /**
     * Gets the value of the entityPropertySet property.
     * 
     * @return
     *     possible object is
     *     {@link EntityPropertySet }
     *     
     */
    public EntityPropertySet getEntityPropertySet() {
        return entityPropertySet;
    }

    /**
     * Sets the value of the entityPropertySet property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityPropertySet }
     *     
     */
    public void setEntityPropertySet(EntityPropertySet value) {
        this.entityPropertySet = value;
    }

    /**
     * Gets the value of the classifierSet property.
     * 
     * @return
     *     possible object is
     *     {@link ClassifierSet }
     *     
     */
    public ClassifierSet getClassifierSet() {
        return classifierSet;
    }

    /**
     * Sets the value of the classifierSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassifierSet }
     *     
     */
    public void setClassifierSet(ClassifierSet value) {
        this.classifierSet = value;
    }

    /**
     * Gets the value of the processTypeSet property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTypeSet }
     *     
     */
    public ProcessTypeSet getProcessTypeSet() {
        return processTypeSet;
    }

    /**
     * Sets the value of the processTypeSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTypeSet }
     *     
     */
    public void setProcessTypeSet(ProcessTypeSet value) {
        this.processTypeSet = value;
    }

    /**
     * Gets the value of the processSet property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessSet }
     *     
     */
    public ProcessSet getProcessSet() {
        return processSet;
    }

    /**
     * Sets the value of the processSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessSet }
     *     
     */
    public void setProcessSet(ProcessSet value) {
        this.processSet = value;
    }

    /**
     * Gets the value of the taskSet property.
     * 
     * @return
     *     possible object is
     *     {@link TaskSet }
     *     
     */
    public TaskSet getTaskSet() {
        return taskSet;
    }

    /**
     * Sets the value of the taskSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskSet }
     *     
     */
    public void setTaskSet(TaskSet value) {
        this.taskSet = value;
    }

    /**
     * Gets the value of the sourceSystemSet property.
     * 
     * @return
     *     possible object is
     *     {@link SourceSystemSet }
     *     
     */
    public SourceSystemSet getSourceSystemSet() {
        return sourceSystemSet;
    }

    /**
     * Sets the value of the sourceSystemSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceSystemSet }
     *     
     */
    public void setSourceSystemSet(SourceSystemSet value) {
        this.sourceSystemSet = value;
    }

    /**
     * Gets the value of the systemUserSet property.
     * 
     * @return
     *     possible object is
     *     {@link SystemUserSet }
     *     
     */
    public SystemUserSet getSystemUserSet() {
        return systemUserSet;
    }

    /**
     * Sets the value of the systemUserSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemUserSet }
     *     
     */
    public void setSystemUserSet(SystemUserSet value) {
        this.systemUserSet = value;
    }

    /**
     * Gets the value of the adapterSet property.
     * 
     * @return
     *     possible object is
     *     {@link AdapterSet }
     *     
     */
    public AdapterSet getAdapterSet() {
        return adapterSet;
    }

    /**
     * Sets the value of the adapterSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdapterSet }
     *     
     */
    public void setAdapterSet(AdapterSet value) {
        this.adapterSet = value;
    }

    /**
     * Gets the value of the arrayOfUIForm property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUIForm }
     *     
     */
    public ArrayOfUIForm getArrayOfUIForm() {
        return arrayOfUIForm;
    }

    /**
     * Sets the value of the arrayOfUIForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUIForm }
     *     
     */
    public void setArrayOfUIForm(ArrayOfUIForm value) {
        this.arrayOfUIForm = value;
    }

    /**
     * Gets the value of the arrayOfFavoriteValue property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFavoriteValue }
     *     
     */
    public ArrayOfFavoriteValue getArrayOfFavoriteValue() {
        return arrayOfFavoriteValue;
    }

    /**
     * Sets the value of the arrayOfFavoriteValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFavoriteValue }
     *     
     */
    public void setArrayOfFavoriteValue(ArrayOfFavoriteValue value) {
        this.arrayOfFavoriteValue = value;
    }

    /**
     * Gets the value of the arrayOfFavoriteUserProcess property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFavoriteUserProcess }
     *     
     */
    public ArrayOfFavoriteUserProcess getArrayOfFavoriteUserProcess() {
        return arrayOfFavoriteUserProcess;
    }

    /**
     * Sets the value of the arrayOfFavoriteUserProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFavoriteUserProcess }
     *     
     */
    public void setArrayOfFavoriteUserProcess(ArrayOfFavoriteUserProcess value) {
        this.arrayOfFavoriteUserProcess = value;
    }

}
