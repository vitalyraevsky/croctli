package ru.baccasoft.croctli.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TasksActivity;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.view.util.CrocUtils;

public class TasksAdapter extends BaseAdapter implements Filterable  {
    @SuppressWarnings("unused")
    private static final String TAG = TasksAdapter.class.getSimpleName();

    // полный списко задач
    private final List<Task> tasks;
    // отфильтрованные задачи
    private List<Task> filteredTasks;

    private TaskFilter filter;

    private Context context;
    private final List<String> displayInRibbonKeyList;

    // вьюшки, соответствующие этим кодам, нужно особым образом обрабатывать
    private final String processNameCode = "ProcessName";
    private final String processCreateDate = "ProcessCreateDate";

    private List<String> cutRibbonKeys;

    public TasksAdapter(Context context, List<Task> tasks, List<String> keys) {
        this.context = context;
        this.tasks = tasks;
        this.filteredTasks = new ArrayList<Task>(tasks);
        this.displayInRibbonKeyList = new ArrayList<String>(keys);

        this.cutRibbonKeys = new ArrayList<String>(keys);
        this.cutRibbonKeys.remove(processNameCode);
        this.cutRibbonKeys.remove(processCreateDate);
    }


    @Override
    public int getCount() {
        return filteredTasks.size();
    }

    @Override
    public Task getItem(int i) {
        return filteredTasks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    private static class ViewHolder {
        private TextView taskNameView;
        private TextView taskCreateDateView;

        private TextView taskId;
        private TextView processNameAndCreateDate;
        private ImageView favoriteIcon;
        private ImageView taskIcon;
        private ImageView noteIcon;

        private LinearLayout textViews;

        private void displayTextInTextViewsChild( String text, int childViewId ) {
            TextView child = (TextView) textViews.findViewById(R.id.task_creator);
            child.setText(text);
            child.setVisibility(View.VISIBLE);
        }
    }

    // какие элементы отображать в описании Task'а, определяется флагом displayInRibbon
    // в классе ViewCustomization напротив соответствующего ключа.
    // Список ключей см. п. 2.8.1 ФС Андроид

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        final Task currentTask = filteredTasks.get(i);


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.task_item, viewGroup, false);
            holder = new ViewHolder();

            holder.textViews = (LinearLayout)view.findViewById(R.id.task_additional_info);
            holder.taskId = (TextView) view.findViewById(R.id.task_id);
            holder.favoriteIcon = (ImageView) view.findViewById(R.id.favorites_icon);
            holder.noteIcon = (ImageView) view.findViewById(R.id.note_icon);
            holder.taskIcon = (ImageView) view.findViewById(R.id.task_icon);
            holder.taskNameView = (TextView) view.findViewById(R.id.task_name);
            holder.taskCreateDateView = (TextView) view.findViewById(R.id.task_create_date);
            holder.processNameAndCreateDate = (TextView) view.findViewById(R.id.process_name_with_process_create_date);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // выставляем значения
        holder.taskId.setText(currentTask.getId());

        // сначала с особым отображением в UI
        if(displayInRibbonKeyList.contains(processNameCode)) {
            holder.processNameAndCreateDate.setText(CrocUtils.getValueByAttributeCode(processNameCode, currentTask));
            holder.processNameAndCreateDate.setVisibility(View.VISIBLE);
        } else {
            holder.processNameAndCreateDate.setText("");
            holder.processNameAndCreateDate.setVisibility(View.VISIBLE);
        }

        if(displayInRibbonKeyList.contains(processCreateDate)) {
            String text = holder.processNameAndCreateDate.getText().toString()
                    + " от " + CrocUtils.getValueByAttributeCode(processCreateDate, currentTask);
            holder.processNameAndCreateDate.setText(text);
        } else {
            if (!displayInRibbonKeyList.contains(processNameCode)) {
                holder.processNameAndCreateDate.setVisibility(View.GONE);
            }
        }

        // потом типовые
        for(String code : cutRibbonKeys) {
            Log.d(TAG, "setting view for code: " + code);
            String value = CrocUtils.getValueByAttributeCode(code, currentTask);
            Log.d(TAG, "value is: "+value);
            if( StringUtils.isEmpty(value)) {
                Log.d(TAG, "Skipping empty value");
                continue;
            }
            if (code.equals(TaskCodes.TaskCreateDate)) {
                holder.taskCreateDateView.setText(value);
            }else if(code.equals(TaskCodes.TaskName)){
                holder.taskNameView.setText(value);
            }else if(code.equals(TaskCodes.TaskCreator)){
                holder.displayTextInTextViewsChild(value, R.id.task_creator);
            }else if(code.equals(TaskCodes.TaskDescription)){
                holder.displayTextInTextViewsChild(value, R.id.task_description);
            }else if(code.equals(TaskCodes.ProcessTypeName)) {
                holder.displayTextInTextViewsChild(value, R.id.process_type_name);
            }else if(code.equals(TaskCodes.ProcessTypeDescription)) {
                holder.displayTextInTextViewsChild(value, R.id.process_type_description);
            }else if(code.equals(TaskCodes.ProcessDescription)){
                holder.displayTextInTextViewsChild(value, R.id.process_description);
            }else if(code.equals(TaskCodes.ProcessInitiator)){
                holder.displayTextInTextViewsChild(value, R.id.process_initiator);
            }else if(code.equals(TaskCodes.TaskStatus)){
                holder.displayTextInTextViewsChild(value, R.id.task_status);
            }else if(code.equals(TaskCodes.TaskDeadline)){
                holder.displayTextInTextViewsChild(value, R.id.task_deadline);
            }else if(code.equals(TaskCodes.IsTaskSingleExecutor)){
                holder.displayTextInTextViewsChild(value, R.id.is_task_sing_executor);
            }else if(code.equals(TaskCodes.TaskChangeDate)){
                holder.displayTextInTextViewsChild(value, R.id.task_change_date);
            }else if(code.equals(TaskCodes.Sourcesystem)){
                holder.displayTextInTextViewsChild(value, R.id.source_system);
            }
        }
        if (!currentTask.isWasRead()) {
            holder.taskIcon.setVisibility(View.VISIBLE);
            holder.taskIcon.setImageResource(R.drawable.icon_new);
        } else if (currentTask.getChangeDateTime() != null
                    && currentTask.getChangeDateTime().isAfter(App.prefs.getLastSyncDate())) {
            holder.taskIcon.setVisibility(View.VISIBLE);
            holder.taskIcon.setImageResource(R.drawable.icon_edited);
        } else {
            holder.taskIcon.setVisibility(View.GONE);
        }

        updateFavoriteFlag(holder.favoriteIcon, currentTask);
        updateNoteFlag(holder.noteIcon, currentTask);

        holder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentTask.setIsFavourite(!currentTask.isIsFavourite());
                updateFavoriteFlag((ImageView) view, currentTask);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            App.getDatabaseHelper().getTasksDao().update(currentTask);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(TasksActivity.ACTION_UPDATE_TASKS);
                        context.sendBroadcast(intent);
                    }
                }).start();
            }
        });
        return view;
    }

    /**
     * Обновить состояние вьюшки, показывающий состояние фрага Task.isFavourite
     *
     * @param v
     * @param task
     */
    public static void updateFavoriteFlag(ImageView v, Task task) {
        int resId = (task.isIsFavourite()) ? R.drawable.con_favorit_filled : R.drawable.icon_favorit_empty;
        v.setImageResource(resId);
    }

    public static void updateNoteFlag(ImageView v, Task task){
        int visAtr = !task.getNotice().isEmpty() ? View.VISIBLE : View.GONE;
        v.setVisibility(visAtr);
    }

    @Override
    public Filter getFilter() {
        if(filter == null)
            filter = new TaskFilter();
        return filter;
    }

    class TaskFilter extends Filter {

        private final String TAG_F = TaskFilter.class.getSimpleName();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if(constraint.equals("")){
                results.values = new ArrayList<Task>(tasks);
                results.count = tasks.size();
                return results;
            }

            List<Task> filterdTaskArray = new ArrayList<Task>();

            constraint = constraint.toString().trim();

            Log.d(TAG_F, "filter getting constraint: " + constraint.toString() );
            Log.d(TAG_F, "filter start filter tasks: " + tasks.size() );




            for ( Task task : tasks ) {

                if(displayInRibbonKeyList.contains(processNameCode)) {
                   Log.d(TAG_F, "filter getting view for code: " + processNameCode + "| with value:\'" + CrocUtils.getValueByAttributeCode(processNameCode, task) + "\'");
                   if(CrocUtils.getValueByAttributeCode(processNameCode, task).toLowerCase().contains(constraint)){
                       filterdTaskArray.add(task);
                       continue;
                   }
                }

                if(displayInRibbonKeyList.contains(processCreateDate)) {
                    Log.d(TAG_F, "filter getting view for code: " + processCreateDate + "| with value:\'" + CrocUtils.getValueByAttributeCode(processCreateDate, task) + "\'");
                    if(CrocUtils.getValueByAttributeCode(processCreateDate, task).toLowerCase().contains(constraint)){
                       filterdTaskArray.add(task);
                       continue;
                    }
                }


                for(String code : cutRibbonKeys) {
                    Log.d(TAG_F, "filter getting view for code: " + code + "| with value:\'" + CrocUtils.getValueByAttributeCode(code, task) + "\'");
                    if(CrocUtils.getValueByAttributeCode(code, task).toLowerCase().contains(constraint)){
                        filterdTaskArray.add(task);
                        continue;
                    }
                }

            }

            results.count = filterdTaskArray.size();
            results.values = filterdTaskArray;

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults( CharSequence constraint, FilterResults results ) {

            if(results.count != 0){
                filteredTasks = (List<Task>) results.values;
            }else{
                filteredTasks.clear();
            }

            Log.d(TAG_F, "filter get " + filteredTasks.size() + " tasks with constraint: " + constraint );

            notifyDataSetChanged();
        }
    }
}
