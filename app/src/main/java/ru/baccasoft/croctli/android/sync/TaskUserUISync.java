package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskUserUI;
import ru.baccasoft.croctli.android.gen.core.TaskUserUISet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.TaskUserUIAPI;

import java.util.List;

public class TaskUserUISync implements ISync {
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        TaskUserUIAPI taskUserUIAPI = new TaskUserUIAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TaskUserUISet, TaskUserUI>();

        @SuppressWarnings("unchecked")
        List<TaskUserUI> taskUserUIs = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskUserUIAPI);

        for(TaskUserUI ui : taskUserUIs) {
            TableUtils.createOrUpdateTaskUserUI(ui);
        }
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        TaskUserUIAPI taskUserUIAPI = new TaskUserUIAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> taskUserUiIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskUserUIAPI);

        TableUtils.deleteTaskUserUIs(taskUserUiIds);
    }
}
