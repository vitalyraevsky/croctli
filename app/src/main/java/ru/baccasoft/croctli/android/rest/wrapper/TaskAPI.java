package ru.baccasoft.croctli.android.rest.wrapper;

import android.util.Log;

import org.apache.http.HttpResponse;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class TaskAPI implements IRestApiCall<TaskSet, ArrayOfTask, ArrayOfString, ArrayOfOperationResponse> {
    @SuppressWarnings("unused")
    private static final String TAG = TaskAPI.class.getSimpleName();

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        final String api = "/Task/Delete/" + dateLastSync;
        boolean needAuth = true;

        String jsonData = Utils.getAsJSON(objects);

        RestClient.callSomeRestApi(api, needAuth, jsonData);
    }

    @Override
    public ArrayOfOperationResponse modifyPost(RestApiDate dateLastSync, ArrayOfTask objects) {
        final String api = "/Task/Modify/" + dateLastSync;
        boolean needAuth = true;

        String jsonData = Utils.getAsJSON(objects);

        HttpResponse response = RestClient.callSomeRestApi(api, needAuth, jsonData);
        if( response == null ) {
            return null;
        }
        final String rPostAnswer = Utils.toString(response);
        return RestClient.fillJsonClass(ArrayOfOperationResponse.class, rPostAnswer);
    }

    @Override
    public TaskSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Task/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(TaskSet.class, url, needAuth);
    }

    //GD
    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
