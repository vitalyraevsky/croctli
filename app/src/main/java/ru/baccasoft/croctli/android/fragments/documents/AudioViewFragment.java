package ru.baccasoft.croctli.android.fragments.documents;


import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class AudioViewFragment extends MediaFileViewFragment {
    @Override
    protected void openFile(String path) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        Uri data = Uri.parse(path);
        Log.d(TAG,"data : " + data.getPath() );
        //TODO: странно но audio/* не обрабатывается на эмулторе,а с помощью video/* можно проиграть аудио форматы
        //требуется протестировать на девайсе
        intent.setDataAndType(data, "video/*");
        startActivity(intent);
    }
}
