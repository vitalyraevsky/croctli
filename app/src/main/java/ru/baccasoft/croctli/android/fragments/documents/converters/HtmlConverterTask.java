package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.util.Log;

import com.artifex.mupdfdemo.AsyncTask;

import org.docx4j.openpackaging.exceptions.Docx4JException;

public class HtmlConverterTask extends AsyncTask<String,Void,String> {

    private static final String TAG = HtmlConverterTask.class.getSimpleName();

    private final Activity activity;

    private final ConvertationProcessor processor;

    public HtmlConverterTask(Activity activity, ConvertationProcessor processor) throws Docx4JException {
        this.activity = activity;
        this.processor = processor;
    }

    @Override
    protected String doInBackground(String... params) {

        if( params == null || params.length < 2 ){
            return null;
        }

        String baseURL = params[0];
        String imageDirName = params[1];

        String html = null;

        try {
              html = processor.convertToHtml(imageDirName, // <-- don't use a path separator here
                                             baseURL,
                                             activity);
        } catch( Exception e ) {
            Log.e(TAG, "Exception occured while converting proccess ", e);
        }

        return html;
    }


}