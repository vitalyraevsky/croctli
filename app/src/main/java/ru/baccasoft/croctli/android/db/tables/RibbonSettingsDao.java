package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.RibbonSettings;

import java.sql.SQLException;

public class RibbonSettingsDao extends BaseDaoImpl<RibbonSettings, String> {
    public RibbonSettingsDao(ConnectionSource connectionSource, Class<RibbonSettings> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
