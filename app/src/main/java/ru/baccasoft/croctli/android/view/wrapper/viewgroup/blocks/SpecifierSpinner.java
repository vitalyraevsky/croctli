package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

/**
 * Created by usermane on 06.11.2014.
 */
public class SpecifierSpinner extends Spinner {
    Context mContext;

    public SpecifierSpinner(Context context, List<String> items) {
        super(context);
        mContext = context;

        initAdapter(items);
    }

    private void initAdapter(List<String> data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, data);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        setAdapter(adapter);
    }
}






















