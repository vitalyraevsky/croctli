package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.IntScalar;

/**
 * Created by developer on 06.11.14.
 */
public class IntScalarWithTitle extends LinearLayout {
    public IntScalarWithTitle(Context context, String title, String text, boolean isReadonly) {
        super(context);

        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView titleView = ViewUtils.makeTitleTextView(context, title, null);
        IntScalar intScalar = new IntScalar(context, text, isReadonly);

        intScalar.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        addView(titleView);
        addView(intScalar);

    }
}
