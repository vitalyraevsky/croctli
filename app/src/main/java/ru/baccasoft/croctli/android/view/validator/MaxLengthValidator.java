package ru.baccasoft.croctli.android.view.validator;

import android.content.Context;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;

public class MaxLengthValidator extends BaseValidator {

    public MaxLengthValidator(Context context, EntityProperty entityProperty, String valueToValidate) {
        super(context, entityProperty, valueToValidate, "Validation_PropertyPresenter_MaxLength");
    }

    @Override
    public boolean isValid() {
        int maxLength = entityProperty.getMaxLen();
        int valueLength = valueToValidate.length();

        return valueLength <= maxLength;
    }
}
