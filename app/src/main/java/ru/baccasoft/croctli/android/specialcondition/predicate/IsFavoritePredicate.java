package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class IsFavoritePredicate implements IBooleanAtom {
	
	public static final String ATOM_NAME = "isfavorite";

	@Override
	public Boolean compute(Task task) {
		return task.isIsFavourite();
	}

	@Override
	public IBooleanAtom create() {
		return new IsFavoritePredicate();
	}

	@Override
	public String getAtomName() {
		return ATOM_NAME;
	}
}
