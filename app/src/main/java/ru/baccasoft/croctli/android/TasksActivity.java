package ru.baccasoft.croctli.android;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

import org.joda.time.DateTime;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.adapters.TabsAdapter;
import ru.baccasoft.croctli.android.adapters.TasksAdapter;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.filters.ITaskFilter;
import ru.baccasoft.croctli.android.filters.SpecialConditionFilter;
import ru.baccasoft.croctli.android.filters.TaskFilterFactory;
import ru.baccasoft.croctli.android.gen.core.ConditionOperator;
import ru.baccasoft.croctli.android.gen.core.Filter;
import ru.baccasoft.croctli.android.gen.core.FilterCondition;
import ru.baccasoft.croctli.android.gen.core.Tab;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.ViewCustomization;
import ru.baccasoft.croctli.android.rest.Utils;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.updater.FirstSyncStepTwoTask;
import ru.baccasoft.croctli.android.updater.StepOneResult;
import ru.baccasoft.croctli.android.utils.DateTimeWrapper;
import ru.baccasoft.croctli.android.view.event.EndUpdateEvent;
import ru.baccasoft.croctli.android.view.event.StartUpdateEvent;
import ru.baccasoft.croctli.android.view.event.UpdateTasksEvent;

public class TasksActivity extends SherlockActivity {
    @SuppressWarnings("unused")
    private static final String TAG = TasksActivity.class.getSimpleName();

    public static final String ACTION_UPDATE_TASKS = "action_update_tasks";

    private ListView tasksView;
    private LinearLayout syncForceView;
    private TextView syncPanelText;
    private ImageView syncPanelImage;

    private BroadcastReceiver updateReceiver;


    // список задач
    private ArrayList<Task> taskList;
    private static List<Task> tasksOnCurrentTab;

    private List<String> displayInRibbonKeyList;

    // список закладок
    private ArrayList<Tab> tabList;
    // списки задач по закладкам
    private Map<Tab, List<Task>> filteredByTabTasks;

    private HashMap<String, String> tabCodesNames;

    private ListView tabsLayout;

    // обходим привычку андроида вызывать onPrepareOptionsMenu при открытии списка в меню на ActionBar
    private boolean needUpdateMenu = false;

    private List<String> sortMenuUINames;

    private int currentTabIndex;

    private SearchView searchView;
    private boolean firstStart = true; //чтобы определять, первый раз сработало onResume или нет

    private boolean updateFromLocalRequest = false;
    private ProgressDialog progressDialog;

    private int syncPeriod; // период регулярной синхронизации в миллисекундах
    private int newSyncperiod;
    private boolean canPerformSync = true;
    private static final String ACTION_START = "action_start";
    private static final String SYNC_PERIOD = "sync_period";
    private static final String ACTION_START_BY_REQUEST = "action_start_by_request";

    private Intent serviceIntent;
    private MyFirstSyncStepTwo myFirstSyncStepTwo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tasks);
        Log.d(TAG, "onCreate IN Tasks Activity");
        getSupportActionBar().setTitle(R.string.App_Title);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        syncPanelText = (TextView) findViewById(R.id.sync_panel_text);
        syncPanelImage = (ImageView) findViewById(R.id.sync_panel_image);
        tasksView = (ListView) findViewById(R.id.task_content);
        tabsLayout = (ListView) findViewById(R.id.tabs_layout);
        syncForceView = (LinearLayout) findViewById(R.id.sync_force);

        StepOneResult result = App.getStepOneResult();

        myFirstSyncStepTwo = new MyFirstSyncStepTwo();
        myFirstSyncStepTwo.execute(result);

        syncPeriod = SettingsActivity.getSyncPeriod() * 1000;

        if(updateReceiver == null) {
            updateReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(TAG, "onReceiver");
                    update();
                }
            };
        }
        registerReceiver(updateReceiver, new IntentFilter(ACTION_UPDATE_TASKS));

    }


    private class MyFirstSyncStepTwo extends FirstSyncStepTwoTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            canPerformSync = false;

            progressDialog = new ProgressDialog(TasksActivity.this);
            progressDialog.setMessage("Загрузка данных...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            progressDialog.incrementProgressBy(progress);
        }

        @Override
        protected void onPostExecute(Void oVoid) {
            Log.d(TAG,"MyFirstSyncStepTwo finished");
            makeStartService(ACTION_START, SYNC_PERIOD, syncPeriod);
            update();
            progressDialog.dismiss();
            canPerformSync = true;
        }
    }

    private void initUIData() {
        initTabNamesMap();
        updateSyncPanelText();

        syncForceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                if(canPerformSync) {
                        makeStartService(ACTION_START_BY_REQUEST, null, syncPeriod);
                    }else{
                        Toast.makeText(getApplicationContext(), "Синхронизация уже запущена.", Toast.LENGTH_SHORT).show();
                    }
                }
        });


        tasksView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(syncPanelImage.getAnimation() != null){
                    syncPanelImage.setAnimation(null);
                }
                Intent mIntent = new Intent(getApplicationContext(), TaskBrowseActivity.class);

                TextView taskIdView = (TextView) view.findViewById(R.id.task_id);
                final String taskId = taskIdView.getText().toString();
                final String tabId = tabList.get(currentTabIndex).getId();

                Log.d(TAG, "clicked task with id: " + taskId);
                mIntent.putExtra(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
                mIntent.putExtra(TaskBrowseActivity.TAB_ID_ARGS_KEY, tabId);

                Log.d(TAG, myFirstSyncStepTwo.getStatus().toString());
                startActivity(mIntent);
            }
        });

        tabsLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setSelected(true);
                view.findViewById(R.id.tab_name).setSelected(true);
                view.findViewById(R.id.tab_count_text).setSelected(true);

                //ViewUtils.hideSoftKeyboard(TasksActivity.this);
                updateFilteredTabs(position);
                sortTasksInTab();


            }
        });

        if(firstStart) currentTabIndex = 0;

        Log.d(TAG, "create finished");
    }

    private void updateSyncPanelText() {
        long lastSync = App.prefs.getLastSyncDate();
        syncPanelText.setText( new DateTimeWrapper(new Date(lastSync)).toGuiString() );
    }

    private void update() {
        final CyclicBarrier showTasksBarrier = new CyclicBarrier(2, new Runnable() {
            @Override
            public void run() { // заполняем filteredByTabTasks
                filteredByTabTasks = new HashMap<Tab, List<Task>>(tabList.size());
                for (Tab tab : tabList) {
                    List<Task> tmpTasks;
                    if (tab.getTabFilter().getSpecialConditionId() == null)
                        tmpTasks = new ArrayList<Task>();
                    else
                        tmpTasks = filterByTab(tab.getTabFilter());
                    Log.d(TAG, "adding in tab with name:\'" + tab.getName() + "\' " +
                                    tmpTasks.size() + " tasks");
                    filteredByTabTasks.put(tab, tmpTasks);
                }
                TasksActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initUIData();
                        if(!updateFromLocalRequest) {
                            updateMenu();
                        }
                        inflateTabs();
                    }
                });
            }
        });

        // TODO: убеждаться, что этот поток запускается после того, как прошла вторая синхронизация

        new Thread(new Runnable() { // TODO: запускать поток после синхронизации тасков
            @Override
            public void run() {
                try {
                    //taskList = new ArrayList<Task>(TableUtils.getTasksExceptClosed());
                    taskList = new ArrayList<Task>(TableUtils.getAllTasks());
                    displayInRibbonKeyList = App.getDatabaseHelper().getViewCustomizationDao().getDisplayInRibbonKeys();
                    Log.d(TAG, "tasks loaded: " + taskList.size());
                    showTasksBarrier.await();
                } catch (Exception e) {  // InterruptedException, BrokenBarrierException
                    String message = "cannot read tasks or viewCustomizations from db";
                    TableUtils.createLogMessage(message, e);
                    Log.e(TAG, message, e);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tabList = new ArrayList<Tab>(App.getDatabaseHelper().getTabDao().queryForAll());
                    Log.d(TAG, "tabs loaded: " + tabList.size());
                    showTasksBarrier.await();
                } catch (Exception e) {  // SQLException, InterruptedException, BrokenBarrierException
                    String message = "cannot read tasksView from db";
                    TableUtils.createLogMessage(message, e);
                    Log.e(TAG, message, e);
                }
            }
        }).start();
    }

    private void initTabNamesMap() {
        tabCodesNames = new HashMap<String, String>();
        String[] names = getResources().getStringArray(R.array.tab_names);
        String[] codes = getResources().getStringArray(R.array.tab_codes);
        for (int i = 0; i < codes.length; i++) {
            tabCodesNames.put(codes[i], names[i]);
        }
    }

    private List<Task> filterByTab(Filter tabFilter) {
        return filterByTab(tabFilter, taskList);
    }

    public static List<Task> getTasksFromTab(Filter tabFilter) {
        try {
            List<Task> listOfTasks = new ArrayList<Task>(App.getDatabaseHelper().getTasksDao().queryForAll());
            return filterByTab(tabFilter, listOfTasks);
        } catch (SQLException sqe) {
            TableUtils.createLogMessage(TableUtils.TASK_READ_SQL_MESSAGE, sqe);
            Log.e(TAG, TableUtils.TASK_READ_SQL_MESSAGE, sqe);
            return null;
        }
    }

    public static List<Task> getTasksOnCurrentTab(){
        List<Task> currentTaskForCurrentTab = tasksOnCurrentTab;
        return currentTaskForCurrentTab;
    }

    private static List<Task> filterByTab(Filter tabFilter, List<Task> listOfTasks) {

        if (tabFilter != null)
            Log.d(TAG, "tab filter id: " + tabFilter.getId());
        else
            Log.e(TAG, "tab filter is null");

        if (tabFilter == null) {
            String message = "tabFilter must not be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
            return null;
        }

        String specialConditionId = tabFilter.getSpecialConditionId();
        Log.d(TAG, "filter specialCondition id: \'" + specialConditionId + "\'");
        Log.d(TAG, "filter specialCondition expression: \'" + TableUtils.getSpecialConditionById(specialConditionId)
                .getExpression() + "\'");

        Log.d(TAG, "filter conditions count: " + tabFilter.getFilterConditions().size());
        List<ITaskFilter> filters = new ArrayList<ITaskFilter>();

        TaskFilterFactory filterFactory = new TaskFilterFactory();
        for (FilterCondition condition : tabFilter.getFilterConditions()) {
            Log.d(TAG, "filter condition operator id: " + condition.getConditionOperatorId() );
            Log.d(TAG, "filter condition ep name: " + condition.getEntityPropertyName() );
            Log.d(TAG, "filter condition value: " + condition.getValue() );
            ConditionOperator operator = TableUtils.getConditionOperatorById(condition.getConditionOperatorId());
            Log.d(TAG, "filter operator: " + operator.getCode());
            filters.add(filterFactory.createFilter(condition, operator));
        }

        filters.add(new SpecialConditionFilter(TableUtils.getSpecialConditionById(tabFilter.getSpecialConditionId())));

        List<Task> filteredTasks = new ArrayList<Task>();
        tasks_loop: for (Task task : listOfTasks) {
            for (ITaskFilter filter : filters) {
                if (!filter.isAvailable(task)) {
                    continue tasks_loop;
                }
            }
            filteredTasks.add(task);
        }
        return filteredTasks;
    }

    private void inflateTabs() {
        Log.d(TAG, "inflating " + tabList.size() + " tabs");
        TabsAdapter adapter = new TabsAdapter(this, tabList, tabCodesNames, filteredByTabTasks);
        tabsLayout.setAdapter(adapter);

        if(firstStart){
            tabsLayout.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "Child items: " + tabsLayout.getChildCount());
                    tabsLayout.getChildAt(currentTabIndex).setSelected(true);
                    tabsLayout.getChildAt(currentTabIndex).findViewById(R.id.tab_name).setSelected(true);
                    tabsLayout.getChildAt(currentTabIndex).findViewById(R.id.tab_count_text).setSelected(true);
                }
            });
            tabsLayout.performItemClick(tabsLayout.getAdapter().getView(currentTabIndex, null, null), currentTabIndex, tabsLayout.getAdapter().getItemId(currentTabIndex));
        }
        updateFilteredTabs(currentTabIndex);
        sortTasksInTab();
    }

    private void sortTasksInTab() {

        Tab tab = tabList.get( currentTabIndex );
        if (tab.getKeyName() == null)
            return;

        Log.d(TAG, "tasks sort key : " + tab.getKeyName() + " acs : " + tab.isAscendingSort());

        Comparator<Task> comparator = Utils.getTaskComparator( tab.getKeyName(), tab.isAscendingSort() );
        sortTasks(comparator);
        if(!updateFromLocalRequest){
            updateMenu();
        }else{
            updateFromLocalRequest = false;
        }


    }

    private void sortTasks(Comparator<Task> comparator) {

        Log.d(TAG, "tasks on current tab index:" + currentTabIndex);

        Tab currentTab = tabList.get(currentTabIndex);

        tasksOnCurrentTab = filteredByTabTasks.get( currentTab );

        if(tasksOnCurrentTab != null) {

            Log.d(TAG, "tasks on current tab:" + tasksOnCurrentTab.size());

            Collections.sort(tasksOnCurrentTab, comparator);

            TasksAdapter adapter = new TasksAdapter( this, tasksOnCurrentTab, displayInRibbonKeyList );

            // если непустой запрос на поиск то фильтруем адаптер
            String searchQuery = currentTab.getSearchQuery();

            Log.d(TAG, "tasks searchQuery:" + searchQuery );

            if ( searchQuery != null ){
                adapter.getFilter().filter(searchQuery);
            }

            tasksView.setAdapter(adapter);

            Log.d(TAG, "tasks result count:" + tasksView.getAdapter().getCount() );
            Log.d(TAG, "tasks tab all task:" + filteredByTabTasks.get( currentTab ).size() );

        }
    }

    private void updateSearchQueryForCurrentTab(String filterQuery) {
        try {
            Tab tab = tabList.get( currentTabIndex );
            tab.setSearchQuery(filterQuery);
            App.getDatabaseHelper().getTabDao().update(tab);
        } catch (SQLException e) {
            Log.e(TAG, "not able to update : " + filterQuery);
        }
    }

    private void updateFilteredTabs(final int tabIndex) {

        currentTabIndex = tabIndex;

        tabsLayout.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Child items: " + tabsLayout.getChildCount()+", idx: "+tabIndex+", curr.idx: "+currentTabIndex);
                View activeTab = tabsLayout.getAdapter().getView(currentTabIndex, null, null);
                /*tabsLayout.getChildAt(currentTabIndex)*/
                activeTab.setSelected(true);
                activeTab.findViewById(R.id.tab_name).setSelected(true);
            }
        });
/*
        Log.d(TAG, ">>>> all tabs view count is: " + allTabsView.getChildCount());

        for (int i = 0; i < allTabsView.getChildCount(); ++i) {
            allTabsView.getChildAt(i).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        }

        allTabsView.getChildAt(tabIndex).setBackgroundColor(getResources().getColor(R.color.selected_tab_color));
*/
        Tab currentTab = tabList.get(tabIndex);

        if( searchView != null ){
            searchView.setQuery( currentTab.getSearchQuery(), false );
        }

        TasksAdapter adapter = new TasksAdapter( this, filteredByTabTasks.get( currentTab ), displayInRibbonKeyList );

        // если непустой запрос на поиск то фильтруем адаптер
        String searchQuery = currentTab.getSearchQuery();
        Log.d(TAG, "tasks searchQuery:" + searchQuery );
        if (searchQuery != null && !searchQuery.equals("")){
            adapter.getFilter().filter(searchQuery);
        }

        tasksView.setAdapter(adapter);

    }

    private void makeStartService(String action, String mark, int syncPeriod){
        serviceIntent = new Intent(this, MyIntentService.class);
        if(ACTION_START.equals(action)){
            serviceIntent.setAction(action);
            serviceIntent.putExtra(mark, syncPeriod);
        }else if(ACTION_START_BY_REQUEST.equals(action)){
            serviceIntent.setAction(action);
        }

        startService(serviceIntent);
    }

    //{{{ Работа с меню
    // обновить содержимое меню
    private void updateMenu() {
        Log.d(TAG, "updateMenu");
        // форсим срабатывание onPrepareOptionsMenu
        invalidateOptionsMenu();
        // и гарантируем, что наш код обновления в onPrepareOptionsMenu сработает только для нас
        needUpdateMenu = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "RESUME IN Tasks Activity");

        newSyncperiod = SettingsActivity.getSyncPeriod() * 1000;
        if(newSyncperiod != syncPeriod){
            syncPeriod = newSyncperiod;
            makeStartService(ACTION_START, SYNC_PERIOD, newSyncperiod);
        }

        if(!firstStart) {
            update();
        } else {
            firstStart = false;
        }

        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().registerSticky(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"PAUSE IN Tasks Activity");

        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(progressDialog !=null){
            progressDialog.dismiss();
            progressDialog = null;
        }
        Log.d(TAG,"STOP IN Tasks Activity");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"RESTART IN Tasks Activity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"DESTROY IN Tasks Activity");
        unregisterReceiver(updateReceiver);
        stopService(new Intent(this, MyIntentService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");

        menu.clear();
        getSupportMenuInflater().inflate(R.menu.tasks_menu_new, menu);

        Log.d(TAG, "inflating search menu.");

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = new SearchView(this);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit : " + query );
                updateSearchQueryForCurrentTab(query);
                sortTasksInTab();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange : " + newText );
                // если потерли весь текст то отображаем все таски
                if(newText.equals("")){
                    updateSearchQueryForCurrentTab(newText);
                    sortTasksInTab();
                    return true;
                }
                return false;
            }
        });

        searchItem.setActionView( searchView );


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu");

        if(needUpdateMenu) {

            SubMenu subMenu = menu.findItem(R.id.action_sort).getSubMenu();
            subMenu.clear();

            try {
                List<ViewCustomization> sortMenuCodeNames = App.getDatabaseHelper().getViewCustomizationDao().getMenuItems();
                sortMenuUINames = Utils.getMenuNames(sortMenuCodeNames, this);
            } catch (SQLException e) {
                TableUtils.createLogMessage(TableUtils.GENERAL_READ_MESSAGE, e);
                Log.e(TAG, TableUtils.GENERAL_READ_MESSAGE, e);
            }

            Log.d(TAG, "inflating sorting menu.");

            Tab currentTab = tabList.get(currentTabIndex);

            for (int i = 0; i < sortMenuUINames.size(); ++i) {

                SpannableString ss = new SpannableString(sortMenuUINames.get(i).concat("    "));
                final String sortKey = Utils.getCodeByAttributeUIName(sortMenuUINames.get(i), this);
                if (sortKey.equals(currentTab.getKeyName())) {
                    Drawable d = currentTab.isAscendingSort()
                            ? getResources().getDrawable(R.drawable.sort_direction_up)
                            : getResources().getDrawable(R.drawable.sort_direction_down);
                    d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                    ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                    ss.setSpan(span, sortMenuUINames.get(i).length() + 2, sortMenuUINames.get(i).length() + 4, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                subMenu.add(ss);
                Log.d(TAG, "name: " + sortMenuUINames.get(i));
            }

            needUpdateMenu = false;
        }

        return true;
    }

    @Override
    public final boolean onMenuItemSelected(int featureId, android.view.MenuItem item) {
        // fix android formatted title bug
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2
                && item.getTitleCondensed() != null) {
            item.setTitleCondensed(item.getTitleCondensed().toString());
        }
        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_sort:  // пункт меню Сортировка
                //updateMenu();
                break;
            case R.id.action_journal:   // пункт меню Журнал
                Utils.showToastNotImplemented(getApplicationContext());
                break;
            case R.id.action_status:   // пункт меню Статус
                Utils.showToastNotImplemented(getApplicationContext());
                break;
            case R.id.action_settings:  // пункт меню Настройки
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_exit:   // пункт меню Выход
                LoginActivity.startSignOut(getBaseContext());
                finish();
                break;
            case R.id.action_search:    // кнопка Поиск (с лупой)
                // Utils.showToastNotImplemented(getApplicationContext());
                item.expandActionView();
                break;
            default:

                // TODO: надо проверять коды, а не локализованные имена

                // Вызываем trim у тайтла иначе в конце присутствую лишние символы
                String sortTitleName = item.getTitle().toString().trim();
                Log.d(TAG, "item id: " + item.getItemId() + ", title:" + sortTitleName +"; " + sortTitleName.length());

                //{{{ Обрабатываем нажатия на пункты меню сортировки
                if(sortMenuUINames.contains(sortTitleName.trim())) {

                    needUpdateMenu = true;
                    final String sortKey = Utils.getCodeByAttributeUIName(sortTitleName.trim(), this);
                    Tab currentTab = tabList.get( currentTabIndex );

                    // SortOptionCode
                    if ( sortKey.equals( currentTab.getKeyName() )) {
                        currentTab.setAscendingSort( !currentTab.isAscendingSort() );
                    } else {
                        currentTab.setAscendingSort( true );
                    }

                    currentTab.setKeyName(sortKey);

                    TableUtils.updateTab(currentTab);

                    sortTasksInTab();
                }
                //}}}
                break;
        }
        return true;
    }


    public void onEvent(StartUpdateEvent event){
        Log.d(TAG,"receive StartUpdateEvent");
        Animation a = AnimationUtils.loadAnimation(getApplication().getApplicationContext(), R.anim.sync_progress_anim);
        syncPanelImage.startAnimation(a);
        canPerformSync = false;
        updateFromLocalRequest = true;
    }

    public void onEvent(EndUpdateEvent event){
        Log.d(TAG,"receive EndUpdateEvent");
        if(syncPanelImage.getAnimation() != null){
            syncPanelImage.setAnimation(null);
        }
        if(event.isResult()){
            syncPanelImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_refresh));
            updateSyncPanelText();
        } else {
            // ставим иконку с ошибкой
            syncPanelImage.setImageDrawable(getResources().getDrawable(R.drawable.refresh_faild));
        }
        canPerformSync = true;
    }

    public void onEvent(UpdateTasksEvent event){
        Log.d(TAG,"receive UpdateTasksEvent");
        update();
    }
    //}}}
}
