package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.util.Log;

import java.io.IOException;
import java.io.StringWriter;

import ae.javax.xml.bind.JAXBException;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.DocFileConversionHandler;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.WordToHtml;

public class DocOldFormatProcessor implements ConvertationProcessor {

    private static final String TAG = DocOldFormatProcessor.class.getSimpleName();

    private WordToHtml wordToHtml;
    private StringWriter writer;

    @Override
    public void init(String officeFileName) throws Exception {
        Log.d(TAG, "init word converter with file: " + officeFileName);
        writer = new StringWriter();
        wordToHtml = WordToHtml.create( officeFileName, writer );
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException {

        wordToHtml.setPictureManager( new DocFileConversionHandler( imageDirPath, targetUri, activity ) );

        wordToHtml.printPage();
        String html = writer != null ? writer.toString() : null ;
        writer.close();
        return html;
    }


}
