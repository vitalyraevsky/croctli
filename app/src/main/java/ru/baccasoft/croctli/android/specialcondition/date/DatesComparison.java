package ru.baccasoft.croctli.android.specialcondition.date;

import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.IExpression;

public final class DatesComparison implements IExpression<Boolean>{
    @SuppressWarnings("unused")
    private static final String TAG = DatesComparison.class.getSimpleName();

	private final IDateOperand left;
	private final IDateOperand right;
	private final String operator;

	public DatesComparison(IDateOperand left, String operator, IDateOperand right) {
		this.left = left;
		this.right = right;
		this.operator = operator;
	}

	@Override
	public Boolean compute(Task task) {
		int a = left.compute(task);
		int b = right.compute(task);
		
		if( "==".equals(operator)) {
			return a == b;
		} else if( "!=".equals(operator)) {
			return a != b;
		} else if( ">=".equals(operator)) {
			return a >= b;
		} else if( "<=".equals(operator)) {
			return a <= b;
		} else if( ">".equals(operator)) {
			return a > b;
		} else if( "<".equals(operator)) {
			return a < b;
		} else {
            String message = "unknown operator "+operator;
            TableUtils.createLogMessage(message);
            Log.e(TAG, message);
			throw new RuntimeException(message);
		}
	}
}
