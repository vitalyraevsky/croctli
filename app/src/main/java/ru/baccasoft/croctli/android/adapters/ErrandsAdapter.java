package ru.baccasoft.croctli.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.dao.Errands;

import java.util.List;

/**
 *  Адаптер для наполнения списка предварительных поручений.
 *  Каждая возращаемая View является отдельным поручением (со списком исполнителей, текстом поручения и срочностью)
 *
 */


public class ErrandsAdapter extends BaseAdapter {
    private static final String TAG = ErrandsAdapter.class.getSimpleName();

//    List<EntityProperty> entityProperties;

    // будут браться на основе entityProperties
    String[] tmpUsers = {"Иванов", "Петров", "Сидоров"};
    List<Errands> errandList;

    Context context;

    LayoutInflater layoutInflater;

    public ErrandsAdapter(List<Errands> errandsList, Context context) {
        this.errandList = errandsList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return errandList.size();
    }

    @Override
    public Object getItem(int position) {
        return errandList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //TODO: переделать во ViewHolderPattern
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            view = layoutInflater.inflate(R.layout.array_dictionary_entity_property, parent, false);

            // создаем новый Layout, в который будет складываться отдельное поручение
            view = new LinearLayout(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            ((LinearLayout) view).setLayoutParams(layoutParams);
        }

        // сама вьюшка, показывающая список исполнителей
        View errandItem = layoutInflater.inflate(R.layout.array_dictionary_entity_property, parent, false);

        LinearLayout executorsLayout;

        Errands currentErrand = errandList.get(position);

        // вручную наполняем список исполнителей
//        executorsLayout = (LinearLayout) errandItem.findViewById(R.id.layout_subitems);
//        for(Errands.UserShort u : currentErrand.getPerformers()) {
//            //TODO: изменился конструктор. починить?
//            ArrayDictionaryEntityPropertyItemView item = new ArrayDictionaryEntityPropertyItemView(context);
//            item.setText(u.nameShort);
//            executorsLayout.addView(item);
//        }

        ((LinearLayout) view).addView(errandItem);

//        executorsLayout.setAdapter(new ArrayDictionaryEntityPropertyAdapter(context, tmpUsers));

        return view;
    }
}
