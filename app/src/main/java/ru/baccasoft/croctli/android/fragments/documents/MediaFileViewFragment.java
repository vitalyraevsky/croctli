package ru.baccasoft.croctli.android.fragments.documents;

import android.content.ActivityNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import ru.baccasoft.croctli.android.FileUtils;
import ru.baccasoft.croctli.android.R;

import java.io.File;
import java.io.IOException;

public abstract class MediaFileViewFragment extends AttachmentViewFragment  {

    public static final String TAG = MediaFileViewFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.attach_mediacontent , null );
        view.findViewById(R.id.open).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    String path = copyFileInPublicFolder();

                    Log.d(TAG,"try open intent for file: " + path);

                    if( path != null )
                        openFile(path);

                } catch (ActivityNotFoundException e){
                    Log.e(TAG,"not abel open media file",e);
                    Toast.makeText(getActivity(), R.string.no_app_to_open_file, Toast.LENGTH_LONG).show();
                }
            }


        });
        return view;
    }

    protected abstract void openFile(String path);

    private String copyFileInPublicFolder() {

        File destDir = new File( Environment.getExternalStorageDirectory() + File.separator + "CrocTLI");

        if( !destDir.exists() )
            destDir.mkdir();

        File dest = new File( destDir, getAttachmentName() );

        File source = new File(getAttachmentPath());

        Log.d(TAG,"copy file to: " + dest.getPath());

        try {
            FileUtils.copyFile(source,dest);
            return dest.getAbsolutePath();
        } catch (IOException e) {
            Log.e(TAG,"could not able to copy file into public folder",e);
        }
        return null;
    }
}
