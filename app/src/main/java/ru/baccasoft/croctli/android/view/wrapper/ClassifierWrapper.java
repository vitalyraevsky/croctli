package ru.baccasoft.croctli.android.view.wrapper;

import android.content.Context;
import android.util.Log;
import android.view.View;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.Classifier;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Для справочных свойств, у которых можно выбрать только одно значение.
 * Работает для DisplayMode=0 и DisplayMode=1.
 *
 *
 */
public class ClassifierWrapper implements IWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierWrapper.class.getSimpleName();

    private boolean readOnly;
    private Context mContext;

    public ClassifierWrapper(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        Classifier classifier = Preload.getClassifier(p);
        int displayMode = classifier.getDisplayMode();

        switch (displayMode) {
            case 0:
                Log.d(TAG, "classifier displayMode0");
                return new DisplayMode0Wrapper(mContext).getLayout(p, globalReadOnly);
            case 1:
                Log.d(TAG, "classifier displayMode1");
                return new DisplayMode1Wrapper(mContext).getLayout(p, globalReadOnly);
            default:
                throw new IllegalArgumentException("displayMode must be \"0\" or \"1\"" +
                        " but we have \'" + displayMode + "\'");
        }
    }

    private void validate(EntityProperty p) {
        if(ViewUtils.isEmpty(p.getClassifierTypeId())) {
            Log.w( TAG, ReflectionToStringBuilder.toString(p));
            throw new IllegalStateException("no classifier item for EntityProperty " + p.getId() );
        }
        if(ViewUtils.isNotEmpty(p.getScalarType())) {
            Log.w( TAG, ReflectionToStringBuilder.toString(p));
            throw new IllegalStateException("EntityProperty " + p.getId() + " with scalarType " + p.getScalarType() + " should be processed elsewhere");
        }
    }

    //    /**
//     * Получить массив строк, которые которые будут предлагаться в выпадающем списке справочника
//     * при редактировании
//     *
//     * @param p
//     * @return
//     */
//    private TreeMap<String, List<String>> getClassifierViewItemList(EntityProperty p) {
//        final String selectedValue;
//        final List<String> itemsListForView = new ArrayList<String>();
//
//        // Если атрибут ClassifierDisplayField заполнен, источником данных будет массив Classifier.ClassifierItem.ItemField.Value,
//        // где Classifier.id = EntityProperty. СlassifierId AND ClassifierItem.Code = EntityProperty. ClassifierDisplayField
//        if(ViewUtils.isNotEmpty(p.getClassifierDisplayField())) {
//            List<ItemField> itemFields = TableUtils.getItemFieldsForDisplayMode0(p);
//            for(ItemField i : itemFields) {
//                itemsListForView.add(i.getValue());
//            }
//            // Выбранное значение классификатора в этом случае хранится как ItemField.Name
//            if(ViewUtils.isNotEmpty(p.getValue())) {
//                selectedValue = TableUtils.getItemFieldByName(p.getValue()).getValue();
//            } else {
//                selectedValue = "";
//            }
//        // По умолчанию источником данных для списка являются элементы классификатора (ClassifierItem.DisplayValue),
//        // id которого передан в поле СlassifierId.
//        } else {
//            List<ClassifierItem> classifierItems = TableUtils
//                    .getClassifierItemsByClassifierId(p.getClassifierTypeId());
//            for(ClassifierItem ci : classifierItems) {
//                itemsListForView.add(ci.getDisplayValue());
//            }
//            // Выбранное значение классификатора в этом случае хранится как ClassifierItem.Code
//            if(ViewUtils.isNotEmpty(p.getValue())) {
//                Log.d(TAG, "[getClassifierViewItemList] saved code: \'" + p.getValue() + "\'");
//
//                ClassifierItem classifierItem = TableUtils.getClassifierItemByCode(p.getClassifierTypeId(), p.getValue());
//
//                if(classifierItem == null) {
//                    if(p.isIsClassifierFree()) {
//                        // значит произвольное значение, введенное в поле справочника со свободным вводом
//                        // было сохранено в EntityProperty.value
//                        selectedValue = p.getValue();
//                    } else {
//                        // иначе нет объяснения, почему не получили соответствующего ClassifierItem
//                        throw new IllegalStateException("cant find classifierItem with code:\'" + p.getValue() + "\'");
//                    }
//                } else {
//                    selectedValue = classifierItem.getDisplayValue();
//                }
//            } else {
//                selectedValue = "";
//            }
//        }
//        Log.d(TAG, "[getClassifierViewItemList] selected value: \'" + selectedValue + "\'");
//
//        TreeMap<String, List<String>> ret = new TreeMap<String, List<String>>();
//        ret.put(selectedValue, itemsListForView);
//
//        return ret;
//    }

}
