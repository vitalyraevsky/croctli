
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for EntityProperty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ScalarType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsSystemUserType" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsNullable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsReadonly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsVisible" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsArrayProperty" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="OrderInArray" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DisplayGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Restriction" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClassifierTypeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BinaryValue" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Attachment"/>
 *         &lt;element name="TaskId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessTypeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntityPropertyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClassifierDisplayField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClassifierValueField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisplayGroupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisplayOrder" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MinValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaxValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaxLen" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ValueDisplay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsClassifierFree" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ShortValueDisplay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityProperty", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "scalarType",
    "isSystemUserType",
    "isNullable",
    "isReadonly",
    "isVisible",
    "isArrayProperty",
    "orderInArray",
    "displayGroup",
    "value",
    "restriction",
    "classifierTypeId",
    "binaryValue",
    "taskId",
    "processTypeId",
    "processId",
    "entityPropertyId",
    "classifierDisplayField",
    "classifierValueField",
    "displayGroupName",
    "displayOrder",
   // "attachmentId",
    "binaryValue",
    "minValue",
    "maxValue",
    "maxLen",
    "valueDisplay",
    "isClassifierFree",
    "shortValueDisplay"
})
@DatabaseTable(tableName = "entity_property")
public class EntityProperty {

    @DatabaseField(id = true, canBeNull = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("EntityPropertyId")
    @XmlElement(name = "EntityPropertyId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String entityPropertyId;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("Name")
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String name;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ScalarType")
    @XmlElement(name = "ScalarType", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String scalarType;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsSystemUserType")
    @XmlElement(name = "IsSystemUserType", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isSystemUserType;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsNullable")
    @XmlElement(name = "IsNullable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isNullable;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsReadonly")
    @XmlElement(name = "IsReadonly", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isReadonly;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsVisible")
    @XmlElement(name = "IsVisible", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isVisible;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsArrayProperty")
    @XmlElement(name = "IsArrayProperty", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isArrayProperty;

    @DatabaseField(dataType = DataType.INTEGER)
    @JsonProperty("OrderInArray")
    @XmlElement(name = "OrderInArray", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int orderInArray;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("DisplayGroup")
    @XmlElement(name = "DisplayGroup", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String displayGroup;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("Value")
    @XmlElement(name = "Value", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String value;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("Restriction")
    @XmlElement(name = "Restriction", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String restriction;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ClassifierTypeId")
    @XmlElement(name = "ClassifierTypeId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String classifierTypeId;

    @DatabaseField(foreign = true)
    @JsonProperty("BinaryValue")
    @XmlElement(name = "BinaryValue", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected Attachment binaryValue;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("TaskId")
    @XmlElement(name = "TaskId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String taskId;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ProcessTypeId")
    @XmlElement(name = "ProcessTypeId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String processTypeId;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ProcessId")
    @XmlElement(name = "ProcessId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String processId;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ClassifierDisplayField")
    @XmlElement(name = "ClassifierDisplayField", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String classifierDisplayField;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ClassifierValueField")
    @XmlElement(name = "ClassifierValueField", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String classifierValueField;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("DisplayGroupName")
    @XmlElement(name = "DisplayGroupName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String displayGroupName;

    @DatabaseField(dataType = DataType.INTEGER_OBJ)
    @JsonProperty("DisplayOrder")
    @XmlElement(name = "DisplayOrder", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected Integer displayOrder;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("MinValue")
    @XmlElement(name = "MinValue", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String minValue;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("MaxValue")
    @XmlElement(name = "MaxValue", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String maxValue;

    @DatabaseField(dataType = DataType.INTEGER_OBJ)
    @JsonProperty("MaxLen")
    @XmlElement(name = "MaxLen", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected Integer maxLen;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ValueDisplay")
    @XmlElement(name = "ValueDisplay", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String valueDisplay;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsClassifierFree")
    @XmlElement(name = "IsClassifierFree", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isClassifierFree;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("ShortValueDisplay")
    @XmlElement(name = "ShortValueDisplay", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String shortValueDisplay;

    @DatabaseField(columnName = "server_time", dataType = DataType.STRING)
    protected String serverTime;

    private List<EntityProperty> entityProperties;
    public void addEntityProperty(EntityProperty entityProperty) {
        if (entityProperties == null)
            entityProperties = new ArrayList<EntityProperty>();
        this.entityProperties.add(entityProperty);
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    @DatabaseField(columnName = "was_changed", dataType = DataType.BOOLEAN, defaultValue = "false")
    private boolean wasChanged;

    public boolean isWasChanged() {
        return wasChanged;
    }

    public void setWasChanged(boolean wasChanged) {
        this.wasChanged = wasChanged;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the scalarType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScalarType() {
        return scalarType;
    }

    /**
     * Sets the value of the scalarType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScalarType(String value) {
        this.scalarType = value;
    }

    /**
     * Gets the value of the isSystemUserType property.
     * 
     */
    public boolean isIsSystemUserType() {
        return isSystemUserType;
    }

    /**
     * Sets the value of the isSystemUserType property.
     * 
     */
    public void setIsSystemUserType(boolean value) {
        this.isSystemUserType = value;
    }

    /**
     * Gets the value of the isNullable property.
     * 
     */
    public boolean isIsNullable() {
        return isNullable;
    }

    /**
     * Sets the value of the isNullable property.
     * 
     */
    public void setIsNullable(boolean value) {
        this.isNullable = value;
    }

    /**
     * Gets the value of the isReadonly property.
     * 
     */
    public boolean isIsReadonly() {
        return isReadonly;
    }

    /**
     * Sets the value of the isReadonly property.
     * 
     */
    public void setIsReadonly(boolean value) {
        this.isReadonly = value;
    }

    /**
     * Gets the value of the isVisible property.
     * 
     */
    public boolean isIsVisible() {
        return isVisible;
    }

    /**
     * Sets the value of the isVisible property.
     * 
     */
    public void setIsVisible(boolean value) {
        this.isVisible = value;
    }

    /**
     * Gets the value of the isArrayProperty property.
     * 
     */
    public boolean isIsArrayProperty() {
        return isArrayProperty;
    }

    /**
     * Sets the value of the isArrayProperty property.
     * 
     */
    public void setIsArrayProperty(boolean value) {
        this.isArrayProperty = value;
    }

    /**
     * Gets the value of the orderInArray property.
     * 
     */
    public int getOrderInArray() {
        return orderInArray;
    }

    /**
     * Sets the value of the orderInArray property.
     * 
     */
    public void setOrderInArray(int value) {
        this.orderInArray = value;
    }

    /**
     * Gets the value of the displayGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayGroup() {
        return displayGroup;
    }

    /**
     * Sets the value of the displayGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayGroup(String value) {
        this.displayGroup = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the restriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRestriction() {
        return restriction;
    }

    /**
     * Sets the value of the restriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRestriction(String value) {
        this.restriction = value;
    }

    /**
     * Gets the value of the classifierTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassifierTypeId() {
        return classifierTypeId;
    }

    /**
     * Sets the value of the classifierTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassifierTypeId(String value) {
        this.classifierTypeId = value;
    }

    /**
     * Gets the value of the binaryValue property.
     * 
     * @return
     *     possible object is
     *     {@link Attachment }
     *     
     */
    public Attachment getBinaryValue() {
        return binaryValue;
    }

    /**
     * Sets the value of the binaryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attachment }
     *     
     */
    public void setBinaryValue(Attachment value) {
        this.binaryValue = value;
    }

    /**
     * Gets the value of the taskId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Sets the value of the taskId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * Gets the value of the processTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeId() {
        return processTypeId;
    }

    /**
     * Sets the value of the processTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeId(String value) {
        this.processTypeId = value;
    }

    /**
     * Gets the value of the processId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Gets the value of the entityPropertyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityPropertyId() {
        return entityPropertyId;
    }

    /**
     * Sets the value of the entityPropertyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityPropertyId(String value) {
        this.entityPropertyId = value;
    }

    /**
     * Gets the value of the classifierDisplayField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassifierDisplayField() {
        return classifierDisplayField;
    }

    /**
     * Sets the value of the classifierDisplayField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassifierDisplayField(String value) {
        this.classifierDisplayField = value;
    }

    /**
     * Gets the value of the classifierValueField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassifierValueField() {
        return classifierValueField;
    }

    /**
     * Sets the value of the classifierValueField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassifierValueField(String value) {
        this.classifierValueField = value;
    }

    /**
     * Gets the value of the displayGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayGroupName() {
        return displayGroupName;
    }

    /**
     * Sets the value of the displayGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayGroupName(String value) {
        this.displayGroupName = value;
    }

    /**
     * Gets the value of the displayOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    /**
     * Sets the value of the displayOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayOrder(Integer value) {
        this.displayOrder = value;
    }

    /**
     * Gets the value of the minValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinValue(String value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the maxValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxValue(String value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the maxLen property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxLen() {
        return maxLen;
    }

    /**
     * Sets the value of the maxLen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxLen(Integer value) {
        this.maxLen = value;
    }

    /**
     * Gets the value of the valueDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueDisplay() {
        return valueDisplay;
    }

    /**
     * Sets the value of the valueDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueDisplay(String value) {
        this.valueDisplay = value;
    }

    /**
     * Gets the value of the isClassifierFree property.
     * 
     */
    public boolean isIsClassifierFree() {
        return isClassifierFree;
    }

    /**
     * Sets the value of the isClassifierFree property.
     * 
     */
    public void setIsClassifierFree(boolean value) {
        this.isClassifierFree = value;
    }

    /**
     * Gets the value of the shortValueDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortValueDisplay() {
        return shortValueDisplay;
    }

    /**
     * Sets the value of the shortValueDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortValueDisplay(String value) {
        this.shortValueDisplay = value;
    }

}
