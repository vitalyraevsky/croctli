package ru.baccasoft.croctli.android.specialcondition.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LexemChain {

	private Queue<ILexem> chain;
	private ILexem current;
	
	public LexemChain( List<ILexem> lexemList ) {
		chain = new LinkedList<ILexem>( lexemList );
		current = chain.remove();
	}
	
	public boolean hasNext() {
		return !chain.isEmpty();
	}
	
	public ILexem next() {
		current = chain.remove();
		return current;
	}
	
	public ILexem current() {
		return current;
	}
	
	public ILexem peekNext() {
		return chain.peek();
	}
}
