package ru.baccasoft.croctli.android.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ConflictData;

import java.util.HashMap;
import java.util.Map;

/**
 * Фрагент отвечающий за отображение предупреждающего сообщения о конфликте
 */
public class ConflictFragment extends Fragment {

    private static final String TAG = ConflictFragment.class.getSimpleName();

    private static final String ARG_CONFLICT_DATA = "conflict_data";


    public interface ConflictMessageClosedListener {
        public void conflictClosed();
    }

    private ConflictMessageClosedListener listener;

    // TODO: требуется оптимизация, нужно вытаскивать ресурсы один раз
    private int redConflicts[];
    private String nonCloseables[];
    private Map<String,String> codesToconflicts;

    private ConflictData conflictData;

    /**
     * Получаем новый экземпляр фрагмента
     * @param conflictData
     * @return
     */
    public static ConflictFragment newInstance(ConflictData conflictData) {
        ConflictFragment fragment = new ConflictFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CONFLICT_DATA, conflictData);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        redConflicts = getResources().getIntArray(R.array.red_conflicts);
        nonCloseables = getResources().getStringArray(R.array.conflict_codes_non_closeable);

        // conflic
        String codes[] = getResources().getStringArray(R.array.conflict_codes);
        String names[] = getResources().getStringArray(R.array.conflict_names);

        if(codes.length != names.length) {
            String message = "arrays of conflict codes and names must be the same size";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }

        codesToconflicts = new HashMap<String, String>();

        for( int i = 0; i < codes.length; i++ ){
            codesToconflicts.put( codes[i], names[i] );
        }

        if (getArguments() != null) {
            conflictData = (ConflictData) getArguments().getSerializable(ARG_CONFLICT_DATA);
        }

        if(conflictData == null)
            closeConflictFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conflict, container, false);

        ImageView icon = (ImageView) view.findViewById(R.id.conflict_icon);
        View close = view.findViewById(R.id.conflict_close);
        View layout = view.findViewById(R.id.conflict_layout);
        TextView text = (TextView) view.findViewById(R.id.conflict_text);

        // устанавливаем текст сообщения
        text.setText(getConflictText(conflictData.getConflictMessage()));

        // определяем тип сообщения о конфиликта
        boolean isRed = isRedConflict(conflictData.getConflictSignal());
        boolean isCloseable = isCloseableConflict(conflictData.getConflictMessage());

        if(isRed){
            icon.setImageResource(R.drawable.icon_conflict_red);
            layout.setBackgroundResource(R.color.red_conflict);
        } else {
            icon.setImageResource(R.drawable.icon_conflict_yellow);
            layout.setBackgroundResource(R.color.yellow_conflict);
        }

        if( isCloseable ){
            close.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeConflictFragment();
                }
            });
            close.setVisibility(View.VISIBLE);
        } else {
            close.setVisibility(View.GONE);
        }

        return view;
    }

    private void closeConflictFragment() {
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }

    private boolean isCloseableConflict(String conflictMessage) {
        for(String code : nonCloseables){
            if(conflictMessage.equals(code)){
                return false;
            }
        }
        return true;
    }

    private boolean isRedConflict(int signal) {
        for(int i =0; i< redConflicts.length;i++){
            if(redConflicts[i] == signal) {
                return true;
            }
        }
        return false;
    }

    private String getConflictText(String message){
        for(String code : nonCloseables){
            if(message.equals(code)){
                return codesToconflicts.get(message);
            }
        }
        return message;
    }

    public void setListener(ConflictMessageClosedListener listener){
        this.listener = listener;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(listener != null)
            listener.conflictClosed();
    }
}
