package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ArrayOfTab;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.Tab;
import ru.baccasoft.croctli.android.gen.core.TabSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.TabAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TabSync implements ISync {
    String TAG = "TabSync";

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
//        sd(dateFrom, dateTo);

        List<Tab> conflictTabs = new ArrayList<Tab>();

        //{{{ Разбираемся со входящими обновлениями
        // забираем с сервера обновления для закладок
        TabAPI tabAPI = new TabAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TabSet, Tab>();

        @SuppressWarnings("unchecked")
        List<Tab> tabsFromServer = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, tabAPI);

        // измененные табы
        final List<Tab> localModifiedTabs = TableUtils.getChangedTabs(dateFrom);

        // удобный мап <taskId, task>
        final Map<String, Tab> localModifiedTabsWithId = new HashMap<String, Tab>();

        for(Tab tab: localModifiedTabs)
            localModifiedTabsWithId.put(tab.getId(), tab);

        // составляем список конфликтных закладок - измененные на клиенте и пришедшие измененными с сервера
        for (Tab tab : tabsFromServer) {
            final String tabId = tab.getId();

            if(localModifiedTabsWithId.containsKey(tabId)) {
                Tab conflict = localModifiedTabsWithId.get(tabId);
                conflictTabs.add(conflict);
            }
        }

        // отправляем на сервер КЛИЕНТСКУЮ версию конфликтных задач
        if(conflictTabs.size() > 0) {
            ArrayOfTab arrayOfTabs = new ArrayOfTab();
            arrayOfTabs.setItem(conflictTabs);
            tabAPI.modifyPost(dateFrom, arrayOfTabs);
            // TODO: обработка конфликтов по ФС андроид
        }

        // сохраняем локальные вкладки в бд
        for (Tab tab : tabsFromServer) {
//            Log.d(TAG, "save tab: " + tab.getName() + " serverTime: " + tab.getServerTime());
//            tab.setServerTime(tabsFromServer.getServerTimeString());

            TableUtils.createOrUpdateTab(tab);
        }
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        TabAPI tabAPI = new TabAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> tabIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, tabAPI);

        TableUtils.deleteTabs(tabIds);
    }

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        TabAPI tabAPI = new TabAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TabSet, Tab>();

        @SuppressWarnings("unchecked")
        List<Tab> tabs = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, tabAPI);

        for(Tab t: tabs) {
            TableUtils.createOrUpdateTab(t);
        }
    }
}
