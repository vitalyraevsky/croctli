package ru.baccasoft.croctli.android.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Action;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.rest.Utils;
import ru.baccasoft.croctli.android.view.GenericAttributes;
import ru.baccasoft.croctli.android.view.SpecialAttributesWrapper;
import ru.baccasoft.croctli.android.view.event.AddObjectArrayElement;
import ru.baccasoft.croctli.android.view.event.EditorOnNegativeEvent;
import ru.baccasoft.croctli.android.view.event.EditorOnPositiveEvent;
import ru.baccasoft.croctli.android.view.event.IChangeEvent;
import ru.baccasoft.croctli.android.view.event.RemoveObjectArrayElement;
import ru.baccasoft.croctli.android.view.util.CrocUtils;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.ExpanderView;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.TextScalarWithTitle;

/**
 * Created by developer on 05.11.14.
 */
public class TaskEditorDialogFragment extends DialogFragment {
    @SuppressWarnings("unused")
    private static final String TAG = TaskEditorDialogFragment.class.getSimpleName();

    private Context mContext;
    private Activity mActivity;

    private LinearLayout rootLinearLayout;
    private ScrollView scrollView;
    private LinearLayout buttonsLayout;
    private TextView positiveButton;
    private TextView negativeButton;
    private ImageView editButton;
//    private DialogButtons dialogButtons;

    private String taskId;
    private Task currentTask;

    private Boolean readOnly = true;

    private Handler h;

    public static TaskEditorDialogFragment newInstance(String taskId) {
        TaskEditorDialogFragment f = new TaskEditorDialogFragment();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // диалог без заголовка
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = mActivity.getApplicationContext();

        Bundle args = getArguments();
        if (args == null || !args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY)) {
            String message = "taskId cannot be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        } else {
            taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);
        }

        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawableResource(R.color.text_FFFFFF);

        View view = inflater.inflate(R.layout.task_editor_dialog, container, false);

        buttonsLayout = (LinearLayout) view.findViewById(R.id.buttons);
        positiveButton = (TextView) view.findViewById(R.id.positive);
        negativeButton = (TextView) view.findViewById(R.id.negative);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EditorOnNegativeEvent());
                dismiss();
            }
        });
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readOnly = true;
                changeEditMode();
                EventBus.getDefault().post(new EditorOnPositiveEvent());
            }
        });

        // если задача завершена, то редактирование недоступно

        editButton = (ImageView) view.findViewById(R.id.edit);

        //TODO: пока отключил редактирование. такое вот странное требование в почте от Фетисова
        editButton.setVisibility(View.GONE);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readOnly = false;
                scrollView.removeAllViews();
                preStartLoading();
                startLoading();
            }
        });

        rootLinearLayout = (LinearLayout) view.findViewById(R.id.root);
        scrollView = (ScrollView) view.findViewById(R.id.scroll_view);

        preStartLoading();
        startLoading();

        return view;
    }

    /**
     * Если для задачи выбрано завершающее действие, все ее поля отображаются только для чтения.<br>
     * Подробно: если заполнено поле Task.performedAction и
     * в соответствующем действии не проставлено поле action.isWithoutComplete
     * значит задача завершена. Т.е. показываем всегда только для чтения
     *
     * @param task
     * @return
     */
    private boolean isTaskCompleted(Task task) {
        String actionId = task.getPerformedActionId();
        if(ViewUtils.isEmpty(actionId))
            return false;

        Action action = TableUtils.getActionById(actionId);
        return !action.isWithoutComplete();
    }

    private void changeEditMode() {
        // завершенные задачи показываются только в режиме для чтения
        if (isTaskCompleted(currentTask)) {
            editButton.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.GONE);
            return;
        }

        if (readOnly) {
            buttonsLayout.setVisibility(View.GONE);
            //TODO: пока отключил редактирование
//            editButton.setVisibility(View.VISIBLE);
            editButton.setVisibility(View.GONE);

        } else {
            buttonsLayout.setVisibility(View.VISIBLE);
            editButton.setVisibility(View.GONE);
        }
    }

    private void preStartLoading() {
        TextView loadingText = ViewUtils.makeTitleTextView(mContext, "", null);
        loadingText.setText("Загрузка...");
        scrollView.addView(loadingText);
    }

    private void startLoading() {
        h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                new updateViewTask().execute();
            }
        }, 1000);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        startLoading();
    }

    private class updateViewTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            currentTask = TableUtils.getTaskById(taskId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TAG, "onPostExecute");
            ViewGroup rootView = updateView(currentTask);
            Log.d(TAG, "removing all views");
            scrollView.removeAllViews();
            Log.d(TAG, "adding new views");
            scrollView.addView(rootView);

            changeEditMode();
        }
    }

    private ViewGroup updateView(Task task) {
        Log.d(TAG, "updateView");
        HashMap<String, String> values = new HashMap<String, String>();
        //{{{ Общие аттрибуты
        // получаем из базы данные для отображения
        try {
            // получаем списки кодов атрибутов, на основе которых определяется,
            // какие поля/значения должны отображаться
            // п.2.8.1, табл. 15
            List<String> displayNameInTaskArea = App.getDatabaseHelper().getViewCustomizationDao().getDisplayNameInTaskArea();
            List<String> displayInTaskArea = App.getDatabaseHelper().getViewCustomizationDao().getDisplayInTaskArea();

            // Из переписки с Кроком
            // >Какую именно информацию о задаче отображать, определяется на сервере, и приходит на клиент. В том числе, приходит от сервера отмашка, нужно ли отображать номер и дату.
            //      Действительно можно, и не сложно, сделать, чтобы дата и номер документа отображались всегда, независимо от требований сервера.
            //      Подтвердите, если именно это требуется, сделаем.
            // >Дата и номер должны отображаться всегда. Подтверждаем
            String forceToShowAttributeCode = "TaskCreateDate";
            if(!displayNameInTaskArea.contains(forceToShowAttributeCode))
                displayNameInTaskArea.add(forceToShowAttributeCode);
            if(!displayInTaskArea.contains(forceToShowAttributeCode))
                displayInTaskArea.add(forceToShowAttributeCode);


            // проходимся по списку и наполняем values
            Log.d(TAG, "general attributes:");
            for(Map.Entry<String, Integer> i : Utils.attributeCodeMap.entrySet()) {
                String name = "", value = "";
                String code = i.getKey();

                Log.d(TAG, "check for code:\'" + code + "\'");
                if(displayNameInTaskArea.contains(code))
                    name = Utils.getAttributeLocalNameByCode(i.getKey(), mContext);
                if(displayInTaskArea.contains(code))
//                    value = Utils.getValueByAttributeCode(task, i.getKey());
                    value = CrocUtils.getValueByAttributeCode(i.getKey(), task);
                Log.d(TAG, "(name, value): [" + name + "], [" + value + "]");

                if(ViewUtils.isNotEmpty(name) && ViewUtils.isNotEmpty(value))
                    values.put(name, value);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("cant read viewCustomization from db", ex);
        }


        LinearLayout rootView = ViewUtils.makeVerticalContainer(mContext);
        rootView.setPadding(20, 0, 0, 0);

        // общие атрибуты
        GenericAttributes genericAttributes = new GenericAttributes(mContext, values);
        rootView.addView(genericAttributes);
        //}}}

        //{{{
        // Специальные аттрибуты
        // специальные (EntityProperty) атрибуты
        List<EntityProperty> props;
        try {
            props = App.getDatabaseHelper().getEntityPropertyDAO()
                    .getByTaskId(task.getId());
        } catch (SQLException ex) {
            throw new RuntimeException("cant read from entityProperty db", ex);
        }

        SpecialAttributesWrapper specialAttributes = new SpecialAttributesWrapper(
                props, mActivity, 0, readOnly);
        View specialAttributesLayout = specialAttributes.getLayout();
        rootView.addView(specialAttributesLayout);

        // для выполненных действий добавляется дополнительное поле (Было выбрано действие:)
        String actionId = task.getPerformedActionId();
        if(actionId != null && !actionId.equals("")) {
//            Для выполненных задач, информация по которым не была синхронизирована с сервером, (задачи на  закладке «Выполненные») на форме задачи после всех атрибутов добавляется разделитель и текстовый атрибут «Было выбрано действие:» со значением <Название действия (Текст на кнопке, которая была нажата для завершения задачи)>».
            final String titleActionPerformed = Utils.getStringResourceByName("VisualTask_EP_PerformedAction", mContext);
            String textActionPerformed;
            Action action = TableUtils.getActionById(actionId);
            textActionPerformed = action.getLocalName();

            TextScalarWithTitle actionPerformedNotification = new TextScalarWithTitle(
                    mContext, titleActionPerformed, textActionPerformed, true);
            rootView.addView(actionPerformedNotification);
        }
        //}}}
        return rootView;
    }


    //{{{ Обработка изменений во вьюшках

    // список EP, которые нужно будет удалить при сохранении
    private List<String> epIdsToDeleteOnPositive = new ArrayList<String>();
    // список EP, которые были созданы в базе, но которые надо удалить,
    // если не была нажата кнопка Сохранить
    private List<String> epIdsToDeleteOnNegative = new ArrayList<String>();
    // хранит все изменения в редакторе, о которых получили оповещение через EventBus
    Map<String, IChangeEvent> changeEvents = new LinkedHashMap<String, IChangeEvent>();

    // Составляем список всех изменений в Редакторе реквизитов
    public void onEvent(IChangeEvent event) {
        Log.d(TAG, "got IChangeEvent event");
        changeEvents.put(event.getKey(), event);
    }

    // для массивов объектов
    public void onEvent(RemoveObjectArrayElement event) {
        epIdsToDeleteOnPositive.add(event.getEntityPropertyId());
        ViewGroup viewGroup = event.getViewGroup();
        viewGroup.setVisibility(View.GONE);
    }

    // для массивов объектов
    public void onEvent (AddObjectArrayElement event) {
        ExpanderView expanderView = (ExpanderView) event.getViewGroup();
        int expanderNestLevel = event.getNestLevel();

        EntityProperty epTemplate = TableUtils.getEPObjectArrayAddingTemplate(event.getEntityPropertyId());
        String origEpTemplateId = epTemplate.getId();
        epTemplate.setOrderInArray(0);

        List<String> createdEps = ViewUtils.createEpsBasedOnTemplate(epTemplate, origEpTemplateId, null);   //todo: передавать и обрабатывать дату создания

        for(String id:createdEps) {
            Log.d(TAG, "created temp EP with id:\'" + id + "\'");
        }

        epIdsToDeleteOnNegative.addAll(createdEps);

        List<EntityProperty> epList = new ArrayList<EntityProperty>();
        epList.add(epTemplate);

        SpecialAttributesWrapper wrapper = new SpecialAttributesWrapper(
                epList, mActivity, expanderNestLevel, false);

        View newView = wrapper.getLayout();

        expanderView.addSubView(newView, false);
    }

    // если нажали Сохранить
    public void onEvent(EditorOnPositiveEvent event) {
//        ChangeEventHandler.saveChanges(modifyEvents);
        for(Map.Entry<String, IChangeEvent> entry : changeEvents.entrySet()) {
            IChangeEvent changeEvent = entry.getValue();
            changeEvent.save();
        }

        TableUtils.deleteEntityProperties(epIdsToDeleteOnPositive);
        epIdsToDeleteOnNegative.clear();
    }

    // если не нажали Сохранить
    public void onEvent(EditorOnNegativeEvent event) {
        TableUtils.deleteEntityProperties(epIdsToDeleteOnNegative);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        onExit();
        super.onDismiss(dialog);
    }

    private void onExit() {
        EventBus.getDefault().post(new EditorOnNegativeEvent());
    }
    //}}}
}
