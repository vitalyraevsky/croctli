package ru.baccasoft.croctli.android.fragments.documents;


import android.util.Log;
import android.webkit.WebView;

import java.io.File;
import java.io.IOException;

import ru.baccasoft.croctli.android.FileUtils;

public class TextViewFragment extends WebViewBasedAttachmentViewFragment {

    private final String TAG = TextViewFragment.class.getSimpleName();

    @Override
    public void loadContent(WebView webView) {
        Log.d(TAG,"start load text attachment");

        File attach = new File(getAttachmentPath() );

        try {

            String content = FileUtils.readFile(attach.getAbsolutePath());
            String contentHtml = "<html><table><tr><td><p>" + content + "</p></td></tr></table></html>";

            Log.d(TAG,"try open text file : " + getAttachmentName() );

            webView.loadData(content,"text/html","UTF-8");

        } catch (IOException e) {
            Log.e(TAG,"",e);
        }

    }
}
