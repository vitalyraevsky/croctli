package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.StringScalarTextView;

/**
 * Created by developer on 27.11.14.
 */
public class DateClassifierWithTitle extends LinearLayout {
    private Context mContext;

    /**
     *
     * @param context
     * @param title
     * @param value выбранное значение.
     *              может быть датой в формате YYYY-MM-DD
     *              или особый код справочник #UserValueLabel
     *              или значение из справочника
     *
     * @param items набор пар значений из ClassifierItem вида [code, displayValue]
     * @param isReadOnly
     */
    public DateClassifierWithTitle(Context context, String title, String value, Map<String, String> items, boolean isReadOnly) {
        super(context);

        mContext = context;

        addTitle(title);
        addContentView(isReadOnly, value, items);
    }

    private void addTitle(String title) {
        TextView titleView = ViewUtils.makeTitleTextView(mContext, title, null);
        addView(titleView);
    }

    private void addContentView(boolean isReadOnly, String value, Map<String, String> items) {
        if(isReadOnly) {
            addReadView(value, items);
        } else {
            addWriteView(value, items);
        }
    }

    private void addReadView(String value, Map<String, String> items) {
        if(value.equals(ViewUtils.USER_VALUE_LABEL)) {
            value = items.get(ViewUtils.USER_VALUE_LABEL);
            if(value == null)
                throw new IllegalStateException("cant find classifier_item with \"code\":" + ViewUtils.USER_VALUE_LABEL);
        }

        TextView contentView = new StringScalarTextView(mContext, value);
        contentView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        addView(contentView);
    }

    private void addWriteView(String value, Map<String, String> items) {
        throw new UnsupportedOperationException();
    }
}
