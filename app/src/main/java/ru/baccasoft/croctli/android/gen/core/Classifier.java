
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * <p>Java class for Classifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Classifier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LocalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Items" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfClassifierItem"/>
 *         &lt;element name="DisplayMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Classifier", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "localName",
    "sourceSystemId",
    "items",
    "displayMode"
})
@DatabaseTable(tableName = "classifier")
public class Classifier {

    @DatabaseField(id = true, dataType = DataType.STRING)
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    protected String id;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Name")
    protected String name;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "LocalName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("LocalName")
    protected String localName;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "SourceSystemId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("SourceSystemId")
    protected String sourceSystemId;

//---
    @XmlElement(name = "Items", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @JsonProperty("Items")
    protected ArrayOfClassifierItem items;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    public Collection<ClassifierItem> itemsCollection;
//---

    @DatabaseField(dataType = DataType.INTEGER)
    @XmlElement(name = "DisplayMode", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayMode")
    protected int displayMode;


//---
    protected String serverTimeString;

    public String getServerTimeString() {
        return serverTimeString;
    }

    public void setServerTimeString(String serverTimeString) {
        this.serverTimeString = serverTimeString;
    }

//---

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the localName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Sets the value of the localName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalName(String value) {
        this.localName = value;
    }

    /**
     * Gets the value of the sourceSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemId() {
        return sourceSystemId;
    }

    /**
     * Sets the value of the sourceSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemId(String value) {
        this.sourceSystemId = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfClassifierItem }
     *     
     */
    public ArrayOfClassifierItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfClassifierItem }
     *     
     */
    public void setItems(ArrayOfClassifierItem value) {
        this.items = value;
    }

    /**
     * Gets the value of the displayMode property.
     * 
     */
    public int getDisplayMode() {
        return displayMode;
    }

    /**
     * Sets the value of the displayMode property.
     * 
     */
    public void setDisplayMode(int value) {
        this.displayMode = value;
    }

}
