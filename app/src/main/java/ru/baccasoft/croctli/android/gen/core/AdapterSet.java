
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.baccasoft.croctli.android.gen.CrocSet;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;


/**
 * <p>Java class for AdapterSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdapterSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arrayOf" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfAdapter"/>
 *         &lt;element name="ServerTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdapterSet", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "arrayOf",
    "serverTime"
})
@JsonIgnoreProperties("serverTime")
public class AdapterSet implements CrocSet<Adapter>{

    @JsonProperty("arrayOf")
    @XmlElement(namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected ArrayOfAdapter arrayOf;

//--
    @XmlElement(name = "ServerTime", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serverTime;

    @JsonProperty("ServerTime")
    protected String serverTimeString;

    @Override
    public List<Adapter> getItems() {
        return arrayOf.getItem();
    }

    public String getServerTimeString() {
        return serverTimeString;
    }

    public void setServerTimeString(String serverTimeString) {
        this.serverTimeString = serverTimeString;
    }
//---

    /**
     * Gets the value of the arrayOf property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdapter }
     *     
     */
    public ArrayOfAdapter getArrayOf() {
        return arrayOf;
    }

    /**
     * Sets the value of the arrayOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdapter }
     *     
     */
    public void setArrayOf(ArrayOfAdapter value) {
        this.arrayOf = value;
    }

    /**
     * Gets the value of the serverTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServerTime() {
        return serverTime;
    }

    /**
     * Sets the value of the serverTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServerTime(XMLGregorianCalendar value) {
        this.serverTime = value;
    }

}
