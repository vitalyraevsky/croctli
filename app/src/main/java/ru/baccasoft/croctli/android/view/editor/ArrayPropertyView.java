package ru.baccasoft.croctli.android.view.editor;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DictionaryMultiAutoCompleteTextView;

/**
 * Created by usermane on 04.11.2014.
 *
 * ЗАЧЕМ НУЖЕН? УДАЛИТЬ?
 */
public class ArrayPropertyView extends LinearLayout {

    private String title;
    private List<ItemHolder> items;
    private Context mContext;

    private TextView titleView;
    private DictionaryMultiAutoCompleteTextView autoCompleteView;

    public ArrayPropertyView(Context context, String title, List<ItemHolder> items) {
        super(context);

        this.mContext = context;
        this.title = title;
        this.items = items;

        setOrientation(HORIZONTAL);

        titleView = ViewUtils.makeTitleTextView(context, title, null);
        autoCompleteView = new DictionaryMultiAutoCompleteTextView(context);

        autoCompleteView.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        autoCompleteView.initAdapter(items);
        autoCompleteView.initTokenizer();
        autoCompleteView.initTextWatcher();

        addView(titleView);
        addView(autoCompleteView);
    }

    public void setTitle(String title) {
        Log.d("", "title: " + title);
        titleView.setText(title);
    }

    public static class ItemHolder {
        public ItemHolder(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String id;
        public String name;
    }


}












