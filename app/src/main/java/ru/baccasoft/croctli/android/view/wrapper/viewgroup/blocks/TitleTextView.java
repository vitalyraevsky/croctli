package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Created by usermane on 04.11.2014.
 */
public class TitleTextView extends TextView {

    public TitleTextView(Context context, String text) {
        super(context);
        setText(text + ": ");

        setTextColor(context.getResources().getColor(R.color.text_777777));
//        setTextSize(context.getResources().getDimension(R.dimen.px_16));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
//        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.px_16));
//        setBackgroundColor(context.getResources().getColor(android.R.color.holo_red_light));
    }

    public void changeTextTo(String text) {
        setText(text);
    }
}
