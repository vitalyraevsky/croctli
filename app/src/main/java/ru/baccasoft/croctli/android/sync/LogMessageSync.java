package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ArrayOfLogMessage;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.LogMessageAPI;

public class LogMessageSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = LogMessageSync.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        throw new UnsupportedOperationException();
    }

    /**
     * Отправляет накопившиеся LogMessages.<br>
     * Если успешно сервер принял данные, помечает отправленные LogMessage's как отправленные.<br>
     *
     * @param dateFrom здесь не используется, может быть null
     * @param dateTo обычно текущая дата (по UTC) на момент выполнения синхронизации
     */
    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        sm(dateTo);
    }

    private void sm(RestApiDate dateFrom) {
        LogMessageAPI logMessageAPI = new LogMessageAPI();
        ArrayOfLogMessage logMessages = TableUtils.getNotSynchronizedLogMessages();

        boolean success = logMessageAPI.modifyPost(dateFrom, logMessages);

        // если сервер принял наши данные без ошибок,
        // отмечаем отправленные сообщения в базе как "отправленные"
        if(success) {
            TableUtils.markAsSentLogMessages(logMessages);
        }
    }
}
