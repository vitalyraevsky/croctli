package ru.baccasoft.croctli.android;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;



import ru.baccasoft.croctli.android.fragments.documents.AttachmentViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.AudioViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.DocViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.PDFViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.TextViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.VideoViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.converters.DocOldFormatProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.DocXProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.PPTProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.PPTXProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.RtfFileProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.SpreadsheetProcessor;

/**
 * Активити для тестирования отоборожения документов
 */
public class TestDocViewActivity extends SherlockFragmentActivity {

    private static final String TAG = TestDocViewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_doc_file_activity);

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        final String items[] = new String[] { "file7old.rtf", "sample-1.rtf", "sample-rsurs.rtf", "file8.doc","file81.doc","file9.docx","sample.docx","file12.xls","file13.xlsx","file10.ppt","pptExamp.ppt","file11.pptx","file6.docx", "file7.docx", "file7old.doc"};
        adb.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface d, int n) {
                Log.d(TAG,"choose file number : " + n + " with name : " + items[n]);
                setupDocFragment(items[n]);
            }

        });
        adb.setNegativeButton("Cancel", null);
        adb.setTitle("Which one?");
        adb.show();

    }

    private void setupDocFragment(String name) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        DocViewFragment viewFragment = new DocViewFragment();

        if( name.contains(".doc") || name.contains(".docx") ){
            if( name.contains(".docx") ) {
                viewFragment.setFileProcessor(new DocXProcessor());
            } else {
                viewFragment.setFileProcessor(new DocOldFormatProcessor());
            }
        } else if(  name.contains(".xls") || name.contains(".xlsx") ){
            viewFragment.setFileProcessor( new SpreadsheetProcessor() );
        } else if(  name.contains(".ppt") || name.contains(".pptx") ){
            if( name.contains(".pptx") ) {
                viewFragment.setFileProcessor(new PPTXProcessor());
            } else {
                viewFragment.setFileProcessor(new PPTProcessor());
            }
        } else if(  name.contains(".rtf") ){
            viewFragment.setFileProcessor( new RtfFileProcessor());
        }

        Bundle args = new Bundle();
        args.putString(AttachmentViewFragment.ARG_ATTACH_PATH,getTestAssetFile(name));
        args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, name);
        viewFragment.setArguments(args);

        ft.replace(R.id.doc_content, viewFragment).commit();
    }


    private void setupPDFFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        PDFViewFragment viewFragment = new PDFViewFragment();
        Bundle args = new Bundle();
        args.putString(AttachmentViewFragment.ARG_ATTACH_PATH,getTestAssetFile("file1.pdf"));
        args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, "file1.pdf");
        viewFragment.setArguments(args);

        ft.replace(R.id.doc_content, viewFragment).commit();

    }

    private void setupAudioFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        AttachmentViewFragment viewFragment = new AudioViewFragment();
        Bundle args = new Bundle();
        args.putString(AttachmentViewFragment.ARG_ATTACH_PATH,getTestAssetFile("file4.mp3"));
        args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, "file4.mp3");
        viewFragment.setArguments(args);

        ft.replace(R.id.doc_content, viewFragment).commit();
    }

    private void setupVideoFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        AttachmentViewFragment viewFragment = new VideoViewFragment();
        Bundle args = new Bundle();
        args.putString(AttachmentViewFragment.ARG_ATTACH_PATH,getTestAssetFile("file5.mp4"));
        args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, "file5.mp4");
        viewFragment.setArguments(args);

        ft.replace(R.id.doc_content, viewFragment).commit();
    }

    private void setupTXTFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        TextViewFragment viewFragment = new TextViewFragment();
        Bundle args = new Bundle();
        args.putString(AttachmentViewFragment.ARG_ATTACH_PATH,getTestAssetFile("file3.html"));
        args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, "file3.html");
        viewFragment.setArguments(args);

        ft.replace(R.id.doc_content, viewFragment).commit();
    }

    private String getTestAssetFile(String fileName) {
        File f = new File(getCacheDir()+"/"+fileName);

        if(f.exists())
            f.delete();

        try {

            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(buffer);
            fos.close();

        } catch (Exception e) {
            Log.e( TAG , "fail getTestAssetFile : " + fileName ,e);
            throw new RuntimeException(e);
        }

        Log.d( TAG , "getTestAssetFile : " + f.getAbsolutePath() );

        return f.getPath();
    }
}
