package ru.baccasoft.croctli.android.fragments.documents;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import java.io.IOException;

import ru.baccasoft.croctli.android.fragments.documents.converters.ConvertationProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.HtmlConverterTask;

public class DocViewFragment extends WebViewBasedAttachmentViewFragment {

    private static final String TAG = DocViewFragment.class.getSimpleName();

    private static final String IMAGE_DIR_NAME = "images";

    public ConvertationProcessor processor;

    @Override
    public void loadContent(final WebView webView) {
        Log.d(TAG, "start load doc attachment");

        if(processor == null)
            throw new IllegalStateException("proccessor was not set");

        try {

            final String baseURL = getActivity().getDir(IMAGE_DIR_NAME, Context.MODE_WORLD_READABLE).toURL().toString();

            processor.init( getAttachmentPath() );

            new HtmlConverterTask( getActivity() , processor ){
                @Override
                protected void onPostExecute(String html) {

                    if( html == null || html.equals("") ){
                        return;
                    }

                    Log.d(TAG, "try open converted html file : " + getAttachmentName());

                    webView.loadDataWithBaseURL(baseURL, html, "text/html", null, null);
                }
            }.execute( baseURL , IMAGE_DIR_NAME );

        } catch (IOException e) {
            Log.e(TAG, "fail to display doc file", e);
        } catch (Exception e) {
            Log.e(TAG, "fail to display doc file", e);
        }
    }


    public void setFileProcessor(ConvertationProcessor processor){
        this.processor = processor;
    }

}
