package ru.baccasoft.croctli.android.specialcondition.parser;

import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtomCreator;
import ru.baccasoft.croctli.android.specialcondition.date.DatetimeNowAtom;
import ru.baccasoft.croctli.android.specialcondition.date.DeadlineValueAtom;
import ru.baccasoft.croctli.android.specialcondition.predicate.*;

import java.util.ArrayList;
import java.util.List;

public class LexemRecognizers {
	public static Iterable<ILexemRecognizer> recognizers() {
		return LIST;
	}
	
	private static final List<ILexemRecognizer> LIST = new ArrayList<ILexemRecognizer>() {
		
		private void add( String sample, LexemType type ) {
			add( new SimpleLexemRecognizer(sample, type) );
		}
		
		private void add( IBooleanAtomCreator booleanAtomCreator ) {
			add( new SimpleLexemRecognizer( booleanAtomCreator.getAtomName(), LexemType.BooleanAtom) );
		}
		
		{
			// {{ пары "чтототам + равно", "просто чтототам", в обратном порядке все сломается!
			add( "!=", LexemType.EqualityOperator );
			add( "!", LexemType.BooleanNot );
			
			add( "<=", LexemType.ComparisonOperator );
			add( "<", LexemType.ComparisonOperator );

			add( ">=", LexemType.ComparisonOperator );
			add( ">", LexemType.ComparisonOperator );
			// }} пары "чтототам + равно", "просто чтототам", в обратном порядке все сломается!
			
			add( "==", LexemType.EqualityOperator );
			add( "&&", LexemType.BooleanAnd );
			add( "||", LexemType.BooleanOr );


            add( new BooleanTrue() );
            add( new BooleanFalse() );
            add( new IsCompletedPredicate() );
            add( new IsSentPredicate() );
            add( new HasConflictsPredicate() );
            add( new IsFavoritePredicate() );
            add( new IsDeletedPredicate() );
            add( new IsCurUserLastModePredicate() );
            add( new DeadLineHasValuePredicate() );

			add( DeadlineValueAtom.NAME, LexemType.DateAtom );
			add( DatetimeNowAtom.NAME, LexemType.DateAtom );

			add( new AddDaysToNowLexemRecognizer() );
		}
	};
}
