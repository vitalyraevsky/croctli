
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * <p>Java class for Filter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Filter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SpecialConditionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Conditions" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfFilterCondition"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Filter", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "specialConditionId",
    "conditions"
})
@DatabaseTable(tableName = "filter")
public class Filter {
    @SuppressWarnings("unused")
    private static final String TAG = Filter.class.getSimpleName();

    public static final String TAB_FK_ID_FIELD = "tab_fk";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = TAB_FK_ID_FIELD)
    private Tab tab;

    public Tab getTab() { return tab; }
    public void setTab(Tab tab) { this.tab = tab; }

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @JsonProperty("SpecialConditionId")
    @XmlElement(name = "SpecialConditionId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "special_condition_id", dataType = DataType.STRING)
    protected String specialConditionId;

    @JsonProperty("Conditions")
    @XmlElement(name = "Conditions", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected ArrayOfFilterCondition conditions;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    private Collection<FilterCondition> filterConditions;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the specialConditionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialConditionId() {
        return specialConditionId;
    }

    /**
     * Sets the value of the specialConditionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialConditionId(String value) {
        this.specialConditionId = value;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFilterCondition }
     *     
     */
    public ArrayOfFilterCondition getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFilterCondition }
     *     
     */
    public void setConditions(ArrayOfFilterCondition value) {
        this.conditions = value;
    }

    public Collection<FilterCondition> getFilterConditions() {
        return filterConditions;
    }

    public void setFilterConditions(Collection<FilterCondition> filterConditions) {
        this.filterConditions = filterConditions;
    }
}
