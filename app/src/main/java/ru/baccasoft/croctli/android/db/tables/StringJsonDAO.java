package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.StringJson;

import java.sql.SQLException;

/**
 * Created by developer on 15.10.14.
 */
public class StringJsonDAO extends BaseDaoImpl<StringJson, Integer> {
    public StringJsonDAO(ConnectionSource connectionSource, Class<StringJson> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
