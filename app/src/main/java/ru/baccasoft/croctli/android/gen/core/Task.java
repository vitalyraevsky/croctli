
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * <p>Java class for Task complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Task">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DeadlineDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsTheOnlyActor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TaskViewUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PerformedActionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChangedById" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedById" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaskStateId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdapterId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StartProcessExceptId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AvailableActions" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfAction"/>
 *         &lt;element name="Executants" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfTaskActor"/>
 *         &lt;element name="DisplayInitialForm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SourceSystemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChangedByName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedByName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaskStateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessCreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ProcessSystemuserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WasRead" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFavourite" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Conflict" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfConflictData"/>
 *         &lt;element name="Notice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChangeStateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "description",
    "createDate",
    "deadlineDate",
    "isTheOnlyActor",
    "changeDate",
    "taskViewUrl",
    "performedActionId",
    "processId",
    "changedById",
    "createdById",
    "taskStateId",
    "adapterId",
    "startProcessExceptId",
    "availableActions",
    "executants",
    "displayInitialForm",
    "sourceSystemName",
    "processTypeName",
    "processName",
    "changedByName",
    "createdByName",
    "taskStateName",
    "processCreateDate",
    "processSystemuserId",
    "processDescription",
    "processTypeDescription",
    "wasRead",
    "isFavourite",
    "conflict",
    "notice",
    "changeStateDate",
    "isDeleted"
})

// TODO: пока игнорируем все даты из-за сложностей парсинга  XMLGregorianCalendar
@JsonIgnoreProperties({"createDate", "deadlineDate", "changeDate", "processCreateDate" , "changeStateDate",
        "availableActionsCollection", "executantsCollection", "createDateTime", "deadlineDateTime", "changeDateTime",
        "conflictDataCollection"
})
//@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable(tableName = "tasks")
public class Task implements Serializable {

    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @JsonProperty("Name")
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;

    @JsonProperty("Description")
    @XmlElement(name = "Description", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "description", dataType = DataType.STRING)
    protected String description;

//---
    @XmlElement(name = "CreateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;

    @XmlElement(name = "DeadlineDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deadlineDate;

    @JsonProperty("CreateDate")
    @DatabaseField(columnName = "create_date", dataType = DataType.STRING)
    protected String createDateString;

    @JsonProperty("DeadlineDate")
    @DatabaseField(columnName = "dead_line_date", dataType = DataType.STRING)
    protected String deadlineDateString;

    @DatabaseField(columnName = "create_date_time", dataType = DataType.DATE_TIME)
    protected DateTime createDateTime;

    @DatabaseField(columnName = "dead_line_date_time", dataType = DataType.DATE_TIME)
    protected DateTime deadlineDateTime;

    public DateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(DateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public DateTime getDeadlineDateTime() {
        return deadlineDateTime;
    }

    public void setDeadlineDateTime(DateTime deadlineDateTime) {
        this.deadlineDateTime = deadlineDateTime;
    }

    //---

    @JsonProperty("IsTheOnlyActor")
    @XmlElement(name = "IsTheOnlyActor", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_the_only_actor", dataType = DataType.BOOLEAN)
    protected boolean isTheOnlyActor;

//---
    @XmlElement(name = "ChangeDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar changeDate;

    @JsonProperty("ChangeDate")
    @DatabaseField(columnName = "change_date", dataType = DataType.STRING)
    protected String changeDateString;

    @DatabaseField(columnName = "change_date_time", dataType = DataType.DATE_TIME)
    protected DateTime changeDateTime;

    public DateTime getChangeDateTime() {
        return changeDateTime;
    }

    public void setChangeDateTime(DateTime changeDateTime) {
        this.changeDateTime = changeDateTime;
    }

    //---

    @JsonProperty("TaskViewUrl")
    @XmlElement(name = "TaskViewUrl", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "task_view_url", dataType = DataType.STRING)
    protected String taskViewUrl;

    @JsonProperty("PerformedActionId")
    @XmlElement(name = "PerformedActionId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "perform_action_id", dataType = DataType.STRING)
    protected String performedActionId;

    @JsonProperty("ProcessId")
    @XmlElement(name = "ProcessId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_id", dataType = DataType.STRING)
    protected String processId;

    @JsonProperty("ChangedById")
    @XmlElement(name = "ChangedById", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "change_by_id", dataType = DataType.STRING)
    protected String changedById;

    @JsonProperty("CreatedById")
    @XmlElement(name = "CreatedById", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "create_by_id", dataType = DataType.STRING)
    protected String createdById;

    @JsonProperty("TaskStateId")
    @XmlElement(name = "TaskStateId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "task_state_id", dataType = DataType.STRING)
    protected String taskStateId;

    @JsonProperty("AdapterId")
    @XmlElement(name = "AdapterId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "adapter_id", dataType = DataType.STRING)
    protected String adapterId;

    @JsonProperty("StartProcessExceptId")
    @XmlElement(name = "StartProcessExceptId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "start_process_except_id", dataType = DataType.STRING)
    protected String startProcessExceptId;

//---
    @JsonProperty("AvailableActions")
    @XmlElement(name = "AvailableActions", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfAction availableActions;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    protected Collection<Action> availableActionsCollection;

    @JsonProperty("Executants")
    @XmlElement(name = "Executants", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfTaskActor executants;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    public Collection<TaskActor> executantsCollection;

//---
    @JsonProperty("DisplayInitialForm")
    @XmlElement(name = "DisplayInitialForm", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "display_initial_form_id", dataType = DataType.INTEGER)
    protected int displayInitialForm;

    @JsonProperty("SourceSystemName")
    @XmlElement(name = "SourceSystemName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "source_system_name", dataType = DataType.STRING)
    protected String sourceSystemName;

    @JsonProperty("ProcessTypeName")
    @XmlElement(name = "ProcessTypeName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_name", dataType = DataType.STRING)
    protected String processTypeName;

    @JsonProperty("ProcessName")
    @XmlElement(name = "ProcessName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_name", dataType = DataType.STRING)
    protected String processName;

    @JsonProperty("ChangedByName")
    @XmlElement(name = "ChangedByName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "change_by_name", dataType = DataType.STRING)
    protected String changedByName;

    @JsonProperty("CreatedByName")
    @XmlElement(name = "CreatedByName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "created_by_name", dataType = DataType.STRING)
    protected String createdByName;

    @JsonProperty("TaskStateName")
    @XmlElement(name = "TaskStateName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "task_state_name", dataType = DataType.STRING)
    protected String taskStateName;

//---
    @XmlElement(name = "ProcessCreateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar processCreateDate;

    @JsonProperty("ProcessCreateDate")
    @DatabaseField(columnName = "process_create_date", dataType = DataType.STRING)
    protected String processCreateDateString;
//---


    @JsonProperty("ProcessSystemuserId")
    @XmlElement(name = "ProcessSystemuserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_system_user_id", dataType = DataType.STRING)
    protected String processSystemuserId;

    @JsonProperty("ProcessDescription")
    @XmlElement(name = "ProcessDescription", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_description", dataType = DataType.STRING)
    protected String processDescription;

    @JsonProperty("ProcessTypeDescription")
    @XmlElement(name = "ProcessTypeDescription", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_description", dataType = DataType.STRING)
    protected String processTypeDescription;

    @JsonProperty("WasRead")
    @XmlElement(name = "WasRead", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "was_read", dataType = DataType.BOOLEAN)
    protected boolean wasRead;

    @JsonProperty("IsFavourite")
    @XmlElement(name = "IsFavourite", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_favorite", dataType = DataType.BOOLEAN)
    protected boolean isFavourite;

    @JsonProperty("Conflict")
    @XmlElement(name = "Conflict", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfConflictData conflict;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    protected Collection<ConflictData> conflictDataCollection;

    @JsonProperty("Notice")
    @XmlElement(name = "Notice", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "notice", dataType = DataType.STRING)
    protected String notice;

//--
    @XmlElement(name = "ChangeStateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar changeStateDate;

    @JsonProperty("ChangeStateDate")
    @DatabaseField(columnName = "change_state_date", dataType = DataType.STRING)
    protected String changeStateDateString;
//--

    @JsonProperty("IsDeleted")
    @XmlElement(name = "IsDeleted", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_deleted", dataType = DataType.BOOLEAN)
    protected boolean isDeleted;


    @JsonProperty("EntityProperties")
    @XmlElement(name = "EntityProperties", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")//, required = true, nillable = true)
    protected ArrayOfEntityProperty entityProperties;

    @JsonIgnore
    public List<EntityProperty> getEntityProperties() {
        return  entityProperties.getItem();
    }

    @JsonProperty("EntityProperties")
    public ArrayOfEntityProperty getArrayOfEntityProperty() {
        if( entityProperties == null ) {
            entityProperties = new ArrayOfEntityProperty();
        }
        return entityProperties;
    }
    @JsonProperty("EntityProperties")
    public void setArrayOfEntityProperty(ArrayOfEntityProperty item) {
        entityProperties = item;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the deadlineDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeadlineDate() {
        return deadlineDate;
    }

    /**
     * Sets the value of the deadlineDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeadlineDate(XMLGregorianCalendar value) {
        this.deadlineDate = value;
    }

    /**
     * Gets the value of the isTheOnlyActor property.
     * 
     */
    public boolean isIsTheOnlyActor() {
        return isTheOnlyActor;
    }

    /**
     * Sets the value of the isTheOnlyActor property.
     * 
     */
    public void setIsTheOnlyActor(boolean value) {
        this.isTheOnlyActor = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeDate(XMLGregorianCalendar value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the taskViewUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskViewUrl() {
        return taskViewUrl;
    }

    /**
     * Sets the value of the taskViewUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskViewUrl(String value) {
        this.taskViewUrl = value;
    }

    /**
     * Gets the value of the performedActionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerformedActionId() {
        return performedActionId;
    }

    /**
     * Sets the value of the performedActionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerformedActionId(String value) {
        this.performedActionId = value;
    }

    /**
     * Gets the value of the processId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Gets the value of the changedById property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangedById() {
        return changedById;
    }

    /**
     * Sets the value of the changedById property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangedById(String value) {
        this.changedById = value;
    }

    /**
     * Gets the value of the createdById property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedById() {
        return createdById;
    }

    /**
     * Sets the value of the createdById property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedById(String value) {
        this.createdById = value;
    }

    /**
     * Gets the value of the taskStateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskStateId() {
        return taskStateId;
    }

    /**
     * Sets the value of the taskStateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskStateId(String value) {
        this.taskStateId = value;
    }

    /**
     * Gets the value of the adapterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdapterId() {
        return adapterId;
    }

    /**
     * Sets the value of the adapterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdapterId(String value) {
        this.adapterId = value;
    }

    /**
     * Gets the value of the startProcessExceptId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartProcessExceptId() {
        return startProcessExceptId;
    }

    /**
     * Sets the value of the startProcessExceptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartProcessExceptId(String value) {
        this.startProcessExceptId = value;
    }

    /**
     * Gets the value of the availableActions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAction }
     *     
     */
    public ArrayOfAction getAvailableActions() {
        return availableActions;
    }

    /**
     * Sets the value of the availableActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAction }
     *     
     */
    public void setAvailableActions(ArrayOfAction value) {
        this.availableActions = value;
    }

    /**
     * Gets the value of the executants property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTaskActor }
     *     
     */
    public ArrayOfTaskActor getExecutants() {
        return executants;
    }

    /**
     * Sets the value of the executants property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTaskActor }
     *     
     */
    public void setExecutants(ArrayOfTaskActor value) {
        this.executants = value;
    }

    /**
     * Gets the value of the displayInitialForm property.
     * 
     */
    public int getDisplayInitialForm() {
        return displayInitialForm;
    }

    /**
     * Sets the value of the displayInitialForm property.
     * 
     */
    public void setDisplayInitialForm(int value) {
        this.displayInitialForm = value;
    }

    /**
     * Gets the value of the sourceSystemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemName() {
        return sourceSystemName;
    }

    /**
     * Sets the value of the sourceSystemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemName(String value) {
        this.sourceSystemName = value;
    }

    /**
     * Gets the value of the processTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeName() {
        return processTypeName;
    }

    /**
     * Sets the value of the processTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeName(String value) {
        this.processTypeName = value;
    }

    /**
     * Gets the value of the processName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessName() {
        return processName;
    }

    /**
     * Sets the value of the processName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessName(String value) {
        this.processName = value;
    }

    /**
     * Gets the value of the changedByName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangedByName() {
        return changedByName;
    }

    /**
     * Sets the value of the changedByName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangedByName(String value) {
        this.changedByName = value;
    }

    /**
     * Gets the value of the createdByName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByName() {
        return createdByName;
    }

    /**
     * Sets the value of the createdByName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByName(String value) {
        this.createdByName = value;
    }

    /**
     * Gets the value of the taskStateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskStateName() {
        return taskStateName;
    }

    /**
     * Sets the value of the taskStateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskStateName(String value) {
        this.taskStateName = value;
    }

    /**
     * Gets the value of the processCreateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessCreateDate() {
        return processCreateDate;
    }

    /**
     * Sets the value of the processCreateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessCreateDate(XMLGregorianCalendar value) {
        this.processCreateDate = value;
    }

    /**
     * Gets the value of the processSystemuserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessSystemuserId() {
        return processSystemuserId;
    }

    /**
     * Sets the value of the processSystemuserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessSystemuserId(String value) {
        this.processSystemuserId = value;
    }

    /**
     * Gets the value of the processDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessDescription() {
        return processDescription;
    }

    /**
     * Sets the value of the processDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessDescription(String value) {
        this.processDescription = value;
    }

    /**
     * Gets the value of the processTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeDescription() {
        return processTypeDescription;
    }

    /**
     * Sets the value of the processTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeDescription(String value) {
        this.processTypeDescription = value;
    }

    /**
     * Gets the value of the wasRead property.
     * 
     */
    public boolean isWasRead() {
        return wasRead;
    }

    /**
     * Sets the value of the wasRead property.
     * 
     */
    public void setWasRead(boolean value) {
        this.wasRead = value;
    }

    /**
     * Gets the value of the isFavourite property.
     * 
     */
    public boolean isIsFavourite() {
        return isFavourite;
    }

    /**
     * Sets the value of the isFavourite property.
     * 
     */
    public void setIsFavourite(boolean value) {
        this.isFavourite = value;
    }

    /**
     * Gets the value of the conflict property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfConflictData }
     *     
     */
    public ArrayOfConflictData getConflict() {
        return conflict;
    }

    /**
     * Sets the value of the conflict property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfConflictData }
     *     
     */
    public void setConflict(ArrayOfConflictData value) {
        this.conflict = value;
    }

    /**
     * Gets the value of the notice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotice() {
        return notice;
    }

    /**
     * Sets the value of the notice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotice(String value) {
        this.notice = value;
    }

    /**
     * Gets the value of the changeStateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeStateDate() {
        return changeStateDate;
    }

    /**
     * Sets the value of the changeStateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeStateDate(XMLGregorianCalendar value) {
        this.changeStateDate = value;
    }

    /**
     * Gets the value of the isDeleted property.
     * 
     */
    public boolean isIsDeleted() {
        return isDeleted;
    }

    /**
     * Sets the value of the isDeleted property.
     * 
     */
    public void setIsDeleted(boolean value) {
        this.isDeleted = value;
    }

    public String getChangeDateString() {
        return changeDateString;
    }

    public void setChangeDateString(String changeDateString) {
        this.changeDateString = changeDateString;
    }

    public String getCreateDateString() {
        return createDateString;
    }

    public void setCreateDateString(String createDateString) {
        this.createDateString = createDateString;
    }

    public String getDeadlineDateString() {
        return deadlineDateString;
    }

    public void setDeadlineDateString(String deadlineDateString) {
        this.deadlineDateString = deadlineDateString;
    }

    public String getProcessCreateDateString() {
        return processCreateDateString;
    }

    public void setProcessCreateDateString(String processCreateDateString) {
        this.processCreateDateString = processCreateDateString;
    }

    public String getChangeStateDateString() {
        return changeStateDateString;
    }

    public void setChangeStateDateString(String changeStateDateString) {
        this.changeStateDateString = changeStateDateString;
    }

    public Collection<ConflictData> getConflictDataCollection() {
        return conflictDataCollection;
    }

    public void setConflictDataCollection(Collection<ConflictData> conflictDataCollection) {
        this.conflictDataCollection = conflictDataCollection;
    }

    public Collection<Action> getAvailableActionsCollection() {
        return availableActionsCollection;
    }

    public void setAvailableActionsCollection(Collection<Action> availableActionsCollection) {
        this.availableActionsCollection = availableActionsCollection;
    }
}
