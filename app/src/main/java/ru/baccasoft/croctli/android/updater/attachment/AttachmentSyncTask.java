package ru.baccasoft.croctli.android.updater.attachment;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.SettingsActivity;
import ru.baccasoft.croctli.android.gen.core.Attachment;
import ru.baccasoft.croctli.android.rest.RestClient;

// На вход приходит список id вложений, которые нужно синхронизировать
// При публикации прогресса отдается

public class AttachmentSyncTask extends AsyncTask<String, Boolean , Void> {

    private static final String TAG = AttachmentSyncTask.class.getSimpleName();
    private List<String> attachmentIds;
    private int maxFileSize;


    @Override
    protected void onPreExecute() {
        maxFileSize = SettingsActivity.getMaxDownloadSize();
    }

    @Override
    protected Void doInBackground(String... params) {

        //TODO: нужен рефакторинг, загрузку файлов нужно положить в отдельную утииту,

        List<Attachment> attachments = null;
        try {
            attachments = App.getDatabaseHelper().getAttachmentDAO().getNotDownloaded();
            Log.d(TAG,"attachments size(): " + attachments.size());
        } catch (SQLException e) {
            Log.e(TAG,"attachments query error : " + e.getMessage());
        }

        if(attachments == null)
            return null;

        String userLogin = App.prefs.getUserLogin();
        File files = App.getContext().getDir("files",0);
        File userDir = new File(files.getAbsolutePath() + File.separator + userLogin);
        userDir.mkdirs();

        for ( Attachment at : attachments ) {

            Log.d(TAG,"attachment mime-type: " + at.getMimeType());
            Log.d(TAG,"attachment id: " + at.getId());
            Log.d(TAG,"attachment name: " + at.getName());
            Log.d(TAG,"attachment local location: " + at.getLocalLocation());
            Log.d(TAG,"attachment track key: " + at.getTrackKey());
            Log.d(TAG,"attachment check sum: " + at.getCheckSum());
            Log.d(TAG,"attachment size: " + at.getSize());

            if(maxFileSize > 0 && at.getSize() > maxFileSize) {
                Log.d(TAG, "NOT downloading attachments since its size > " + maxFileSize);
                continue;
            }

            if(at.getLocalLocation() != null) {
                if (at.getLocalLocation().startsWith(userDir.getAbsolutePath())) {
                    Log.d(TAG, "pass this attachment to download");
                    continue;
                }
            }

            try {

                Log.d(TAG,"start download attachment");

                File attachFile = new File(userDir.getAbsolutePath(),at.getName());

                HttpResponse response = RestClient.getAttachmentByID(at.getId());

                InputStream is = response.getEntity().getContent();
                OutputStream bos = new BufferedOutputStream(new FileOutputStream(attachFile));

                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                int len = 0;
                while ((len = is.read(buffer)) != -1) {
                    bos.write(buffer, 0, len);
                }

                if( bos != null )
                    bos.close();

                try {
                    at.setLocalLocation(attachFile.getAbsolutePath());
                    App.getDatabaseHelper().getAttachmentDAO().update(at);
                } catch (SQLException e) {
                    Log.e(TAG,"attachments query error : " + e.getMessage());
                }


            } catch (IOException e) {
                Log.e(TAG,"not able to download attachemnt");
            }


        }

        return null;
    }

    private void handleDownloadError(String id) {

        // По ФС:
        //•	При загрузке которых возникла ошибка два раза подряд в текущем сеансе работы с приложением
        // (вложения, при загрузки которых возникла ошибка, были поставлены в конец очереди загрузки,
        // при повторном возникновении ошибки, загрузка приостанавливается до следующего запуска приложения).
        // Делаю:
        // То же самое, но не в текущем сеансе работы, а в пределах одного запуска этого таска

        // TODO: переставляю id в конец очереди, если первая ошибка
        // удаляю из очереди, если вторая

    }
}
