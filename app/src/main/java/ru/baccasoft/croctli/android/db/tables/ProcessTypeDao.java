package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ProcessType;

import java.sql.SQLException;

public class ProcessTypeDao extends BaseDaoImpl<ProcessType, String> {
    public ProcessTypeDao(ConnectionSource connectionSource, Class<ProcessType> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
