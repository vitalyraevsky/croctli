package ru.baccasoft.croctli.android.fragments.documents.converters.utils;

import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Formatter;

import and.awt.Dimension;
import and.org.apache.poi.xslf.usermodel.XMLSlideShow;
import and.org.apache.poi.xslf.usermodel.XSLFSlide;


public class PptxToHtml {

    private static final String TAG = PptxToHtml.class.getSimpleName();

    private SlideToImageConversionHandler imageConversionHandler;
    private XMLSlideShow slideShow;
    private Writer output;
    private Formatter out;

    private PptxToHtml(XMLSlideShow slideShow, Writer output) {
        this.slideShow = slideShow;
        this.output = output;
    }

    public static PptxToHtml create(String path, Writer out) throws IOException {
        return create(new FileInputStream(path), out);
    }

    private static PptxToHtml create(InputStream in, Writer out) {
        try {
            XMLSlideShow slideShow = new XMLSlideShow(in);
            return create(slideShow, out);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot create workbook from stream", e);
        }
    }

    private static PptxToHtml create(XMLSlideShow slideShow, Writer out) {
        return new PptxToHtml(slideShow, out);
    }

    private void ensureOut() {
        if (out == null)
            out = new Formatter(output);
    }

    public void printSlides() throws IOException {

        ensureOut();

        out.format("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>%n");
        out.format("<html>%n");
        out.format("<head>%n");
        out.format("</head>%n");
        out.format("<body>%n");

        print();

        out.format("</body>%n");
        out.format("</html>%n");

        Log.d(TAG, "parsed ppt : " + output.toString());
    }

    private void print() throws IOException {

        XSLFSlide[] slides = slideShow.getSlides();
        for (int i = 0; i < slides.length; i++) {
            XSLFSlide slide = slides[i];
            printSlideAsImage( slide,"slide" + (i+1), slideShow.getPageSize() );
        }

    }

    private void printSlideAsImage(XSLFSlide slide,String suggestedName, Dimension pgsize) throws IOException {
        String imagePath = imageConversionHandler.convertSlideToImage(slide,suggestedName,pgsize);
        out.format("<img name='" + suggestedName + ".png' src='" + imagePath + "'/>%n");
        out.format("</br>%n");
        out.format("</br>%n");
    }

    public void setImageConversionHandler(SlideToImageConversionHandler imageConversionHandler) {
        this.imageConversionHandler = imageConversionHandler;
    }

}
