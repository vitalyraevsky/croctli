package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Для скаляров типа "text" в режиме просмотра (НЕ редактирования)
 * if(scalarType.equals("text"))
 *
 */
public class TextScalarTextView extends TextView {
    public TextScalarTextView(Context context, String text) {
        super(context);

        setText(text);
        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
    }
}
