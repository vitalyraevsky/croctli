package ru.baccasoft.croctli.android.adapters;

/**
 * Created by 123 on 27.04.2015.
 */
public interface TaskCodes {
    String TaskName = "TaskName";

    String TaskCreateDate = "TaskCreateDate";

    String TaskCreator = "TaskCreator";

    String TaskDescription = "TaskDescription";

    String ProcessTypeName = "ProcessTypeName";

    String ProcessTypeDescription = "ProcessTypeDescription";

    String ProcessDescription = "ProcessDescription";

    String ProcessInitiator = "ProcessInitiator";

    String TaskStatus = "TaskStatus";

    String TaskDeadline = "TaskDeadline";

    String IsTaskSingleExecutor = "IsTaskSingleExecutor";

    String TaskChangeDate = "TaskChangeDate";

    String Sourcesystem = "SourceSystem";


}
