package ru.baccasoft.croctli.android.specialcondition.parser;

import java.util.LinkedList;
import java.util.List;

public class Lexer {
	public static LexemChain toLexemChain( LexerSource source ) throws ParseException {
		List<ILexem> result = new LinkedList<ILexem>();
		
		skipWhitespace(source);
		if( source.getUnparsedTail().isEmpty() ) {
			throw new ParseException(source, "empty expression");
		}
		
		while( !source.getUnparsedTail().isEmpty() ) {
			ILexemRecognizer theRecognizer = null;
			for( ILexemRecognizer recognizer: LexemRecognizers.recognizers() ) {
				if( recognizer.isApplicable(source) ) {
					theRecognizer = recognizer;
					break;
				}
			}
			if( theRecognizer == null ) {
				throw new ParseException(source, "unrecognizable");
			}
			
			result.add(theRecognizer.parse(source));
			skipWhitespace(source);
		}
		
		return new LexemChain(result);
		
	}
	
	private static void skipWhitespace( LexerSource source ) {
		for( int pos = 0; pos < source.getUnparsedTail().length(); ++pos ) {
			if( !Character.isWhitespace(source.getUnparsedTail().charAt(pos)) ) {
				if( pos > 0 ) {
					source.moveAhead(pos);
				}
				return;
			}
		}
	}
}
