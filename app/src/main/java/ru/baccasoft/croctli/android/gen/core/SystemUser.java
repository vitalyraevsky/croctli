
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigInteger;
import java.util.Collection;


/**
 * <p>Java class for SystemUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SystemUser">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LocalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Users" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfUserInSystem"/>
 *         &lt;element name="UseDelegation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ClientState" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Department" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemUser", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "localName",
    "email",
    "users",
    "useDelegation",
    "clientState",
    "department",
    "shortName"
})
@DatabaseTable(tableName = "system_user")
public class SystemUser {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Name")
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;

    @XmlElement(name = "LocalName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("LocalName")
    @DatabaseField(columnName = "local_name", dataType = DataType.STRING)
    protected String localName;

    @XmlElement(name = "Email", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Email")
    @DatabaseField(columnName = "email", dataType = DataType.STRING)
    protected String email;

    @XmlElement(name = "Users", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @JsonProperty("Users")
    protected ArrayOfUserInSystem users;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    public Collection<UserInSystem> userInSystemCollection;

    @XmlElement(name = "UseDelegation", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("UseDelegation")
    @DatabaseField(columnName = "use_delegation", dataType = DataType.BOOLEAN)
    protected boolean useDelegation;

    @XmlElement(name = "ClientState", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("ClientState")
    @DatabaseField(columnName = "client_state", dataType = DataType.BIG_INTEGER)
    protected BigInteger clientState;

    @XmlElement(name = "Department", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Department")
    @DatabaseField(columnName = "department", dataType = DataType.STRING)
    protected String department;

    @XmlElement(name = "ShortName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("ShortName")
    @DatabaseField(columnName = "short_name", dataType = DataType.STRING)
    protected String shortName;

    @DatabaseField(columnName = "server_time", dataType = DataType.STRING)
    protected String serverTime;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the localName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Sets the value of the localName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalName(String value) {
        this.localName = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the users property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUserInSystem }
     *     
     */
    public ArrayOfUserInSystem getUsers() {
        return users;
    }

    /**
     * Sets the value of the users property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUserInSystem }
     *     
     */
    public void setUsers(ArrayOfUserInSystem value) {
        this.users = value;
    }

    /**
     * Gets the value of the useDelegation property.
     * 
     */
    public boolean isUseDelegation() {
        return useDelegation;
    }

    /**
     * Sets the value of the useDelegation property.
     * 
     */
    public void setUseDelegation(boolean value) {
        this.useDelegation = value;
    }

    /**
     * Gets the value of the clientState property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getClientState() {
        return clientState;
    }

    /**
     * Sets the value of the clientState property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setClientState(BigInteger value) {
        this.clientState = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the shortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets the value of the shortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortName(String value) {
        this.shortName = value;
    }

    public Collection<UserInSystem> getUserInSystemCollection() {
        return userInSystemCollection;
    }

    public void setUserInSystemCollection(Collection<UserInSystem> userInSystemCollection) {
        this.userInSystemCollection = userInSystemCollection;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}
