
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import android.graphics.Color;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;


/**
 * <p>Java class for Tab complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tab">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Order" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Color" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsSystemTab" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TabFilter" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Filter"/>
 *         &lt;element name="KeyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AscendingSort" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsHidden" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CanBeHidden" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsNewTabAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFilterAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFilterMinimized" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tab", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "code",
    "order",
    "color",
    "isSystemTab",
    "tabFilter",
    "keyName",
    "ascendingSort",
    "isHidden",
    "canBeHidden",
    "isNewTabAvailable",
    "isFilterAvailable",
    "isFilterMinimized",
    "type"
})

@DatabaseTable(tableName = "tabs")
public class Tab {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Name")
    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    protected String name;

    @XmlElement(name = "Code", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Code")
    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    protected String code;

    @XmlElement(name = "Order", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("Order")
    @DatabaseField(dataType = DataType.INTEGER)
    protected int order;

    @XmlElement(name = "Color", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Color")
    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    protected String color;

    @XmlElement(name = "IsSystemTab", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsSystemTab")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean isSystemTab;

    @XmlElement(name = "TabFilter", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("TabFilter")
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    protected Filter tabFilter;

    @XmlElement(name = "KeyName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("KeyName")
    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    protected String keyName;

    @XmlElement(name = "AscendingSort", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("AscendingSort")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean ascendingSort;

    @XmlElement(name = "IsHidden", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsHidden")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean isHidden;

    @XmlElement(name = "CanBeHidden", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("CanBeHidden")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean canBeHidden;

    @XmlElement(name = "IsNewTabAvailable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsNewTabAvailable")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean isNewTabAvailable;

    @XmlElement(name = "IsFilterAvailable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsFilterAvailable")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean isFilterAvailable;

    @XmlElement(name = "IsFilterMinimized", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("IsFilterMinimized")
    @DatabaseField(dataType = DataType.BOOLEAN)
    protected boolean isFilterMinimized;

    @XmlElement(name = "Type", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("Type")
    @DatabaseField(dataType = DataType.INTEGER)
    protected int type;

    @JsonProperty("ServerTime")
    @DatabaseField(dataType = DataType.STRING)
    protected String serverTime;

    public String getServerTime() {
        return serverTime;
    }

    public int getParsedColor() {
        return Color.parseColor(color);
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    @DatabaseField(dataType =  DataType.STRING )
    protected String searchQuery;

    public void setSearchQuery(String searchQuery){ this.searchQuery = searchQuery;}

    public String getSearchQuery(){ return searchQuery;}


    @DatabaseField(columnName = "change_date_time", dataType = DataType.DATE_TIME)
    protected DateTime changeDateTime;

    public DateTime getChangeDateTime() {
        return changeDateTime;
    }

    public void setChangeDateTime(DateTime changeDateTime) {
        this.changeDateTime = changeDateTime;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the order property.
     * 
     */
    public int getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     */
    public void setOrder(int value) {
        this.order = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the isSystemTab property.
     * 
     */
    public boolean isIsSystemTab() {
        return isSystemTab;
    }

    /**
     * Sets the value of the isSystemTab property.
     * 
     */
    public void setIsSystemTab(boolean value) {
        this.isSystemTab = value;
    }

    /**
     * Gets the value of the tabFilter property.
     * 
     * @return
     *     possible object is
     *     {@link Filter }
     *     
     */
    public Filter getTabFilter() {
        return tabFilter;
    }

    /**
     * Sets the value of the tabFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Filter }
     *     
     */
    public void setTabFilter(Filter value) {
        this.tabFilter = value;
    }

    /**
     * Gets the value of the keyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     * Sets the value of the keyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyName(String value) {
        this.keyName = value;
    }

    /**
     * Gets the value of the ascendingSort property.
     * 
     */
    public boolean isAscendingSort() {
        return ascendingSort;
    }

    /**
     * Sets the value of the ascendingSort property.
     * 
     */
    public void setAscendingSort(boolean value) {
        this.ascendingSort = value;
    }

    /**
     * Gets the value of the isHidden property.
     * 
     */
    public boolean isIsHidden() {
        return isHidden;
    }

    /**
     * Sets the value of the isHidden property.
     * 
     */
    public void setIsHidden(boolean value) {
        this.isHidden = value;
    }

    /**
     * Gets the value of the canBeHidden property.
     * 
     */
    public boolean isCanBeHidden() {
        return canBeHidden;
    }

    /**
     * Sets the value of the canBeHidden property.
     * 
     */
    public void setCanBeHidden(boolean value) {
        this.canBeHidden = value;
    }

    /**
     * Gets the value of the isNewTabAvailable property.
     * 
     */
    public boolean isIsNewTabAvailable() {
        return isNewTabAvailable;
    }

    /**
     * Sets the value of the isNewTabAvailable property.
     * 
     */
    public void setIsNewTabAvailable(boolean value) {
        this.isNewTabAvailable = value;
    }

    /**
     * Gets the value of the isFilterAvailable property.
     * 
     */
    public boolean isIsFilterAvailable() {
        return isFilterAvailable;
    }

    /**
     * Sets the value of the isFilterAvailable property.
     * 
     */
    public void setIsFilterAvailable(boolean value) {
        this.isFilterAvailable = value;
    }

    /**
     * Gets the value of the isFilterMinimized property.
     * 
     */
    public boolean isIsFilterMinimized() {
        return isFilterMinimized;
    }

    /**
     * Sets the value of the isFilterMinimized property.
     * 
     */
    public void setIsFilterMinimized(boolean value) {
        this.isFilterMinimized = value;
    }

    /**
     * Gets the value of the type property.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }

}
