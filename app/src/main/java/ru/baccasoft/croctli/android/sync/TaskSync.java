package ru.baccasoft.croctli.android.sync;

import android.util.Log;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.EntityPropertyAPI;
import ru.baccasoft.croctli.android.rest.wrapper.TaskAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    /**
     * Синхронизация задач с сервера на клиент.<br>
     * Происходит следующим образом:
     * 1. Получаем с сервера все задачи за указанный промежуток времени.
     * 2. Если в локальной базе существуют задачи, для которых выполняются условия:
     * а) задача совпадает по id с одной из пришедших задач
     * б) и задача была изменена локально до выполнения текущей синхронизации
     * то синхронизируем такие задачи с клиента на сервер.
     * 3. После этого _все_пришедшие задачи кладутся в базу (добавляются/изменяются)
     *  @param dateFrom - начало периода синхронизации (обычно дата последней синхронизации)
     * @param dateTo - конец периода синхронизации (обычно дата начала текущей синхронизации)
     */
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        TaskAPI taskAPI = new TaskAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TaskSet, Task>();

        @SuppressWarnings("unchecked")
        List<Task> tasks = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskAPI);

        for(Task t : tasks) {
            TableUtils.createOrUpdateTask(t);
        }
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd();
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>sd(dateFrom);
/*
        List<Task> conflictTasks = new ArrayList<Task>();

        //{{{ Разбираемся со входящими обновлениями
        // забираем с сервера обновления для задач
        final TaskAPI taskAPI = new TaskAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<TaskSet, Task>();
        @SuppressWarnings("unchecked")
        List<Task> tasksFromServer = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskAPI);

        // локальные задачи, измененные после даты последнего обновления
        final List<Task> localModifiedTasks = TableUtils.getChangedTasks(dateFrom);
        // удобный мап <taskId, task>
        final Map<String, Task> localModifiedTasksWithId = new HashMap<String, Task>();

        for(Task task: localModifiedTasks)
            localModifiedTasksWithId.put(task.getId(), task);

        // составляем список конфликтных задач - измененные на клиенте и пришедшие измененными с сервера
        for (Task task : tasksFromServer) {
            final String taskId = task.getId();

            if(localModifiedTasksWithId.containsKey(taskId)) {
                Task lmTask = localModifiedTasksWithId.get(taskId);
                conflictTasks.add(lmTask);
            }
        }

        // отправляем на сервер КЛИЕНТСКУЮ версию конфликтных задач
        Log.d(TAG, "conflicted Tasks: " + conflictTasks.size());
        if(conflictTasks.size() > 0) {
            ArrayOfTask arrayOfTask = new ArrayOfTask();
            arrayOfTask.setItem(conflictTasks);

            ArrayOfOperationResponse opResponses = taskAPI.modifyPost(dateFrom, arrayOfTask);
            for(OperationResponse opr : opResponses.getItem()) {
                if(opr.getCode() != 0) { //п.2.9.16
                    String taskId = opr.getObjectId();
                    TableUtils.updateConflictDataForTask(taskId, opr);
                }
            }

            // TODO: обработка конфликтов по ФС андроид
        }

        // сохраняем все пришедшие задачи в локальную бд
        for(Task task : tasksFromServer) {
            TableUtils.createOrUpdateTask(task);
        }
        //}}}

        //{{{ Отправляем локально измененные задачи
        localModifiedTasks.removeAll(conflictTasks);     // чтобы не отсылать конфликтные задачи по второму разу

        if(localModifiedTasks.size() > 0) {
            ArrayOfTask arrayOfTask = new ArrayOfTask();
            arrayOfTask.setItem(localModifiedTasks);
            taskAPI.modifyPost(dateFrom, arrayOfTask);
        }
        //}}}

/*/
        ////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////     New: first POST then GET     /////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////

        // 1. локальные задачи, измененные после даты последнего обновления
        final List<Task> localModifiedTasks = TableUtils.getChangedTasks(dateFrom);
        // Добавляем проперти к таскам
        EntityPropertyAPI entityPropertyAPI = new EntityPropertyAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<EntityPropertySet, EntityProperty>();

//        List<EntityProperty> entityProperties = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, entityPropertyAPI);
//        List<Attachment> oldAttachments = null;
//        try {
//            oldAttachments = App.getDatabaseHelper().getAttachmentDAO().getNotDownloaded();
//            Log.d("TAG", "Size: " + oldAttachments.size());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        if(oldAttachments != null && !oldAttachments.isEmpty()) {
//            for(EntityProperty ep : entityProperties) {
//                if (ep.getBinaryValue() != null) {
//                    for (Attachment at : oldAttachments) {
//                        if (at.getName().equals(ep.getBinaryValue().getName())
//                                && at.getCheckSum().compareTo(ep.getBinaryValue().getCheckSum()) != 0) {
//                            at.setLocalLocation(null);
//                        }
//                    }
//                }
//            }
//        }
        // удобный мап <taskId, task>
        final Map<String, Task> localModifiedTasksWithId = new HashMap<String, Task>();
        final List<EntityProperty> localModifiedEntity = TableUtils.getChangedEntityPeoperties();
        for(Task task: localModifiedTasks) {
            localModifiedTasksWithId.put(task.getId(), task);

            List<EntityProperty> epList = getEPByTaskID(task.getId(), localModifiedEntity);
//            List<EntityProperty> epList =  getAllEPByIdTask(task.getId());
            ArrayOfEntityProperty entity = new ArrayOfEntityProperty();
            entity.setItem(epList);
            task.setArrayOfEntityProperty(entity);
        }

        // 2. Отправляем локально измененные задачи
        final TaskAPI taskAPI = new TaskAPI();
        if(localModifiedTasks.size() > 0) {
            ArrayOfTask arrayOfTask = new ArrayOfTask();
            arrayOfTask.setItem(localModifiedTasks);
            ArrayOfOperationResponse res = taskAPI.modifyPost(dateFrom, arrayOfTask);
            if (res != null) {
                // TODO интерпретировать респонзы...
                for (Task task: localModifiedTasks) {
                    for (EntityProperty ep: task.getEntityProperties()) {
                        TableUtils.updateEntityProperty(ep, false);
                    }
                }
            }
            Log.d(TAG, "1");
        }

        // 3. Разбираемся со входящими обновлениями
        // забираем с сервера обновления для задач
        /*RestApiRecursiveCall*/ recursiveCall = new RestApiRecursiveCall<TaskSet, Task>();
        @SuppressWarnings("unchecked")
        List<Task> tasksFromServer = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, taskAPI);

        // сохраняем все пришедшие задачи в локальную бд
        for(Task task : tasksFromServer) {
            TableUtils.createOrUpdateTask(task);
        }
        //*/

    }

    private List<EntityProperty> getEPByTaskID(String id, List<EntityProperty> localModifiedEntity) {
        List<EntityProperty> epList = new ArrayList<EntityProperty>();
        List<EntityProperty>child = TableUtils.getEntityPropertyChildsById(id);
        if (child.size() <= 0)
            child = TableUtils.getEntityPropertyByTaskId(id);
        for (EntityProperty ch:child) {
            List<EntityProperty> epChild = getEPByTaskID(ch.getId(), localModifiedEntity);
            for (EntityProperty epChild2: epChild) {
                epList.add(epChild2);
            }
            for (EntityProperty ep: localModifiedEntity) {
                if (ep.getId().equals(id)) {
                    epList.add(ep);
                }
            }
        }
        for (EntityProperty ep: localModifiedEntity) {
            if (id.equals(ep.getId()))
                epList.add(ep);
        }
        return epList;
    }

    private List<EntityProperty> getAllEPByIdTask(String taskId) {
        List<EntityProperty> epList = TableUtils.getEntityPropertyByTaskId(taskId);

        for (EntityProperty ep: epList) {
            List<EntityProperty> tmpList = getAllEPByIdTask(ep.getId());
            for (EntityProperty chEp: tmpList)
                ep.addEntityProperty(chEp);
        }
        return epList;
    }

    /**
     * Синхронизация удаленных задач с сервера на клиент.<br>
     * Сначала получаем список идентификаторов задач с сервера
     * и удаляем локальные задачи, id которых отсутствуют в этом списке.<br>
     */
    private void gd() {
        //{{{ Удаляем задачи, которых больше нет на сервере
        //запрашиваем список задач, которые СУЩЕСТВУЮТ на сервере (т.е. не удалены)
        StringSet serverTaskIds = RestClient.fillJsonClass(StringSet.class, "/api/task/listid", true);

        StringSet localTaskIds = TableUtils.getTaskIds();
        List<String> taskIdForDelete = new ArrayList<String>();

        // составляем список задач, которые есть локально, но отсутствуют на сервере
        // т.е. список задач, которые нужно удалить
        for (String localId : localTaskIds.getArrayOf().getItem()) {
            if (!serverTaskIds.getArrayOf().getItem().contains(localId))
                taskIdForDelete.add(localId);
        }
        TableUtils.deleteTasks(taskIdForDelete);
        //}}}
    }

    /**
     * Отправляем на удаление задачи, помеченные локально на удаление
     */
    private void sd(RestApiDate dateFrom) {
        TaskAPI taskAPI = new TaskAPI();

        ArrayOfString arrayOfIds = new ArrayOfString();
        List<String> deletedTaskIds = TableUtils.getDeletedTaskIds();
        arrayOfIds.setItem(deletedTaskIds);
        taskAPI.deletePost(dateFrom, arrayOfIds);
    }
}
