package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.event.ClassifierSimpleChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class ClassifierSimple extends AutoCompleteTextView {
//    private PassThroughFocusChangeListener mPassThroughFocusChangeListener;
    private PassThroughClickListener mPassThroughClickListener;
    private PassThroughKeyListener mPassThroughKeyListener;

    Context mContext;
    Map<String, String> items;
    String entityPropertyId;

    ArrayAdapter adapter;
    ArrayAdapter clearAdapter;
    final int itemResId = android.R.layout.simple_dropdown_item_1line;
    final Validator validator = new Validator();

    /**
     *
     * @param context
     * @param items пары [code справочника : выводимое значение]
     * @param entityPropertyId
     */
    public ClassifierSimple(final Context context, final Map<String, String> items, String entityPropertyId) {
        super(context);
        this.items = items;
        this.mContext = context;
        this.entityPropertyId = entityPropertyId;
        clearAdapter = new ArrayAdapter(context, itemResId);

        // настраиваем вьюшку
        setThreshold(1);
        setMaxLines(1);

        List<String> values = new ArrayList<String>();
        for(Map.Entry<String, String> e : this.items.entrySet()) {
            values.add(e.getValue());
        }

        adapter = new ArrayAdapter(context, itemResId, values);
        setAdapter(adapter);

        setValidator(validator);

//        // отслеживаем, если для дополнения предлагается только один вариант и подставляем его
//        adapter.registerDataSetObserver(new DataSetObserver() {
//            @Override
//            public void onChanged() {
//                super.onChanged();
//
////                int count = adapter.getCount();
////                Log.d("", "" + count);
////                if (count == 1 && !isPerformingCompletion()) { // подставляем предлагаемое значение и скрываем попап
//////                    final String text = (String) adapter.getItem(0);
//////                    setItemText(text);
////
////                    setListSelection(0);
//////                    performFiltering();
//////                    performCompletion();
////
//////                    clearListSelection();
////                }
//            }
//        });

//        mPassThroughFocusChangeListener = new PassThroughFocusChangeListener();
//        super.setOnFocusChangeListener(mPassThroughFocusChangeListener);

        mPassThroughClickListener = new PassThroughClickListener();
        super.setOnClickListener(mPassThroughClickListener);

        mPassThroughKeyListener = new PassThroughKeyListener();
        super.setOnKeyListener(mPassThroughKeyListener);

        initThis();
    }

//    @Override
//    public void setOnFocusChangeListener(OnFocusChangeListener l) {
//        mPassThroughFocusChangeListener.mWrapped = l;
//    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        mPassThroughClickListener.mWrapped = listener;
    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        mPassThroughKeyListener.mWrapped = l;
    }

    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер.
     * Мы же отслеживаем backspace и удаляем всю строку
     *
     */
    private class PassThroughKeyListener implements OnKeyListener {
        private OnKeyListener mWrapped;

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            onKeyImpl(keyCode, event);

            if(mWrapped != null)
                mWrapped.onKey(v, keyCode, event);

            return false;
        }
    }

    private void onKeyImpl(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP) {
            // очищаем ввод
            onClear();
        }
    }


    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер
     */
//    private class PassThroughFocusChangeListener implements OnFocusChangeListener {
//        private OnFocusChangeListener mWrapped;
//
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            onFocusChangeImpl(hasFocus);
//
//            if(mWrapped != null)
//                mWrapped.onFocusChange(v, hasFocus);
//        }
//    }
//    private void onFocusChangeImpl(boolean hasFocus) {
//        Log.d("", "onFocusChangeImpl");
//        if (hasFocus) {
////            showDropDown();
//            performFiltering(getText(), -1);
//        } else {
//            // если введенный текст не соответствует ни одному из значений справочника,
//            // очищаем ввод
////            String text = getText().toString();
////            Log.d("text: ", text);
////            if(!items.contains(text)) {
////                onClear();
////            }
//        }
//    }

    @Override
    public void onFilterComplete(int count) {
        super.onFilterComplete(count);

        if (count == 1) { // подставляем предлагаемое значение и скрываем попап
            setListSelection(0);
            performCompletion();

            String value = getText().toString();
            EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, value, mContext));
        } else if (count == items.size()) {
            showDropDown();
        }
    }

    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер
     */
    private class PassThroughClickListener implements OnClickListener {
        private OnClickListener mWrapped;

        @Override
        public void onClick(View v) {
            onClickImpl();

            if(mWrapped != null)
                mWrapped.onClick(v);
        }
    }

    private void onClickImpl() {
        if(!isPopupShowing())
            showDropDown();
    }

    /**
     * Проверяет, что введено значение только из переданного списка.
     * В качестве исправления неверного ввода отдает пустую строку.
     */
    private class Validator implements AutoCompleteTextView.Validator {

        @Override
        public boolean isValid(CharSequence text) {
            return items.containsValue(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            return "";
        }
    }

    private void onClear() {
        setText("");
        performFiltering("", -1);
//        showDropDown();
    }

    private void initThis() {
        setTextColor(getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, mContext);
//        setTextSize(getResources().getDimension(R.dimen.px_16));
    }



//    private void setItemText(String text) {
////        setAdapter(clearAdapter);
//        setText(text);
////        setAdapter(adapter);
////        performFiltering(text, -1);
//    }
}
