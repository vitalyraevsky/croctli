package ru.baccasoft.croctli.android.rest;

import org.apache.http.HttpResponse;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.gen.core.Process;

import java.util.HashMap;
import java.util.Map;

// хотел избавиться от кучи классов для каждого API.
// "затея не удалась. за попытку спасибо"(c)
@Deprecated()
public class RestUniversalAPI {
    @SuppressWarnings("unused")
    private static final String TAG = RestUniversalAPI.class.getSimpleName();

    private static final Integer SYNC_MAX_COUNT = 100;

    public enum TYPE {
        SM("Modify"),
        SD("Delete"),
        GM("Modify"),
        GD("Delete");

        private String name;

        TYPE(String name) { this.name = name; }
        public String getName() { return name; }
    }

    private static Map<Class, RestUniversalAPI.SyncObjectInfo> apiPrefixes = new HashMap<Class, SyncObjectInfo>();
    static {
        apiPrefixes.put(UserActivity.class, new SyncObjectInfo("/server/activity", true, false));
        apiPrefixes.put(AppVersion.class, new SyncObjectInfo("/server/version", true, false));
        apiPrefixes.put(ConditionOperator.class, new SyncObjectInfo("/api/conditionoperator/list", false, false));
        apiPrefixes.put(SpecialCondition.class, new SyncObjectInfo("/api/specialcondition/list", false, false));
        apiPrefixes.put(SourceSystem.class, new SyncObjectInfo("/SourceSystem", true, true));

        apiPrefixes.put(Adapter.class, new SyncObjectInfo("/Adapter", false, true));
        apiPrefixes.put(TaskStateInSystem.class, new SyncObjectInfo("/TaskStateInSystem", true, true));
        apiPrefixes.put(Classifier.class, new SyncObjectInfo("/Classifier", true, true));
        apiPrefixes.put(SystemUser.class, new SyncObjectInfo("/SystemUser", true, true));
        apiPrefixes.put(ViewCustomization.class, new SyncObjectInfo("/ViewCustomization", true, true));
        apiPrefixes.put(Tab.class, new SyncObjectInfo("/Tab", true, true));
        apiPrefixes.put(ProcessType.class, new SyncObjectInfo("/ProcessType", true, true));
        apiPrefixes.put(Process.class, new SyncObjectInfo("/Process", true, true));

        apiPrefixes.put(Task.class, new SyncObjectInfo("/Task", true, true)); // задачи вообще такие особенные

        apiPrefixes.put(TaskUserUI.class, new SyncObjectInfo("/TaskUserUI", true, true));
        apiPrefixes.put(EntityProperty.class, new SyncObjectInfo("/EntityProperty", true, true));
        apiPrefixes.put(HandmadeDelegate.class, new SyncObjectInfo("/HandmadeDelegate", true, true));
        apiPrefixes.put(RibbonSettings.class, new SyncObjectInfo("/RibbonSettings", true, true));
        apiPrefixes.put(UIForm.class, new SyncObjectInfo("/UIForm/ListAll", false, false));
    }

    private static class SyncObjectInfo {
        public final String apiPrefix;  // префикс урла rest-вызова
        public final boolean needAuth;  // нужна ли авторизация (ST-тикет) для вызова
        public final boolean paramsRequired;    // нужно ли добавлять к префиксу дополнительные параметры

        public SyncObjectInfo(String apiPrefix, boolean needAuth, boolean paramsRequired) {
            this.apiPrefix = apiPrefix;
            this.needAuth = needAuth;
            this.paramsRequired = paramsRequired;
        }
    }

    /**
     *
     * @param operation тип вызова: удаление/изменение, получение/отправка
     * @param dateFrom
     * @param dateTo
     * @param jsonData
     * @param <T> зависит от типа операции (operation)
     * @return
     */
    public static <T> T callApi(TYPE operation, RestApiDate dateFrom, RestApiDate dateTo, Class<T> dataSetType, String jsonData) {
        //validate();//todo
        String apiUrl = getApiUrl(operation, dateFrom, dateTo, dataSetType.getClass());
        boolean needAuth = getNeedAuth(dataSetType.getClass());

        switch (operation) {
            case GM:
            case GD:
                return performGet(apiUrl, needAuth, dataSetType);
            case SM:
            case SD:
                return performPost(apiUrl, needAuth, jsonData, dataSetType);
            default:
                throw new IllegalArgumentException("unsupported operation type:" + operation);
        }
    }

    private static <T> T performPost(String apiUrl, boolean needAuth, String jsonData, Class<T> dataType) {
        HttpResponse hr = RestClient.callSomeRestApi(apiUrl, needAuth, jsonData);
        Utils.logHttpResponse(hr, TAG);

        return null;
    }

    private static <T> T performGet(String apiUrl, boolean needAuth, Class<T> dataType) {
        return RestClient.fillJsonClass(dataType, apiUrl, needAuth);
    }

    //{{{ местные утилсы
    private static boolean getNeedAuth(Class dataType) {
        return apiPrefixes.get(dataType).needAuth;
    }

    private static String getApiUrl(TYPE operation, RestApiDate dateFrom, RestApiDate dateTo, Class dataType) {
        String apiUrl = apiPrefixes.get(dataType).apiPrefix;
        if(!apiPrefixes.get(dataType).paramsRequired)
            return apiUrl;

        apiUrl += "/" + operation.getName() + "/";

        switch (operation) {
            case GM:
            case GD:
                apiUrl += dateFrom.toRestDate() + "/" + dateTo.toRestDate() + "/" + String.valueOf(SYNC_MAX_COUNT);
                break;
            case SM:
            case SD:
                apiUrl += dateFrom.toRestDate();
                break;
            default:
                throw new IllegalArgumentException("unsupported operation type:" + operation);
        }
        return apiUrl;
    }
    //}}}

    // Из ФС на фасад:
//    Если количество объектов, которые необходимо вернуть клиенту, больше параметра
// {максимальное количество записей для возврата}, то возвращаются только первые N
// (где N = {максимальное количество записей для возврата}) записей, упорядоченных по дате изменения
// по возрастанию (иначе говоря, результирующий массив обрезается до требуемого размера, остаются только
// «старые» записи).  В поле ServerTime возвращаемого объекта, в случае если массив «пришлось» обрезать,
// выставляется SyncUnit.ChangeLastTime последней записи в отдаваемом массиве. Если размер массива не
// превысил заданных ограничений, в параметре ServerTime передается дата-время в клиентском запросе «дата по».
}
