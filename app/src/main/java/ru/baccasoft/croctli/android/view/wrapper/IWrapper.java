package ru.baccasoft.croctli.android.view.wrapper;

import android.view.View;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;

/**
 * Created by developer on 28.11.14.
 */
public interface IWrapper {
    public View getLayout(EntityProperty p, Boolean globalReadOnly);
}
