package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;
import ru.baccasoft.croctli.android.gen.core.ViewCustomization;
import ru.baccasoft.croctli.android.gen.core.ViewCustomizationSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.ViewCustomizationAPI;

import java.util.List;

public class ViewCustomizationSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        ViewCustomizationAPI viewCustomizationAPI = new ViewCustomizationAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> viewCustomizationIds = recursiveCall.getRestApiSetAsList(
                dateFrom, dateTo, SYNC_MAX_COUNT, viewCustomizationAPI);

        TableUtils.deleteViewCustomizations(viewCustomizationIds);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        ViewCustomizationAPI viewCustomizationAPI = new ViewCustomizationAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<ViewCustomizationSet, ViewCustomization>();

        @SuppressWarnings("unchecked")
        List<ViewCustomization> viewCustomizations = recursiveCall.getRestApiSetAsList(
                dateFrom, dateTo, SYNC_MAX_COUNT, viewCustomizationAPI);

        for(ViewCustomization v : viewCustomizations) {
            TableUtils.createOrUpdateViewCustomization(v);
        }
    }
}
