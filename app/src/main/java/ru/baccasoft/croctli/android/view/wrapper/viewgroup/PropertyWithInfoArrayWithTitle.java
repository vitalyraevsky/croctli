package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ClassifierPopup;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.PropertyWithInfo;

import java.util.List;
import java.util.Map;

/**
 *
 *   Массивные справочные свойства с DisplayMode = 1
 *
 *   Показывает список справочных элементов с .
 *   При нажатии на каждый элемент открывается попап с дополнительной информацией.
 *
 *
 */
public class PropertyWithInfoArrayWithTitle extends LinearLayout {

    private Context mContext;
    private ClassifierPopup mPopup;


    public PropertyWithInfoArrayWithTitle(Context context, String title, List<PropertiesHolder> props) {
        super(context);
        mContext = context;

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        addView(makeLeftView(title));
        addView(makeRightView(props));

//        setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("", "onClick");
//
//                Object tmp = v.getTag();
//                if(tmp == null)
//                    return;
//
//                if(mPopup != null && mPopup.isShowing()) {
//                    Log.d("", "dismissing popup");
//                    mPopup.dismiss();
//                }
//
//
//                PropertiesHolder h = (PropertiesHolder) tmp;
//                mPopup = new ClassifierPopup(mContext, h.popupTitle, h.popupInfo);    //TODO: не создавать, а переинициализировать
//                mPopup.showUnder(v);
//            }
//        });
    }

    private View makeLeftView(String title) {
        TextView v = new TextView(mContext);
        v.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        v.setText(title);

        v.setTextColor(getResources().getColor(R.color.text_777777));
        v.setTextSize(getResources().getDimension(R.dimen.px_16));

        return v;
    }

    private View makeRightView(List<PropertiesHolder> props) {
        LinearLayout v = new LinearLayout(mContext);

        v.setOrientation(VERTICAL);
        v.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for(PropertiesHolder p : props) {
            PropertyWithInfo pView = new PropertyWithInfo(mContext, p.name, p.popupTitle, p.popupInfo);
            pView.setLayoutParams(new LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
//            pView.setTag(p);
            v.addView(pView);
        }

        return v;
    }

    /**
     * Холдер для отдельных свойств, которые имеют дополнительную справочную информацию
     */
    public static class PropertiesHolder {
        // Что будет написано на элементе интерфейса (в оригинале - на кнопке)
        public String name;
        // Заголовок попапа, который открывается по клику на этот элемент интерфейса
        public String popupTitle;
        // Массив значений, которые будут отображаться в попапе.
        // На его основе будет заполняться список, который показывается в попапе.
        // Элемент списка состоит из двух строк и соответствует одной Map.Entry.
        // Первая строка=getKey(), вторая строка=getValue()
        public Map<String, String> popupInfo;

        public PropertiesHolder() { }

        public PropertiesHolder(String name, String popupTitle, Map<String, String> popupInfo) {
            this.name = name;
            this.popupTitle = popupTitle;
            this.popupInfo = popupInfo;
        }
    }

}
