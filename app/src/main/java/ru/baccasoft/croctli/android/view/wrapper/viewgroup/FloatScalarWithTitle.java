package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.FloatScalar;

/**
 * Created by developer on 06.11.14.
 */
public class FloatScalarWithTitle extends LinearLayout {
    public FloatScalarWithTitle(Context context, String title, String text, boolean isReadonly) {
        super(context);

        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView titleView = ViewUtils.makeTitleTextView(context, title, null);
        FloatScalar floatScalar = new FloatScalar(context, text, isReadonly);

        floatScalar.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        addView(titleView);
        addView(floatScalar);
    }
}
