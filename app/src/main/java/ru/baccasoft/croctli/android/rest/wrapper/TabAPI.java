package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.ArrayOfTab;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TabSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class TabAPI implements IRestApiCall<TabSet, ArrayOfTab, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = TabAPI.class.getSimpleName();

    @Override
    public TabSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Tab/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(TabSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Tab/Delete/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfTab objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
