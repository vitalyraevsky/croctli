package ru.baccasoft.croctli.android.view.util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import org.joda.time.LocalDate;

/**
 * Created by developer on 02.12.14.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LocalDate date = new LocalDate();
        return new DatePickerDialog(getActivity(), this,
                date.getYear(), date.getMonthOfYear(), date.getDayOfMonth());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        //nothing
    }
}
