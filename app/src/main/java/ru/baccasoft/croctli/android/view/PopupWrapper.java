package ru.baccasoft.croctli.android.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.List;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.adapters.FilesPopupAdapter;
import ru.baccasoft.croctli.android.dao.PopupFileInfo;
import ru.baccasoft.croctli.android.fragments.IAttachmentOpener;

public final class PopupWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = PopupWrapper.class.getSimpleName();

    public static PopupWindow newPopup(final Activity activity, List<PopupFileInfo> data, final IAttachmentOpener callback) {
        LayoutInflater layoutInflater
                = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.popup_file_list_layout, null);

        ListView listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(new FilesPopupAdapter(data, activity));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // анимация
                Animation anim = AnimationUtils.loadAnimation(activity, R.anim.file_icon_press);
                view.startAnimation(anim);
                String fileId = ((TextView) view.findViewById(R.id.file_id)).getText().toString();
                Log.d(TAG, "clicked item with file id:\'" + fileId + "\'");
                callback.attachmentSelected(fileId, position);
            }
        });

//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "clicking on popupWindow");
//            }
//        });

        int width = getViewWidth(layoutInflater, data);
        PopupWindow pw = new PopupWindow(
                view,
                width,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true);

        // чтобы попап скрывался при клику за пределами
        pw.setBackgroundDrawable(new BitmapDrawable());
//        pw.setOutsideTouchable(true);
//        pw.setTouchable(true);

        return pw;
    }

    /**
     * Небольшой хак, чтобы список файлов отображался с нормальной шириной.
     * Без него попап показывается слишким широким, даже хотя везде стоит wrap_content.
     *
     * Делаем такой трюк: находим элемент с самым длинным текстом, создаем вьюшку для него
     * и замеряем ее ширину.
     *
     * @param data список элементов, которые будут отображатся в попапе
     * @return ширину вьюшки с самым длинным элементом
     */
    private static int getViewWidth(LayoutInflater inflater, List<PopupFileInfo> data) {
        PopupFileInfo info = findLongestItem(data);
        View v = inflater.inflate(R.layout.popup_file_item, null);

        ((ImageView) v.findViewById(R.id.icon)).setImageResource(info.typeRes);
        ((TextView) v.findViewById(R.id.name)).setText(info.name);
        ((TextView) v.findViewById(R.id.size)).setText(FilesPopupAdapter.readableSize(info.size));
        v.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

//        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        return v.getMeasuredWidth();

    }

    /**
     * Находит элемент с самими длинные строками.
     */
    private static PopupFileInfo findLongestItem(List<PopupFileInfo> data) {
        int maxLength = 0;
        PopupFileInfo result = data.get(0);

        for(PopupFileInfo i : data) {
            int tmp = i.name.length() + FilesPopupAdapter.readableSize(i.size).length();
            tmp = (int) (1.3 * tmp);    // коэфициент просто на всякий случай
            if(tmp > maxLength) {
                maxLength = tmp;
                result = i;
            }
        }
        return result;
    }

}
