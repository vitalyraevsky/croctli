package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.AdapterSet;
import ru.baccasoft.croctli.android.gen.core.ArrayOfAdapter;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 * GD, GM
 */
public class AdapterAPI implements IRestApiCall<AdapterSet, ArrayOfAdapter, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = AdapterAPI.class.getSimpleName();

    @Override
    public AdapterSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Adapter/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(AdapterSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Adapter/Delete/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfAdapter objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
