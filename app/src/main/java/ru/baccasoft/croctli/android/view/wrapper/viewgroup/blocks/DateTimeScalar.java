package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import de.greenrobot.event.EventBus;
import org.joda.time.DateTime;

import java.util.Date;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.utils.DateTimeWrapper;
import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.event.ScalarChangeEvent;

/**
 * Отображение скалярных свойств с датой и временем.
 * По клику на вьюшку открывается диалог для выбора даты и времени.
 * Выбранное значение (при Сохранении) отправляется через EventBus заинтересованным.
 *
 */
public class DateTimeScalar extends TextView {
    private Activity mActivity;
    private Context mContext;

//    private DatePickerDialog datePickerDialog;
    private Dialog dateTimePickerDialog;
    private DatePicker datePicker;
    private TimePicker timePicker;
    private DatePicker.OnDateChangedListener onDateChangedListener;
    private TimePicker.OnTimeChangedListener onTimeChangedListener;

    private TextView negativeButton;
    private TextView positiveButton;

    private final String entityPropertyId;
    private IDateField selectedValue;

    public DateTimeScalar(Activity activity, IDateField date, boolean isReadonly, final String entityPropertyId) {
        super(activity.getApplicationContext());
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.entityPropertyId = entityPropertyId;
        this.selectedValue = date;

        initThis();

        if (!isReadonly) {
            dateTimePickerDialog = new Dialog(mActivity);
            dateTimePickerDialog.setContentView(R.layout.date_time_picker_dialog);
            dateTimePickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            initDatePicker();
            initTimePicker();
            initButtons();

            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker();
                }
            });
        }
    }

    private void initDatePicker() {
        onDateChangedListener = new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                DateTime previousValue = new DateTime( selectedValue.smartGetTime() );
                DateTime newValue = new DateTime(year, monthOfYear+1, dayOfMonth,
                        previousValue.getHourOfDay(), previousValue.getMinuteOfHour());
                selectedValue = new DateTimeWrapper(new Date(newValue.getMillis()));
            }
        };

        DateTime date = new DateTime(selectedValue.smartGetTime());
        datePicker = (DatePicker) dateTimePickerDialog.findViewById(R.id.datePicker1);
        datePicker.init(date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth(),
                onDateChangedListener);
    }

    private void initTimePicker() {
        onTimeChangedListener = new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                DateTime previousValue = new DateTime( selectedValue.smartGetTime() );
                DateTime newValue = new DateTime(
                        previousValue.getYear(), previousValue.getMonthOfYear(), previousValue.getDayOfMonth(),
                        hourOfDay, minute);
                selectedValue = new DateTimeWrapper(new Date(newValue.getMillis()));
            }
        };

        timePicker = (TimePicker) dateTimePickerDialog.findViewById(R.id.timePicker1);
        timePicker.setIs24HourView(true);
        timePicker.setOnTimeChangedListener(onTimeChangedListener);

        DateTime dateTime = new DateTime(selectedValue.smartGetTime());
        timePicker.setCurrentHour(dateTime.getHourOfDay());
        timePicker.setCurrentMinute(dateTime.getMinuteOfHour());
    }

    private void initThis() {
        setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
        setTextColor(mContext.getResources().getColor(R.color.text_2A2A2A));
        setTextSize(mContext.getResources().getDimension(R.dimen.px_16));
    }

    private void showDatePicker() {
        dateTimePickerDialog.show();
    }

    private void initButtons() {
        negativeButton = (TextView) dateTimePickerDialog.findViewById(R.id.negative);
        positiveButton = (TextView) dateTimePickerDialog.findViewById(R.id.positive);
        negativeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimePickerDialog.dismiss();
            }
        });
        positiveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // обновляем дату на вьюшке
                updateThis(selectedValue);
                sendOnChangeEvent();
            }
        });
    }

    private void updateThis(IDateField date) {
        setText(date.toGuiString());
    }

    /**
     * Сообщить, какое EntityProperty было изменено
     */
    private void sendOnChangeEvent() {
        //RestDate selRestDate = new RestDate(selectedDateTime);
        String selDateString = selectedValue.toGuiString();

        EventBus.getDefault().post(new ScalarChangeEvent(entityPropertyId, selDateString, mContext));
//        EventBus.getDefault().post(new ValueModifiedEvent(
//                entityPropertyId, selDateString, VIEW_TYPE.SCALAR));
    }
}