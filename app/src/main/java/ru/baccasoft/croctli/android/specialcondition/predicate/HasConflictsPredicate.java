package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.gen.core.ArrayOfConflictData;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class HasConflictsPredicate implements IBooleanAtom {
	
	public static final String ATOM_NAME = "hasconflicts";

	@Override
	public Boolean compute(Task task) {
        ArrayOfConflictData conf = task.getConflict();
        if (conf == null)
            return false;
        return conf.getItem() != null && task.getConflict().getItem().size() > 0;
	}

	@Override
	public IBooleanAtom create() {
		return new HasConflictsPredicate();
	}

	@Override
	public String getAtomName() {
		return ATOM_NAME;
	}
}
