package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ActionBehavior;

import java.sql.SQLException;

public class ActionBehaviorDAO extends BaseDaoImpl<ActionBehavior, String> {
    public ActionBehaviorDAO(ConnectionSource connectionSource, Class<ActionBehavior> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
