
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskPackage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskPackage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlainProcess" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Process"/>
 *         &lt;element name="PlainTask" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Task"/>
 *         &lt;element name="Properties" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfEntityProperty"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskPackage", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "plainProcess",
    "plainTask",
    "properties"
})
public class TaskPackage {

    @XmlElement(name = "PlainProcess", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected Process plainProcess;
    @XmlElement(name = "PlainTask", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected Task plainTask;
    @XmlElement(name = "Properties", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfEntityProperty properties;

    /**
     * Gets the value of the plainProcess property.
     * 
     * @return
     *     possible object is
     *     {@link Process }
     *     
     */
    public Process getPlainProcess() {
        return plainProcess;
    }

    /**
     * Sets the value of the plainProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process }
     *     
     */
    public void setPlainProcess(Process value) {
        this.plainProcess = value;
    }

    /**
     * Gets the value of the plainTask property.
     * 
     * @return
     *     possible object is
     *     {@link Task }
     *     
     */
    public Task getPlainTask() {
        return plainTask;
    }

    /**
     * Sets the value of the plainTask property.
     * 
     * @param value
     *     allowed object is
     *     {@link Task }
     *     
     */
    public void setPlainTask(Task value) {
        this.plainTask = value;
    }

    /**
     * Gets the value of the properties property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEntityProperty }
     *     
     */
    public ArrayOfEntityProperty getProperties() {
        return properties;
    }

    /**
     * Sets the value of the properties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEntityProperty }
     *     
     */
    public void setProperties(ArrayOfEntityProperty value) {
        this.properties = value;
    }

}
