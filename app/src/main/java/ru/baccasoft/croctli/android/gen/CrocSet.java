package ru.baccasoft.croctli.android.gen;

import java.util.List;

/**
 * Created by developer on 18.12.14.
 */
public interface CrocSet<ItemType> {
    List<ItemType> getItems();
    String getServerTimeString();
}
