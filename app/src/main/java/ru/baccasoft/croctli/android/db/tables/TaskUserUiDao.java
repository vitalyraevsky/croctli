package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.TaskUserUI;

import java.sql.SQLException;

public class TaskUserUiDao extends BaseDaoImpl<TaskUserUI, String> {
    public TaskUserUiDao(ConnectionSource connectionSource, Class<TaskUserUI> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
