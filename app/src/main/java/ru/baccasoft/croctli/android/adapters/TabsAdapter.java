package ru.baccasoft.croctli.android.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.gen.core.Tab;
import ru.baccasoft.croctli.android.gen.core.Task;

/**
 * Created by Developer on 03.04.2015.
 */
public class TabsAdapter extends BaseAdapter {

    public static final String TAG = TabsAdapter.class.getSimpleName();

    private Context context;
    private final List<Tab> tabsList;
    private final HashMap<String, String> tabCodesNames;
    private final Map<Tab, List<Task>> filteredByTabTasks;

    public TabsAdapter(Context context, List<Tab> tabsList, HashMap<String, String> tabCodesNames, Map<Tab, List<Task>> filteredByTabTasks) {
        this.context = context;
        this.tabsList = tabsList;
        this.tabCodesNames = new HashMap<String, String>(tabCodesNames);
        this.filteredByTabTasks = new HashMap<Tab, List<Task>>(filteredByTabTasks);

    }

    @Override
    public int getCount() {
        return tabsList.size();
    }

    @Override
    public Tab getItem(int i) {
        return tabsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        View colorIndicator;
        TextView tabName;
        TextView tabCountTasks;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //Log.d(TAG, "TAB IN ADAPTER " + tabsList.size() + " " + tabCodesNames.size() + " " + filteredByTabTasks.size());
        ViewHolder holder;
        final Tab currentTab = tabsList.get(i) ;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.tab_item, viewGroup, false);
            holder = new ViewHolder();
            holder.colorIndicator = view.findViewById(R.id.color_indicator);

            holder.tabName = (TextView) view.findViewById(R.id.tab_name);

            holder.tabCountTasks = (TextView) view.findViewById(R.id.tab_count_text);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        int color = currentTab.getParsedColor();
        holder.colorIndicator.setBackgroundColor(color);

        setTabName(currentTab, holder.tabName);


        List<Task> tabTasks = filteredByTabTasks.get(currentTab);

        holder.tabCountTasks.setText(MessageFormat.format("{0}/{1}", countNotReadTasks(tabTasks), tabTasks.size()));

        Log.d(TAG, tabsList.get(i).getName().concat(" inflated"));

        return view;
    }

    private void setTabName(Tab tab, TextView tv) {
        if (!tab.isIsSystemTab()) {
            tv.setText(tab.getName());
        }
        if (tabCodesNames.containsKey(tab.getCode())) {
            tv.setText(tabCodesNames.get(tab.getCode()));
        } else {
            tv.setText(tab.getName());
        }

    }

    private int countNotReadTasks(List<Task> tasks) {
        int count = 0;
        for (Task task : tasks)
            if (!task.isWasRead()) ++count;
        return count;
    }
}
