package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.LogMessageCategory;

import java.sql.SQLException;

public class LogMessageCategoryDao extends BaseDaoImpl<LogMessageCategory, Integer> {
    public LogMessageCategoryDao(ConnectionSource connectionSource, Class<LogMessageCategory> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
