package ru.baccasoft.croctli.android.rest;

import android.content.Context;
import android.graphics.Rect;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.ViewCustomization;
import ru.baccasoft.croctli.android.rest.wrapper.LogMessageAPI;
import ru.baccasoft.croctli.android.view.util.CrocUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class Utils {
    @SuppressWarnings("unused")
    private static final String TAG = Utils.class.getSimpleName();

    public static final String CAS_LOGIN_FIELD_NAME = "username";
    public static final String CAS_PASSWORD_FIELD_NAME = "password";

    public static List<NameValuePair> getCASCredentials(String username, String password) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(CAS_LOGIN_FIELD_NAME, username));
        params.add(new BasicNameValuePair(CAS_PASSWORD_FIELD_NAME, password));
        return params;
    }

    /**
     * Вытаскивает тикет TGT из урла. <br>
     * улр такого вида: <br>
     * http://SERVER:PORT/cas-server-3.4.11/v1/tickets/TGT-179-RaBe4efNwaVTalmd1qMBmqVRUNPqsMO7Ng3nr0Sy6cFFlb7LAA-cas01.example.org
     *
     * //TODO: поправить на более интеллектуальный вариант
     */
    public static String parseTGTfromUrl(String url) {
        String[] separated = url.split("/");
        return separated[separated.length - 1];
    }
    //}}}


    //{{{ Разные сетевые хелперы
    /**
     * Дернуть произвольный rest-api за его урл.
     *
     * @param url полный урл на конечный rest-api
     * @param needAuth нужна ли авторизация для доступа. влияет на наличие специального заголовка в запросе
     * @return может вернуть null
     */
    public static HttpResponse performGet(String url, boolean needAuth) {
        try {
            HttpParams httpParams = new BasicHttpParams();
            setHttpParams(httpParams);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpGet get = new HttpGet(url);

            if(needAuth)
                setHeaders(get);

            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //do something with the response
//                Log.i("GET RESPONSE", EntityUtils.toString(entity));
            }
//            logHttpResponse(response);
            return response;
        } catch (Exception ex) {
            Log.e(TAG, "", ex);
            return null;
        }
    }

    public static HttpResponse performPost(String url, List<NameValuePair> params) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            post.setEntity(ent);
            HttpResponse responsePOST = client.execute(post);
            HttpEntity resEntity = responsePOST.getEntity();
            if (resEntity != null) {
//                Log.i("POST RESPONSE",EntityUtils.toString(resEntity));
            }
            return responsePOST;
        } catch (Exception ex) {
            Log.e(TAG, null, ex);
            return null;
        }
    }

    private static StringEntity serializeJsonObject(Object object) {
        final ObjectMapper mapper = new ObjectMapper();

        try {
            final String serializedObject = mapper.writeValueAsString(object);
            return new StringEntity(serializedObject);
        } catch (JsonProcessingException ex) {
            String message = "cannot serialize object " + object.getClass().getSimpleName();
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        } catch (UnsupportedEncodingException ex) {
            String message = "cannot serialize object " + object.getClass().getSimpleName();
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        }
    }

    public static <T> String getAsJSON(T object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(object);

            Log.d("getAsJson", object.getClass().getSimpleName() + "|" + data);

            return data;
        } catch (JsonProcessingException ex) {
            String message = "cant serialize class:\'" + object.getClass().getSimpleName() + "\'";
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        }
    }


    /**
     * Устанавливает разумные умолчания для сетевых запросов
     *
     * @param params
     */
    private static void setHttpParams(HttpParams params) {
//        final int some_reasonable_timeout = (int) (20L * DateUtils.SECOND_IN_MILLIS);
        final int some_reasonable_timeout = (int) (200L * DateUtils.SECOND_IN_MILLIS); // увеличил таймаут, иначе задачи не успевают прийти

        // тайм-аут
        HttpConnectionParams.setConnectionTimeout(params, some_reasonable_timeout);
        HttpConnectionParams.setSoTimeout(params, some_reasonable_timeout);

        // кодировка
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setHttpElementCharset(params, "UTF-8");
    }

    /**
     * Устанавливает заголовок, который должен передаваться
     * вместе с каждым запросом, требующим аутентификаци.
     *
     * @param request
     */
    private static void setHeaders(HttpRequestBase request) {
        final String auth_header_name = "X-TLI-DEVICE";
        final String android_id = App.prefs.getDeviceId();

        request.setHeader(auth_header_name, android_id);
    }

    /**
     * Вернуть содержимое HttpResponse строкой. <br>
     * Нужен для унифицированной обработки исключения IOException при вызове EntityUtils.toString()<br>
     *
     * @param response
     * @return результат выполнения EntityUtils.toString(@param)
     */
    public static String toString(HttpResponse response) {
        try {
            return EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
        } catch (IOException ex) {
            String message = "during converting HttpResponse.entity to String";
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            throw new RuntimeException(message);
        }
    }

    /**
     * Получить HTTP-код из HttpResponse
     *
     * @param response
     * @return код ответа
     */
    public static int getResponseCode(HttpResponse response) {
        return response.getStatusLine().getStatusCode();
    }
    //}}}

    //{{{ Логирование
    public static void logHeaders(HttpResponse response) {
        final Header[] headers = response.getAllHeaders();

        Log.v(TAG, "[HttpResponse headers]");
        for(Header h : headers)
            Log.v(TAG, h.toString());
    }

    public static void logHttpResponse(HttpResponse response, String tag) {
        Log.v(tag, toString(response));
    }

    public static void logHttpResponse(HttpResponse response) {
        logHttpResponse(response, TAG);
    }

    //}}}


    //{{{ Чтобы всего лишь сравнивать Task'и

    // список кодов атрибутов, которые могут прийти в поле keyName струтуры ViewCustomization
    // порядок и число сохранены элементов должны соответствовать таковым в структуре attributeCodeUINameIds[]
    private static final String[] attributeCodes = {
            "TaskCreateDate", "TaskName", "ProcessDescription",
            "TaskDescription", "ProcessName", "ProcessTypeName",
            "ProcessTypeDescription", "ProcessInitiator", "TaskCreator",
            "TaskStatus", "ProcessCreateDate", "TaskDeadline",
            "IsTaskSingleExecutor", "TaskChangeDate", "SourceSystem"
    };
    // список строковых ресурсов, соответствующих отдельным кодам атрибутов (см. список кодов атрибутов)
    // порядок и число сохранены элементов должны соответствовать таковым в структуре attributeCodes[]
    private static final int[] attributeCodeUINameIds = {
            R.string.TaskCreateDate_LocalName, R.string.TaskName_LocalName, R.string.ProcessDescription_LocalName,
            R.string.TaskDescription_LocalName, R.string.ProcessName_LocalName, R.string.ProcessTypeName_LocalName,
            R.string.ProcessTypeDescription_LocalName, R.string.ProcessInitiator_LocalName, R.string.TaskCreator_LocalName,
            R.string.TaskStatus_LocalName, R.string.ProcessCreateDate_LocalName, R.string.TaskDeadline_LocalName,
            R.string.IsTaskSingleExecutor_LocalName, R.string.TaskChangeDate_LocalName, R.string.SourceSystem_LocalName
    };

    public static String getAttributeUINameByCode(String codeName, Context ctx) {
        for(int i = 0; i < attributeCodes.length; i++) {
            if(attributeCodes[i].equals(codeName)) {
                return ctx.getResources().getString(attributeCodeUINameIds[i]);
            }
        }
        return "";
    }

    public static String getCodeByAttributeUIName(String attributeUIName, Context ctx) {
        for(int i=0; i<attributeCodeUINameIds.length; i++) {
            if(ctx.getResources().getString(attributeCodeUINameIds[i]).equals(attributeUIName)) {
                return attributeCodes[i];
            }
        }
        return "";
    }

    public static List<String> getMenuNames(List<ViewCustomization> sortMenuCodeNames, Context ctx) {
        List<String> ret = new ArrayList<String>();
        for(ViewCustomization v : sortMenuCodeNames) {
            final String code = v.getKeyName();

            for(int i=0; i<attributeCodes.length; i++) {
                if(attributeCodes[i].equals(code)) {
                    ret.add(ctx.getResources().getString(attributeCodeUINameIds[i]));
                }
            }
        }

        return ret;
    }

    private static final List<String> attributeCodesList;
    static
    {
        attributeCodesList = Arrays.asList(attributeCodes);
    }


    // значение мапа указывает, является ли поле датой. 1 - значит дата, 0 - строка
    // значение мапа является кодом атрибута, лежащим в структуре ViewCustomization в поле .keyName
    public static final HashMap<String, Integer> attributeCodeMap;
    static
    {
        attributeCodeMap = new HashMap<String, Integer>();
        attributeCodeMap.put("TaskCreateDate", 1);
        attributeCodeMap.put("TaskName", 0);
        attributeCodeMap.put("ProcessDescription", 0);
        attributeCodeMap.put("TaskDescription", 0);
        attributeCodeMap.put("ProcessName", 0);
        attributeCodeMap.put("ProcessTypeName", 0);
        attributeCodeMap.put("ProcessTypeDescription", 0);
        attributeCodeMap.put("ProcessInitiator", 0);
        attributeCodeMap.put("TaskCreator", 0);
        attributeCodeMap.put("TaskStatus", 0);
        attributeCodeMap.put("ProcessCreateDate", 1);
        attributeCodeMap.put("TaskDeadline", 0);
        attributeCodeMap.put("IsTaskSingleExecutor", 0);
        attributeCodeMap.put("TaskChangeDate", 1);
        attributeCodeMap.put("SourceSystem", 0);
    }

    /**
     * Компаратор для меню сортировки <br>
     * п. 2.8.1 ФС Андроид
     *
     * @param code код сортировки
     * @param descOrder направление сортировки. если true - значит по убыванию
     * @return подходящий компаратор на основе параметра code
     */
    public static Comparator<Task> getTaskComparator(final String code, final boolean descOrder) {
        if (!attributeCodeMap.containsKey(code))
            throw new IllegalStateException("illegal sorting attribute code name: \'" + code + "\'");

        return new Comparator<Task>() {
            @Override
            public int compare(Task lhs, Task rhs) {
//                String lField = CrocUtils.getValueByAttributeCode(code, lhs);
//                String rField = CrocUtils.getValueByAttributeCode(code, rhs);
                String lField = getValueByAttributeCode(lhs, code);
                String rField = getValueByAttributeCode(rhs, code);

                if(descOrder) {
                    String tmp = lField;
                    lField = rField;
                    rField = tmp;
                }
                
                if(attributeCodeMap.get(code) > 0) {   // значит имеем дело с датой, в формате строки
                    DateTime lDateTime;
                    DateTime rDateTime;

                    // если дата в строке отформатированна неверно, подставляем самую старую дату - 0 (=01.01.1970)
                    try {
                        lDateTime = new DateTime(lField, DateTimeZone.UTC);
                    } catch (IllegalArgumentException e) {
                        Log.e(TAG, null, e);
                        lDateTime = new DateTime(0,DateTimeZone.UTC);
                    }

                    try {
                        rDateTime = new DateTime(rField, DateTimeZone.UTC);
                    } catch (IllegalArgumentException e) {
                        Log.e(TAG, null, e);
                        rDateTime = new DateTime(0,DateTimeZone.UTC);
                    }

                    return lDateTime.isBefore(rDateTime) ? -1 : lDateTime.isAfter(rDateTime) ? 1 : 0;
                }

                return lField.compareTo(rField);
            }
        };
    }

    /**
     * Получить значение поля, соответствующее переданному коду аттрибута. <br>
     * Сюда должны приходить ТОЛЬКО валидные коды. Иначе вернет пустую строку.
     *
     * @param task
     * @param code
     * @return
     */
    @Deprecated // есть более правильный аналог в CrocUtils. нужен небольшой рефакторинг, чтобы их объединить
    public static String getValueByAttributeCode(Task task, String code) {
        String ret = "";

        if (code.equals("TaskCreateDate")) {
            ret = task.getCreateDateString();
        } else if (code.equals("TaskName")) {
            ret = task.getName();
        } else if (code.equals("ProcessDescription")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("TaskDescription")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("ProcessName")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("ProcessTypeName")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("ProcessTypeDescription")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("ProcessInitiator")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("TaskCreator")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("TaskStatus")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        } else if (code.equals("ProcessCreateDate")) {
            ret = task.getProcessCreateDateString();
        } else if (code.equals("TaskDeadline")) {
            ret = task.getDeadlineDateString();
        } else if (code.equals("IsTaskSingleExecutor")) {
            ret = (task.executantsCollection.size() == 1) ? "true" : "false";
        } else if (code.equals("TaskChangeDate")) {
            ret = task.getChangeDateString();
        } else if (code.equals("SourceSystem")) {
            ret = CrocUtils.getValueByAttributeCode(code, task);
        }

        // в полях могут быть null'евые значения
        if (ret == null)
            ret = "";

        return ret;
    }

    public static boolean isInSortingMenu(String name) {
        return attributeCodeMap.containsKey(name);
    }
    //}}}

    /**
     * Выполнить POST-запрос, в теле которого содержатся сериализованные данные
     *
     * @param url полный урл api-вызова (т.е. вместе с доменом и, опционально, ST-тикетом)
     * @param needAuth флаг, нужна ли авторизация
     * @param jsonData сериализованный(-ые) объект(-ы)
     *
     * @return может вернуть null
     */
    public static HttpResponse performPostJson(String url, boolean needAuth, String jsonData) {
        final String tag1 = "performPostJson";
        try {
            Log.d(tag1, url);

            HttpParams httpParams = new BasicHttpParams();
            setHttpParams(httpParams);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost post = new HttpPost(url);

            StringEntity jsonEntity = new StringEntity(jsonData, HTTP.UTF_8);
            jsonEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(jsonEntity);


            if(needAuth) {
                setHeaders(post);
            }

            HttpResponse response;
            try {
                response = client.execute(post);
            } catch( IOException e ) {
                // ошибки связи это ошибки связи и складывать их в logmessages незачем
                Log.w(tag1, "", e);
                return null;
            }

            if( response.getStatusLine() == null || response.getStatusLine().getStatusCode() != 200) {
                String status = response.getStatusLine() == null
                        ? "is null!"
                        : ""+response.getStatusLine().getStatusCode()+"/"+response.getStatusLine().getReasonPhrase();
                String logTitle = "POST "+url+": status "+status;
                Log.e(tag1, logTitle);
                String logBody = "Request data: "+jsonData+", response: "+toString(response);
                Log.e(tag1, logBody);
                if( !url.contains(LogMessageAPI.URI)) {
                    TableUtils.createLogMessage(logTitle, logBody);
                }
                return null;
            }

            return response;
        } catch (Exception ex) {
            String logTitle = "POST "+url+": failed with exception";
            String logBody = "Request data: "+jsonData+", error: "+Log.getStackTraceString(ex);

            Log.e(TAG, logTitle, ex);
            TableUtils.createLogMessage(logTitle, logBody);
            return null;
        }
    }

    public enum apiCallMethods {
        MODIFY("Modify"),
        DELETE("Delete");

        private String value;
        apiCallMethods(String v) {
            value = v;
        }
    }

    public static String getApiString(String className, apiCallMethods call, RestApiDate dateFrom, RestApiDate dateTo, int count) {
        final String ret = "/" + className + "/" + call.value + "/"
                + dateFrom.toRestDate() + "/"
                + dateTo.toRestDate() + "/" + String.valueOf(count);
        Log.d(TAG, "api: " + ret);
        return ret;
    }
    //}}}


    public static String getAttributeLocalNameByCode(String code, Context context) {
        final String attributeName = code + "_LocalName";
        final int resId = context.getResources().getIdentifier(attributeName, "string", context.getPackageName());
        return context.getResources().getString(resId);
    }

    public static Rect locateView(View v)
    {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try
        {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe)
        {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    public static String getStringResourceByName(String resName, Context ctx) {
        final int id = ctx.getResources().getIdentifier(resName, "string", ctx.getPackageName());
        return ctx.getResources().getString(id);
    }

    public static void showToastNotImplemented(Context ctx) {
        Toast.makeText(ctx, "Не реализовано", Toast.LENGTH_SHORT).show();
    }

}
