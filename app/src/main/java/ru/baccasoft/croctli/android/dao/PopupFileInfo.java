package ru.baccasoft.croctli.android.dao;

/**
 * Храним инфу о файле для передачи в адаптер, связанный с попапом
 */
public class PopupFileInfo {
    public String id;
    public String name;
    public int typeRes;    // рисунок файла
    public int size;   // в байтах

    public PopupFileInfo(String id, String name, int typeRes, int size) {
        this.id = id;
        this.name = name;
        this.typeRes = typeRes;
        this.size = size;
    }
}
