package ru.baccasoft.croctli.android.specialcondition;

import ru.baccasoft.croctli.android.gen.core.Task;

public class LogicalNot implements IExpression<Boolean>{

	private IExpression<Boolean> operand;
	
	public LogicalNot( IExpression<Boolean> operand ) {
		this.operand = operand;
	}

	@Override
	public Boolean compute(Task task) {
		return !operand.compute(task);
	}
	
	
}
