package ru.baccasoft.croctli.android.specialcondition.predicate;


import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class IsDeletedPredicate implements IBooleanAtom {

    public static final String ATOM_NAME = "isdeleted";

    @Override
    public Boolean compute(Task task) {
        return task.isIsDeleted();
    }

    @Override
    public IBooleanAtom create() {
        return new IsDeletedPredicate();
    }

    @Override
    public String getAtomName() {
        return ATOM_NAME;
    }
}
