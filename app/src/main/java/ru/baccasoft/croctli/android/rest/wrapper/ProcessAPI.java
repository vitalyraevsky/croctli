package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfProcess;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.ProcessSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class ProcessAPI implements IRestApiCall<ProcessSet, ArrayOfProcess, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = ProcessAPI.class.getSimpleName();

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Process/Delete/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public ProcessSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/Process/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(ProcessSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        final String api = "/Process/Delete/" + dateLastSync;
        boolean needAuth = true;

        String jsonData = Utils.getAsJSON(objects);

        RestClient.callSomeRestApi(api, needAuth, jsonData);
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfProcess objects) {
        final String api = "/Process/Modify/" + dateLastSync;
        boolean needAuth = true;

        String jsonData = Utils.getAsJSON(objects);

        RestClient.callSomeRestApi(api, needAuth, jsonData);
        return null;
    }
}
