package ru.baccasoft.croctli.android.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.ArrayScalarWrapper;
import ru.baccasoft.croctli.android.view.wrapper.ClassifierWrapper;
import ru.baccasoft.croctli.android.view.wrapper.ScalarWrapper;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test.ClassifierArrayWrapper;

/**
 *
 */

public class ErrandViewWrapper {
    private static final String TAG = ErrandViewWrapper.class.getSimpleName();

    Context mContext;
    Activity mActivity;

    List<EntityProperty> props;
    private Boolean globalReadOnly;


    /**
     *
     * @param props набор свойств для отображения. все они будут отображены в пределах одного экспандера.
     *              массивные свойства пойдут во вложенный экспандер
     * @param readOnly false - все вьюшки будут в режиме редактирования, true - все вьюшки в режиме просмотра,
     *                 null - редактирование или просмотр определяется по флагу EntityProperty.isReadOnly
     *                  для каждой вьюшки отдельно
     */
    public ErrandViewWrapper(List<EntityProperty> props, Activity activity, Boolean readOnly) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        this.props = props;
        this.globalReadOnly = readOnly;
    }

    public ViewGroup getLayout() {
        LinearLayout root = ViewUtils.makeVerticalContainer(mContext);
//        root.setBackgroundColor(mContext.getResources().getColor(R.color.text_FFFFFF));
        Log.d(TAG, "### ErrandViewWrapper");
        for(EntityProperty p : props) {
            // пропускаем невидимые свойства
            if(!p.isIsVisible())
                continue;
            if(p.isIsArrayProperty() ) {
                if(ViewUtils.isNotEmpty(p.getClassifierTypeId())) {
                    Preload.loadClassifierItemsByClassifierId(p);
                    Preload.loadSelectedItems(p);
                }
            }
            if(ViewUtils.isNotEmpty(p.getClassifierTypeId()) && !p.isIsArrayProperty()) {
                Preload.loadClassifiers(p);
            }
            View vp = getPropertyLayout(p);
            if(vp != null){
                vp.setPadding(0,3,0,0);
                root.addView(vp);
            }
        }
        return root;
    }

    /**
     * Создает нужную View в зависимости от типа свойства (скаляр, справочник и т.д.)
     *
     * @param p свойство EntityProperty, для которого нужно сделать вьюшку
     * @return готовую заполненную вьюшку
     */
    private View getPropertyLayout(EntityProperty p) {
        Log.d(TAG, "EntityPropertyId: \'"+p.getId() + "\'");

        // 2.8.2.1	Скалярные свойства
        if(ViewUtils.isNotEmpty(p.getScalarType()) && !p.isIsArrayProperty()) {
            ScalarWrapper scalarWrapper = new ScalarWrapper(mActivity);
            return scalarWrapper.getLayout(p, globalReadOnly);
        }

        // 2.8.2.3	Массивные свойства
        // Это может быть
        //      либо массивное необъектное свойство (string, date etc)
        //      либо массивное справочное свойство
        // Это НЕ может быть
        //      массивное сложное свойство
        if(p.isIsArrayProperty() ) {
            Log.d(TAG, "arrayProperty with title:\'" + p.getDisplayGroup() + "\'");
            if(ViewUtils.isNotEmpty(p.getScalarType())) {
                // массив скаляров
                Log.d(TAG, "scalar array");
                return new ArrayScalarWrapper(mActivity).getLayout(p, globalReadOnly);
            }
            if(ViewUtils.isNotEmpty(p.getClassifierTypeId())) {
                // массив на основе справочника
                //TODO: новый врапер
                Log.d(TAG, "classifier array");
                return new ClassifierArrayWrapper(mContext)
                        .getLayout(p, globalReadOnly);
//                return new ArrayPropertyWrapper(mActivity, nestLevel)
//                        .getLayout(p, globalReadOnly);
            }
        }

        // 2.8.2.2	Справочные свойства
        // (DisplayMode = 0 или DisplayMode = 1)
        // должно вызываться после проверки наличия скалярного типа,
        // потому что поля типа "date" и "dateTime" также могут иметь непустое поле classifierTypeId,
        // и в этом случае их следует обрабатывать как справочник
        // также нужно вызывать после построения массивных свойств
        if(ViewUtils.isNotEmpty(p.getClassifierTypeId()) && !p.isIsArrayProperty()) {
            Log.d(TAG, "classifier property");
            return new ClassifierWrapper(mContext).getLayout(p, globalReadOnly);
        }

        // 2.8.2.2	Свойства с аттачментами
        if(p.getBinaryValue() != null) {
            return null;    //TODO:
        }

        throw new IllegalStateException("can't make view. unrecognized EntityProperty with id:\'" + p.getId() + "\'");
    }
}
