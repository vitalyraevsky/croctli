
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for SourceSystem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SourceSystem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UseWLIDelegation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="FastCallbackRestUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AllowHandmadeDelegateCore" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SourceSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "description",
    "useWLIDelegation",
    "fastCallbackRestUrl",
    "allowHandmadeDelegateCore"
})
@DatabaseTable(tableName = "source_system")
public class SourceSystem {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(id = true, columnName = "id", dataType = DataType.STRING)
    protected String id;

    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Name")
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;

    @XmlElement(name = "Description", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Description")
    @DatabaseField(columnName = "description", dataType = DataType.STRING)
    protected String description;

    @XmlElement(name = "UseWLIDelegation", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("UseWLIDelegation")
    @DatabaseField(columnName = "use_wli_delegation", dataType = DataType.BOOLEAN)
    protected boolean useWLIDelegation;

    @XmlElement(name = "FastCallbackRestUrl", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("FastCallbackRestUrl")
    @DatabaseField(columnName = "fast_callback_rest_url", dataType = DataType.STRING)
    protected String fastCallbackRestUrl;

    @XmlElement(name = "AllowHandmadeDelegateCore", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("AllowHandmadeDelegateCore")
    @DatabaseField(columnName = "allow_handmade_delegate_core", dataType = DataType.BOOLEAN)
    protected boolean allowHandmadeDelegateCore;

    @DatabaseField(columnName = "server_time", dataType = DataType.STRING)
    protected String serverTime;

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the useWLIDelegation property.
     * 
     */
    public boolean isUseWLIDelegation() {
        return useWLIDelegation;
    }

    /**
     * Sets the value of the useWLIDelegation property.
     * 
     */
    public void setUseWLIDelegation(boolean value) {
        this.useWLIDelegation = value;
    }

    /**
     * Gets the value of the fastCallbackRestUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFastCallbackRestUrl() {
        return fastCallbackRestUrl;
    }

    /**
     * Sets the value of the fastCallbackRestUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFastCallbackRestUrl(String value) {
        this.fastCallbackRestUrl = value;
    }

    /**
     * Gets the value of the allowHandmadeDelegateCore property.
     * 
     */
    public boolean isAllowHandmadeDelegateCore() {
        return allowHandmadeDelegateCore;
    }

    /**
     * Sets the value of the allowHandmadeDelegateCore property.
     * 
     */
    public void setAllowHandmadeDelegateCore(boolean value) {
        this.allowHandmadeDelegateCore = value;
    }

}
