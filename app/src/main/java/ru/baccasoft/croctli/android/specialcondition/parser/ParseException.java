package ru.baccasoft.croctli.android.specialcondition.parser;

public class ParseException extends Exception {
	public ParseException( LexerSource source, String message ) {
		super( "Error parsing "+source.getExpression()+": "+message+". Unparsed substring: "+source.getUnparsedTail() );
	}
	
	public ParseException( String message ) {
		super( message );
	}
}
