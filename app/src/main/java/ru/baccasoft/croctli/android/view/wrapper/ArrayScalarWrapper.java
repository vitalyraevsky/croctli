package ru.baccasoft.croctli.android.view.wrapper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.ScalarArrayWithTitle;

import java.util.Arrays;
import java.util.List;

import static ru.baccasoft.croctli.android.view.util.ViewUtils.SCALAR_TYPE;
/**
 * Делает вьюшку
 * для массивных скалярных свойств.
 * Т.е. for EntityProperty with isArray==true, scalarType!=null, classifierId==nul
 *
 */
public class ArrayScalarWrapper implements IWrapper {
    @SuppressWarnings("unused")
    private final static String TAG = ArrayScalarWrapper.class.getSimpleName();

    private Activity mActivity;
    private Context mContext;
    private boolean readOnly;

    public ArrayScalarWrapper(Activity activity) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        SCALAR_TYPE scalarType = SCALAR_TYPE.forValue(p.getScalarType());
        List<String> items = restoreContent(p.getValue());

        ScalarArrayWithTitle view = new ScalarArrayWithTitle(
                mActivity, p.getDisplayGroup(), scalarType, items, readOnly, p.getId());


        //TODO: возвращать не скаляр, а вьюшку для массивных справочных свойств
//        // с DisplayMode=0 (Название: дата, дата, дата)
//        StringScalarWithTitle view = new StringScalarWithTitle(
//                mContext, p.getDisplayGroup(), items, readOnly, p.getId());
        return view;
    }

    private void validate(EntityProperty p) {
        if(!p.isIsArrayProperty())
            throw new IllegalStateException("field isArrayProperty must be true");
        if(ViewUtils.isNotEmpty(p.getClassifierTypeId()))
            throw new IllegalStateException("field classifierId must be empty. " +
                    "probably trying to make wrong view for EP with id:\'" + p.getId());
        if(!SCALAR_TYPE.isValid(p.getScalarType()))
            throw new IllegalStateException("scalarType:\'" + p.getScalarType() + "\' is not valid" +
                    "must be one of these: " + Arrays.asList(ViewUtils.SCALAR_TYPE.values()));
    }

    /**
     * Распарсить данные, сохраненные в строку:
     * все значения разделены символом @
     * @param value
     * @return
     */
    private List<String> restoreContent(String value) {
        Log.d(TAG, "initial value:\'" + value + "\'");
        String values[] = value.split("@");
        Log.d(TAG, "parsed values:\'" + values);
        return Arrays.asList(values);
    }


}
