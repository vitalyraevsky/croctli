package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.SystemUser;
import ru.baccasoft.croctli.android.gen.core.SystemUserSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.SystemUserAPI;

import java.util.List;

public class SystemUserSync implements ISync {
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        SystemUserAPI systemUserAPI = new SystemUserAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<SystemUserSet, SystemUser>();

        @SuppressWarnings("unchecked")
        List<SystemUser> systemUsers = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, systemUserAPI);

        for(SystemUser su : systemUsers) {
            TableUtils.createOrUpdateSystemUser(su);
        }
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        SystemUserAPI systemUserAPI = new SystemUserAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> systemUserDeleteIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, systemUserAPI);

        TableUtils.deleteSystemUsers(systemUserDeleteIds);
    }
}
