package ru.baccasoft.croctli.android.view.validator;

import android.content.Context;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class MaxValueValidator extends BaseValidator {

    public MaxValueValidator(Context context, EntityProperty entityProperty, String valueToValidate) {
        super(context, entityProperty, valueToValidate, "Validation_PropertyPresenter_MaxValue");
    }

    @Override
    public boolean isValid() {
        String maxValue = entityProperty.getMaxValue();
        String type = entityProperty.getScalarType();

        if(ViewUtils.isEmpty(type))
            return true;

        if(type.equals("int")) {
            try {
                int maxValueConverted = Integer.parseInt(maxValue);
                int valueConverted = Integer.parseInt(valueToValidate);

                return (valueConverted <= maxValueConverted);
            } catch (NumberFormatException ex) {
                return true;
            }
        } else if(type.equals("float") || type.equals("decimal")) {
            try {
                float maxValueConverted = Float.parseFloat(maxValue);
                float valueConverted = Float.parseFloat(valueToValidate);

                return (valueConverted <= maxValueConverted);
            } catch (NumberFormatException ex) {
                return true;
            }
        } else {
            return true;
        }
    }
}
