package ru.baccasoft.croctli.android.view.validator;

import android.content.Context;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class NotNullValidator extends BaseValidator {

    public NotNullValidator(Context context, EntityProperty entityProperty, String valueToValidate) {
        super(context, entityProperty, valueToValidate, "Validation_PropertyPresenter_NotNull");
    }

    @Override
    public boolean isValid() {
        return ViewUtils.isNotEmpty(valueToValidate);
    }
}
