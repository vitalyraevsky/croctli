package ru.baccasoft.croctli.android.view.validator;

import android.content.Context;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class MinValueValidator extends BaseValidator {

    protected MinValueValidator(Context context, EntityProperty entityProperty, String valueToValidate) {
        super(context, entityProperty, valueToValidate, "Validation_PropertyPresenter_MinValue");
    }

    @Override
    public boolean isValid() {
        String minValue = entityProperty.getMinValue();
        String type = entityProperty.getScalarType();

        if(ViewUtils.isEmpty(type))
            return true;

        if(type.equals("int")) {
            try {
                int minValueConverted = Integer.parseInt(minValue);
                int valueConverted = Integer.parseInt(valueToValidate);

                return (valueConverted >= minValueConverted);
            } catch (NumberFormatException ex) {
                return true;
            }
        } else if(type.equals("float") || type.equals("decimal")) {
            try {
                float minValueConverted = Float.parseFloat(minValue);
                float valueConverted = Float.parseFloat(valueToValidate);

                return (valueConverted > minValueConverted);
            } catch (NumberFormatException ex) {
                return true;
            }
        } else {
            return true;
        }
    }
}
