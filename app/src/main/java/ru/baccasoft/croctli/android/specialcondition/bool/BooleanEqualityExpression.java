package ru.baccasoft.croctli.android.specialcondition.bool;

import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.IExpression;
import ru.baccasoft.croctli.android.specialcondition.parser.ParseException;

public class BooleanEqualityExpression implements IExpression<Boolean> {
    @SuppressWarnings("unused")
    private static final String TAG = BooleanEqualityExpression.class.getSimpleName();

	private final IExpression<Boolean> left;
	private final IExpression<Boolean> right;
	/** истина, если сравнение на равенство */
	private final boolean checkingForEquality;
	
	public BooleanEqualityExpression( IExpression<Boolean> left, String operator, IExpression<Boolean> right) throws ParseException {
		this.left = left;
		this.right = right;
		if( "==".equals(operator) ) {
			checkingForEquality = true;
		} else if( "!=".equals(operator) ) {
			checkingForEquality = false;
		} else {
            String message = "Expected == or !=, found "+operator;
            TableUtils.createLogMessage(message);
            Log.e(TAG, message);
			throw new ParseException(message);
		}
	}

	@Override
	public Boolean compute(Task task) {
		boolean leftResult = left.compute(task);
		boolean rightResult = right.compute(task);

		// кто помнит, что такое "жигалкинский плюс", может взять с полки пирожок!
		return checkingForEquality ^ (leftResult ^ rightResult);
	}
}