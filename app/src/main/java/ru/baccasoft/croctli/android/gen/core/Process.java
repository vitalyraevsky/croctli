
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;


/**
 * <p>Java class for Process complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Process">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ProcessTypeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InitiatorId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LinkTask" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdapterId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comments" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfComment"/>
 *         &lt;element name="ProcessTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InitiatorName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceSystemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Process", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "description",
    "createDate",
    "processTypeId",
    "initiatorId",
    "linkTask",
    "adapterId",
    "comments",
    "processTypeName",
    "initiatorName",
    "sourceSystemName"
})
@JsonIgnoreProperties({"createDate"})
@DatabaseTable(tableName = "process")
public class Process {

    @JsonProperty("Id")
    @DatabaseField(columnName = "Id", id = true, dataType = DataType.STRING)
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @JsonProperty("Name")
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String name;

    @JsonProperty("Description")
    @DatabaseField(columnName = "description", dataType = DataType.STRING)
    @XmlElement(name = "Description", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String description;


    @XmlElement(name = "CreateDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;

    @JsonProperty("CreateDate")
    @DatabaseField(columnName = "create_date", dataType = DataType.STRING)
    protected String createDateString;

    @JsonProperty("ProcessTypeId")
    @XmlElement(name = "ProcessTypeId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_id", dataType = DataType.STRING)
    protected String processTypeId;

    @JsonProperty("InitiatorId")
    @XmlElement(name = "InitiatorId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "initiator_id", dataType = DataType.STRING)
    protected String initiatorId;

    @JsonProperty("LinkTask")
    @XmlElement(name = "LinkTask", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "link_task", dataType = DataType.STRING)
    protected String linkTask;

    @JsonProperty("AdapterId")
    @XmlElement(name = "AdapterId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "adapter_id", dataType = DataType.STRING)
    protected String adapterId;

    @JsonProperty("Comments")
    @XmlElement(name = "Comments", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected ArrayOfComment comments;

    @ForeignCollectionField
    public Collection<Comment> commentsCollection;

    @JsonProperty("ProcessTypeName")
    @XmlElement(name = "ProcessTypeName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_name", dataType = DataType.STRING)
    protected String processTypeName;

    @JsonProperty("InitiatorName")
    @XmlElement(name = "InitiatorName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "initiator_name", dataType = DataType.STRING)
    protected String initiatorName;

    @JsonProperty("SourceSystemName")
    @XmlElement(name = "SourceSystemName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "source_system_name", dataType = DataType.STRING)
    protected String sourceSystemName;

    @DatabaseField(columnName = "server_time", dataType = DataType.STRING)
    protected String serverTime;


    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the processTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeId() {
        return processTypeId;
    }

    /**
     * Sets the value of the processTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeId(String value) {
        this.processTypeId = value;
    }

    /**
     * Gets the value of the initiatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitiatorId() {
        return initiatorId;
    }

    /**
     * Sets the value of the initiatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitiatorId(String value) {
        this.initiatorId = value;
    }

    /**
     * Gets the value of the linkTask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkTask() {
        return linkTask;
    }

    /**
     * Sets the value of the linkTask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkTask(String value) {
        this.linkTask = value;
    }

    /**
     * Gets the value of the adapterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdapterId() {
        return adapterId;
    }

    /**
     * Sets the value of the adapterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdapterId(String value) {
        this.adapterId = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfComment }
     *     
     */
    public ArrayOfComment getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfComment }
     *     
     */
    public void setComments(ArrayOfComment value) {
        this.comments = value;
    }

    /**
     * Gets the value of the processTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeName() {
        return processTypeName;
    }

    /**
     * Sets the value of the processTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeName(String value) {
        this.processTypeName = value;
    }

    /**
     * Gets the value of the initiatorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitiatorName() {
        return initiatorName;
    }

    /**
     * Sets the value of the initiatorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitiatorName(String value) {
        this.initiatorName = value;
    }

    /**
     * Gets the value of the sourceSystemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemName() {
        return sourceSystemName;
    }

    /**
     * Sets the value of the sourceSystemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemName(String value) {
        this.sourceSystemName = value;
    }

    public String getCreateDateString() {
        return createDateString;
    }

    public void setCreateDateString(String createDateString) {
        this.createDateString = createDateString;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}
