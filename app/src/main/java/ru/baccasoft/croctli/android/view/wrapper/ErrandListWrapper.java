package ru.baccasoft.croctli.android.view.wrapper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.ErrandViewWrapper;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

import java.util.List;

/**
 * Создает View со вложенными вьюшками по одной на каждое поручение.
 * На вход принимает корневой EP, указывающий на все доступные резолюции.
 */
public class ErrandListWrapper implements IWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = ErrandListWrapper.class.getSimpleName();

    private Activity mActivity;
    private Context mContext;
    private Resources mResources;

    public ErrandListWrapper(Activity activity) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mResources = mActivity.getResources();
    }

    @Override
    public ViewGroup getLayout(EntityProperty ep, Boolean globalReadOnly) {
        LinearLayout errandsContainer = ViewUtils.makeVerticalContainer(mActivity.getApplicationContext());
        errandsContainer.setBackgroundColor(mResources.getColor(R.color.text_F2F2F2));

        List<EntityProperty> childRootEps = TableUtils.getEntityPropertyChildsById(ep.getId());
        if(childRootEps.size() == 0) {
            return errandsContainer;
        }

        for(EntityProperty p : childRootEps) {
            List<EntityProperty> cps = TableUtils.getEntityPropertyChildsById(p.getId());
            ErrandViewWrapper wrapper = new ErrandViewWrapper(cps, mActivity, globalReadOnly);
            ViewGroup errandView = wrapper.getLayout();

            ViewUtils.makeErrandViewPretty(errandView, mResources);
            errandsContainer.addView(errandView, ViewUtils.errandViewLP);
        }
        return errandsContainer;
    }
}
