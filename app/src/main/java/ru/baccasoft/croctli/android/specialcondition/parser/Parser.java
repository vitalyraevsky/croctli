package ru.baccasoft.croctli.android.specialcondition.parser;

import ru.baccasoft.croctli.android.specialcondition.IExpression;
import ru.baccasoft.croctli.android.specialcondition.LogicalAnd;
import ru.baccasoft.croctli.android.specialcondition.LogicalNot;
import ru.baccasoft.croctli.android.specialcondition.LogicalOr;
import ru.baccasoft.croctli.android.specialcondition.bool.BooleanAtomFactory;
import ru.baccasoft.croctli.android.specialcondition.bool.BooleanEqualityExpression;
import ru.baccasoft.croctli.android.specialcondition.date.*;
import ru.baccasoft.croctli.android.specialcondition.predicate.BooleanTrue;

/**
 * 
 * 
<code>
Expression ::= NotExpression | AndExpression | OrExpression | BooleanAtom | Comparison.
NotExpression ::= '!' Predicate.
AndExpression ::= Expression '&&' Expression.
OrExpression ::= Exression '||' Expression.
Comparison ::= ( BooleanAtom ( '==' | '!=' ) BooleanAtom )
			|	(DateAtom ( '==' | '!=' | '>' | '<' | '>=' | '<=' ) DateAtom).
BooleanAtom ::= Predicate | 'true' | 'false'.
Predicate ::= (одно из набора ключевых слов).
DateAtom ::= (одно из набора ключевых слов).
<code>
 * 
 */

public final class Parser {
	
	/** Вся строка, подлежащая разбору. */
	private final String expression;
	
	private LexemChain lexems;
	
	public Parser( String expression ) throws ParseException {
        if( expression == null || expression.trim().length() == 0 ) {
            this.expression = null;
            this.lexems = null;
        } else {
            this.expression = expression.toLowerCase();
            this.lexems = Lexer.toLexemChain(new LexerSource(this.expression));
        }
	}
	
	public IExpression<Boolean> parse() throws ParseException {
        if( expression == null ) {
            return new BooleanTrue();
        }

		return parseOrChain();
	}
	
	private ParseException parseException( String message ) {
		return new ParseException( 
				"Error parsing "+expression+": "+message+" at "+lexems.current().getParserTail());
	}
	
	/**
	 * Вход в парсер: на верхнем уровне у нас цепочка ИЛИ (возможно, вырожденная в одно простое 
	 * булево выражение).
	 */
	private LogicalOr parseOrChain() throws ParseException {
		LogicalOr result = new LogicalOr();
		
		for(;;) {
			IExpression<Boolean> currentAtom = parseCurrentBooleanAtom();

			if( !lexems.hasNext() ) {
				result.append(currentAtom);
				return result;
			}
			
			lexems.next();
			
			switch( lexems.current().getType() ) {
			case BooleanOr:
				result.append(currentAtom);
				break;
			case BooleanAnd:
				result.append( parseAndChain( currentAtom ) );
				if( !lexems.hasNext() ) {
					return result;
				}
				break;
			default:
				throw parseException("expected AND or OR");
			}
			stepToNextLexem();
		}
		
	}
	
	/**
	 * Разобрать цепочку логических И.
	 * <p/>
	 * Первое простое булево выражение уже разобрано, мы стоим на лексеме "И".
	 * <p/>
	 * После окончания разбора мы либо будем стоять на последней лексеме "глобально разбираемого"
	 * выражения, либо на лексеме "ИЛИ" после разобранной цепочки. Любая другая лексема приведет
	 * к ошибке. 
	 */
	private LogicalAnd parseAndChain( IExpression<Boolean> firstAtom ) throws ParseException {
		LogicalAnd result = new LogicalAnd();
		result.append(firstAtom);
		stepToNextLexem();
		
		for(;;) {
			result.append(parseCurrentBooleanAtom());
			
			if( !lexems.hasNext() ) {
				return result;
			} 
			stepToNextLexem();
			
			switch( lexems.current().getType() ) {

			case BooleanOr:
				return result;
			
			case BooleanAnd:
				stepToNextLexem();
				break;
			
			default:
				throw parseException("expected AND or OR");
			}
		}
	}

	/**
	 * Разобрать простое булево выражение, на первой лексеме которого мы стоим.
	 * После окончания разбора мы будем стоять на последней лексеме разобранного выражения.
	 */
	private IExpression<Boolean> parseCurrentBooleanAtom() throws ParseException {
		switch( lexems.current().getType() ) {
		case BooleanNot:
			stepToNextLexem();
			return new LogicalNot( parseCurrentBooleanAtom() );
		case BooleanAtom:
			return parseCurrentPredicateOrBooleanComparison();
		case DateAtom:
			return parseCurrentDateComparison();
		default:
			throw parseException("expected boolean");
		}
	}
	
	/**
	 * Разобрать сравнение дат, на первой лексеме которого мы стоим. 
	 * После окончания разбора будем стоять на последней лексеме разобранного выражения.
	 */
	private IExpression<Boolean> parseCurrentDateComparison() throws ParseException {
		IDateOperand left = toDateAtom(lexems.current());
		
		stepToNextLexem();
		
		switch( lexems.current().getType() ) {
		case EqualityOperator:
		case ComparisonOperator:
			// no error, continue
			break;
		default:
			throw parseException("expected dates comparison");
		}
		String operator = lexems.current().getLexemString();
		
		stepToNextLexem();
		
		if( !lexems.current().getType().equals(LexemType.DateAtom)) {
			throw parseException("expected date atom");
		}
		IDateOperand right = toDateAtom(lexems.current());
		
		return new DatesComparison(left, operator, right);
	}
	
	private IDateOperand toDateAtom( ILexem lexem ) throws ParseException {
		if( DatetimeNowAtom.NAME.equals(lexem.getLexemString())) {
			return new DatetimeNowAtom();
		} else if( DeadlineValueAtom.NAME.equals(lexem.getLexemString())) {
			return new DeadlineValueAtom();
		} else if( lexem instanceof AddDaysToNowLexem ) {
			return new DatetimeAddDaysToNowAtom(((AddDaysToNowLexem)lexem).days);
		} else {
			throw parseException( "unrecognized date atom" );
		}
	}
	/**
	 * Разобрать единственный предикат, на лексеме которого мы стоим, или сравнение булевых "атомов". 
	 * После окончания разбора будем стоять на последней лексеме разобранного выражения.
	 */
	private IExpression<Boolean> parseCurrentPredicateOrBooleanComparison() throws ParseException {
		IExpression<Boolean> left = BooleanAtomFactory.createAtom( lexems.current() );
		
		ILexem operator = lexems.peekNext(); // заглядываем на шаг вперед, но не шагаем!
		if( operator == null || !operator.getType().equals(LexemType.EqualityOperator)) {
			return left; // одиночный булевый "атом"
		}
		stepToNextLexem(); // стали на оператор сравнения
		stepToNextLexem(); // а теперь стали на следующую лексему
		if( !lexems.current().getType().equals(LexemType.BooleanAtom)) {
			throw parseException("expected boolean atom");
		}
		IExpression<Boolean> right = BooleanAtomFactory.createAtom(lexems.current());
		
		return new BooleanEqualityExpression(left, operator.getLexemString(), right);
	}
	
	private void stepToNextLexem() throws ParseException {
		if( !lexems.hasNext() ) {
			throw new ParseException( "Unexpected end of string: "+expression );
		}
		lexems.next();
	}
}
