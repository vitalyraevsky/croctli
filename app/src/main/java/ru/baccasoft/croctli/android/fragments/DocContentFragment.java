package ru.baccasoft.croctli.android.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.dao.PopupFileInfo;
import ru.baccasoft.croctli.android.fragments.documents.AttachmentViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.AudioViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.DocViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.ImageViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.PDFViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.TextViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.VideoViewFragment;
import ru.baccasoft.croctli.android.fragments.documents.converters.DocOldFormatProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.DocXProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.PPTProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.PPTXProcessor;
import ru.baccasoft.croctli.android.fragments.documents.converters.SpreadsheetProcessor;
import ru.baccasoft.croctli.android.gen.core.Attachment;
import ru.baccasoft.croctli.android.gen.core.ConflictData;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.view.PagerContainer;
import ru.baccasoft.croctli.android.view.PopupWrapper;
import ru.baccasoft.croctli.android.view.event.SetFavoriteEvent;

// TODO: Требуется рефакторинг много всео помещено в данный фрагмент: работа с заметкой и конфликтами. Нужно разделить все это по отдельным контроллерам
public class DocContentFragment extends Fragment implements ConflictFragment.ConflictMessageClosedListener,
        IAttachmentOpener {

    @SuppressWarnings("unused")
    private static final String TAG = DocContentFragment.class.getSimpleName();

    private enum DOC_TYPE { DOC, DOCX, PDF, EXCEL, PPT, PPTX, VIDEO, AUDIO, IMAGE, TXT, RTF, HTML , UNKNOWN };

    private PagerContainer pageContainer;
    private LinearLayout docContent;
    private ImageView noticeView;
    private Task task;

    private View attachLayout;
    private TextView attachCount;

    private ViewGroup conflictsLayout;
    private List<ConflictData> conflicts;

    private int conflictsShowed;

    private TextView noticeMessage;
    private static boolean showNoticeMessage;

    private static Animation fileIconAnimation;

    private ImageView attachedFilesIcon;
    private PopupWindow filesPopupWindow;
    private List<PopupFileInfo> popupFilesData;

    private Map<String, Attachment> attachmentMap;

    private ViewPager pager;
    private Attachment currentAttachment = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.doc_content, container, false);

        task = ((TaskBrowseActivity) getActivity()).getActiveTask();

        noticeView = (ImageView) view.findViewById(R.id.notice);

        final View noticePlan = view.findViewById(R.id.notice_plan);
        noticeMessage = (TextView) view.findViewById(R.id.notice_text);
        noticeMessage.setText( task.getNotice() != null ? task.getNotice() : "" );
        noticeMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNoticePopup();
            }
        });
        noticeMessage.setVisibility( showNoticeMessage ? View.VISIBLE : View.GONE );

        noticeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (noticeMessage.getVisibility() == View.VISIBLE) {

                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_down);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            noticeMessage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    noticePlan.startAnimation(anim);
                    showNoticeMessage = false;

                } else {
                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_up);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            noticeMessage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    noticePlan.startAnimation(anim);
                    showNoticeMessage = true;
                }

            }
        });

        fileIconAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.file_icon_press);

        updateNoticeImage();

        pageContainer = (PagerContainer) view.findViewById(R.id.attach_container);
        pageContainer.getViewPager().setPageMargin(0);
        pageContainer.getViewPager().setHorizontalFadingEdgeEnabled(false);
        pageContainer.getViewPager().setVerticalFadingEdgeEnabled(false);

        docContent = (LinearLayout) view.findViewById(R.id.content);
        attachLayout = view.findViewById(R.id.attach_layout);
        attachCount = (TextView) view.findViewById(R.id.attach_count);

        conflictsLayout = (ViewGroup) view.findViewById(R.id.conflicts);
        conflictsLayout.setVisibility(View.GONE);

        popupFilesData = initAttachemnts(pageContainer, task.getId());
        initConflicts(task);

        //{{{ готовим попап
        attachedFilesIcon = (ImageView) view.findViewById(R.id.attach_icon);
        if( popupFilesData.isEmpty() ) {
            attachedFilesIcon.setVisibility(View.GONE);
        } else {
            attachedFilesIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (filesPopupWindow == null) filesPopupWindow = PopupWrapper.newPopup(
                            getActivity(), popupFilesData, DocContentFragment.this);

                    attachedFilesIcon.startAnimation(fileIconAnimation);

                    if (filesPopupWindow.isShowing()) {
                        filesPopupWindow.dismiss();
                    } else {
                        filesPopupWindow.showAsDropDown(attachedFilesIcon);
                    }

                }
            });
        }
        //}}}

        if ( pageContainer.getViewPager().getAdapter() != null ) {
            pageContainer.getViewPager().setOffscreenPageLimit(pageContainer.getViewPager().getAdapter().getCount());
            attachCount.setText( String.valueOf( pageContainer.getViewPager().getAdapter().getCount() ) );
        }

        return view;
    }

    private List<PopupFileInfo> initAttachemnts(PagerContainer container, String taskId) {
        try {

            List<Attachment> attachments = App.getDatabaseHelper().getEntityPropertyDAO().getAttachmentsByTaskId(taskId);
            attachmentMap = new HashMap<String, Attachment>(attachments.size());
            for(Attachment at : attachments) attachmentMap.put(at.getId(), at);

//            List<Attachment> attachments = App.getDatabaseHelper().getAttachmentDAO().queryForAll();
            pager = container.getViewPager();

            Log.d(TAG, "entities with attachment " + attachments.size());

            if( attachments.size() > 0 ) {

                AttachmentPagerAdapter adapter = new AttachmentPagerAdapter(attachments);
                container.setOnPageChangedListener(adapter);
                pager.setAdapter(adapter);
                pager.setOffscreenPageLimit( adapter.getCount() );
                pager.setPageMargin(15);
                pager.setClipChildren(false);
                pager.setPageMargin(-50);
                pager.setHorizontalFadingEdgeEnabled(true);
                pager.setFadingEdgeLength(30);

                // Сразу показываем первый докмуент
                displayAttachment( attachments.get(0) );


            }

            List<PopupFileInfo> data = new ArrayList<PopupFileInfo>(attachments.size());
            for(Attachment at : attachments) {
                data.add(new PopupFileInfo(
                        at.getId(),
                        at.getName(),
                        getTypeIcon(at.getName()),
                        at.getSize()));
            }
            return data;

        } catch (SQLException e) {
            Log.e(TAG,"not able to get EntityProperties : " + e.getMessage());
        }

        return new ArrayList<PopupFileInfo>();
    }

    public void initConflicts(Task task){

        try {
            conflicts = App.getDatabaseHelper().getConflictDataDao().getByTask(task);
            for(ConflictData conflict : conflicts){
                addConflictMessage(conflict);
            }
            Log.d(TAG,"conflicts size : " + conflicts.size());
        } catch (SQLException e) {
            Log.e(TAG,"Sql exception",e);
        }
//      testConflicts();
    }

    private synchronized void addConflictMessage( ConflictData conflict ) {
        ConflictFragment conflictFragment = ConflictFragment.newInstance(conflict);
        conflictFragment.setListener(this);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.conflicts,conflictFragment).commit();
        conflictsShowed++;
        conflictsLayout.setVisibility(View.VISIBLE);
        Log.d(TAG,"conflict showed, conflict size : " + conflictsShowed);
    }

    @Override
    public synchronized void conflictClosed() {
        conflictsShowed--;
        Log.d(TAG,"conflict closed, conflict size : " + conflictsShowed);
        if(conflictsShowed == 0)
            conflictsLayout.setVisibility(View.GONE);
    }

    private class AttachmentPagerAdapter extends PagerAdapter implements  ViewPager.OnPageChangeListener {

        private List<Attachment> entities;
        private Map<Integer,View> positionToView;


        public AttachmentPagerAdapter(List<Attachment> entities){
            this.entities = entities;
            positionToView = new HashMap<Integer, View>(entities.size());
        }

        @Override
        public Object instantiateItem( ViewGroup container, final int position ) {

            final Context cxt = DocContentFragment.this.getActivity();

            final Attachment attachment = entities.get(position);

            Log.d(TAG,"attachment mime-type: " + attachment.getMimeType());
            Log.d(TAG,"attachment id: " + attachment.getId());
            Log.d(TAG,"attachment name: " + attachment.getName());
            Log.d(TAG,"attachment local location: " + attachment.getLocalLocation());
            Log.d(TAG,"attachment track key: " + attachment.getTrackKey());
            Log.d(TAG,"attachment check sum: " + attachment.getCheckSum());
            Log.d(TAG,"attachment size: " + attachment.getSize());

//              int icon = R.drawable.icon_word;
//            String docname = attachment.getName();
            int fileTypeIcon = getTypeIcon(attachment.getName());

            LinearLayout layout = new LinearLayout(cxt);

            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setGravity(Gravity.CENTER);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.MATCH_PARENT));

            final ImageView pic = new ImageView(cxt);
            pic.setImageResource(fileTypeIcon);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins( 10, 0, 10, 0 );
            pic.setLayoutParams(params);

            TextView text = new TextView(cxt);
            text.setText(getAttachName(attachment.getName()));
            text.setTextColor(getResources().getColor(R.color.text_0099CC));

            text.setTypeface( null, Typeface.BOLD );
            text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            layout.addView( pic );
            layout.addView( text );
            container.addView( layout );

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG,"click " + position );
                    pic.startAnimation(fileIconAnimation);
                    displayAttachment(attachment);
                }
            });

            if( position == 0 && positionToView.get(position) == null ){
                layout.setAlpha(1.0f);
                layout.setScaleX(1.3f);
                layout.setScaleY(1.3f);
            } else if( positionToView.get(position) == null){
                layout.setAlpha(0.7f);
                layout.setScaleX(1.0f);
                layout.setScaleY(1.0f);
            }

            positionToView.put(position,layout);

            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            positionToView.remove(position);
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return entities.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            Log.d(TAG,"onPageScrolled position : " + position);
        }

        @Override
        public void onPageSelected(int position) {
            Log.d(TAG,"onPageSelected position : " + position);

            displayAttachment(entities.get(position));

            Set<Integer> keys = positionToView.keySet();
            for(Integer key : keys){
                View v = positionToView.get(key);

                if(key.intValue() == position){
                    v.setAlpha(1.0f);
                    v.setScaleX(1.3f);
                    v.setScaleY(1.3f);
                } else {
                    v.setAlpha(0.6f);
                    v.setScaleX(1.0f);
                    v.setScaleY(1.0f);
                }
            }

        }

        @Override
        public void onPageScrollStateChanged(int position) {
            Log.d(TAG, "onPageScrollStateChanged position : " + position);
            //displayAttachment(entities.get(position));
        }
    }

    /**
     * Если в названии одно слово, то возвращем его.
     * Если два - делаем перенос по пробелу.
     * Если больше двух - после первого переносим.
     *                    А ко второму подставляем троеточие и расширение фаила.
     *
     * @param s название файла с расширением
     * @return отформатированное название фаила.
     */
    private String getAttachName(String s){
        String[] words = StringUtils.split(s);
        StringBuilder stringBuilder = new StringBuilder();
        if(words.length == 1){
            return s;
        }else if(words.length == 2){
            stringBuilder.append(words[0]).append("\n").append(words[1]);
            return String.valueOf(stringBuilder);
        }else if(words.length > 2){
            stringBuilder.append(words[0]).append("\n")
                    .append(words[1]).append("...").append(FilenameUtils.getExtension(s));
            return String.valueOf(stringBuilder);
        }
        return s;
    }

    /**
     * Получить картинку для обозначения типа файла.
     * Прим.: получаем тупо по расширению.
     *
     * @param docName Имя файла вместе с расширением.
     * @return id графического ресурса
     */
    private int getTypeIcon(String docName) {
        final DOC_TYPE docType = docTypeByName(docName);

        switch (docType) {
            case PDF:
                return R.drawable.icon_pdf;
            case DOC:
                return R.drawable.icon_word;
            case DOCX:
                return R.drawable.icon_word;
            case EXCEL:
                return R.drawable.icon_exel;
            case PPT:
                return R.drawable.icon_ppt;
            case PPTX:
                return R.drawable.icon_ppt;
            case TXT:
                return R.drawable.icon_txt;
            case IMAGE:
                return R.drawable.icon_img;
            case AUDIO:
                return R.drawable.icon_audio;
            case VIDEO:
                return R.drawable.icon_video;
            default:
                return R.drawable.icon_unknown;
        }
    }

    private void displayAttachment( Attachment attachment ) {

        if(currentAttachment == attachment){
            return;
        }else{
            currentAttachment = attachment;
            AttachmentViewFragment attachmentFragment = getAttachmentFragment(attachment);
            if( attachmentFragment != null ) {
                Bundle args = new Bundle();
                args.putString(AttachmentViewFragment.ARG_ATTACH_PATH, attachment.getLocalLocation());
                args.putString(AttachmentViewFragment.ARG_ATTACH_NAME, attachment.getName());
                attachmentFragment.setArguments(args);
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content, attachmentFragment).commit();
            } else {
                Toast.makeText(getActivity(), "Отображение документов данного типа не реализована.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private AttachmentViewFragment getAttachmentFragment(Attachment attachment) {

        DOC_TYPE doctype = docTypeByName(attachment.getName());

        switch (doctype){
            case PDF:
                return new PDFViewFragment();

            case IMAGE:
                return new ImageViewFragment();
            case TXT:
                return new TextViewFragment();

            case VIDEO:
                return new VideoViewFragment();
            case AUDIO:
                return new AudioViewFragment();

            case DOC: {
                DocViewFragment docFragment = new DocViewFragment();
                docFragment.setFileProcessor( new DocOldFormatProcessor());
                return docFragment;
            }
            case DOCX: {
                DocViewFragment docFragment = new DocViewFragment();
                docFragment.setFileProcessor( new DocXProcessor() );
                return docFragment;
            }
            case EXCEL: {
                DocViewFragment docFragment = new DocViewFragment();
                docFragment.setFileProcessor( new SpreadsheetProcessor() );
                return docFragment;
            }
            case PPT: {
                DocViewFragment docFragment = new DocViewFragment();
                docFragment.setFileProcessor( new PPTProcessor() );
                return docFragment;
            }
            case PPTX: {
                DocViewFragment docFragment = new DocViewFragment();
                docFragment.setFileProcessor( new PPTXProcessor() );
                return docFragment;
            }
            case UNKNOWN:
            default:
                return null;
        }

    }

    private void showNoticePopup() {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = inflater.inflate(R.layout.notice_popup, null);
        final PopupWindow dialog = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        final EditText noticeText = (EditText) popupView.findViewById(R.id.notice_edit_text);
        noticeText.setText(task.getNotice());
        popupView.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        popupView.findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            try {
                task.setNotice(noticeText.getText().toString());
                noticeMessage.setText(task.getNotice() != null ? task.getNotice() : "");
                Log.e(TAG, "save notice: " + task.getNotice());
                if(!task.getNotice().isEmpty()){
                    task.setIsFavourite(true);
                }
                App.getDatabaseHelper().getTasksDao().update(task);
                EventBus.getDefault().post(new SetFavoriteEvent());
                dialog.dismiss();
                updateNoticeImage();
            } catch (SQLException e) {
                Log.e(TAG, "not able to save notice: " + task.getNotice());
            }
            }
        });
        dialog.showAtLocation(noticeView, Gravity.CENTER, 0, 0);
        noticeText.requestFocus();
    }

    private void updateNoticeImage() {
        String notice = task.getNotice();
        if (notice != null && !notice.isEmpty()) {
            noticeView.setImageResource(R.drawable._but_filled_note);
        } else {
            noticeView.setImageResource(R.drawable.but_empty_note);
        }
    }
    //TODO: нужно более адекватный способ определять тип документа
    public static DOC_TYPE docTypeByName(String docname){
        DOC_TYPE doctype = DOC_TYPE.PDF;
        if( docname != null  ) {
            if (docname.endsWith(".pdf")) {
                doctype = DOC_TYPE.PDF;
            } else if ( docname.endsWith(".docx") ) {
                doctype = DOC_TYPE.DOCX;
            } else if ( docname.endsWith(".doc") ) {
                doctype = DOC_TYPE.DOC;
            } else if ( docname.endsWith(".rtf") || docname.endsWith(".rtfd.zip")) {
                doctype = DOC_TYPE.RTF;
            } else if (docname.endsWith(".xlsx") || docname.endsWith(".xls") ) {
                doctype = DOC_TYPE.EXCEL;
            } else if (docname.endsWith(".pptx") ) {
                doctype = DOC_TYPE.PPTX;
            } else if (docname.endsWith(".ppt") ) {
                doctype = DOC_TYPE.PPT;
            } else if ( docname.endsWith(".txt") || docname.endsWith(".csv") || docname.endsWith(".htm") || docname.endsWith(".html") ) {
                doctype = DOC_TYPE.TXT;
            } else if ( docname.endsWith(".hls") || docname.endsWith(".mov") || docname.endsWith(".m4v") || docname.endsWith(".mp4") || docname.endsWith(".avi") || docname.endsWith(".mpeg") ){
                doctype = DOC_TYPE.VIDEO;
            } else if (docname.endsWith(".aac") || docname.endsWith(".mp3") || docname.endsWith(".wav") ){
                doctype = DOC_TYPE.AUDIO;
            } else if ( docname.endsWith(".tiff") || docname.endsWith(".bmp") || docname.endsWith(".gif") || docname.endsWith(".jpeg") || docname.endsWith(".jpg") || docname.endsWith(".jpe") || docname.endsWith(".jp2") || docname.endsWith(".png") || docname.endsWith(".svg") ) {
                doctype = DOC_TYPE.IMAGE;
            } else {
                doctype = DOC_TYPE.UNKNOWN;
            }
        }
        return doctype;
    }

    private void testConflicts() {
        ConflictData data = new ConflictData();
        data.setConflictMessage("Message yellow");
        data.setConflictSignal(1);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 5");
        data.setConflictSignal(5);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 6");
        data.setConflictSignal(6);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 8");
        data.setConflictSignal(8);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 9");
        data.setConflictSignal(9);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 11");
        data.setConflictSignal(11);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("Message red 12");
        data.setConflictSignal(12);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("api.task.do.deleted");
        data.setConflictSignal(7);
        addConflictMessage(data);

        data = new ConflictData();
        data.setConflictMessage("api.task.do.completed");
        data.setConflictSignal(2);
        addConflictMessage(data);

//        data = new ConflictData();
//        data.setConflictMessage("api.task.do.changed");
//        data.setConflictSignal(9);
//        addConflictMessage(data);
//
//        data = new ConflictData();
//        data.setConflictMessage("api.task.do.changedSyncWait");
//        data.setConflictSignal(11);
//        addConflictMessage(data);
//
//        data = new ConflictData();
//        data.setConflictMessage("api.task.do.validationfailed");
//        data.setConflictSignal(12);
//        addConflictMessage(data);
    }

    @Override
    public void attachmentSelected(String attachmentId, int attachmentPosition) {
        Attachment at = attachmentMap.get(attachmentId);
        pager.setCurrentItem(attachmentPosition, true);
        displayAttachment(at);
        if(filesPopupWindow.isShowing()) filesPopupWindow.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(filesPopupWindow != null && filesPopupWindow.isShowing()) filesPopupWindow.dismiss();
    }
}
