package ru.baccasoft.croctli.android.filters;

import ru.baccasoft.croctli.android.gen.core.Task;

public interface ITaskFilter {
    boolean isAvailable(Task task);
}
