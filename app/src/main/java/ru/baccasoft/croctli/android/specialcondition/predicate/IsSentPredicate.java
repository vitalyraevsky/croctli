package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;


public class IsSentPredicate implements IBooleanAtom {

    public static final String ATOM_NAME = "issent";

    @Override
    public Boolean compute(Task task) {
        return task.getChangeDateTime().isBefore( App.prefs.getLastSyncDate() );
    }

    @Override
    public IBooleanAtom create() {
        return new IsSentPredicate();
    }

    @Override
    public String getAtomName() {
        return ATOM_NAME;
    }
}
