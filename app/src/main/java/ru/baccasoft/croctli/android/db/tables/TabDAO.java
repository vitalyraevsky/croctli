package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import org.joda.time.DateTime;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Filter;
import ru.baccasoft.croctli.android.gen.core.Tab;

import java.sql.SQLException;
import java.util.List;

public class TabDAO extends BaseDaoImpl<Tab, String> {
    private static final  String TAG = TabDAO.class.getSimpleName();

    public TabDAO(ConnectionSource connectionSource, Class<Tab> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(Tab tab) throws SQLException {
        int ret = super.create(tab);

        Filter filter = tab.getTabFilter();
        filter.setTab(tab);
        ret += App.getDatabaseHelper().getFilterDao().create(filter);

        return ret;
    }

    /**
     * Получить все закладки, измененные после конкретной даты.<br>
     * Обычно - с даты последней синхронизации.
     *
     * @param dateFrom дата в формате DateTime(Timezone.UTC)
     * @return
     * @throws SQLException
     */
    public List<Tab> getModifiedTabs(DateTime dateFrom) throws SQLException {
        QueryBuilder<Tab, String> q = queryBuilder();
        q.where().ge("change_date_time", dateFrom); // сравнение '>='

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<Tab> preparedQuery = q.prepare();
        return query(preparedQuery);
    }

    /**
     * Отсортированы по полю "order"
     * @return
     * @throws SQLException
     */
    @Override
    public List<Tab> queryForAll() throws SQLException {
        QueryBuilder<Tab, String> q = queryBuilder();
        q.orderBy("order", true);

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<Tab> preparedQuery = q.prepare();
        return query(preparedQuery);

    }
}
