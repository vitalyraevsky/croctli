package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.FilterCondition;

import java.sql.SQLException;
import java.util.Collection;

public class FilterConditionDao extends BaseDaoImpl<FilterCondition, String> {
    public FilterConditionDao(ConnectionSource connectionSource, Class<FilterCondition> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public Collection<FilterCondition> loadFilterConditionsForFilter(String filterId) {
        QueryBuilder<FilterCondition, String> builder = queryBuilder();
        try {
            builder.where().eq("filter_id", filterId);
            return query(builder.prepare());
        } catch (SQLException e) {
            TableUtils.createLogMessage("cant read from FilterCondition", e);
            Log.e("", "cant read from FilterCondition", e);
            return null;
        }
    }
}
