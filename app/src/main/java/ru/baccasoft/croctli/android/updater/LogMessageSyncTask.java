package ru.baccasoft.croctli.android.updater;

import android.os.AsyncTask;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.sync.LogMessageSync;

/**
 * Отправляем на сервер сведения об имевших место ошибках синхронизации.
 *
 * Ошибки при синхронизации могут возникать в следующих случаях:
 * запись/чтение из базы
 * пропала связь с сервером
 * непредвиденное (все, что не подпадает под описанные варианты)
 *
 * Дату-время LogMessage надо выставлять по UTC.
 *
 * public class LogMessage {
 *  String message;             - ??? [краткое содержание ошибки]
 *  String dateTimeOfString;    - дата-время по UTC
 *  String machineName;         - App.prefs.getDeviceId(); [идентификатор устройства]
 *          (уходит на сервер в каждом запросе, требующем авторизацию, в заголовке X-TLI-DEVICE)
 *  int severity; (1..3)        - 3 (на данный момент для всех, потому что только ошибки отправляем)
 *  int priority; (1..3)        - 2 (на данный момент для всех, потому что отправляем только ошибки. которые как дети, важны все одинаково)
 *  String additionalInfo;      - stackTrace
 *  String subSystem;           - null
 *  ArrayOfString categories;   - null
 * }
 */

public class LogMessageSyncTask extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... params) {
        LogMessageSync logMessageSync = new LogMessageSync();
        RestApiDate dateTo = new RestApiDate(RestApiDate.now());

        logMessageSync.byRequest(null, dateTo);

        return null;
    }
}
