package ru.baccasoft.croctli.android.view.validator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import ru.baccasoft.croctli.android.gen.core.EntityProperty;

public abstract class BaseValidator {
    protected Context mContext;
    protected EntityProperty entityProperty;

    protected final String valueToValidate;
    private final String message;

    private static final String title = "Ошибка ввода";
    private static final String button = "OK";

    protected BaseValidator(Context context, EntityProperty entityProperty,
                            String valueToValidate, String attributeName) {
        this.mContext = context;
        this.entityProperty = entityProperty;
        this.valueToValidate = valueToValidate;

        final int resId = context.getResources().getIdentifier(attributeName, "string", context.getPackageName());
        this.message = context.getResources().getString(resId);
    }

    private AlertDialog createAlertDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        return builder.create();
    }

    public abstract boolean isValid();

    public final void alertIfInvalid(Activity activity) {
        if( !isValid() ) {
            createAlertDialog(activity).show();
        }
    }
}
