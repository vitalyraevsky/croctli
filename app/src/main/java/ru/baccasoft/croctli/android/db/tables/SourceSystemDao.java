package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.SourceSystem;

import java.sql.SQLException;

public class SourceSystemDao extends BaseDaoImpl<SourceSystem, String> {
    public SourceSystemDao(ConnectionSource connectionSource, Class<SourceSystem> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
