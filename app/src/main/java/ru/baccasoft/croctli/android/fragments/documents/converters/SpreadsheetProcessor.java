package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.util.Log;

import java.io.IOException;
import java.io.StringWriter;

import ae.javax.xml.bind.JAXBException;


public class SpreadsheetProcessor implements ConvertationProcessor {

    private static final String TAG = SpreadsheetProcessor.class.getSimpleName();

    private ToHtml spreadsheetToHtml;
    private StringWriter writer;

    @Override
    public void init(String officeFileName) throws IOException {
        Log.d(TAG, "init xlsx converter with file: " + officeFileName);
        writer = new StringWriter();
        spreadsheetToHtml = ToHtml.create(officeFileName, writer);
        spreadsheetToHtml.setCompleteHTML(true);
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException {
        spreadsheetToHtml.printPage();
        String html = writer != null ? writer.toString() : null ;
        writer.close();
        return html;
    }



}
