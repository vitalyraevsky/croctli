package ru.baccasoft.croctli.android.view.event;

import android.view.ViewGroup;

/**
 * Событие.
 * Оповещает, что надо добавить во viewGroup новую вьюшку с массивом объектов.
 * Шаблон для добавления берется как дочернее EP по id родителя (this.entityPropertyId)
 * с полем orderInArray == -1
 *
 * Нужно отсылать ТОЛЬКО из ExpanderView.
 * Потому что в onEvent ViewGroup будет кастоваться в ExpanderView,
 * чтобы добавить новую вьюшку (новый массив объектов) в нужное место ExpanderView.
 */
public class AddObjectArrayElement {

    private String entityPropertyId;
    private ViewGroup viewGroup;
    private int nestLevel;

    public AddObjectArrayElement(String entityPropertyId, ViewGroup viewGroup, int nestLevel) {
        this.entityPropertyId = entityPropertyId;
        this.viewGroup = viewGroup;
        this.nestLevel = nestLevel;
    }

    public ViewGroup getViewGroup() {
        return viewGroup;
    }

    public String getEntityPropertyId() {
        return entityPropertyId;
    }

    public int getNestLevel() {
        return nestLevel;
    }
}
