package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;

import java.sql.SQLException;

public class TaskStateInSystemDao extends BaseDaoImpl<TaskStateInSystem, String> {
    public TaskStateInSystemDao(ConnectionSource connectionSource, Class<TaskStateInSystem> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
