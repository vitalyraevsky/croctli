package ru.baccasoft.croctli.android.view.wrapper;

import android.content.Context;
import android.view.View;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.StringScalarWithTitle;

/**
 * Создает вьюшку для EntityProperty типа "date" и "dateTime".
 * Получаем ClassifierItem'ы для свойства.
 *
 * //TODO:
 * Выбираем такое, что code=#UserValueLabel.
 * Если такое есть, рисуем особый DateClassifierView (одно из значений списка позволяет выбрать дату)
 * Если такого нет, рисуем обычный ClassifierView
 *
 * Также надо валидатить введенную дату из поля value (возможно неверное форматирование
 * если isClassifierFree=true)
 *
 * Формальная логика:
 *
 * Проверяем флаг readOnly
 * если true
 *      рисуем обычные строковые значения
 *      при этом особо обрабатываем UserValueLabel
 * если false
 *      готовим все для вывода режима редактирования
 *      // TODO:
 *
 */
public class DateScalarWrapper implements IWrapper {
//    final String USER_VALUE_LABEL = "#UserValueLabel";
    private Context mContext;
    private boolean readOnly;

    public DateScalarWrapper(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);

        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        if(this.readOnly) {
            return getReadLayout(p);
        } else {
            return getWriteLayout(p);
        }
    }

    private void validate(EntityProperty p) {
        String scalarType = p.getScalarType();
        if(ViewUtils.isEmpty(scalarType) || (
                !scalarType.equals("date") &&
                !scalarType.equals("dateTime")))
            throw new IllegalStateException("expects scalarType \"date\" or \"dateTime\" but get " +
                scalarType);
    }

    private View getReadLayout(EntityProperty p) {
        String title = (ViewUtils.isNotEmpty(p.getDisplayGroup())) ?
                p.getDisplayGroup() : "";
        String value = (ViewUtils.isNotEmpty(p.getValue())) ?
                p.getValue() : "";

        if(value.equals(ViewUtils.USER_VALUE_LABEL)) {
            ClassifierItem classifierItem = TableUtils.getClassifierItemByCode(p.getClassifierTypeId(),
                    ViewUtils.USER_VALUE_LABEL);

            if(classifierItem == null)
                throw new IllegalStateException("entityProperty with id:\'" + p.getId() +
                " have scalar type: \'"+ p.getScalarType() + "\' " +
                " and value: \'" + p.getValue() + "\'" +
                " but cant find classifierItem with code: \'" + p.getValue() + "\'");

            value = classifierItem.getDisplayValue();
        }

        return new StringScalarWithTitle(mContext, title, value, true, null);
    }

    private View getWriteLayout(EntityProperty p) {
//        String title = (ViewUtils.isNotEmpty(p.getDisplayGroup())) ?
//                p.getDisplayGroup() : "";
//        String value = (ViewUtils.isNotEmpty(p.getValue())) ?
//                p.getValue() : "";
//
//        Map<String, String> items = new HashMap<String, String>();
//        List<ClassifierItem> classifierItems =
//                TableUtils.getClassifierItemsByClassifierId(p.getClassifierTypeId());
//
//        for(ClassifierItem ci : classifierItems)
//            items.put(ci.getCode(), ci.getDisplayValue());
//
//        DateClassifierWithTitle view = new DateClassifierWithTitle(
//                mContext, title, value, items, globalReadOnly);

        //TODO:
        throw new UnsupportedOperationException("not supported yet");
    }
}
