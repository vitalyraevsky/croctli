package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import ae.javax.xml.bind.JAXBException;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.HSLFSlideToImageConversionHandler;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.PptToHtml;

public class PPTProcessor implements ConvertationProcessor {

    private String TAG = PPTXProcessor.class.getSimpleName();

    private PptToHtml pptToHtml;
    private Writer writer;
    private Handler handler;

    @Override
    public void init(String officeFileName) throws IOException {
        Log.d(TAG, "init pptx converter with file: " + officeFileName);

        writer = new StringWriter();
        handler = new Handler();
        pptToHtml = PptToHtml.create(officeFileName, writer);
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException {

        pptToHtml.setImageConversionHandler(new HSLFSlideToImageConversionHandler(imageDirPath,activity, handler));
        pptToHtml.printSlides();
        String html = writer != null ? writer.toString() : null ;
        writer.close();
        return html;
    }


}
