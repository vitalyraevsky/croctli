package ru.baccasoft.croctli.android.specialcondition.bool;

public interface IBooleanAtomCreator {
	IBooleanAtom create();
	String getAtomName();
}
