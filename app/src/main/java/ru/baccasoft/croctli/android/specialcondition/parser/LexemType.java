package ru.baccasoft.croctli.android.specialcondition.parser;

public enum LexemType {

	BooleanAnd,
	BooleanOr,
	BooleanNot,
	
//	BooleanPredicate,
	
	EqualityOperator, // равно - не равно, позволяет обрабатывать и булевы переменные, и даты
	ComparisonOperator, // больше - меньше, работает только для дат
	
	BooleanAtom,
	DateAtom,
}
