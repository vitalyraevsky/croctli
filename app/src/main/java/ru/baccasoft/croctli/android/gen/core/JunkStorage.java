package ru.baccasoft.croctli.android.gen.core;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Хранит неконсистентные данные.
 * Не закончен.
 * Решили отложить (видимо, после ФДА)
 *
 */


@DatabaseTable(tableName = JunkStorage.TABLE_NAME)
public class JunkStorage {

    public static final String TABLE_NAME = "junk_storage";

    public enum FIELD {
        ID("id"),   // идентификатор конкретного объекта, хранящегося сериализованным в поле "data"
        TYPE("type"),   // тип объекта - по имени класса
        DATA("data");

        String value;

        FIELD(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * Тип json-данных, который хранится в отдельной записи таблицы
     */
    public enum TYPE {
        PROCESS,
        TYPE,
        ENTITY_PROPERTY
    }
}
