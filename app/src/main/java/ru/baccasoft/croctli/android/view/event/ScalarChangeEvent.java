package ru.baccasoft.croctli.android.view.event;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.validator.BaseValidator;
import ru.baccasoft.croctli.android.view.validator.MaxLengthValidator;
import ru.baccasoft.croctli.android.view.validator.MaxValueValidator;

public class ScalarChangeEvent implements IChangeEvent {
    private final String entityPropertyId;
    private final String value;
    private final List<BaseValidator> validators;

    public ScalarChangeEvent(String entityPropertyId, String value, Context context) {
        this.entityPropertyId = entityPropertyId;
        this.value = value;
        this.validators = new ArrayList<BaseValidator>();

        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        String type = ep.getScalarType();

        if(type != null) {
            if (type.equals("int") || type.equals("float") || type.equals("decimal")) {
                validators.add(new MaxValueValidator(context, ep, value));
                validators.add(new MaxValueValidator(context, ep, value));
            } else if (type.equals("string")) {
                validators.add(new MaxLengthValidator(context, ep, value));
            }
        }
    }

    @Override
    public void save() {
        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        ep.setValue(value);
        TableUtils.updateEntityProperty(ep, true);
    }

    @Override
    public String getKey() {
        return entityPropertyId;
    }

    @Override
    public boolean isNotEmpty() {
        if(value.isEmpty()){
            return false;
        }
        return true;
    }

    @Override
    public List<BaseValidator> getValidators() {
        return validators;
    }

}
