package ru.baccasoft.croctli.android.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.ErrandListWrapper;

import java.util.List;

/**
 * На этом фрагменте показываются Предварительные поручения.
 *
 */

public class TaskErrandFragment extends Fragment implements View.OnClickListener {
    @SuppressWarnings("unused")
    private static final String TAG = TaskErrandFragment.class.getSimpleName();

    private static final String TASK_ERRAND_FRAGMENT_TITLE_ARG_KEY = "TASK_ERRAND_FRAGMENT_TITLE_ARG_KEY";

    private String taskId;
    private String titleString;
    private ScrollView rootScrollView;
    private TextView titleView;

    private boolean hasErrandsRoot = false; // связаны ли с задачей какие-либо резолюции
    private boolean hasErrandsEPs = false;  // есть ли связанное(-ые) EP, содержащие резолюции
    private boolean hasErrandsTemplateEP = false;   // если шаблонные EP для создания резолюций


    public static TaskErrandFragment newInstance(String taskId, String title) {
        TaskErrandFragment f = new TaskErrandFragment();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        args.putString(TASK_ERRAND_FRAGMENT_TITLE_ARG_KEY, title);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();


        if(args == null) {
            String message = "must pass not null arguments";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }
        if(!args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY)) {
            String message = "taskId cant be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }
        if(!args.containsKey(TASK_ERRAND_FRAGMENT_TITLE_ARG_KEY)) {
            String message = "title cant be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }

        taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);
        titleString = args.getString(TASK_ERRAND_FRAGMENT_TITLE_ARG_KEY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_errand_fragment, container, false);
        titleView = (TextView) view.findViewById(R.id.title);
        rootScrollView = (ScrollView) view.findViewById(R.id.scroll_view);

        titleView.setText(titleString);

        // получаем свойство, указывающее на дочерние резолюции
        EntityProperty rootEp = TableUtils.getRootEpForErrands(taskId);
        hasErrandsRoot = (rootEp != null);

        if(hasErrandsRoot) {
            List<EntityProperty> childEps = TableUtils.getEntityPropertyChildsById(rootEp.getId());
            hasErrandsEPs = (ViewUtils.isNotEmpty(childEps));
            EntityProperty templateEp = TableUtils.getEPObjectArrayAddingTemplate(rootEp.getId());
            hasErrandsTemplateEP = (templateEp != null);

            String title = rootEp.getDisplayGroup();
            titleView.setText(title);
        }

        // обновляем вьюшку
        if(hasErrandsEPs) {
            new syncThis().execute(rootEp);
        }

        // если нечего отображать и нет шаблона для отображения,
        // по клику диалог редактирования открываться не будет
        if(hasErrandsRoot &&
                (hasErrandsEPs || hasErrandsTemplateEP)) {
            view.setOnClickListener(this);
        }

        return view;
    }

    /**
     * Получает из базы и отображает содержимое блока,
     * а именно все резолюции в режиме чтения.
     * На вход принимает корневой EP, указывающий на все доступные резолюции.
     * Прим.: перед вызовом надо проверять, что резолюции таки есть.
     */
    class syncThis extends AsyncTask<EntityProperty, Void, View> {
        @Override
        protected View doInBackground(EntityProperty... params) {
            EntityProperty ep = params[0];
            ErrandListWrapper errandListWrapper = new ErrandListWrapper(getActivity());
            return errandListWrapper.getLayout(ep, true);
        }

        @Override
        protected void onPostExecute(View view) {
            view.setOnClickListener(TaskErrandFragment.this);
            rootScrollView.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getFragmentManager();
        String dialogTitle = titleView.getText().toString();
        TaskErrandEditorDialogFragmentNew errandEditor =
                TaskErrandEditorDialogFragmentNew.newInstance(taskId, dialogTitle);
        errandEditor.show(fm, "task_errand_editor");
    }
}
