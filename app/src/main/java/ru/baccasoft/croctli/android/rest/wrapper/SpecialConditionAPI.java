package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class SpecialConditionAPI implements IRestApiCall<SpecialConditionSet, ArrayOfSpecialCondition, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = SpecialConditionAPI.class.getSimpleName();

    /**
     * Прим.: особый вызов, плюет на входные параметры
     * @param dateFrom игнорит
     * @param dateTo игнорит
     * @param max игнорит
     */
    @Override
    public SpecialConditionSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String api = "/api/specialcondition/list";
        final boolean needAuth = false;

        final SpecialConditionSet specialConditionSet= RestClient.fillJsonClass(SpecialConditionSet.class, api, needAuth);
        final String serverDate = specialConditionSet.getServerTimeString();
        for (SpecialCondition s : specialConditionSet.getArrayOf().getItem()) {
            s.setServerTime(serverDate);
        }
        return specialConditionSet;
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfSpecialCondition objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
