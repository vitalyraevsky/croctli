package ru.baccasoft.croctli.android.gen.core;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "log_message_category")
public class LogMessageCategory {
    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = "id")
    protected int id;

    @DatabaseField(foreign = true)
    protected LogMessage message;

    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    protected String name;
}
