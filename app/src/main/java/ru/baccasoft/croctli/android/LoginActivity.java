package ru.baccasoft.croctli.android;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import java.util.List;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.MetaAuth;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.Utils;

public class LoginActivity extends SherlockActivity {
    @SuppressWarnings("unused")
    private static final String TAG = "LoginActivity";

    public static final String EXTRA_SIGN_OUT = "EXTRA_SIGN_OUT";

    public static final String LOGIN_KEY = "LOGIN";
    public static final String PASSWORD_KEY = "PASS";
    public static final String REMEMBER_KEY = "AUTOLOGIN";
    public static final String TICKET_KEY = "TGT";
    private static final String TGT_HEADER_NAME = "Location";

    private RelativeLayout rootLayout;
    private EditText loginView;
    private EditText passwordView;
    private CheckBox rememberCheckBox;
    private Button loginButton;
    private ImageButton optionButton;//Кнопка "Настройки" для вызова настроек

    private boolean isFirstDraw = true;

    private MetaAuth metaAuth;
    private String casExtUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_new);

        new checkConnectionServerTask().execute();

        rootLayout = (RelativeLayout) findViewById(R.id.root);
        loginButton = (Button) findViewById(R.id.login_button);
        loginView = (EditText) findViewById(R.id.login);
        passwordView = (EditText) findViewById(R.id.password);
        rememberCheckBox = (CheckBox) findViewById(R.id.remember_indicator);
        optionButton = (ImageButton) findViewById(R.id.option_button);

        // Если передан флаг для выхода, то стираем данные
        boolean signOut = getIntent().getBooleanExtra(EXTRA_SIGN_OUT,false);
        if(signOut)
            erraseLoginData();

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        loginView.setText(prefs.getString(LOGIN_KEY, ""));
        passwordView.setText(prefs.getString(PASSWORD_KEY, ""));
        rememberCheckBox.setChecked(prefs.getBoolean(REMEMBER_KEY, false));
        /*//FIXME: если одно из полей пустое, сохранение не будет работать
        if (!(loginView.getText().toString().isEmpty() && passwordView.getText().toString().isEmpty()))
            rememberCheckBox.setChecked(true);*/

        // Вешаем слушателя на поля логина && пароля, и активируем кнопку входа если оба поля НЕ пустые.
        loginView.addTextChangedListener(textWatcher);
        passwordView.addTextChangedListener(textWatcher);

        // Запускаем раз, чтобы изначально кнопка была НЕ активна
        checkIsEmptyTextFields();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String l = loginView.getText().toString();
                final String s = passwordView.getText().toString();
                loginButton.setEnabled(false);
                loginView.setEnabled(false);
                passwordView.setEnabled(false);
                rememberCheckBox.setEnabled(false);
                Log.d(TAG, "new autorized task");
                new authorizeTask(Utils.getCASCredentials(l, s)).execute();
            }
        });

        optionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity.this.openOptionsMenu();
            }
        });


        String savedLogin = prefs.getString(LOGIN_KEY, "");
        String savedPassword = prefs.getString(PASSWORD_KEY, "");

        Log.d(TAG, "new autorized task");

        if (!TextUtils.isEmpty(savedLogin) && !TextUtils.isEmpty(savedPassword)) {
            new authorizeTask(Utils.getCASCredentials(savedLogin, savedPassword)).execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        Log.d(TAG, "onCreateOptionsMenuOnLoginActivity");

        menu.clear();
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(!isFirstDraw) { return; }
        isFirstDraw = false;

        Point size = getScreenSize();
        int width = size.x;
        int height = size.y;

        Log.d(TAG, "device screen size. width:\'"+width+"\', height:\'"+height+"\'");

        placeViewsOnScreen(width, height);
    }

    private void placeViewsOnScreen(int width, int height) {
        int l = width/3;    // отступ слева/справа для некоторых вьюшек.

        setMargins(loginView, l, 0, l, 0);
        setMargins(passwordView, l, 0, l, 0);
        setMargins(rememberCheckBox, l, 0, 0, 0);
        setMargins(loginButton, 0, 0, l, 0);
    }

    private static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    private Point getScreenSize() {
        int width = rootLayout.getMeasuredWidth();
        int height = rootLayout.getMeasuredHeight();
        return new Point(width, height);
    }

    /**
     * Собственно входим в проложение, запуская сначала сплешСкрин.
     */
    private void performLogin() {
        getPreferences(MODE_PRIVATE).edit()
            .putString(LOGIN_KEY, rememberCheckBox.isChecked() ? loginView.getText().toString() : "")
            .putString(PASSWORD_KEY, rememberCheckBox.isChecked() ? passwordView.getText().toString() : "")
            .putBoolean(REMEMBER_KEY, rememberCheckBox.isChecked() ? true : false)
            .commit();
        loginView.setText("");
        passwordView.setText("");
        startActivity(new Intent(this, SplashScreen.class));
    }

    private void erraseLoginData(){
        getPreferences(MODE_PRIVATE).edit()
                .putString(LOGIN_KEY, "")
                .putString(PASSWORD_KEY, "")
                .putBoolean(REMEMBER_KEY, false)
                .commit();
    }

    public static void startSignOut(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(LoginActivity.EXTRA_SIGN_OUT, true);
        context.startActivity(intent);
    }


    private enum AUTH_RESULT {
        OK, NOT_AUTH , NO_CONNECTION
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            checkIsEmptyTextFields();
        }
    };

    private void checkIsEmptyTextFields(){
        if(loginView.getText().toString().isEmpty() || passwordView.getText().toString().isEmpty())
        {
            loginButton.setEnabled(false);
            rememberCheckBox.setEnabled(false);
        }else
        {
            loginButton.setEnabled(true);
            rememberCheckBox.setEnabled(true);
        }

    }

    private void setVisibility(){
            loginView.setVisibility(View.VISIBLE);
            passwordView.setVisibility(View.VISIBLE);
            rememberCheckBox.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.VISIBLE);
            optionButton.setVisibility(View.VISIBLE);
    }

    /**
     * Получаем данные для авторизации: <br>
     * улр сервера для авторизации и от него тикет TGT
     */
    class authorizeTask extends AsyncTask<Void, Void, AUTH_RESULT> {

        private List<NameValuePair> creds;

        authorizeTask(List<NameValuePair> params) {
            this.creds = params;
//            this.creds = Utils.getCASCredentials();
        }

        @Override
        protected AUTH_RESULT doInBackground(Void... params) {

            //AUTH_RESULT result = AUTH_RESULT.NOT_AUTH;
            AUTH_RESULT result = null;

            try {

                Log.d(TAG, "casExtUrl:" + casExtUrl);

                //Перед началом аутентификации клиент проверяет доступность приложения CAS по адресу из metaauth-схемы.
                // Код возврата 200, 201 или 302 считается признаком доступности, после чего клиент переходит к процедуре
                // аутентификации.
                final HttpResponse rGet = Utils.performGet(casExtUrl, false);
                if(rGet == null)
                    TableUtils.createLogMessage("cant get casExtUrl url: " + casExtUrl, "");

                final int code = Utils.getResponseCode(rGet);

                Log.d(TAG, "code:"+code);
                Log.d(TAG, Utils.toString(rGet));

                // FIXME: код надо проверять не для этого урла
                switch (code) {
                    case 200:
                    case 201:
                    case 302:

                        final String authUrl = RestClient.getAuthUrl(metaAuth);
                        Log.d(TAG, "authUrl:" + authUrl);

                        final String tgtUrl = RestClient.getTGT(authUrl, creds);
                        Log.i(TAG, "tgtUrl: " + tgtUrl);

                        // если авторизация не прошла
                        if(tgtUrl == null) {
                            Log.d(TAG, "ВЫ не авторизованы");
                            result = AUTH_RESULT.NOT_AUTH;
                            break;
                        }

                        // инициализируем преференцес
                        App.getInstance().initSingletons(creds.get(0).getValue());
                        App.prefs.setTGTurl(tgtUrl);
                        App.prefs.setTGTkey(Utils.parseTGTfromUrl(tgtUrl));

                        // засовываем данные metaAuth в базу
                        TableUtils.createMetaAuth(metaAuth);

                        //Для авторизации на сервере и получения информации о текущем авторизованном пользователе используется
                        // rest-метод server/who.
                        //В случае удачной авторизации (пользователь найден в серверной БД) возвращается json-объект SystemUser.
                        //В случае неудачной авторизации возвращается http код 403. Клиент при этом выводит сообщение с текстом
                        // StartupScreen_WrongUserOrPassword и кнопкой MessageBox_OkCaption.
                        // FIXME: TGT url нужно передавать в явном виде, так как сейчас урл берется внутри вызова callSomeRestApi из prefs и приходится создавать настрйоки пользователя до того как удалось залогиниться

                        if (RestClient.isAuthorized()) {
                            result = AUTH_RESULT.OK;
                        } else {
                            result = AUTH_RESULT.NOT_AUTH;
                        }

                        break;
                    default:
                        result = AUTH_RESULT.NO_CONNECTION;
                        break;
                }
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage(), ex);
                result = AUTH_RESULT.NO_CONNECTION;
            }
            return result;
        }


        @Override
        protected void onPostExecute(AUTH_RESULT result) {
            try {

                switch(result){

                    case OK:
                        performLogin();
                        finish();
                        break;

                    case NOT_AUTH:
                        new AlertDialog.Builder(LoginActivity.this)
                                .setMessage(R.string.not_autorized)
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .show();
                        break;

                    case NO_CONNECTION:
                        new AlertDialog.Builder(LoginActivity.this)
                                .setMessage(R.string.no_connection)
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .show();
                        break;
                    default:

                        break;
                }

            } finally {
                Log.d(TAG, "task completed");
                loginButton.setEnabled(true);
                passwordView.setEnabled(true);
                loginView.setEnabled(true);
            }
        }
    }

    class checkConnectionServerTask extends AsyncTask<Void, Void, AUTH_RESULT>{

        private ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Проверка подключения...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected AUTH_RESULT doInBackground(Void... params) {
            AUTH_RESULT result = null;
            try {
                // сначала получаем адрес сервиса CAS из metaauth-схемы
                metaAuth = RestClient.getMetaAuth();
                casExtUrl = metaAuth.getCasExternal();
                if(casExtUrl != null)
                result = AUTH_RESULT.OK;
            }catch (Exception ex){
                Log.e(TAG, ex.getMessage(), ex);
                result = AUTH_RESULT.NO_CONNECTION;
            }
            return result;
        }

        @Override
        protected void onPostExecute(AUTH_RESULT result) {
            super.onPostExecute(result);


            switch (result){
                case OK:
                    progressDialog.dismiss();
                    setVisibility();
                    break;
                case NO_CONNECTION:
                    progressDialog.dismiss();
                    new AlertDialog.Builder(LoginActivity.this)
                            .setMessage(R.string.no_connection)
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    finish();
                                }
                            })
                            .show();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
