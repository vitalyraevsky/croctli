
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;


/**
 * <p>Java class for RibbonSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RibbonSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaskName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntityPropertyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShouldDisplayInRibbon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ShouldDisplayNameInRibbon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsCommonField" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProcessTypeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RibbonSettings", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "taskName",
    "entityPropertyName",
    "shouldDisplayInRibbon",
    "shouldDisplayNameInRibbon",
    "isCommonField",
    "processTypeId"
})
public class RibbonSettings {

    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "id", id = true, dataType = DataType.STRING)
    protected String id;

    @JsonProperty("TaskName")
    @XmlElement(name = "TaskName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "task_name", dataType = DataType.STRING)
    protected String taskName;

    @JsonProperty("EntityPropertyName")
    @XmlElement(name = "EntityPropertyName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "entity_property_name", dataType = DataType.STRING)
    protected String entityPropertyName;

    @JsonProperty("ShouldDisplayInRibbon")
    @XmlElement(name = "ShouldDisplayInRibbon", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "should_display_in_ribbon", dataType = DataType.BOOLEAN)
    protected boolean shouldDisplayInRibbon;

    @JsonProperty("ShouldDisplayNameInRibbon")
    @XmlElement(name = "ShouldDisplayNameInRibbon", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "should_display_name_in_ribbon", dataType = DataType.BOOLEAN)
    protected boolean shouldDisplayNameInRibbon;

    @JsonProperty("IsCommonField")
    @XmlElement(name = "IsCommonField", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_common_field", dataType = DataType.BOOLEAN)
    protected boolean isCommonField;

    @JsonProperty("ProcessTypeId")
    @XmlElement(name = "ProcessTypeId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "process_type_id", dataType = DataType.STRING)
    protected String processTypeId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the taskName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Sets the value of the taskName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Gets the value of the entityPropertyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityPropertyName() {
        return entityPropertyName;
    }

    /**
     * Sets the value of the entityPropertyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityPropertyName(String value) {
        this.entityPropertyName = value;
    }

    /**
     * Gets the value of the shouldDisplayInRibbon property.
     * 
     */
    public boolean isShouldDisplayInRibbon() {
        return shouldDisplayInRibbon;
    }

    /**
     * Sets the value of the shouldDisplayInRibbon property.
     * 
     */
    public void setShouldDisplayInRibbon(boolean value) {
        this.shouldDisplayInRibbon = value;
    }

    /**
     * Gets the value of the shouldDisplayNameInRibbon property.
     * 
     */
    public boolean isShouldDisplayNameInRibbon() {
        return shouldDisplayNameInRibbon;
    }

    /**
     * Sets the value of the shouldDisplayNameInRibbon property.
     * 
     */
    public void setShouldDisplayNameInRibbon(boolean value) {
        this.shouldDisplayNameInRibbon = value;
    }

    /**
     * Gets the value of the isCommonField property.
     * 
     */
    public boolean isIsCommonField() {
        return isCommonField;
    }

    /**
     * Sets the value of the isCommonField property.
     * 
     */
    public void setIsCommonField(boolean value) {
        this.isCommonField = value;
    }

    /**
     * Gets the value of the processTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTypeId() {
        return processTypeId;
    }

    /**
     * Sets the value of the processTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTypeId(String value) {
        this.processTypeId = value;
    }

}
