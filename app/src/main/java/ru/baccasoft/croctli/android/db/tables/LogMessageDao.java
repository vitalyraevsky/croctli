package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ArrayOfLogMessage;
import ru.baccasoft.croctli.android.gen.core.LogMessage;

import java.sql.SQLException;
import java.util.List;

public class LogMessageDao extends BaseDaoImpl<LogMessage, Integer> {
    public LogMessageDao(ConnectionSource connectionSource, Class<LogMessage> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public ArrayOfLogMessage getNotSynchronized() throws SQLException {
        QueryBuilder<LogMessage, Integer> q = queryBuilder();

        q.where().eq("synchronized", false);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<LogMessage> preparedQuery = q.prepare();

        List<LogMessage> list = query(preparedQuery);
        return new ArrayOfLogMessage(list);
    }
}
