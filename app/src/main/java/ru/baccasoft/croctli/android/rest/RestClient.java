package ru.baccasoft.croctli.android.rest;

import android.util.Log;
import android.util.Patterns;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.SettingsActivity;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.MetaAuth;

/**
 * Что класс должен делать:
 * - возвращать json-данные, завернутые в объекты
 * - должен работать с сетью, все преобразования и парсинг должны быть в утилсах или еще где
 * Чего не должен
 * - работать с Preferences
 */
public class RestClient {
    @SuppressWarnings("unsude")
    private static final String TAG = RestClient.class.getSimpleName();

//    private static final String ENDPOINT_REST_API = "http://172.26.7.28:8080/wliClientSynch"; // endpoint для получения json-данных
    private static final String REST_API_CALL_METAAUTH_SCHEMA = "/server/metaauth/schema";    // rest-api вызов для получения информации о серверах и способах авторизации
    private static final String AUTH_URL_SUFFIX = "/v1/tickets";     // добавляется в конец урла, полученного из схемы авторизации. добавляется к адресу сервера, полученному из metaauth схемы

    private static final String TGT_HEADER_NAME = "Location";

    private static final String REQUIRES_AUTH_URLS_SUFFIX = "/server/metaauth/list";     // для получения списка api-вызовов, требующих авторизации

    //{{{ Все, что касается авторизации

    public static MetaAuth getMetaAuth(){
        final MetaAuth m = fillJsonClass(MetaAuth.class, REST_API_CALL_METAAUTH_SCHEMA, false);
        return m;
    }

    /**
     * Получить урл, по которому будет происходить авторизация.
     *
     * @return урл строкой или null
     */
    public static String getAuthUrl(MetaAuth m){

        final String url = m.getCasExternal();  // прямо сейчас урл на авторизацию лежит в "CasInternal"

        if(url == null) {
            String message = "server url from schema == null";
            TableUtils.createLogMessage(message);
            Log.e(TAG, message);
            return null;
        }

        if(!Patterns.WEB_URL.matcher(url).matches())
            throw new IllegalStateException("server url from schema is not valid url");

        return url + AUTH_URL_SUFFIX;
    }

    /**
     * Натянуть Json-данные на объект.
     *
     * @param valueType Класс объекта, на который нужно натянуть данные.
     * @param url полный урл, по которому можно получить json данные
     * @param needAuth флаг, нужна ли авторизация для доступа к этим данным
     * @return экземпляр класса с заполненными данными
     */
    public static <T> T fillJsonClass(Class<T> valueType, String url, boolean needAuth) {
        final ObjectMapper mapper = new ObjectMapper();
        final HttpResponse rGet = callSomeRestApi(url, needAuth, null);

        try {
            final String content = Utils.toString(rGet);

            Log.d(TAG, url + "|" + needAuth);
            Log.d(TAG, content);

            return mapper.readValue(content, valueType);
        } catch (IOException ex) {
            String message = "cannot map json data to class";
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        }
    }

    /**
     * Натянуть Json-данные на объект.
     *
     * @param valueType Класс объекта, на который нужно натянуть данные.
     * @param jsonData plain-json данные
     * @return экземпляр класса с заполненными данными
     */
    public static <T> T fillJsonClass(Class<T> valueType, final String jsonData) {
        final ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(jsonData, valueType);
        } catch (IOException ex) {
            String message = "cannot map json data to class";
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        }
    }

    /**
     * Получить от сервера тикет TGT. <br> Тикет приходит как часть заголовка Location.<br>
     *
     * @param url Полный урл, на который нужно сделать пост-запрос для получения TGT тикета.
     * @return содержимое заголовка Location. как правило это полный урл, включая имя сервера и путь. <br>
     * возвращается улр такого вида: <br>
     * http://SERVER:PORT/cas-server-3.4.11/v1/tickets/TGT-179-RaBe4efNwaVTalmd1qMBmqVRUNPqsMO7Ng3nr0Sy6cFFlb7LAA-cas01.example.org <br>
     * после последнего слеша '/' идет сам тикет
     *
     */
    public static String getTGT(String url, List<NameValuePair> params) {
        final HttpResponse rPost = Utils.performPost(url, params);
        Log.d(TAG,"rPost : " + rPost.getFirstHeader(TGT_HEADER_NAME));
        // TODO: прошла ли авторизация проверять по возвращаемому коду
        if (rPost.getStatusLine().getStatusCode() == 200 ||
            rPost.getStatusLine().getStatusCode() == 201 ||
            rPost.getStatusLine().getStatusCode() == 302) {
            if (rPost.containsHeader(TGT_HEADER_NAME)) {
                return rPost.getHeaders(TGT_HEADER_NAME)[0].getValue();
            } else {
                String message = "cant get TGT from url \'" + url + "\'";
                TableUtils.createLogMessage(message);
                Log.e(TAG, message);
                return null;
            }
        }else{
            return null;
        }
    }

    /**
     * Выполнить указанный rest api вызов.<br>
     * Если jsonObject == null, выполняется GET-запрос,
     * иначе выполняется POST-запрос, в теле которого содержится сериализованный jsonObject
     *
     * @param apiCall урл api-вызова (вида "/server/who", т.е. без адреса сервера)
     * @param needAuth флаг, нужна ли авторизация (т.е. нужно ли получать ST-XX тикет) для доступа к этому api
     * @param jsonData сериализованный json-данные
     * @return HttpResponse, из которого можно восстановить данные ответа
     */
    public static HttpResponse callSomeRestApi(String apiCall, boolean needAuth, String jsonData) {
        final String ENDPOINT_REST_API = SettingsActivity.getEndpointRestApiUrl();
        String serverApiCall = ENDPOINT_REST_API + apiCall;

        try {
            if (needAuth)   // получаем одноразовый тикет ST-XXX
                serverApiCall += getSTticket(serverApiCall, App.prefs.getTGTurl());

            Log.d(TAG, serverApiCall);
            //TODO: проверять, что вызов возвращает данные. (вдруг мы неавторизованы)

            if(jsonData == null) {
                final HttpResponse rGet = Utils.performGet(serverApiCall, needAuth);
                if (rGet == null) {
                    String message = "cant call (GET) some api: " + serverApiCall;
                    TableUtils.createLogMessage(message, "");
                    Log.e(TAG, message);
                    return null;
                }

                return rGet;
            } else {
                final HttpResponse rPost = Utils.performPostJson(serverApiCall, needAuth, jsonData);
                if(rPost == null)
                    throw new IllegalStateException("cant call (POST) some api: " + serverApiCall);
                return rPost;
            }
        } catch (Exception e) {
            Log.d(TAG, null, e);
            return null;
        }
    }

    /**
     * Получить одноразовый тикет вида ST-XXX для доступа к rest-ресурсам, требующим аутентификации.
     *
     * @param serverApiCall полный урл конкретного rest api вызова, включая адрес сервера
     * @param serverAuth урл, по которому происходит выдача ST-тикетов
     * @return строку с закодированным в utf-8 параметром (ST-тикетом), которую можно прикреплять сзади к основному гет-запросу
     * @throws IOException пробрасываем исключение наверх
     */
    private static String getSTticket(String serverApiCall, String serverAuth) {

        List<NameValuePair> postParams = new ArrayList<NameValuePair>();
        postParams.add(new BasicNameValuePair("service", serverApiCall));

        final HttpResponse rPost = Utils.performPost(serverAuth, postParams);
        final String ST_TICKET = Utils.toString(rPost);

        List<NameValuePair> retParams = new ArrayList<NameValuePair>();
        retParams.add(new BasicNameValuePair("ticket", ST_TICKET));

        return ("?" + URLEncodedUtils.format(retParams, "utf-8"));
    }

    /**
     * Проверить, авторизован ли текущий пользователь. <br>
     * В случае удачной авторизации (пользователь найден в серверной БД) возвращается json-объект SystemUser. <br>
     * В случае неудачной авторизации возвращается http код 403.
     * @return false, если код==403. иначе - true
     */
    public static boolean isAuthorized() {

        final HttpResponse who = RestClient.callSomeRestApi("/server/who", true, null);
        final int code = Utils.getResponseCode(who);

        Log.d(TAG,"server/who code: " + code);

        // TODO: вероятно, помимо кода, надо проверять, что данные мапятся в json-объект SystemUser
        if(code == 403)
            return false;

        return true;
    }
    //}}}

    //{{{ Вызовы, требующие особой обработки

    /**
     * FIXME: теперь серверное время получаем другим способом. переделать. см. ФС андроид
     * Получить серверное время. <br>
     * Сервер возвращает обычную строку, которая не мапится напрямую ни в один из классов,
     * описанных в выданной Кроком схеме.<br>
     * Поэтому получаем время и парсим, как нам вздумается. <br>
     *
     * @return время строкой (Пример: 2014-16-10.12:47:43.560)
     * @throws IOException
     */
    public static String getServerTime() {
        final HttpResponse response = callSomeRestApi("/server/time", true, null);
        return Utils.toString(response);
    }



    public static HttpResponse getAttachmentByID( String id ) throws IOException {
        final HttpResponse response = callSomeRestApi("/Attachment/"+id, true, null);
        return response;
    }

    //}}}

//
//    public static void sync() {
//        Log.i(TAG, "sync");
//        for (RestResource res : Constants.beforeMainScreenStart) {
//            Log.i(TAG, res.getApiUrlSuf());
//            HttpResponse response = callSomeRestApi(res.getApiUrlSuf(), res.isNeedAuth());
//
//            String responseJson = Utils.toString(response);
//            Log.v(TAG, responseJson);
//        }
//    }
}
