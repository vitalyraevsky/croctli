
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for ViewCustomization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ViewCustomization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="KeyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisplayInRibbon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayInTaskArea" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayInFilter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayInSort" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="MayBeDisplayInRibbon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayNameInRibbon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayNameInTaskArea" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayInRibbonReq" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DisplayNameInTaskTab" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SourceSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ViewCustomization", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "keyName",
    "displayInRibbon",
    "displayInTaskArea",
    "displayInFilter",
    "displayInSort",
    "mayBeDisplayInRibbon",
    "displayNameInRibbon",
    "displayNameInTaskArea",
    "displayInRibbonReq",
    "displayNameInTaskTab",
    "sourceSystemId"
})
@DatabaseTable(tableName = "view_customization")
public class ViewCustomization {

    @DatabaseField(id = true, canBeNull = true)
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    protected String id;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "KeyName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("KeyName")
    protected String keyName;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayInRibbon", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayInRibbon")
    protected boolean displayInRibbon;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayInTaskArea", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayInTaskArea")
    protected boolean displayInTaskArea;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayInFilter", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayInFilter")
    protected boolean displayInFilter;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = "displayInSort")
    @XmlElement(name = "DisplayInSort", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayInSort")
    protected boolean displayInSort;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "MayBeDisplayInRibbon", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("MayBeDisplayInRibbon")
    protected boolean mayBeDisplayInRibbon;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayNameInRibbon", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayNameInRibbon")
    protected boolean displayNameInRibbon;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayNameInTaskArea", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayNameInTaskArea")
    protected boolean displayNameInTaskArea;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayInRibbonReq", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayInRibbonReq")
    protected boolean displayInRibbonReq;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @XmlElement(name = "DisplayNameInTaskTab", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @JsonProperty("DisplayNameInTaskTab")
    protected boolean displayNameInTaskTab;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "SourceSystemId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("SourceSystemId")
    protected String sourceSystemId;

    @DatabaseField(dataType = DataType.STRING)
    protected String serverTime;
    public String getServerTime() {
        return serverTime;
    }
    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the keyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     * Sets the value of the keyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyName(String value) {
        this.keyName = value;
    }

    /**
     * Gets the value of the displayInRibbon property.
     * 
     */
    public boolean isDisplayInRibbon() {
        return displayInRibbon;
    }

    /**
     * Sets the value of the displayInRibbon property.
     * 
     */
    public void setDisplayInRibbon(boolean value) {
        this.displayInRibbon = value;
    }

    /**
     * Gets the value of the displayInTaskArea property.
     * 
     */
    public boolean isDisplayInTaskArea() {
        return displayInTaskArea;
    }

    /**
     * Sets the value of the displayInTaskArea property.
     * 
     */
    public void setDisplayInTaskArea(boolean value) {
        this.displayInTaskArea = value;
    }

    /**
     * Gets the value of the displayInFilter property.
     * 
     */
    public boolean isDisplayInFilter() {
        return displayInFilter;
    }

    /**
     * Sets the value of the displayInFilter property.
     * 
     */
    public void setDisplayInFilter(boolean value) {
        this.displayInFilter = value;
    }

    /**
     * Gets the value of the displayInSort property.
     * 
     */
    public boolean isDisplayInSort() {
        return displayInSort;
    }

    /**
     * Sets the value of the displayInSort property.
     * 
     */
    public void setDisplayInSort(boolean value) {
        this.displayInSort = value;
    }

    /**
     * Gets the value of the mayBeDisplayInRibbon property.
     * 
     */
    public boolean isMayBeDisplayInRibbon() {
        return mayBeDisplayInRibbon;
    }

    /**
     * Sets the value of the mayBeDisplayInRibbon property.
     * 
     */
    public void setMayBeDisplayInRibbon(boolean value) {
        this.mayBeDisplayInRibbon = value;
    }

    /**
     * Gets the value of the displayNameInRibbon property.
     * 
     */
    public boolean isDisplayNameInRibbon() {
        return displayNameInRibbon;
    }

    /**
     * Sets the value of the displayNameInRibbon property.
     * 
     */
    public void setDisplayNameInRibbon(boolean value) {
        this.displayNameInRibbon = value;
    }

    /**
     * Gets the value of the displayNameInTaskArea property.
     * 
     */
    public boolean isDisplayNameInTaskArea() {
        return displayNameInTaskArea;
    }

    /**
     * Sets the value of the displayNameInTaskArea property.
     * 
     */
    public void setDisplayNameInTaskArea(boolean value) {
        this.displayNameInTaskArea = value;
    }

    /**
     * Gets the value of the displayInRibbonReq property.
     * 
     */
    public boolean isDisplayInRibbonReq() {
        return displayInRibbonReq;
    }

    /**
     * Sets the value of the displayInRibbonReq property.
     * 
     */
    public void setDisplayInRibbonReq(boolean value) {
        this.displayInRibbonReq = value;
    }

    /**
     * Gets the value of the displayNameInTaskTab property.
     * 
     */
    public boolean isDisplayNameInTaskTab() {
        return displayNameInTaskTab;
    }

    /**
     * Sets the value of the displayNameInTaskTab property.
     * 
     */
    public void setDisplayNameInTaskTab(boolean value) {
        this.displayNameInTaskTab = value;
    }

    /**
     * Gets the value of the sourceSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemId() {
        return sourceSystemId;
    }

    /**
     * Sets the value of the sourceSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemId(String value) {
        this.sourceSystemId = value;
    }

}
