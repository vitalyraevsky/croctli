package ru.baccasoft.croctli.android.fragments;

/**
 * Связывает фрагмент, в котором отображаются документы,
 * с попапом, в котором показывается список всех файлов
 */

public interface IAttachmentOpener {
    public void attachmentSelected(String attachmentId, int attachmentPosition);
}
