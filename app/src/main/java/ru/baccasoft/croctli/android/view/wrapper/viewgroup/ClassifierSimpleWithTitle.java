package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;

import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.ClassifierSimple;

/**
 * Created by developer on 07.11.14.
 */
public class ClassifierSimpleWithTitle extends LinearLayout {
    public ClassifierSimpleWithTitle(Context context, String title, Map<String, String> items, EntityProperty p) {
        super(context);

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView titleView = ViewUtils.makeTitleTextView(context, title, null);

        ClassifierSimple classifierSimple = new ClassifierSimple(context, items, p.getId());
        classifierSimple.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        classifierSimple.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d("", "onFocusChange");
            }
        });

        addView(titleView);
        addView(classifierSimple);

    }
}
