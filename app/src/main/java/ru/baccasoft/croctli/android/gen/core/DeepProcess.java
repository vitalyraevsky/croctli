
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeepProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeepProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Shallow" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Process"/>
 *         &lt;element name="Tree" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfDeepEntityProperty"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeepProcess", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "shallow",
    "tree"
})
public class DeepProcess {

    @XmlElement(name = "Shallow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected Process shallow;
    @XmlElement(name = "Tree", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfDeepEntityProperty tree;

    /**
     * Gets the value of the shallow property.
     * 
     * @return
     *     possible object is
     *     {@link Process }
     *     
     */
    public Process getShallow() {
        return shallow;
    }

    /**
     * Sets the value of the shallow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process }
     *     
     */
    public void setShallow(Process value) {
        this.shallow = value;
    }

    /**
     * Gets the value of the tree property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDeepEntityProperty }
     *     
     */
    public ArrayOfDeepEntityProperty getTree() {
        return tree;
    }

    /**
     * Sets the value of the tree property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDeepEntityProperty }
     *     
     */
    public void setTree(ArrayOfDeepEntityProperty value) {
        this.tree = value;
    }

}
