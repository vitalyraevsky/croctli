package ru.baccasoft.croctli.android.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeWrapper extends DateWrapperBase implements IDateField {

    public DateTimeWrapper( String value ) { super(value, DATETIME_PATTERNS); }
    public DateTimeWrapper( Date value ) { super(value); }

    @Override
    public String toGuiString() {
        return getValue(DateFormat.getDateTimeInstance(), "");
    }

    @Override
    public String toRestString() {
        return getValue(new SimpleDateFormat(DEFAULT_DATETIME), "0000-00-00T00:00:00.000");
    }

}
