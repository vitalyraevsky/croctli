package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ProcessType;
import ru.baccasoft.croctli.android.gen.core.ProcessTypeSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.ProcessTypeAPI;

import java.util.List;

public class ProcessTypeSync implements ISync {
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        ProcessTypeAPI processTypeAPI = new ProcessTypeAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<ProcessTypeSet, ProcessType>();

        @SuppressWarnings("unchecked")
        List<ProcessType> processTypes = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, processTypeAPI);

        for(ProcessType pt : processTypes) {
            TableUtils.createOrUpdateProcessType(pt);
        }
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        ProcessTypeAPI processTypeAPI = new ProcessTypeAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, ProcessType>();

        @SuppressWarnings("unchecked")
        List<String> processTypeIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, processTypeAPI);

        TableUtils.deleteProcessTypes(processTypeIds);
    }

//    @Override
//    public void getList() {
//
//    }
}
