
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for TaskStateInSystem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskStateInSystem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NameInSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LocalNameInSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskStateInSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "nameInSystem",
    "localNameInSystem",
    "sourceSystemId"
})
@DatabaseTable(tableName = "task_state_in_system")
public class TaskStateInSystem {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("Id")
    @DatabaseField(id = true, dataType = DataType.STRING, canBeNull = false)
    protected String id;

    @XmlElement(name = "NameInSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("NameInSystem")
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    protected String nameInSystem;

    @XmlElement(name = "LocalNameInSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("LocalNameInSystem")
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    protected String localNameInSystem;

    @XmlElement(name = "SourceSystemId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @JsonProperty("SourceSystemId")
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    protected String sourceSystemId;

    @DatabaseField(dataType = DataType.STRING)
    protected String serverTime;
    public String getServerTime() {
        return serverTime;
    }
    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the nameInSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameInSystem() {
        return nameInSystem;
    }

    /**
     * Sets the value of the nameInSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameInSystem(String value) {
        this.nameInSystem = value;
    }

    /**
     * Gets the value of the localNameInSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalNameInSystem() {
        return localNameInSystem;
    }

    /**
     * Sets the value of the localNameInSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalNameInSystem(String value) {
        this.localNameInSystem = value;
    }

    /**
     * Gets the value of the sourceSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemId() {
        return sourceSystemId;
    }

    /**
     * Sets the value of the sourceSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemId(String value) {
        this.sourceSystemId = value;
    }

}
