package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import ae.javax.xml.bind.JAXBException;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.PptxToHtml;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.SlideToImageConversionHandler;

public class PPTXProcessor implements ConvertationProcessor {

    private String TAG = PPTXProcessor.class.getSimpleName();

    private PptxToHtml pptxToHtml;
    private Writer writer;
    private Handler handler;


    @Override
    public void init(String officeFileName) throws IOException {
        Log.d(TAG,"init pptx converter with file: " + officeFileName );
        writer = new StringWriter();
        handler = new Handler();
        pptxToHtml = PptxToHtml.create(officeFileName, writer);
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException {
        pptxToHtml.setImageConversionHandler(new SlideToImageConversionHandler(imageDirPath, activity, handler));
        pptxToHtml.printSlides();
        String html = writer != null ? writer.toString() : null ;
        writer.close();
        return html;
    }

}
