package ru.baccasoft.croctli.android.sync;

import android.util.Log;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.SpecialCondition;
import ru.baccasoft.croctli.android.gen.core.SpecialConditionSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystem;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.SpecialConditionAPI;

import java.sql.SQLException;

/**
 * получить список (один раз?)
 */
public class SpecialConditionSync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystem.class.getSimpleName();

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        SpecialConditionAPI specialConditionAPI = new SpecialConditionAPI();
        final SpecialConditionSet conditionSet = specialConditionAPI.modifyGet(null, null, 0);
        for (SpecialCondition condition : conditionSet.getArrayOf().getItem()) {
            try {
                condition.setServerTime(conditionSet.getServerTimeString());
                App.getDatabaseHelper().getSpecialConditionDao().createOrUpdate(condition);
            } catch (SQLException e) {
                String message = "cannot save SpecialCondition to db";
                TableUtils.createLogMessage(message, e);
                Log.e(TAG, message, e);
            }
        }
    }


    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
