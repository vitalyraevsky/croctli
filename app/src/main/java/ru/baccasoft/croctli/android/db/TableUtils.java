package ru.baccasoft.croctli.android.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.dao.Dao;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.gen.core.Process;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class TableUtils {
    @SuppressWarnings("unused")
    private static final String TAG = TableUtils.class.getSimpleName();

    public final static String TASK_READ_SQL_MESSAGE = "cant read from task db";
    public static final String TASK_WRITE_SQL_MESSAGE = "cant write to task db";
    public final static String CLASSIFIER_READ_SQL_MESSAGE = "cant read from classifier db";
    public final static String ENTITY_PROPERTY_READ_SQL_MESSAGE = "cant read from entityProperty db";
    public final static String ENTITY_PROPERTY_WRITE_SQL_MESSAGE = "cant write entityProperty to db";

    public final static String GENERAL_READ_MESSAGE = "cant read from db";
    public final static String GENERAL_WRITE_MESSAGE = "cant write to db";

    /**
     *
     * @param id
     * @return Action, если нашли.
     *  или null, во всех остальных случаях
     */
    public static Action getActionById(String id) {
        try {
            Action action = App.getDatabaseHelper().getActionDAO().queryForId(id);

            if (action == null) {
                String message = "cant find action with id=\'" + id + "\'";
                TableUtils.createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return action;
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
        }
        return null;
    }

    /**
     * Удаляет задачи (Task) и связанные с каждой задачей EP рекурсивно.<br>
     * Удаление каждой задачи работает (по идее) в одну транзакцию.
     *
     * @param ids список идентификаторов задач
     */
    public static Integer deleteTasks(Collection<String> ids) {
        int ret = 0;
        for(String taskId : ids) {
            ret += deleteTask(taskId);
        }
        return ret;
    }

    /**
     * Удаляет задачу (Task) и все рекурсивно связанные EP.<br>
     * Работает (по идее) в одну транзакцию.
     *
     */
    public static Integer deleteTask(String taskId) {
        int ret = 0;
        final DatabaseHelper dbHelper = App.getDatabaseHelper();
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            List<String> epIds = dbHelper.getEntityPropertyDAO()
                    .getIdsByTaskId(taskId);

            ret += deleteEntityPropertiesRecursive(epIds);

            ret += App.getDatabaseHelper().getTasksDao()
                    .deleteById(taskId);

            db.setTransactionSuccessful();
            return ret;
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
        } finally {
            db.endTransaction();
        }
        return null;
    }

    /**
     * Удаляет связанные EP ресурсивно.
     *
     * @param epIds - список корневых EP, откуда начать удаление
     * @throws SQLException
     */
    public static Integer deleteEntityPropertiesRecursive(List<String> epIds) throws SQLException {
        int ret = 0;

        for(String epId : epIds) {
            List<String> epChildIds = App.getDatabaseHelper().getEntityPropertyDAO()
                    .getChildsIdsByEpId(epId);

            ret += deleteEntityProperties(epChildIds);

//            App.getDatabaseHelper().getEntityPropertyDAO().deleteIds(epChildIds);
            ret += App.getDatabaseHelper().getEntityPropertyDAO().deleteById(epId);
        }
        return ret;
    }

    public static StringSet getTaskIds() {
        List<String> idList;
        StringSet ret = new StringSet();
        ArrayOfString arrayOfString = new ArrayOfString();

        try {
            idList = App.getDatabaseHelper().getTasksDao()
                    .getAllIds();
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);

            return null;
        }

        arrayOfString.setItem(idList);
        ret.setArrayOf(arrayOfString);

        return ret;
    }

    public static Boolean taskIdExist(String id) {
        try {
            return App.getDatabaseHelper().getTasksDao().idExists(id);
        } catch (SQLException e) {
            createLogMessage(TASK_READ_SQL_MESSAGE, e);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, e);
            return null;
        }
    }

    public static Task getTaskById(String id) {
        try {
            return App.getDatabaseHelper().getTasksDao().queryForId(id);
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static List<String> getDeletedTaskIds() {
        try {
            return App.getDatabaseHelper().getTasksDao()
                    .getDeletedTaskIds();
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Обновляет задачу в базе
     * @param task
     * @return
     */
    public static Integer updateTask(Task task) {
        try {
            task.setChangeDateTime(RestApiDate.now());
            return App.getDatabaseHelper().getTasksDao().update(task);
        } catch (SQLException ex) {
            createLogMessage(TASK_WRITE_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_WRITE_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static List<Task> getChangedTasks(RestApiDate dateFrom) {
        try {
            return App.getDatabaseHelper().getTasksDao().getModifiedTasks(dateFrom.toDateTime());
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static List<EntityProperty> getChangedEntityPeoperties() {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO().getModifiedEntityProperties();
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Обновляет состояие закладки в базе
     * @param tab
     * @return
     */
    public static Integer updateTab(Tab tab) {
        try {
            tab.setChangeDateTime(RestApiDate.now());
            return App.getDatabaseHelper().getTabDao().update(tab);
        } catch (SQLException ex) {
            createLogMessage(TASK_WRITE_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_WRITE_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static Integer updateEntityProperty(EntityProperty ep, boolean wasChanded) {
        try {
            ep.setWasChanged(wasChanded);
            return App.getDatabaseHelper().getEntityPropertyDAO().update(ep);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static List<EntityProperty> getEntityPropertyByTaskId(String taskId) {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO().getByTaskId(taskId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static List<Tab> getChangedTabs(RestApiDate dateFrom) {
        try {
            return App.getDatabaseHelper().getTabDao().getModifiedTabs(dateFrom.toDateTime());
        } catch (SQLException ex) {
            createLogMessage(TASK_READ_SQL_MESSAGE, ex);
            Log.e(TAG, TASK_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static Classifier getClassifierById(String classifierId) {
        try {
            return App.getDatabaseHelper().getClassifierDAO().queryForId(classifierId);
        } catch (SQLException ex) {
            createLogMessage(CLASSIFIER_READ_SQL_MESSAGE, ex);
            Log.e(TAG, CLASSIFIER_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Получить список дочерних EntityProperty.
     *
     * @param parentId идентификатор родителя (поле entityPropertyId у дочерних свойств)
     * @return список всех потомков, за исключением тех, у которых
     * поле orderInArray == -1 (исключенные EntityProperty являются шаблоном и не отображаются, см. ФС андроид)
     */
    public static List<EntityProperty> getEntityPropertyChildsById(String parentId) {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO().getChildsById(parentId);
        } catch (SQLException ex) {
            createLogMessage(ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            Log.e(TAG, ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Получить список ItemField для наполнения контрола при отображении в режиме displayMode=0 <br>
     * для случая, если атрибут EntityProperty.ClassifierDisplayField заполнен.
     *
     * @param p
     * @return может вернуть null
     */
    public static List<ItemField> getItemFieldsForDisplayMode0(EntityProperty p) {
        try {
            List<ClassifierItem> ci = App.getDatabaseHelper().getClassifierItemDAO()
                    .getForDisplayMode0(p);

            switch (ci.size()) {
                case 0:
                    return null;
                case 1:
                    return App.getDatabaseHelper().getItemFieldDAO()
                            .getByClassifierItem(ci.get(0));
                default:
                    String message = "must be only one ClassifierItem but we have " + ci.size();
                    createLogMessage(message, "");
                    Log.e(TAG, message);
                    return null;
            }
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }


    public static List<ClassifierItem> getClassifierItemsByClassifierId(String classifierTypeId) {
        try {
            return App.getDatabaseHelper().getClassifierItemDAO().getByClassifierId(classifierTypeId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Выбрать ItemField по полю ItemField.name
     *
     * @param name
     * @return единственный ItemField. или null
     */
    public static ItemField getItemFieldByName(String name) {
        try {
            List<ItemField> itemFields = App.getDatabaseHelper().getItemFieldDAO()
                    .getByName(name);
            switch (itemFields.size()) {
                case 0:
                    return null;
                case 1:
                    return itemFields.get(0);
                default:
                    String message = "must be only one itemField but we have " + itemFields.size();
                    createLogMessage(message, "");
                    Log.e(TAG, message);
                    return null;
            }
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }


    /**
     * Получить один ClassifierItem
     *
     * @param classifierId идентификатор связанного EntityProperty
     * @param code значение поля 'code' ClassifierItem. ?всегда? берется из EntityProperty.value
     * @return может вернуть null, если ничего не найдено
     */
    public static ClassifierItem getClassifierItemByCode(String classifierId, String code) {
        try {
            List<ClassifierItem> classifierItems = App.getDatabaseHelper().getClassifierItemDAO()
                    .getByCode(classifierId, code);
            switch (classifierItems.size()) {
                case 0:
                    return null;
                case 1:
                    return classifierItems.get(0);
                default:
                    String message = "must be only one classifierItem but we have " + classifierItems.size();
                    createLogMessage(message, "");
                    Log.e(TAG, message);
                    return null;
            }
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Получить список элементов справочника по полю 'code'.
     *
     * @param value строка, содержщая список кодов (ClassifierItem.code), разделенных символами '@'
     * @return
     */
    public static List<ClassifierItem> getClassifierItemsByCodes(String classifierId, String value) {
        List<ClassifierItem> ret = new ArrayList<ClassifierItem>();
        if(ViewUtils.isEmpty(value))
            return ret;

        String[] codes = value.split("@");
        for(String c : codes) {
            Log.d("parse value", c);
            ClassifierItem item = getClassifierItemByCode(classifierId, c);
            if(item != null)
                ret.add(item);
        }

        return ret;
    }

    public static EntityProperty getEntityPropertyById(String epId) {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO()
                    .queryForId(epId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Получить корневой EP, в котором содержится информация о поручениях.
     * А именно EP, который должен содержать дочерние EP, каждое из которых
     * содержит массив объектов (EP), относящихся к отдельной резолюции.
     *
     * @param taskId
     * @return может вернуть null, если ничего не найдено
     */
    public static EntityProperty getRootEpForErrands(String taskId) {
        try {
            List<EntityProperty> eps = App.getDatabaseHelper().getEntityPropertyDAO()
                    .getErrands(taskId);
            if(eps.size() == 0)
                return null;
            if(eps.size() > 1) {
                String message = "must be only one root EP with code \'instructions\'";
                createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return eps.get(0);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

//    /**
//     *
//     * @param taskId
//     * @return
//     */
//    public static List<EntityProperty> getChildEPsForErrands(String taskId) {
//        EntityProperty rootEp = getRootEpForErrands(taskId);
//        if(rootEp == null)
//            return null;
//        return getEntityPropertyChildsById(rootEp.getId());
//    }

    /**
     * Получить шаблон для добавления новых элементов в массив объектов
     *
     * @param parentId идентификатор родителя (поле entityPropertyId у дочерних свойств)
     * @return список всех потомков, у которых
     * поле orderInArray == -1 (исключенные EntityProperty являются шаблоном для добавления новых элементов)
     */
    public static EntityProperty getEPObjectArrayAddingTemplate(String parentId) {
        try {
            List<EntityProperty> eps = App.getDatabaseHelper().getEntityPropertyDAO().getTemplateChildById(parentId);
            for(EntityProperty p: eps) {
                Log.d(TAG, "id:\'" + p.getId() + "\'");
            }
            if(eps.size() != 1) {
                String message = "must be only one child with \"orderInArray\"==-1 but we have " +
                    eps.size() + " for parent EP with id=\'" + parentId + "\'";
                createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return eps.get(0);
        } catch (SQLException ex) {
            createLogMessage(ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            Log.e(TAG, ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static Integer insertNewEntityProperty(EntityProperty newEp) {
        Log.d(TAG, "trying to insert new EntityProperty in db");
        try {
            int count = App.getDatabaseHelper().getEntityPropertyDAO().create(newEp);
            if(count != 1) {
                String message = "entityProperty with id:\'" + newEp + "\' was not inserted in db";
                TableUtils.createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return count;
        } catch (SQLException ex) {
            createLogMessage(ENTITY_PROPERTY_WRITE_SQL_MESSAGE, ex);
            Log.e(TAG, ENTITY_PROPERTY_WRITE_SQL_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteEntityProperties(Collection<String> ids) {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(ENTITY_PROPERTY_WRITE_SQL_MESSAGE, ex);
            Log.e(TAG, ENTITY_PROPERTY_WRITE_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     *
     * @param taskId
     * @param displayGroup
     * @return может вернуть null, если ничего не найдено
     */
    public static EntityProperty findEpByDisplayGroup(String taskId, String displayGroup) {
        try {
            List<EntityProperty> eps = App.getDatabaseHelper().getEntityPropertyDAO().getByDisplayGroup(taskId, displayGroup);
            if(eps.size() > 1) {
                String message = "found " + eps.size() + " EPs with \"displayGroup\"=" + displayGroup +
                        "while expecting not more than 1";
                TableUtils.createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            if(eps.size() == 1)
                return eps.get(0);

            return null;
        } catch (SQLException ex) {
            createLogMessage(ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            Log.e(TAG, ENTITY_PROPERTY_READ_SQL_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Если для задачи выбрано завершающее действие, все ее поля отображаются только для чтения.<br>
     * Подробно: если заполнено поле Task.performedAction и
     * в соответствующем действии не проставлено поле action.isWithoutComplete
     * значит задача завершена. Т.е. показываем всегда только для чтения
     *
     * @param task
     * @return
     */
    public static boolean isTaskCompleted(Task task) {
        String actionId = task.getPerformedActionId();
        if(ViewUtils.isEmpty(actionId))
            return false;

        Action action = TableUtils.getActionById(actionId);
        return !action.isWithoutComplete();
    }

    public static TaskStateInSystem getTaskStateById(String stateId) {
        try {
            TaskStateInSystem taskStateInSystem = App.getDatabaseHelper().getTaskStateInSystemDao()
                    .queryForId(stateId);
            if(taskStateInSystem == null) {
                String message = "cant find taskStateInSystem with id:\'" + stateId + "\'";
                createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return taskStateInSystem;
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static Process getProcessById(String processId) {
        try {
            Process process = App.getDatabaseHelper().getProcessDao()
                    .queryForId(processId);
            if(process == null) {
                String message = "cant find process with id:\'" + processId + "\'";
                createLogMessage(message, "");
                Log.e(TAG, message);
                return null;
            }
            return process;
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static UserInSystem getUserInSystemById(String creatorId) {
        try {
            return App.getDatabaseHelper().getUserInSystemDao()
                    .queryForId(creatorId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static ProcessType getProcessTypeById(String processTypeId) {
        try {
            return App.getDatabaseHelper().getProcessTypeDao()
                    .queryForId(processTypeId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static Adapter getAdapterById(String adapterId) {
        try {
            return App.getDatabaseHelper().getAdapterDao()
                    .queryForId(adapterId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static SourceSystem getSourceSystemById(String sourceSystemId) {
        try {
            return App.getDatabaseHelper().getSourceSystemDao()
                    .queryForId(sourceSystemId);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }


    public static List<Task> getTasksExceptClosed() {
        try {
            List<Task> tasks = App.getDatabaseHelper().getTasksDao()
                    .queryForAll();
            List<Task> completedTasks = new ArrayList<Task>(tasks.size());
            for(Task t:tasks) {
                String actionId = t.getPerformedActionId();
                if(ViewUtils.isNotEmpty(actionId)) {
                    Action action = getActionById(actionId);
                    if(!action.isWithoutComplete()) {
                        completedTasks.add(t);
                    }
                }
            }
            tasks.removeAll(completedTasks);
            return tasks;
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static List<Task> getAllTasks(){
        List<Task> tasks = null;
        try {
            tasks = App.getDatabaseHelper().getTasksDao()
                    .queryForAll();
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
        return  tasks;
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateClassifier(Classifier c) {
        try {
            return App.getDatabaseHelper().getClassifierDAO().createOrUpdate(c);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateSourceSystem(SourceSystem ss) {
        try {
            return App.getDatabaseHelper().getSourceSystemDao().createOrUpdate(ss);
        } catch (SQLException e) {
            String message = "cannot save SourceSystem to db";
            createLogMessage(message, e);
            Log.e(TAG, message, e);
            return null;
        }
    }

    public static Integer deleteSourceSystem(List<String> ids) {
        try {
            return App.getDatabaseHelper().getSourceSystemDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteClassifiers(List<String> ids) {
        try {
            return App.getDatabaseHelper().getClassifierDAO()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateViewCustomization(ViewCustomization v) {
        try {
            return App.getDatabaseHelper().getViewCustomizationDao().createOrUpdate(v);
        } catch (SQLException e) {
            createLogMessage(GENERAL_WRITE_MESSAGE, e);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, e);
            return null;
        }
    }

    public static Integer deleteViewCustomizations(List<String> ids) {
        try {
            return App.getDatabaseHelper().getViewCustomizationDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateAdapter(Adapter a) {
        try {
            return App.getDatabaseHelper().getAdapterDao()
                    .createOrUpdate(a);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteAdapters(List<String> ids) {
        try {
            return App.getDatabaseHelper().getAdapterDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateSystemUser(SystemUser su) {
        try {
            return App.getDatabaseHelper().getSystemUserDao()
                    .createOrUpdate(su);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteSystemUsers(List<String> ids) {
        try {
            return App.getDatabaseHelper().getSystemUserDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateTaskStateInSystem(TaskStateInSystem ts) {
        try {
            return App.getDatabaseHelper().getTaskStateInSystemDao()
                    .createOrUpdate(ts);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteTaskStateInSystems(List<String> ids) {
        try {
            return App.getDatabaseHelper().getTaskStateInSystemDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateProcessType(ProcessType pt) {
        try {
            return App.getDatabaseHelper().getProcessTypeDao()
                    .createOrUpdate(pt);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteProcessTypes(List<String> ids) {
        try {
            return App.getDatabaseHelper().getProcessTypeDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateProcess(Process p) {
        try {
            return App.getDatabaseHelper().getProcessDao()
                    .createOrUpdate(p);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteProcesses(List<String> ids) {
        try {
            return App.getDatabaseHelper().getProcessDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateEntityProperty(EntityProperty ep) {
        try {
            return App.getDatabaseHelper().getEntityPropertyDAO()
                    .createOrUpdate(ep);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateTask(Task task) {
        try {
            return App.getDatabaseHelper().getTasksDao()
                    .createOrUpdate(task);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateTaskUserUI(TaskUserUI ui) {
        try {
            return App.getDatabaseHelper().getTaskUserUiDao()
                    .createOrUpdate(ui);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }


    public static Integer deleteTaskUserUIs(List<String> ids) {
        try {
            return App.getDatabaseHelper().getTaskUserUiDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteHandmadeDelegates(List<String> ids) {
        try {
            return App.getDatabaseHelper().getHandmadeDelegateDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateHandmadeDelegate(HandmadeDelegate d) {
        try {
            return App.getDatabaseHelper().getHandmadeDelegateDao()
                    .createOrUpdate(d);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateTab(Tab tab) {
        try {
            return App.getDatabaseHelper().getTabDao()
                    .createOrUpdate(tab);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteTabs(List<String> ids) {
        try {
            return App.getDatabaseHelper().getTabDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer deleteRibbonSettings(List<String> ids) {
        try {
            return App.getDatabaseHelper().getRibbonSettingsDao()
                    .deleteIds(ids);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus createOrUpdateRibbonSetting(RibbonSettings r) {
        try {
            return App.getDatabaseHelper().getRibbonSettingsDao()
                    .createOrUpdate(r);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    /**
     *
     * @param id
     * @return возвращает null, если ничего не найдено
     */
    public static SystemUser getSystemUserById(String id) {
        try {
            return App.getDatabaseHelper().getSystemUserDao()
                    .queryForId(id);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static List<SystemUser> getAllUsers() {
        try {
            return App.getDatabaseHelper().getSystemUserDao().getAllUsers();
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static Dao.CreateOrUpdateStatus updateConflictDataForTask(String taskId, OperationResponse operationResponse) {
        Task task = getTaskById(taskId);
        String conflictSignal = operationResponse.getSubCode();
        String conflictMessage = operationResponse.getMessage();

        if(task == null) {
            String message = "cant find task with id:\'" + taskId + "\'";
            createLogMessage(message, "");
            Log.e(TAG, message);
            return null;
        }

        ConflictData conflictData = new ConflictData();
        try {
            conflictData.setConflictSignal(Integer.valueOf(conflictSignal));
        } catch (NumberFormatException ex) {
            String message = "conflictSignal=\'" + conflictSignal + "\' is malformed and cant be set to ConflictData.conflictSignal (expects Integer value)";
            createLogMessage(message, "");
            Log.e(TAG, message);
            return null;
        }

        conflictData.setConflictMessage(conflictMessage);
        conflictData.setTask(task);

        return createOrUpdateConflictData(conflictData);
    }

    private static Dao.CreateOrUpdateStatus createOrUpdateConflictData(ConflictData conflictData) {
        try {
            return App.getDatabaseHelper().getConflictDataDao()
                    .createOrUpdate(conflictData);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_READ_MESSAGE, ex);
            Log.e(TAG, GENERAL_READ_MESSAGE, ex);
            return null;
        }
    }

    public static ArrayOfLogMessage getNotSynchronizedLogMessages() {
        try {
            return App.getDatabaseHelper().getLogMessageDao()
                    .getNotSynchronized();
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    public static Integer markAsSentLogMessages(ArrayOfLogMessage logMessages) {
        int res = 0;

        try {
            for(LogMessage l : logMessages.getItem()) {
                l.setWasSynchronized(true);
                res += App.getDatabaseHelper().getLogMessageDao()
                        .update(l);
            }
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
        return res;
    }

    /**
     * Создать новую запись об ошибке и сохранить в базу.
     *
     * @return null, если произошла ошибка. иначе - число созданных записей в бд
     */
    public static Integer createLogMessage(String message, String additionalInfo) {
        try {
            LogMessage logMessage = new LogMessage(message, additionalInfo);

            return App.getDatabaseHelper().getLogMessageDao()
                    .create(logMessage);
        } catch (SQLException ex) {
            createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }

    }

    /**
     * Создать новую запись об ошибке и сохранить в базу.
     *
     * @return null, если произошла ошибка. иначе - число созданных записей в бд
     */
    public static Integer createLogMessage(String message, Throwable th) {
        return createLogMessage(message, Log.getStackTraceString(th));
    }

    public static Integer createMetaAuth(MetaAuth data) {
        try {
            // соответствующие связанные данные сохраняются в базу в переопределенном методе .create(..)
            return App.getDatabaseHelper().getMetaAuthDao()
                    .create(data);
        } catch (SQLException ex) {
            TableUtils.createLogMessage(GENERAL_WRITE_MESSAGE, ex);
            Log.e(TAG, GENERAL_WRITE_MESSAGE, ex);
            return null;
        }
    }

    /**
     * Создать новую запись об ошибке и сохранить в базу.
     *
     * @return null, если произошла ошибка. иначе - число созданных записей в бд
     */
    public static Integer createLogMessage(String message) {
        return createLogMessage(message, "");
    }

    public static SpecialCondition getSpecialConditionById(String specialConditionId)  {
        try {
            return App.getDatabaseHelper().getSpecialConditionDao().queryForId(specialConditionId);
        } catch (SQLException ex) {
            String message = "cannot load specialCondition by id: " + specialConditionId;
            TableUtils.createLogMessage(message, ex);
            Log.e(TAG, message, ex);
            return null;
        }
    }

    public static ConditionOperator getConditionOperatorById(String id) {
        try {
            return App.getDatabaseHelper().getConditionOperatorDao().queryForId(id);
        } catch (SQLException e) {
            TableUtils.createLogMessage(TableUtils.GENERAL_READ_MESSAGE, e);
            Log.e(TAG, TableUtils.GENERAL_READ_MESSAGE, e);
            return null;
        }
    }


}
