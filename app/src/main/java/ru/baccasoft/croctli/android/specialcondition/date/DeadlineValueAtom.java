package ru.baccasoft.croctli.android.specialcondition.date;

import ru.baccasoft.croctli.android.gen.core.Task;

public class DeadlineValueAtom implements IDateOperand {

	public static final String NAME = "deadline.value.date";

	@Override
	public Integer compute(Task task) { return DatetimeNowAtom.millisToInt(task.getDeadlineDateTime().getMillis()); }

}
