package ru.baccasoft.croctli.android.updater;


import android.os.AsyncTask;
import android.util.Log;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.sync.*;
import ru.baccasoft.croctli.android.updater.attachment.AttachmentSyncTask;

/**
 * Второй шаг в первой загрузке данных, выполняется после захода в приложение,
 * в качестве параметров передаются две даты dateFrom и dateTo
 */
public class FirstSyncStepTwoTask extends AsyncTask<StepOneResult, Integer, Void> {

    private static final String TAG = FirstSyncStepTwoTask.class.getSimpleName();

    protected final int steps = 9;   // число вызываемых методов
    protected final int maxProgress = 100;
    protected int progress;

    @Override
    protected Void doInBackground(StepOneResult... params) {

        if( params == null ) {
            throw new IllegalStateException("must be result of first step");
        }

        StepOneResult result = params[0];

        if( result.status == StepOneResult.FIRST_SYNC_STATUS.FAILED || result.status == StepOneResult.FIRST_SYNC_STATUS.UNKNOWN )
            throw new IllegalStateException("status must be success or skip");

        if( result.status == StepOneResult.FIRST_SYNC_STATUS.SKIP ) {
            publishProgress(maxProgress);
            return null;
        }

        Log.d(TAG,"First sync step two start");

        RestApiDate dateFrom = result.dateFrom;
        RestApiDate dateTo = result.dateTo;

        Log.d(TAG,"result.dateFrom : " + result.dateFrom + " result.dateTo : " + result.dateTo );

        Log.d(TAG,"process type sync ...");
        new ProcessTypeSync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "process type sync finished");

        Log.d(TAG,"process sync ...");
        new ProcessSync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "process sync finished");

        Log.d(TAG,"task sync ...");
        new TaskSync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "task sync finished");

        Log.d(TAG,"task user sync ...");
        new TaskUserUISync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "task user sync finished");

        Log.d(TAG,"entity property sync ...");
        new EntityPropertySync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "entity property sync finished");

        Log.d(TAG,"handmade delegate sync ...");
        new HandmadeDelegateSync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "handmade delegate sync finished");

        Log.d(TAG,"ribbon settings sync ...");
        new RibbonSettingsSync().atFirst(dateFrom, dateTo);
        updateProgress();
        Log.d(TAG, "ribbon settings sync finished");

        Log.d(TAG, "Attachment sync ...");
        new AttachmentSyncTask().execute(); // FIXME worker thread?!!
        updateProgress();
        Log.d(TAG, "Attachment sync finished");

        Log.d(TAG, "LogMessage sync ...");
        new LogMessageSyncTask().execute(); // FIXME worker thread?!!
        updateProgress();
        Log.d(TAG, "LogMessage sync finished");

        // сохраняем новую дату последней синхронизации
        App.prefs.setLastSyncDate(dateTo.toDateTime().getMillis());

        Log.d(TAG,"First sync step two finished");

        return null;
    }

    private void updateProgress() {
        progress += maxProgress/steps;
        publishProgress(progress);
    }
}
