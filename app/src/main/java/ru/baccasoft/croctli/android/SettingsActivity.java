package ru.baccasoft.croctli.android;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.*;
import android.util.Log;

import java.util.List;

public class SettingsActivity extends PreferenceActivity {
    @SuppressWarnings("unused")
    private static final String TAG = SettingsActivity.class.getSimpleName();

    /**
     * {@inheritDoc}
     *
     * Мы ориентируемся только на пару моделей планшетов,
     * поэтому всегда показываем несколько панелей.
     */
    @Override
    public boolean onIsMultiPane() {
        return true;
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.prefs_headers, target);
    }

    /***
     * Проверка для меню "Настройки"
     * isValidFragment - проверяется на киткате (api19) и выше - если ее нет, приложение падает
     * @param fragmentName
     * @return
     */
    @Override
    protected boolean isValidFragment(String fragmentName) {
        if(GeneralPreferenceFragment.class.getName().equals((fragmentName))){
            return true;
        }
        return false;
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener
            = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.prefs);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("server_address"));
            bindPreferenceSummaryToValue(findPreference("attachment_download_size"));
            bindPreferenceSummaryToValue(findPreference("synchronization_period"));
            bindPreferenceSummaryToValue(findPreference("version"));
        }
    }

    //{{{ Получение всех настроек
    /**
     * Получает адрес сервера
     * @return
     */
    public static String getEndpointRestApiUrl() {
        // получаем SharedPreferences, которое работает с файлом настроек
        Context ctx = App.getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        String key = ctx.getString(R.string.settings_server_address_key);
        String apiUrl = sp.getString(key, "");
//        Log.d(TAG, "api url from settings:\'" + apiUrl + "\'");
        return apiUrl;
    }

    /**
     * Получить максимальный размер файлов, которые можно автоматом синхронизировать
     *
     * @return максимальный размер в байтах. 0 - значит без ограничений
     */
    public static int getMaxDownloadSize() {
        Context ctx = App.getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        String key = ctx.getString(R.string.settings_attachment_download_size_key);
        String value = sp.getString(key, "0");
        return Integer.valueOf(value) * 1024;
    }

    /**
     * Получить период автоматической синхронизаци.
     * @return период синхронизации в секундах. 0 - значит только вручную.
     */
    public static int getSyncPeriod() {
        Context ctx = App.getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        String key = ctx.getString(R.string.settings_synchronization_period_key);
        String value = sp.getString(key, "0");
        return Integer.valueOf(value) * 60;
    }

    //}}}
}
