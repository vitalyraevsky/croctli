package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.CheckBox;
import ru.baccasoft.croctli.android.R;

/**
 * Created by developer on 06.11.14.
 */
public class BooleanScalar extends CheckBox {

    public BooleanScalar(Context context, boolean state, boolean isReadonly) {
        super(context);

        setChecked(state);
        setEnabled(!isReadonly);

        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        setTextSize(context.getResources().getDimension(R.dimen.px_16));
    }
}
