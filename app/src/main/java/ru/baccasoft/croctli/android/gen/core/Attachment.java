
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for Attachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LocalLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CheckSum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TrackKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "mimeType",
    "size",
    "localLocation",
    "checkSum",
    "trackKey"
})
@DatabaseTable(tableName = "attachment")
public class Attachment {

    public static final String EP_FK_FIELD = "fk_ep";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = EP_FK_FIELD)
    private EntityProperty entityProperty;

    public EntityProperty getEntityProperty() {
        return entityProperty;
    }

    public void setEntityProperty(EntityProperty entityProperty) {
        this.entityProperty = entityProperty;
    }

    @DatabaseField(id = true, dataType = DataType.STRING, generatedId = false)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("Name")
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String name;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("MimeType")
    @XmlElement(name = "MimeType", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String mimeType;

    @DatabaseField(dataType = DataType.INTEGER, canBeNull = true)
    @JsonProperty("Size")
    @XmlElement(name = "Size", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int size;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("LocalLocation")
    @XmlElement(name = "LocalLocation", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String localLocation;

    @DatabaseField(dataType = DataType.LONG_OBJ, canBeNull = true)
    @JsonProperty("CheckSum")
    @XmlElement(name = "CheckSum", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, type = Long.class, nillable = true)
    protected Long checkSum;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("TrackKey")
    @XmlElement(name = "TrackKey", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String trackKey;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the mimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the value of the mimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Gets the value of the size property.
     * 
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     */
    public void setSize(int value) {
        this.size = value;
    }

    /**
     * Gets the value of the localLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalLocation() {
        return localLocation;
    }

    /**
     * Sets the value of the localLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalLocation(String value) {
        this.localLocation = value;
    }

    /**
     * Gets the value of the checkSum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCheckSum() {
        return checkSum;
    }

    /**
     * Sets the value of the checkSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCheckSum(Long value) {
        this.checkSum = value;
    }

    /**
     * Gets the value of the trackKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackKey() {
        return trackKey;
    }

    /**
     * Sets the value of the trackKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackKey(String value) {
        this.trackKey = value;
    }

}
