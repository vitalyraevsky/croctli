package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ConditionOperator;
import ru.baccasoft.croctli.android.gen.core.ConditionOperatorSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class ConditionOperatorAPI implements IRestApiCall<ConditionOperatorSet, Void, Void, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = ConditionOperatorAPI.class.getSimpleName();

    /**
     * Прим.: особый вызов, плюет на входные параметры
     * @param dateFrom игнорит
     * @param dateTo игнорит
     * @param max игнорит
     */
    @Override
    public ConditionOperatorSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String api = "/api/conditionoperator/list";
        final boolean needAuth = false;

        final ConditionOperatorSet conditionOperatorSet = RestClient.fillJsonClass(ConditionOperatorSet.class, api, needAuth);
        final String serverDate = conditionOperatorSet.getServerTimeString();
        for (ConditionOperator c : conditionOperatorSet.getArrayOf().getItem()) {
            c.setServerTime(serverDate);
        }
        return conditionOperatorSet;
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
