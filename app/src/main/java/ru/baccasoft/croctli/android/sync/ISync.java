package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.rest.RestApiDate;

public interface ISync {

    final int SYNC_MAX_COUNT = 1000;

    /**
     * Синхронизация при первом запуске (обычно только получение данных)
     * @param dateFrom
     * @param dateTo
     */
    void atFirst(RestApiDate dateFrom, RestApiDate dateTo);

    /**
     * Синхронизация по запросу (получение данных, а для некоторых сущностей отправка удаленных и проч.)
     * @param dateFrom
     * @param dateTo
     */
    void byRequest(RestApiDate dateFrom, RestApiDate dateTo);

//    void getList();
}
