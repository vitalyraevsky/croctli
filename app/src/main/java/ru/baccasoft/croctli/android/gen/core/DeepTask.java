
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeepTask complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeepTask">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Shallow" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}Task"/>
 *         &lt;element name="Tree" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfDeepEntityProperty"/>
 *         &lt;element name="DelegateToUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeepTask", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "shallow",
    "tree",
    "delegateToUserId"
})
public class DeepTask {

    @XmlElement(name = "Shallow", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected Task shallow;
    @XmlElement(name = "Tree", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfDeepEntityProperty tree;
    @XmlElement(name = "DelegateToUserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String delegateToUserId;

    /**
     * Gets the value of the shallow property.
     * 
     * @return
     *     possible object is
     *     {@link Task }
     *     
     */
    public Task getShallow() {
        return shallow;
    }

    /**
     * Sets the value of the shallow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Task }
     *     
     */
    public void setShallow(Task value) {
        this.shallow = value;
    }

    /**
     * Gets the value of the tree property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDeepEntityProperty }
     *     
     */
    public ArrayOfDeepEntityProperty getTree() {
        return tree;
    }

    /**
     * Sets the value of the tree property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDeepEntityProperty }
     *     
     */
    public void setTree(ArrayOfDeepEntityProperty value) {
        this.tree = value;
    }

    /**
     * Gets the value of the delegateToUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegateToUserId() {
        return delegateToUserId;
    }

    /**
     * Sets the value of the delegateToUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegateToUserId(String value) {
        this.delegateToUserId = value;
    }

}
