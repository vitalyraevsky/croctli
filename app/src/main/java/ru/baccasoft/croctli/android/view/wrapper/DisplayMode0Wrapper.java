package ru.baccasoft.croctli.android.view.wrapper;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.CrocUtils;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 *
 * Создает вьюшку для EntityProperty,
 * у которого в связанном Classifier указан displayMode = 0
 *
 * Обычный справочник.
 * Свободный справочник.
 * Справочник со свободной датой.
 */
//TODO:
    //незавершен

public class DisplayMode0Wrapper implements IWrapper {
    @SuppressWarnings("unused")
    private static final String TAG = DisplayMode0Wrapper.class.getSimpleName();

    private Context mContext;
    private boolean readOnly;

    public DisplayMode0Wrapper(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        // выбираем тип справочника
        if(p.isIsClassifierFree()) {
            // справочник со свободным вводом
            Log.d(TAG, "classifier free");
            return makeClassifierFree(p);
        } else if (p.getClassifierDisplayField() != null) {
            // обычный справочник
            Log.d(TAG, "classifier simple");
            return makeClassifierSimple(p);
        } else {
            throw new IllegalStateException("cant recognize type of classifier view to make");
        }
    }

    private void validate(EntityProperty p) {
        //TODO:
    }

    /**
     * Создает вьюшку для справочника со свободным вводом
     */
    private View makeClassifierFree(EntityProperty p) {
        String value = CrocUtils.getValueFromClassifier(p);//Sorokin тут заменить

        if(this.readOnly) {
            return ViewUtils.makeTextScalarWithTitle(mContext, p.getDisplayGroup(), value, this.readOnly);
        } else {
            List<ClassifierItem> classifierItems = TableUtils.getClassifierItemsByClassifierId(p.getClassifierTypeId());
            // вида [код:значение]
            Map<String, String> items = new HashMap<String, String>();
            Log.d(TAG, "classifierFree items:");
            for(ClassifierItem ci : classifierItems) {
                Log.d(TAG, "item:\'[" + ci.getCode() + "],["+ci.getDisplayValue() + "]");
                items.put(ci.getCode(), ci.getDisplayValue());
            }

            LinearLayout view = ViewUtils.makemakeClassifierFreeWithTitle(mContext, p.getDisplayGroup(), items,p.getId(), value, p );
            return view;
        }
    }

    /**
     * Создает вьюшку для простого справочника
     *
     * @param p
     * @return
     */
    private View makeClassifierSimple(EntityProperty p) {
        TreeMap<String, Map<String, String>> viewItems
                = ViewUtils.getValuesForClassifierSimple(p);

        // TODO: проверять флаг readOnly

        String selectedItem = viewItems.firstEntry().getKey();
        Map<String, String> itemsList = viewItems.firstEntry().getValue();

        return ViewUtils.makeClassifierSimpleWithTitle(mContext, selectedItem, itemsList, p);
    }
}
