package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.text.InputType;
import android.widget.EditText;
import ru.baccasoft.croctli.android.R;

/**
 * Created by developer on 06.11.14.
 */
public class FloatScalar extends EditText {

    public FloatScalar(Context context, String text, boolean isReadonly) {
        super(context);
        setMaxLines(1);
        setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        setTextSize(context.getResources().getDimension(R.dimen.px_16));

        setText(text);
        setEnabled(!isReadonly);
    }
}
