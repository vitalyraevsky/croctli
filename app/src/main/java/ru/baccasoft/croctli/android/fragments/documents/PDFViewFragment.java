package ru.baccasoft.croctli.android.fragments.documents;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.artifex.mupdfdemo.AsyncTask;
import com.artifex.mupdfdemo.FilePicker;
import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.MuPDFPageAdapter;
import com.artifex.mupdfdemo.MuPDFReaderView;
import com.artifex.mupdfdemo.OutlineActivityData;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import ru.baccasoft.croctli.android.R;

public class PDFViewFragment extends AttachmentViewFragment implements FilePicker.FilePickerSupport {

    private static final String TAG = PDFViewFragment.class.getSimpleName();
    private ViewGroup docContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pdf_view_layout,null);
        docContent = (ViewGroup) view.findViewById(R.id.content);

        loadDocument(getAttachmentPath());

        return view;
    }

    protected void loadDocument(String path) {
        new MuCoreLoader().execute(  path );
    }

    @Override
    public void performPickFor(FilePicker picker) {
        Log.d(TAG,"performPickFor");
    }

    private MuPDFCore openBuffer(Context ctx,byte buffer[], String magic) {

        Log.d(TAG, "Trying to open byte buffer");

        try
        {
            MuPDFCore core = new MuPDFCore( ctx, buffer, magic );
            // New file: drop the old outline data
            OutlineActivityData.set(null);
            return core;
        }
        catch (Exception e)
        {
            Log.d(TAG, "Exception " + e);
            return null;
        }

    }

    private void initPDFView(final Context cxt, ViewGroup layout, final MuPDFCore core) {

        if (core == null)
            return;

        MuPDFReaderView docView = new MuPDFReaderView(cxt) {
            @Override
            protected void onMoveToChild(int i) {
                if (core == null)
                    return;
                super.onMoveToChild(i);
            }

            @Override
            protected void onTapMainDocArea() {

            }

            @Override
            protected void onDocMotion() {

            }

        };

        docView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        layout.addView(docView);
        docView.setAdapter(new MuPDFPageAdapter(cxt, this, core));

    }

    public class MuCoreLoader extends AsyncTask<String, Void , MuPDFCore> {

        @Override
        protected MuPDFCore doInBackground(String... params) {

            if(params == null)
                return null;

            String path = params[0];
            Log.d(TAG, "try open pdf " + path);

            MuPDFCore core = null;

            byte buffer[] = null;
            try {

                InputStream is = new BufferedInputStream( new FileInputStream(new File(path)));
                int len = is.available();
                buffer = new byte[len];
                is.read(buffer, 0, len);
                is.close();

                core = openBuffer( getActivity(),buffer,"application/pdf");

                Log.d(TAG, "pdfCore opened : " + core.countPages() + " buffer size : " + buffer.length);

            } catch (OutOfMemoryError e) {
                Log.e(TAG,"Out of memory during buffer reading");
            } catch (IOException e) {
                Log.e(TAG, "not able to read file");
            }

            return core;
        }

        @Override
        protected void onPostExecute(MuPDFCore core) {
            initPDFView( getActivity(), docContent, core );
        }
    }

}
