package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 *
 * @param <G> возвращаемые объекты методов GET (получение данных с сервера)
 * @param <SM_O> тип объектов, принимаемых для выполнения запроса на изменения. всегда(?) - это ArrayOf[тип_данных]
 * @param <SD_O> тип объектов, принимаемых для выполнения запроса на удаление. всегда(?) - это ArrayOfString (список идентификаторов)
 * @param <SM_A> тип объектов, возвращаемых после запроса на отправку измененных данных. можно ожидать, что обычно(?) - это ArrayOf[Something]
 */
public interface IRestApiCall<G, SM_O, SD_O, SM_A> {

    G modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max);

    SM_A modifyPost(RestApiDate dateLastSync, SM_O objects);

    StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max);

    void deletePost(RestApiDate dateLastSync, SD_O objects);

}
