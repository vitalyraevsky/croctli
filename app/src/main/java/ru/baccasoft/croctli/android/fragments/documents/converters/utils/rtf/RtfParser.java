package ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class RtfParser extends Parser {
    public static final String[] tokenNames = new String[] {
            "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ANSI", "ANSICPG", "AUTHOR", "B",
            "BLUE", "BULLET", "CELL", "CF", "CLOSEBRACE", "COLORTBL", "CONTROL", "CREATIM",
            "DEFF", "DEFLANG", "DEFLANGFE", "DEFTAB", "DY", "EMDASH", "ENDASH", "F",
            "FALT", "FBIDI", "FCHARSET", "FDECOR", "FI", "FMODERN", "FNAME", "FNIL",
            "FONTTBL", "FPRQ", "FROMAN", "FS", "FSCRIPT", "FSWISS", "FTECH", "GENERATOR",
            "GREEN", "HEADER", "HEX", "HEXCHAR", "HR", "I", "INFO", "INTBL", "LANG",
            "LDBLQUOTE", "LI", "LINE", "MAC", "MIN", "MO", "NBSP", "NEWLINE", "NUMBER",
            "OPENBRACE", "OPERATOR", "OTHER", "PAR", "PARD", "PLAIN", "PNSTART", "PRINTIM",
            "QC", "QJ", "RDBLQUOTE", "RED", "REVTIM", "ROW", "RQUOTE", "RTF", "SEC",
            "SLASH", "STAR", "STYLESHEET", "TAB", "TEXT", "TITLE", "TREE", "UC", "WS",
            "YR", "'{'", "'}'"
    };
    public static final int EOF=-1;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int ANSI=4;
    public static final int ANSICPG=5;
    public static final int AUTHOR=6;
    public static final int B=7;
    public static final int BLUE=8;
    public static final int BULLET=9;
    public static final int CELL=10;
    public static final int CF=11;
    public static final int CLOSEBRACE=12;
    public static final int COLORTBL=13;
    public static final int CONTROL=14;
    public static final int CREATIM=15;
    public static final int DEFF=16;
    public static final int DEFLANG=17;
    public static final int DEFLANGFE=18;
    public static final int DEFTAB=19;
    public static final int DY=20;
    public static final int EMDASH=21;
    public static final int ENDASH=22;
    public static final int F=23;
    public static final int FALT=24;
    public static final int FBIDI=25;
    public static final int FCHARSET=26;
    public static final int FDECOR=27;
    public static final int FI=28;
    public static final int FMODERN=29;
    public static final int FNAME=30;
    public static final int FNIL=31;
    public static final int FONTTBL=32;
    public static final int FPRQ=33;
    public static final int FROMAN=34;
    public static final int FS=35;
    public static final int FSCRIPT=36;
    public static final int FSWISS=37;
    public static final int FTECH=38;
    public static final int GENERATOR=39;
    public static final int GREEN=40;
    public static final int HEADER=41;
    public static final int HEX=42;
    public static final int HEXCHAR=43;
    public static final int HR=44;
    public static final int I=45;
    public static final int INFO=46;
    public static final int INTBL=47;
    public static final int LANG=48;
    public static final int LDBLQUOTE=49;
    public static final int LI=50;
    public static final int LINE=51;
    public static final int MAC=52;
    public static final int MIN=53;
    public static final int MO=54;
    public static final int NBSP=55;
    public static final int NEWLINE=56;
    public static final int NUMBER=57;
    public static final int OPENBRACE=58;
    public static final int OPERATOR=59;
    public static final int OTHER=60;
    public static final int PAR=61;
    public static final int PARD=62;
    public static final int PLAIN=63;
    public static final int PNSTART=64;
    public static final int PRINTIM=65;
    public static final int QC=66;
    public static final int QJ=67;
    public static final int RDBLQUOTE=68;
    public static final int RED=69;
    public static final int REVTIM=70;
    public static final int ROW=71;
    public static final int RQUOTE=72;
    public static final int RTF=73;
    public static final int SEC=74;
    public static final int SLASH=75;
    public static final int STAR=76;
    public static final int STYLESHEET=77;
    public static final int TAB=78;
    public static final int TEXT=79;
    public static final int TITLE=80;
    public static final int TREE=81;
    public static final int UC=82;
    public static final int WS=83;
    public static final int YR=84;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public RtfParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public RtfParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }
    @Override public String[] getTokenNames() { return RtfParser.tokenNames; }
    @Override public String getGrammarFileName() { return "/Users/green/dev/Rtf.g"; }


    public static class rtf_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "rtf"
    // /Users/green/dev/Rtf.g:15:1: rtf : '{' ! RTF ^ NUMBER ( entity )* '}' !;
    public final RtfParser.rtf_return rtf() throws RecognitionException {
        RtfParser.rtf_return retval = new RtfParser.rtf_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal1=null;
        Token RTF2=null;
        Token NUMBER3=null;
        Token char_literal5=null;
        ParserRuleReturnScope entity4 =null;

        Object char_literal1_tree=null;
        Object RTF2_tree=null;
        Object NUMBER3_tree=null;
        Object char_literal5_tree=null;

        try {
            // /Users/green/dev/Rtf.g:15:4: ( '{' ! RTF ^ NUMBER ( entity )* '}' !)
            // /Users/green/dev/Rtf.g:15:6: '{' ! RTF ^ NUMBER ( entity )* '}' !
            {
                root_0 = (Object)adaptor.nil();


                char_literal1=(Token)match(input,85,FOLLOW_85_in_rtf38);
                RTF2=(Token)match(input,RTF,FOLLOW_RTF_in_rtf41);
                RTF2_tree = (Object)adaptor.create(RTF2);
                root_0 = (Object)adaptor.becomeRoot(RTF2_tree, root_0);

                NUMBER3=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_rtf44);
                NUMBER3_tree = (Object)adaptor.create(NUMBER3);
                adaptor.addChild(root_0, NUMBER3_tree);

                assert((NUMBER3!=null?NUMBER3.getText():null).equals("1"));
                // /Users/green/dev/Rtf.g:15:61: ( entity )*
                loop1:
                while (true) {
                    int alt1=2;
                    int LA1_0 = input.LA(1);
                    if ( ((LA1_0 >= ANSI && LA1_0 <= ANSICPG)||LA1_0==B||(LA1_0 >= BULLET && LA1_0 <= CLOSEBRACE)||LA1_0==CONTROL||(LA1_0 >= DEFF && LA1_0 <= DEFTAB)||(LA1_0 >= EMDASH && LA1_0 <= FBIDI)||(LA1_0 >= FDECOR && LA1_0 <= FNIL)||(LA1_0 >= FROMAN && LA1_0 <= GENERATOR)||LA1_0==HEXCHAR||LA1_0==I||(LA1_0 >= INTBL && LA1_0 <= MAC)||LA1_0==NBSP||LA1_0==OPENBRACE||(LA1_0 >= PAR && LA1_0 <= PNSTART)||(LA1_0 >= QC && LA1_0 <= RDBLQUOTE)||(LA1_0 >= ROW && LA1_0 <= RQUOTE)||LA1_0==SLASH||(LA1_0 >= TAB && LA1_0 <= TEXT)||LA1_0==UC||LA1_0==85) ) {
                        alt1=1;
                    }

                    switch (alt1) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:15:61: entity
                        {
                            pushFollow(FOLLOW_entity_in_rtf48);
                            entity4=entity();
                            state._fsp--;

                            adaptor.addChild(root_0, entity4.getTree());

                        }
                        break;

                        default :
                            break loop1;
                    }
                }

                char_literal5=(Token)match(input,86,FOLLOW_86_in_rtf51);
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rtf"


    public static class entity_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "entity"
    // /Users/green/dev/Rtf.g:17:1: entity : ( '{' ( entity )* '}' -> ^( TREE ( entity )* ) | '{' ! compound ^ '}' !| unknown | text | word );
    public final RtfParser.entity_return entity() throws RecognitionException {
        RtfParser.entity_return retval = new RtfParser.entity_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal6=null;
        Token char_literal8=null;
        Token char_literal9=null;
        Token char_literal11=null;
        ParserRuleReturnScope entity7 =null;
        ParserRuleReturnScope compound10 =null;
        ParserRuleReturnScope unknown12 =null;
        ParserRuleReturnScope text13 =null;
        ParserRuleReturnScope word14 =null;

        Object char_literal6_tree=null;
        Object char_literal8_tree=null;
        Object char_literal9_tree=null;
        Object char_literal11_tree=null;
        RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
        RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");
        RewriteRuleSubtreeStream stream_entity=new RewriteRuleSubtreeStream(adaptor,"rule entity");

        try {
            // /Users/green/dev/Rtf.g:17:7: ( '{' ( entity )* '}' -> ^( TREE ( entity )* ) | '{' ! compound ^ '}' !| unknown | text | word )
            int alt3=5;
            switch ( input.LA(1) ) {
                case 85:
                {
                    switch ( input.LA(2) ) {
                        case STAR:
                        {
                            alt3=3;
                        }
                        break;
                        case ANSI:
                        case ANSICPG:
                        case B:
                        case BULLET:
                        case CELL:
                        case CF:
                        case CLOSEBRACE:
                        case CONTROL:
                        case DEFF:
                        case DEFLANG:
                        case DEFLANGFE:
                        case DEFTAB:
                        case EMDASH:
                        case ENDASH:
                        case F:
                        case FALT:
                        case FBIDI:
                        case FDECOR:
                        case FI:
                        case FMODERN:
                        case FNAME:
                        case FNIL:
                        case FROMAN:
                        case FS:
                        case FSCRIPT:
                        case FSWISS:
                        case FTECH:
                        case GENERATOR:
                        case HEXCHAR:
                        case I:
                        case INTBL:
                        case LANG:
                        case LDBLQUOTE:
                        case LI:
                        case LINE:
                        case MAC:
                        case NBSP:
                        case OPENBRACE:
                        case PAR:
                        case PARD:
                        case PLAIN:
                        case PNSTART:
                        case QC:
                        case QJ:
                        case RDBLQUOTE:
                        case ROW:
                        case RQUOTE:
                        case SLASH:
                        case TAB:
                        case TEXT:
                        case UC:
                        case 85:
                        case 86:
                        {
                            alt3=1;
                        }
                        break;
                        case AUTHOR:
                        case COLORTBL:
                        case CREATIM:
                        case FONTTBL:
                        case HEADER:
                        case INFO:
                        case OPERATOR:
                        case PRINTIM:
                        case REVTIM:
                        case STYLESHEET:
                        case TITLE:
                        {
                            alt3=2;
                        }
                        break;
                        default:
                            int nvaeMark = input.mark();
                            try {
                                input.consume();
                                NoViableAltException nvae =
                                        new NoViableAltException("", 3, 1, input);
                                throw nvae;
                            } finally {
                                input.rewind(nvaeMark);
                            }
                    }
                }
                break;
                case CONTROL:
                {
                    alt3=3;
                }
                break;
                case BULLET:
                case CLOSEBRACE:
                case EMDASH:
                case ENDASH:
                case HEXCHAR:
                case NBSP:
                case OPENBRACE:
                case SLASH:
                case TEXT:
                {
                    alt3=4;
                }
                break;
                case ANSI:
                case ANSICPG:
                case B:
                case CELL:
                case CF:
                case DEFF:
                case DEFLANG:
                case DEFLANGFE:
                case DEFTAB:
                case F:
                case FALT:
                case FBIDI:
                case FDECOR:
                case FI:
                case FMODERN:
                case FNAME:
                case FNIL:
                case FROMAN:
                case FS:
                case FSCRIPT:
                case FSWISS:
                case FTECH:
                case GENERATOR:
                case I:
                case INTBL:
                case LANG:
                case LDBLQUOTE:
                case LI:
                case LINE:
                case MAC:
                case PAR:
                case PARD:
                case PLAIN:
                case PNSTART:
                case QC:
                case QJ:
                case RDBLQUOTE:
                case ROW:
                case RQUOTE:
                case TAB:
                case UC:
                {
                    alt3=5;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 3, 0, input);
                    throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // /Users/green/dev/Rtf.g:18:2: '{' ( entity )* '}'
                {
                    char_literal6=(Token)match(input,85,FOLLOW_85_in_entity62);
                    stream_85.add(char_literal6);

                    // /Users/green/dev/Rtf.g:18:6: ( entity )*
                    loop2:
                    while (true) {
                        int alt2=2;
                        int LA2_0 = input.LA(1);
                        if ( ((LA2_0 >= ANSI && LA2_0 <= ANSICPG)||LA2_0==B||(LA2_0 >= BULLET && LA2_0 <= CLOSEBRACE)||LA2_0==CONTROL||(LA2_0 >= DEFF && LA2_0 <= DEFTAB)||(LA2_0 >= EMDASH && LA2_0 <= FBIDI)||(LA2_0 >= FDECOR && LA2_0 <= FNIL)||(LA2_0 >= FROMAN && LA2_0 <= GENERATOR)||LA2_0==HEXCHAR||LA2_0==I||(LA2_0 >= INTBL && LA2_0 <= MAC)||LA2_0==NBSP||LA2_0==OPENBRACE||(LA2_0 >= PAR && LA2_0 <= PNSTART)||(LA2_0 >= QC && LA2_0 <= RDBLQUOTE)||(LA2_0 >= ROW && LA2_0 <= RQUOTE)||LA2_0==SLASH||(LA2_0 >= TAB && LA2_0 <= TEXT)||LA2_0==UC||LA2_0==85) ) {
                            alt2=1;
                        }

                        switch (alt2) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:18:6: entity
                            {
                                pushFollow(FOLLOW_entity_in_entity64);
                                entity7=entity();
                                state._fsp--;

                                stream_entity.add(entity7.getTree());
                            }
                            break;

                            default :
                                break loop2;
                        }
                    }

                    char_literal8=(Token)match(input,86,FOLLOW_86_in_entity67);
                    stream_86.add(char_literal8);

                    // AST REWRITE
                    // elements: entity
                    // token labels:
                    // rule labels: retval
                    // token list labels:
                    // rule list labels:
                    // wildcard labels:
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

                    root_0 = (Object)adaptor.nil();
                    // 18:18: -> ^( TREE ( entity )* )
                    {
                        // /Users/green/dev/Rtf.g:18:21: ^( TREE ( entity )* )
                        {
                            Object root_1 = (Object)adaptor.nil();
                            root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TREE, "TREE"), root_1);
                            // /Users/green/dev/Rtf.g:18:28: ( entity )*
                            while ( stream_entity.hasNext() ) {
                                adaptor.addChild(root_1, stream_entity.nextTree());
                            }
                            stream_entity.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                }
                break;
                case 2 :
                    // /Users/green/dev/Rtf.g:19:2: '{' ! compound ^ '}' !
                {
                    root_0 = (Object)adaptor.nil();


                    char_literal9=(Token)match(input,85,FOLLOW_85_in_entity81);
                    pushFollow(FOLLOW_compound_in_entity84);
                    compound10=compound();
                    state._fsp--;

                    root_0 = (Object)adaptor.becomeRoot(compound10.getTree(), root_0);
                    char_literal11=(Token)match(input,86,FOLLOW_86_in_entity87);
                }
                break;
                case 3 :
                    // /Users/green/dev/Rtf.g:20:2: unknown
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_unknown_in_entity93);
                    unknown12=unknown();
                    state._fsp--;

                    adaptor.addChild(root_0, unknown12.getTree());

                }
                break;
                case 4 :
                    // /Users/green/dev/Rtf.g:21:2: text
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_text_in_entity98);
                    text13=text();
                    state._fsp--;

                    adaptor.addChild(root_0, text13.getTree());

                }
                break;
                case 5 :
                    // /Users/green/dev/Rtf.g:22:2: word
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_word_in_entity103);
                    word14=word();
                    state._fsp--;

                    adaptor.addChild(root_0, word14.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "entity"


    public static class text_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "text"
    // /Users/green/dev/Rtf.g:24:1: text : ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE )+ ;
    public final RtfParser.text_return text() throws RecognitionException {
        RtfParser.text_return retval = new RtfParser.text_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set15=null;

        Object set15_tree=null;

        try {
            // /Users/green/dev/Rtf.g:24:5: ( ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE )+ )
            // /Users/green/dev/Rtf.g:24:7: ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE )+
            {
                root_0 = (Object)adaptor.nil();


                // /Users/green/dev/Rtf.g:24:7: ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE )+
                int cnt4=0;
                loop4:
                while (true) {
                    int alt4=2;
                    int LA4_0 = input.LA(1);
                    if ( (LA4_0==BULLET||LA4_0==CLOSEBRACE||(LA4_0 >= EMDASH && LA4_0 <= ENDASH)||LA4_0==HEXCHAR||LA4_0==NBSP||LA4_0==OPENBRACE||LA4_0==SLASH||LA4_0==TEXT) ) {
                        alt4=1;
                    }

                    switch (alt4) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:
                        {
                            set15=input.LT(1);
                            if ( input.LA(1)==BULLET||input.LA(1)==CLOSEBRACE||(input.LA(1) >= EMDASH && input.LA(1) <= ENDASH)||input.LA(1)==HEXCHAR||input.LA(1)==NBSP||input.LA(1)==OPENBRACE||input.LA(1)==SLASH||input.LA(1)==TEXT ) {
                                input.consume();
                                adaptor.addChild(root_0, (Object)adaptor.create(set15));
                                state.errorRecovery=false;
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                throw mse;
                            }
                        }
                        break;

                        default :
                            if ( cnt4 >= 1 ) break loop4;
                            EarlyExitException eee = new EarlyExitException(4, input);
                            throw eee;
                    }
                    cnt4++;
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "text"


    public static class word_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "word"
    // /Users/green/dev/Rtf.g:26:1: word : ( ( TAB | LDBLQUOTE | RDBLQUOTE | MAC | ANSI | LI | LINE | ANSICPG | B | DEFF | DEFLANG | DEFLANGFE | DEFTAB | F | FALT | FNAME | FS | GENERATOR | I | LANG | PAR | PARD | PLAIN | CELL | ROW | INTBL | PNSTART | RQUOTE | QC | QJ | CF | FI | UC ) ( NUMBER )? | fontfamily );
    public final RtfParser.word_return word() throws RecognitionException {
        RtfParser.word_return retval = new RtfParser.word_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set16=null;
        Token NUMBER17=null;
        ParserRuleReturnScope fontfamily18 =null;

        Object set16_tree=null;
        Object NUMBER17_tree=null;

        try {
            // /Users/green/dev/Rtf.g:26:5: ( ( TAB | LDBLQUOTE | RDBLQUOTE | MAC | ANSI | LI | LINE | ANSICPG | B | DEFF | DEFLANG | DEFLANGFE | DEFTAB | F | FALT | FNAME | FS | GENERATOR | I | LANG | PAR | PARD | PLAIN | CELL | ROW | INTBL | PNSTART | RQUOTE | QC | QJ | CF | FI | UC ) ( NUMBER )? | fontfamily )
            int alt6=2;
            int LA6_0 = input.LA(1);
            if ( ((LA6_0 >= ANSI && LA6_0 <= ANSICPG)||LA6_0==B||(LA6_0 >= CELL && LA6_0 <= CF)||(LA6_0 >= DEFF && LA6_0 <= DEFTAB)||(LA6_0 >= F && LA6_0 <= FALT)||LA6_0==FI||LA6_0==FNAME||LA6_0==FS||LA6_0==GENERATOR||LA6_0==I||(LA6_0 >= INTBL && LA6_0 <= MAC)||(LA6_0 >= PAR && LA6_0 <= PNSTART)||(LA6_0 >= QC && LA6_0 <= RDBLQUOTE)||(LA6_0 >= ROW && LA6_0 <= RQUOTE)||LA6_0==TAB||LA6_0==UC) ) {
                alt6=1;
            }
            else if ( (LA6_0==FBIDI||LA6_0==FDECOR||LA6_0==FMODERN||LA6_0==FNIL||LA6_0==FROMAN||(LA6_0 >= FSCRIPT && LA6_0 <= FTECH)) ) {
                alt6=2;
            }

            else {
                NoViableAltException nvae =
                        new NoViableAltException("", 6, 0, input);
                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // /Users/green/dev/Rtf.g:26:7: ( TAB | LDBLQUOTE | RDBLQUOTE | MAC | ANSI | LI | LINE | ANSICPG | B | DEFF | DEFLANG | DEFLANGFE | DEFTAB | F | FALT | FNAME | FS | GENERATOR | I | LANG | PAR | PARD | PLAIN | CELL | ROW | INTBL | PNSTART | RQUOTE | QC | QJ | CF | FI | UC ) ( NUMBER )?
                {
                    root_0 = (Object)adaptor.nil();


                    set16=input.LT(1);
                    if ( (input.LA(1) >= ANSI && input.LA(1) <= ANSICPG)||input.LA(1)==B||(input.LA(1) >= CELL && input.LA(1) <= CF)||(input.LA(1) >= DEFF && input.LA(1) <= DEFTAB)||(input.LA(1) >= F && input.LA(1) <= FALT)||input.LA(1)==FI||input.LA(1)==FNAME||input.LA(1)==FS||input.LA(1)==GENERATOR||input.LA(1)==I||(input.LA(1) >= INTBL && input.LA(1) <= MAC)||(input.LA(1) >= PAR && input.LA(1) <= PNSTART)||(input.LA(1) >= QC && input.LA(1) <= RDBLQUOTE)||(input.LA(1) >= ROW && input.LA(1) <= RQUOTE)||input.LA(1)==TAB||input.LA(1)==UC ) {
                        input.consume();
                        adaptor.addChild(root_0, (Object)adaptor.create(set16));
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }
                    // /Users/green/dev/Rtf.g:61:4: ( NUMBER )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);
                    if ( (LA5_0==NUMBER) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:61:4: NUMBER
                        {
                            NUMBER17=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_word325);
                            NUMBER17_tree = (Object)adaptor.create(NUMBER17);
                            adaptor.addChild(root_0, NUMBER17_tree);

                        }
                        break;

                    }

                }
                break;
                case 2 :
                    // /Users/green/dev/Rtf.g:61:14: fontfamily
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_fontfamily_in_word330);
                    fontfamily18=fontfamily();
                    state._fsp--;

                    adaptor.addChild(root_0, fontfamily18.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "word"


    public static class fontfamily_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "fontfamily"
    // /Users/green/dev/Rtf.g:63:1: fontfamily : ( FNIL | FROMAN | FSWISS | FMODERN | FSCRIPT | FDECOR | FTECH | FBIDI );
    public final RtfParser.fontfamily_return fontfamily() throws RecognitionException {
        RtfParser.fontfamily_return retval = new RtfParser.fontfamily_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set19=null;

        Object set19_tree=null;

        try {
            // /Users/green/dev/Rtf.g:63:11: ( FNIL | FROMAN | FSWISS | FMODERN | FSCRIPT | FDECOR | FTECH | FBIDI )
            // /Users/green/dev/Rtf.g:
            {
                root_0 = (Object)adaptor.nil();


                set19=input.LT(1);
                if ( input.LA(1)==FBIDI||input.LA(1)==FDECOR||input.LA(1)==FMODERN||input.LA(1)==FNIL||input.LA(1)==FROMAN||(input.LA(1) >= FSCRIPT && input.LA(1) <= FTECH) ) {
                    input.consume();
                    adaptor.addChild(root_0, (Object)adaptor.create(set19));
                    state.errorRecovery=false;
                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null,input);
                    throw mse;
                }
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "fontfamily"


    public static class compound_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "compound"
    // /Users/green/dev/Rtf.g:64:1: compound : ( fonttbl | colortbl | stylesheet | info | AUTHOR ^ TEXT | TITLE ^ TEXT | OPERATOR ^ TEXT | CREATIM ^ time | REVTIM ^ time | PRINTIM ^ time | HEADER ^ ( entity )* );
    public final RtfParser.compound_return compound() throws RecognitionException {
        RtfParser.compound_return retval = new RtfParser.compound_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token AUTHOR24=null;
        Token TEXT25=null;
        Token TITLE26=null;
        Token TEXT27=null;
        Token OPERATOR28=null;
        Token TEXT29=null;
        Token CREATIM30=null;
        Token REVTIM32=null;
        Token PRINTIM34=null;
        Token HEADER36=null;
        ParserRuleReturnScope fonttbl20 =null;
        ParserRuleReturnScope colortbl21 =null;
        ParserRuleReturnScope stylesheet22 =null;
        ParserRuleReturnScope info23 =null;
        ParserRuleReturnScope time31 =null;
        ParserRuleReturnScope time33 =null;
        ParserRuleReturnScope time35 =null;
        ParserRuleReturnScope entity37 =null;

        Object AUTHOR24_tree=null;
        Object TEXT25_tree=null;
        Object TITLE26_tree=null;
        Object TEXT27_tree=null;
        Object OPERATOR28_tree=null;
        Object TEXT29_tree=null;
        Object CREATIM30_tree=null;
        Object REVTIM32_tree=null;
        Object PRINTIM34_tree=null;
        Object HEADER36_tree=null;

        try {
            // /Users/green/dev/Rtf.g:64:9: ( fonttbl | colortbl | stylesheet | info | AUTHOR ^ TEXT | TITLE ^ TEXT | OPERATOR ^ TEXT | CREATIM ^ time | REVTIM ^ time | PRINTIM ^ time | HEADER ^ ( entity )* )
            int alt8=11;
            switch ( input.LA(1) ) {
                case FONTTBL:
                {
                    alt8=1;
                }
                break;
                case COLORTBL:
                {
                    alt8=2;
                }
                break;
                case STYLESHEET:
                {
                    alt8=3;
                }
                break;
                case INFO:
                {
                    alt8=4;
                }
                break;
                case AUTHOR:
                {
                    alt8=5;
                }
                break;
                case TITLE:
                {
                    alt8=6;
                }
                break;
                case OPERATOR:
                {
                    alt8=7;
                }
                break;
                case CREATIM:
                {
                    alt8=8;
                }
                break;
                case REVTIM:
                {
                    alt8=9;
                }
                break;
                case PRINTIM:
                {
                    alt8=10;
                }
                break;
                case HEADER:
                {
                    alt8=11;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 8, 0, input);
                    throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // /Users/green/dev/Rtf.g:66:2: fonttbl
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_fonttbl_in_compound378);
                    fonttbl20=fonttbl();
                    state._fsp--;

                    adaptor.addChild(root_0, fonttbl20.getTree());

                }
                break;
                case 2 :
                    // /Users/green/dev/Rtf.g:66:12: colortbl
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_colortbl_in_compound382);
                    colortbl21=colortbl();
                    state._fsp--;

                    adaptor.addChild(root_0, colortbl21.getTree());

                }
                break;
                case 3 :
                    // /Users/green/dev/Rtf.g:66:23: stylesheet
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_stylesheet_in_compound386);
                    stylesheet22=stylesheet();
                    state._fsp--;

                    adaptor.addChild(root_0, stylesheet22.getTree());

                }
                break;
                case 4 :
                    // /Users/green/dev/Rtf.g:66:36: info
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_info_in_compound390);
                    info23=info();
                    state._fsp--;

                    adaptor.addChild(root_0, info23.getTree());

                }
                break;
                case 5 :
                    // /Users/green/dev/Rtf.g:67:2: AUTHOR ^ TEXT
                {
                    root_0 = (Object)adaptor.nil();


                    AUTHOR24=(Token)match(input,AUTHOR,FOLLOW_AUTHOR_in_compound395);
                    AUTHOR24_tree = (Object)adaptor.create(AUTHOR24);
                    root_0 = (Object)adaptor.becomeRoot(AUTHOR24_tree, root_0);

                    TEXT25=(Token)match(input,TEXT,FOLLOW_TEXT_in_compound398);
                    TEXT25_tree = (Object)adaptor.create(TEXT25);
                    adaptor.addChild(root_0, TEXT25_tree);

                }
                break;
                case 6 :
                    // /Users/green/dev/Rtf.g:68:2: TITLE ^ TEXT
                {
                    root_0 = (Object)adaptor.nil();


                    TITLE26=(Token)match(input,TITLE,FOLLOW_TITLE_in_compound403);
                    TITLE26_tree = (Object)adaptor.create(TITLE26);
                    root_0 = (Object)adaptor.becomeRoot(TITLE26_tree, root_0);

                    TEXT27=(Token)match(input,TEXT,FOLLOW_TEXT_in_compound406);
                    TEXT27_tree = (Object)adaptor.create(TEXT27);
                    adaptor.addChild(root_0, TEXT27_tree);

                }
                break;
                case 7 :
                    // /Users/green/dev/Rtf.g:69:2: OPERATOR ^ TEXT
                {
                    root_0 = (Object)adaptor.nil();


                    OPERATOR28=(Token)match(input,OPERATOR,FOLLOW_OPERATOR_in_compound411);
                    OPERATOR28_tree = (Object)adaptor.create(OPERATOR28);
                    root_0 = (Object)adaptor.becomeRoot(OPERATOR28_tree, root_0);

                    TEXT29=(Token)match(input,TEXT,FOLLOW_TEXT_in_compound414);
                    TEXT29_tree = (Object)adaptor.create(TEXT29);
                    adaptor.addChild(root_0, TEXT29_tree);

                }
                break;
                case 8 :
                    // /Users/green/dev/Rtf.g:70:2: CREATIM ^ time
                {
                    root_0 = (Object)adaptor.nil();


                    CREATIM30=(Token)match(input,CREATIM,FOLLOW_CREATIM_in_compound419);
                    CREATIM30_tree = (Object)adaptor.create(CREATIM30);
                    root_0 = (Object)adaptor.becomeRoot(CREATIM30_tree, root_0);

                    pushFollow(FOLLOW_time_in_compound422);
                    time31=time();
                    state._fsp--;

                    adaptor.addChild(root_0, time31.getTree());

                }
                break;
                case 9 :
                    // /Users/green/dev/Rtf.g:71:2: REVTIM ^ time
                {
                    root_0 = (Object)adaptor.nil();


                    REVTIM32=(Token)match(input,REVTIM,FOLLOW_REVTIM_in_compound427);
                    REVTIM32_tree = (Object)adaptor.create(REVTIM32);
                    root_0 = (Object)adaptor.becomeRoot(REVTIM32_tree, root_0);

                    pushFollow(FOLLOW_time_in_compound430);
                    time33=time();
                    state._fsp--;

                    adaptor.addChild(root_0, time33.getTree());

                }
                break;
                case 10 :
                    // /Users/green/dev/Rtf.g:72:2: PRINTIM ^ time
                {
                    root_0 = (Object)adaptor.nil();


                    PRINTIM34=(Token)match(input,PRINTIM,FOLLOW_PRINTIM_in_compound435);
                    PRINTIM34_tree = (Object)adaptor.create(PRINTIM34);
                    root_0 = (Object)adaptor.becomeRoot(PRINTIM34_tree, root_0);

                    pushFollow(FOLLOW_time_in_compound438);
                    time35=time();
                    state._fsp--;

                    adaptor.addChild(root_0, time35.getTree());

                }
                break;
                case 11 :
                    // /Users/green/dev/Rtf.g:73:2: HEADER ^ ( entity )*
                {
                    root_0 = (Object)adaptor.nil();


                    HEADER36=(Token)match(input,HEADER,FOLLOW_HEADER_in_compound443);
                    HEADER36_tree = (Object)adaptor.create(HEADER36);
                    root_0 = (Object)adaptor.becomeRoot(HEADER36_tree, root_0);

                    // /Users/green/dev/Rtf.g:73:10: ( entity )*
                    loop7:
                    while (true) {
                        int alt7=2;
                        int LA7_0 = input.LA(1);
                        if ( ((LA7_0 >= ANSI && LA7_0 <= ANSICPG)||LA7_0==B||(LA7_0 >= BULLET && LA7_0 <= CLOSEBRACE)||LA7_0==CONTROL||(LA7_0 >= DEFF && LA7_0 <= DEFTAB)||(LA7_0 >= EMDASH && LA7_0 <= FBIDI)||(LA7_0 >= FDECOR && LA7_0 <= FNIL)||(LA7_0 >= FROMAN && LA7_0 <= GENERATOR)||LA7_0==HEXCHAR||LA7_0==I||(LA7_0 >= INTBL && LA7_0 <= MAC)||LA7_0==NBSP||LA7_0==OPENBRACE||(LA7_0 >= PAR && LA7_0 <= PNSTART)||(LA7_0 >= QC && LA7_0 <= RDBLQUOTE)||(LA7_0 >= ROW && LA7_0 <= RQUOTE)||LA7_0==SLASH||(LA7_0 >= TAB && LA7_0 <= TEXT)||LA7_0==UC||LA7_0==85) ) {
                            alt7=1;
                        }

                        switch (alt7) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:73:10: entity
                            {
                                pushFollow(FOLLOW_entity_in_compound446);
                                entity37=entity();
                                state._fsp--;

                                adaptor.addChild(root_0, entity37.getTree());

                            }
                            break;

                            default :
                                break loop7;
                        }
                    }

                }
                break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "compound"


    public static class time_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "time"
    // /Users/green/dev/Rtf.g:74:1: time : YR NUMBER MO NUMBER DY NUMBER HR NUMBER MIN NUMBER ( SEC NUMBER )? ;
    public final RtfParser.time_return time() throws RecognitionException {
        RtfParser.time_return retval = new RtfParser.time_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token YR38=null;
        Token NUMBER39=null;
        Token MO40=null;
        Token NUMBER41=null;
        Token DY42=null;
        Token NUMBER43=null;
        Token HR44=null;
        Token NUMBER45=null;
        Token MIN46=null;
        Token NUMBER47=null;
        Token SEC48=null;
        Token NUMBER49=null;

        Object YR38_tree=null;
        Object NUMBER39_tree=null;
        Object MO40_tree=null;
        Object NUMBER41_tree=null;
        Object DY42_tree=null;
        Object NUMBER43_tree=null;
        Object HR44_tree=null;
        Object NUMBER45_tree=null;
        Object MIN46_tree=null;
        Object NUMBER47_tree=null;
        Object SEC48_tree=null;
        Object NUMBER49_tree=null;

        try {
            // /Users/green/dev/Rtf.g:74:5: ( YR NUMBER MO NUMBER DY NUMBER HR NUMBER MIN NUMBER ( SEC NUMBER )? )
            // /Users/green/dev/Rtf.g:74:7: YR NUMBER MO NUMBER DY NUMBER HR NUMBER MIN NUMBER ( SEC NUMBER )?
            {
                root_0 = (Object)adaptor.nil();


                YR38=(Token)match(input,YR,FOLLOW_YR_in_time454);
                YR38_tree = (Object)adaptor.create(YR38);
                adaptor.addChild(root_0, YR38_tree);

                NUMBER39=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time456);
                NUMBER39_tree = (Object)adaptor.create(NUMBER39);
                adaptor.addChild(root_0, NUMBER39_tree);

                MO40=(Token)match(input,MO,FOLLOW_MO_in_time458);
                MO40_tree = (Object)adaptor.create(MO40);
                adaptor.addChild(root_0, MO40_tree);

                NUMBER41=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time460);
                NUMBER41_tree = (Object)adaptor.create(NUMBER41);
                adaptor.addChild(root_0, NUMBER41_tree);

                DY42=(Token)match(input,DY,FOLLOW_DY_in_time462);
                DY42_tree = (Object)adaptor.create(DY42);
                adaptor.addChild(root_0, DY42_tree);

                NUMBER43=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time464);
                NUMBER43_tree = (Object)adaptor.create(NUMBER43);
                adaptor.addChild(root_0, NUMBER43_tree);

                HR44=(Token)match(input,HR,FOLLOW_HR_in_time466);
                HR44_tree = (Object)adaptor.create(HR44);
                adaptor.addChild(root_0, HR44_tree);

                NUMBER45=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time468);
                NUMBER45_tree = (Object)adaptor.create(NUMBER45);
                adaptor.addChild(root_0, NUMBER45_tree);

                MIN46=(Token)match(input,MIN,FOLLOW_MIN_in_time470);
                MIN46_tree = (Object)adaptor.create(MIN46);
                adaptor.addChild(root_0, MIN46_tree);

                NUMBER47=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time472);
                NUMBER47_tree = (Object)adaptor.create(NUMBER47);
                adaptor.addChild(root_0, NUMBER47_tree);

                // /Users/green/dev/Rtf.g:74:58: ( SEC NUMBER )?
                int alt9=2;
                int LA9_0 = input.LA(1);
                if ( (LA9_0==SEC) ) {
                    alt9=1;
                }
                switch (alt9) {
                    case 1 :
                        // /Users/green/dev/Rtf.g:74:59: SEC NUMBER
                    {
                        SEC48=(Token)match(input,SEC,FOLLOW_SEC_in_time475);
                        SEC48_tree = (Object)adaptor.create(SEC48);
                        adaptor.addChild(root_0, SEC48_tree);

                        NUMBER49=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_time477);
                        NUMBER49_tree = (Object)adaptor.create(NUMBER49);
                        adaptor.addChild(root_0, NUMBER49_tree);

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "time"


    public static class fonttbl_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "fonttbl"
    // /Users/green/dev/Rtf.g:76:1: fonttbl : FONTTBL (fi+= fontinfo | '{' fi+= fontinfo '}' )+ -> ^( FONTTBL $fi) ;
    public final RtfParser.fonttbl_return fonttbl() throws RecognitionException {
        RtfParser.fonttbl_return retval = new RtfParser.fonttbl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token FONTTBL50=null;
        Token char_literal51=null;
        Token char_literal52=null;
        List<Object> list_fi=null;
        RuleReturnScope fi = null;
        Object FONTTBL50_tree=null;
        Object char_literal51_tree=null;
        Object char_literal52_tree=null;
        RewriteRuleTokenStream stream_FONTTBL=new RewriteRuleTokenStream(adaptor,"token FONTTBL");
        RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
        RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");
        RewriteRuleSubtreeStream stream_fontinfo=new RewriteRuleSubtreeStream(adaptor,"rule fontinfo");

        try {
            // /Users/green/dev/Rtf.g:76:8: ( FONTTBL (fi+= fontinfo | '{' fi+= fontinfo '}' )+ -> ^( FONTTBL $fi) )
            // /Users/green/dev/Rtf.g:76:10: FONTTBL (fi+= fontinfo | '{' fi+= fontinfo '}' )+
            {
                FONTTBL50=(Token)match(input,FONTTBL,FOLLOW_FONTTBL_in_fonttbl488);
                stream_FONTTBL.add(FONTTBL50);

                // /Users/green/dev/Rtf.g:76:18: (fi+= fontinfo | '{' fi+= fontinfo '}' )+
                int cnt10=0;
                loop10:
                while (true) {
                    int alt10=3;
                    int LA10_0 = input.LA(1);
                    if ( (LA10_0==F) ) {
                        alt10=1;
                    }
                    else if ( (LA10_0==85) ) {
                        alt10=2;
                    }

                    switch (alt10) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:76:19: fi+= fontinfo
                        {
                            pushFollow(FOLLOW_fontinfo_in_fonttbl493);
                            fi=fontinfo();
                            state._fsp--;

                            stream_fontinfo.add(fi.getTree());
                            if (list_fi==null) list_fi=new ArrayList<Object>();
                            list_fi.add(fi.getTree());
                        }
                        break;
                        case 2 :
                            // /Users/green/dev/Rtf.g:76:34: '{' fi+= fontinfo '}'
                        {
                            char_literal51=(Token)match(input,85,FOLLOW_85_in_fonttbl497);
                            stream_85.add(char_literal51);

                            pushFollow(FOLLOW_fontinfo_in_fonttbl501);
                            fi=fontinfo();
                            state._fsp--;

                            stream_fontinfo.add(fi.getTree());
                            if (list_fi==null) list_fi=new ArrayList<Object>();
                            list_fi.add(fi.getTree());
                            char_literal52=(Token)match(input,86,FOLLOW_86_in_fonttbl503);
                            stream_86.add(char_literal52);

                        }
                        break;

                        default :
                            if ( cnt10 >= 1 ) break loop10;
                            EarlyExitException eee = new EarlyExitException(10, input);
                            throw eee;
                    }
                    cnt10++;
                }

                // AST REWRITE
                // elements: fi, FONTTBL
                // token labels:
                // rule labels: retval
                // token list labels:
                // rule list labels: fi
                // wildcard labels:
                retval.tree = root_0;
                RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
                RewriteRuleSubtreeStream stream_fi=new RewriteRuleSubtreeStream(adaptor,"token fi",list_fi);
                root_0 = (Object)adaptor.nil();
                // 76:57: -> ^( FONTTBL $fi)
                {
                    // /Users/green/dev/Rtf.g:76:60: ^( FONTTBL $fi)
                    {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_FONTTBL.nextNode(), root_1);
                        adaptor.addChild(root_1, stream_fi.nextTree());
                        adaptor.addChild(root_0, root_1);
                    }

                }


                retval.tree = root_0;

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "fonttbl"


    public static class fontinfo_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "fontinfo"
    // /Users/green/dev/Rtf.g:77:1: fontinfo : F fn= NUMBER ( fontfamily | FCHARSET NUMBER | FPRQ NUMBER | unknown | text )* -> ^( F $fn text ) ;
    public final RtfParser.fontinfo_return fontinfo() throws RecognitionException {
        RtfParser.fontinfo_return retval = new RtfParser.fontinfo_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token fn=null;
        Token F53=null;
        Token FCHARSET55=null;
        Token NUMBER56=null;
        Token FPRQ57=null;
        Token NUMBER58=null;
        ParserRuleReturnScope fontfamily54 =null;
        ParserRuleReturnScope unknown59 =null;
        ParserRuleReturnScope text60 =null;

        Object fn_tree=null;
        Object F53_tree=null;
        Object FCHARSET55_tree=null;
        Object NUMBER56_tree=null;
        Object FPRQ57_tree=null;
        Object NUMBER58_tree=null;
        RewriteRuleTokenStream stream_F=new RewriteRuleTokenStream(adaptor,"token F");
        RewriteRuleTokenStream stream_FPRQ=new RewriteRuleTokenStream(adaptor,"token FPRQ");
        RewriteRuleTokenStream stream_FCHARSET=new RewriteRuleTokenStream(adaptor,"token FCHARSET");
        RewriteRuleTokenStream stream_NUMBER=new RewriteRuleTokenStream(adaptor,"token NUMBER");
        RewriteRuleSubtreeStream stream_text=new RewriteRuleSubtreeStream(adaptor,"rule text");
        RewriteRuleSubtreeStream stream_unknown=new RewriteRuleSubtreeStream(adaptor,"rule unknown");
        RewriteRuleSubtreeStream stream_fontfamily=new RewriteRuleSubtreeStream(adaptor,"rule fontfamily");

        try {
            // /Users/green/dev/Rtf.g:77:9: ( F fn= NUMBER ( fontfamily | FCHARSET NUMBER | FPRQ NUMBER | unknown | text )* -> ^( F $fn text ) )
            // /Users/green/dev/Rtf.g:77:11: F fn= NUMBER ( fontfamily | FCHARSET NUMBER | FPRQ NUMBER | unknown | text )*
            {
                F53=(Token)match(input,F,FOLLOW_F_in_fontinfo521);
                stream_F.add(F53);

                fn=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_fontinfo525);
                stream_NUMBER.add(fn);

                // /Users/green/dev/Rtf.g:77:23: ( fontfamily | FCHARSET NUMBER | FPRQ NUMBER | unknown | text )*
                loop11:
                while (true) {
                    int alt11=6;
                    switch ( input.LA(1) ) {
                        case 85:
                        {
                            int LA11_2 = input.LA(2);
                            if ( (LA11_2==STAR) ) {
                                alt11=4;
                            }

                        }
                        break;
                        case FBIDI:
                        case FDECOR:
                        case FMODERN:
                        case FNIL:
                        case FROMAN:
                        case FSCRIPT:
                        case FSWISS:
                        case FTECH:
                        {
                            alt11=1;
                        }
                        break;
                        case FCHARSET:
                        {
                            alt11=2;
                        }
                        break;
                        case FPRQ:
                        {
                            alt11=3;
                        }
                        break;
                        case CONTROL:
                        {
                            alt11=4;
                        }
                        break;
                        case BULLET:
                        case CLOSEBRACE:
                        case EMDASH:
                        case ENDASH:
                        case HEXCHAR:
                        case NBSP:
                        case OPENBRACE:
                        case SLASH:
                        case TEXT:
                        {
                            alt11=5;
                        }
                        break;
                    }
                    switch (alt11) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:77:25: fontfamily
                        {
                            pushFollow(FOLLOW_fontfamily_in_fontinfo529);
                            fontfamily54=fontfamily();
                            state._fsp--;

                            stream_fontfamily.add(fontfamily54.getTree());
                        }
                        break;
                        case 2 :
                            // /Users/green/dev/Rtf.g:77:38: FCHARSET NUMBER
                        {
                            FCHARSET55=(Token)match(input,FCHARSET,FOLLOW_FCHARSET_in_fontinfo533);
                            stream_FCHARSET.add(FCHARSET55);

                            NUMBER56=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_fontinfo535);
                            stream_NUMBER.add(NUMBER56);

                        }
                        break;
                        case 3 :
                            // /Users/green/dev/Rtf.g:77:56: FPRQ NUMBER
                        {
                            FPRQ57=(Token)match(input,FPRQ,FOLLOW_FPRQ_in_fontinfo539);
                            stream_FPRQ.add(FPRQ57);

                            NUMBER58=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_fontinfo541);
                            stream_NUMBER.add(NUMBER58);

                        }
                        break;
                        case 4 :
                            // /Users/green/dev/Rtf.g:77:70: unknown
                        {
                            pushFollow(FOLLOW_unknown_in_fontinfo545);
                            unknown59=unknown();
                            state._fsp--;

                            stream_unknown.add(unknown59.getTree());
                        }
                        break;
                        case 5 :
                            // /Users/green/dev/Rtf.g:77:80: text
                        {
                            pushFollow(FOLLOW_text_in_fontinfo549);
                            text60=text();
                            state._fsp--;

                            stream_text.add(text60.getTree());
                        }
                        break;

                        default :
                            break loop11;
                    }
                }

                // AST REWRITE
                // elements: fn, text, F
                // token labels: fn
                // rule labels: retval
                // token list labels:
                // rule list labels:
                // wildcard labels:
                retval.tree = root_0;
                RewriteRuleTokenStream stream_fn=new RewriteRuleTokenStream(adaptor,"token fn",fn);
                RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

                root_0 = (Object)adaptor.nil();
                // 77:87: -> ^( F $fn text )
                {
                    // /Users/green/dev/Rtf.g:77:90: ^( F $fn text )
                    {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_F.nextNode(), root_1);
                        adaptor.addChild(root_1, stream_fn.nextNode());
                        adaptor.addChild(root_1, stream_text.nextTree());
                        adaptor.addChild(root_0, root_1);
                    }

                }


                retval.tree = root_0;

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "fontinfo"


    public static class colortbl_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "colortbl"
    // /Users/green/dev/Rtf.g:79:1: colortbl : COLORTBL ^ ( ( RED NUMBER )? ( GREEN NUMBER )? ( BLUE NUMBER )? TEXT )* ;
    public final RtfParser.colortbl_return colortbl() throws RecognitionException {
        RtfParser.colortbl_return retval = new RtfParser.colortbl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token COLORTBL61=null;
        Token RED62=null;
        Token NUMBER63=null;
        Token GREEN64=null;
        Token NUMBER65=null;
        Token BLUE66=null;
        Token NUMBER67=null;
        Token TEXT68=null;

        Object COLORTBL61_tree=null;
        Object RED62_tree=null;
        Object NUMBER63_tree=null;
        Object GREEN64_tree=null;
        Object NUMBER65_tree=null;
        Object BLUE66_tree=null;
        Object NUMBER67_tree=null;
        Object TEXT68_tree=null;

        try {
            // /Users/green/dev/Rtf.g:79:9: ( COLORTBL ^ ( ( RED NUMBER )? ( GREEN NUMBER )? ( BLUE NUMBER )? TEXT )* )
            // /Users/green/dev/Rtf.g:79:11: COLORTBL ^ ( ( RED NUMBER )? ( GREEN NUMBER )? ( BLUE NUMBER )? TEXT )*
            {
                root_0 = (Object)adaptor.nil();


                COLORTBL61=(Token)match(input,COLORTBL,FOLLOW_COLORTBL_in_colortbl569);
                COLORTBL61_tree = (Object)adaptor.create(COLORTBL61);
                root_0 = (Object)adaptor.becomeRoot(COLORTBL61_tree, root_0);

                // /Users/green/dev/Rtf.g:79:21: ( ( RED NUMBER )? ( GREEN NUMBER )? ( BLUE NUMBER )? TEXT )*
                loop15:
                while (true) {
                    int alt15=2;
                    int LA15_0 = input.LA(1);
                    if ( (LA15_0==BLUE||LA15_0==GREEN||LA15_0==RED||LA15_0==TEXT) ) {
                        alt15=1;
                    }

                    switch (alt15) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:79:22: ( RED NUMBER )? ( GREEN NUMBER )? ( BLUE NUMBER )? TEXT
                        {
                            // /Users/green/dev/Rtf.g:79:22: ( RED NUMBER )?
                            int alt12=2;
                            int LA12_0 = input.LA(1);
                            if ( (LA12_0==RED) ) {
                                alt12=1;
                            }
                            switch (alt12) {
                                case 1 :
                                    // /Users/green/dev/Rtf.g:79:23: RED NUMBER
                                {
                                    RED62=(Token)match(input,RED,FOLLOW_RED_in_colortbl574);
                                    RED62_tree = (Object)adaptor.create(RED62);
                                    adaptor.addChild(root_0, RED62_tree);

                                    NUMBER63=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl576);
                                    NUMBER63_tree = (Object)adaptor.create(NUMBER63);
                                    adaptor.addChild(root_0, NUMBER63_tree);

                                }
                                break;

                            }

                            // /Users/green/dev/Rtf.g:79:36: ( GREEN NUMBER )?
                            int alt13=2;
                            int LA13_0 = input.LA(1);
                            if ( (LA13_0==GREEN) ) {
                                alt13=1;
                            }
                            switch (alt13) {
                                case 1 :
                                    // /Users/green/dev/Rtf.g:79:37: GREEN NUMBER
                                {
                                    GREEN64=(Token)match(input,GREEN,FOLLOW_GREEN_in_colortbl581);
                                    GREEN64_tree = (Object)adaptor.create(GREEN64);
                                    adaptor.addChild(root_0, GREEN64_tree);

                                    NUMBER65=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl583);
                                    NUMBER65_tree = (Object)adaptor.create(NUMBER65);
                                    adaptor.addChild(root_0, NUMBER65_tree);

                                }
                                break;

                            }

                            // /Users/green/dev/Rtf.g:79:52: ( BLUE NUMBER )?
                            int alt14=2;
                            int LA14_0 = input.LA(1);
                            if ( (LA14_0==BLUE) ) {
                                alt14=1;
                            }
                            switch (alt14) {
                                case 1 :
                                    // /Users/green/dev/Rtf.g:79:53: BLUE NUMBER
                                {
                                    BLUE66=(Token)match(input,BLUE,FOLLOW_BLUE_in_colortbl588);
                                    BLUE66_tree = (Object)adaptor.create(BLUE66);
                                    adaptor.addChild(root_0, BLUE66_tree);

                                    NUMBER67=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl590);
                                    NUMBER67_tree = (Object)adaptor.create(NUMBER67);
                                    adaptor.addChild(root_0, NUMBER67_tree);

                                }
                                break;

                            }

                            TEXT68=(Token)match(input,TEXT,FOLLOW_TEXT_in_colortbl594);
                            TEXT68_tree = (Object)adaptor.create(TEXT68);
                            adaptor.addChild(root_0, TEXT68_tree);

                        }
                        break;

                        default :
                            break loop15;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "colortbl"


    public static class stylesheet_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stylesheet"
    // /Users/green/dev/Rtf.g:81:1: stylesheet : STYLESHEET ^ ( entity )* ;
    public final RtfParser.stylesheet_return stylesheet() throws RecognitionException {
        RtfParser.stylesheet_return retval = new RtfParser.stylesheet_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token STYLESHEET69=null;
        ParserRuleReturnScope entity70 =null;

        Object STYLESHEET69_tree=null;

        try {
            // /Users/green/dev/Rtf.g:81:11: ( STYLESHEET ^ ( entity )* )
            // /Users/green/dev/Rtf.g:81:13: STYLESHEET ^ ( entity )*
            {
                root_0 = (Object)adaptor.nil();


                STYLESHEET69=(Token)match(input,STYLESHEET,FOLLOW_STYLESHEET_in_stylesheet604);
                STYLESHEET69_tree = (Object)adaptor.create(STYLESHEET69);
                root_0 = (Object)adaptor.becomeRoot(STYLESHEET69_tree, root_0);

                // /Users/green/dev/Rtf.g:81:25: ( entity )*
                loop16:
                while (true) {
                    int alt16=2;
                    int LA16_0 = input.LA(1);
                    if ( ((LA16_0 >= ANSI && LA16_0 <= ANSICPG)||LA16_0==B||(LA16_0 >= BULLET && LA16_0 <= CLOSEBRACE)||LA16_0==CONTROL||(LA16_0 >= DEFF && LA16_0 <= DEFTAB)||(LA16_0 >= EMDASH && LA16_0 <= FBIDI)||(LA16_0 >= FDECOR && LA16_0 <= FNIL)||(LA16_0 >= FROMAN && LA16_0 <= GENERATOR)||LA16_0==HEXCHAR||LA16_0==I||(LA16_0 >= INTBL && LA16_0 <= MAC)||LA16_0==NBSP||LA16_0==OPENBRACE||(LA16_0 >= PAR && LA16_0 <= PNSTART)||(LA16_0 >= QC && LA16_0 <= RDBLQUOTE)||(LA16_0 >= ROW && LA16_0 <= RQUOTE)||LA16_0==SLASH||(LA16_0 >= TAB && LA16_0 <= TEXT)||LA16_0==UC||LA16_0==85) ) {
                        alt16=1;
                    }

                    switch (alt16) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:81:25: entity
                        {
                            pushFollow(FOLLOW_entity_in_stylesheet607);
                            entity70=entity();
                            state._fsp--;

                            adaptor.addChild(root_0, entity70.getTree());

                        }
                        break;

                        default :
                            break loop16;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stylesheet"


    public static class info_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "info"
    // /Users/green/dev/Rtf.g:83:1: info : INFO ^ ( entity )* ;
    public final RtfParser.info_return info() throws RecognitionException {
        RtfParser.info_return retval = new RtfParser.info_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token INFO71=null;
        ParserRuleReturnScope entity72 =null;

        Object INFO71_tree=null;

        try {
            // /Users/green/dev/Rtf.g:83:5: ( INFO ^ ( entity )* )
            // /Users/green/dev/Rtf.g:83:7: INFO ^ ( entity )*
            {
                root_0 = (Object)adaptor.nil();


                INFO71=(Token)match(input,INFO,FOLLOW_INFO_in_info617);
                INFO71_tree = (Object)adaptor.create(INFO71);
                root_0 = (Object)adaptor.becomeRoot(INFO71_tree, root_0);

                // /Users/green/dev/Rtf.g:83:13: ( entity )*
                loop17:
                while (true) {
                    int alt17=2;
                    int LA17_0 = input.LA(1);
                    if ( ((LA17_0 >= ANSI && LA17_0 <= ANSICPG)||LA17_0==B||(LA17_0 >= BULLET && LA17_0 <= CLOSEBRACE)||LA17_0==CONTROL||(LA17_0 >= DEFF && LA17_0 <= DEFTAB)||(LA17_0 >= EMDASH && LA17_0 <= FBIDI)||(LA17_0 >= FDECOR && LA17_0 <= FNIL)||(LA17_0 >= FROMAN && LA17_0 <= GENERATOR)||LA17_0==HEXCHAR||LA17_0==I||(LA17_0 >= INTBL && LA17_0 <= MAC)||LA17_0==NBSP||LA17_0==OPENBRACE||(LA17_0 >= PAR && LA17_0 <= PNSTART)||(LA17_0 >= QC && LA17_0 <= RDBLQUOTE)||(LA17_0 >= ROW && LA17_0 <= RQUOTE)||LA17_0==SLASH||(LA17_0 >= TAB && LA17_0 <= TEXT)||LA17_0==UC||LA17_0==85) ) {
                        alt17=1;
                    }

                    switch (alt17) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:83:13: entity
                        {
                            pushFollow(FOLLOW_entity_in_info620);
                            entity72=entity();
                            state._fsp--;

                            adaptor.addChild(root_0, entity72.getTree());

                        }
                        break;

                        default :
                            break loop17;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "info"


    public static class unknown_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unknown"
    // /Users/green/dev/Rtf.g:85:1: unknown : ( control !| '{' ! STAR ! word ^ ( entity )* '}' !| '{' ! STAR ! CONTROL ! ( NUMBER !)? ( entity !)* '}' !);
    public final RtfParser.unknown_return unknown() throws RecognitionException {
        RtfParser.unknown_return retval = new RtfParser.unknown_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal74=null;
        Token STAR75=null;
        Token char_literal78=null;
        Token char_literal79=null;
        Token STAR80=null;
        Token CONTROL81=null;
        Token NUMBER82=null;
        Token char_literal84=null;
        ParserRuleReturnScope control73 =null;
        ParserRuleReturnScope word76 =null;
        ParserRuleReturnScope entity77 =null;
        ParserRuleReturnScope entity83 =null;

        Object char_literal74_tree=null;
        Object STAR75_tree=null;
        Object char_literal78_tree=null;
        Object char_literal79_tree=null;
        Object STAR80_tree=null;
        Object CONTROL81_tree=null;
        Object NUMBER82_tree=null;
        Object char_literal84_tree=null;

        try {
            // /Users/green/dev/Rtf.g:85:8: ( control !| '{' ! STAR ! word ^ ( entity )* '}' !| '{' ! STAR ! CONTROL ! ( NUMBER !)? ( entity !)* '}' !)
            int alt21=3;
            int LA21_0 = input.LA(1);
            if ( (LA21_0==CONTROL) ) {
                alt21=1;
            }
            else if ( (LA21_0==85) ) {
                int LA21_2 = input.LA(2);
                if ( (LA21_2==STAR) ) {
                    int LA21_3 = input.LA(3);
                    if ( (LA21_3==CONTROL) ) {
                        alt21=3;
                    }
                    else if ( ((LA21_3 >= ANSI && LA21_3 <= ANSICPG)||LA21_3==B||(LA21_3 >= CELL && LA21_3 <= CF)||(LA21_3 >= DEFF && LA21_3 <= DEFTAB)||(LA21_3 >= F && LA21_3 <= FBIDI)||(LA21_3 >= FDECOR && LA21_3 <= FNIL)||(LA21_3 >= FROMAN && LA21_3 <= GENERATOR)||LA21_3==I||(LA21_3 >= INTBL && LA21_3 <= MAC)||(LA21_3 >= PAR && LA21_3 <= PNSTART)||(LA21_3 >= QC && LA21_3 <= RDBLQUOTE)||(LA21_3 >= ROW && LA21_3 <= RQUOTE)||LA21_3==TAB||LA21_3==UC) ) {
                        alt21=2;
                    }

                    else {
                        int nvaeMark = input.mark();
                        try {
                            for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
                                input.consume();
                            }
                            NoViableAltException nvae =
                                    new NoViableAltException("", 21, 3, input);
                            throw nvae;
                        } finally {
                            input.rewind(nvaeMark);
                        }
                    }

                }

                else {
                    int nvaeMark = input.mark();
                    try {
                        input.consume();
                        NoViableAltException nvae =
                                new NoViableAltException("", 21, 2, input);
                        throw nvae;
                    } finally {
                        input.rewind(nvaeMark);
                    }
                }

            }

            else {
                NoViableAltException nvae =
                        new NoViableAltException("", 21, 0, input);
                throw nvae;
            }

            switch (alt21) {
                case 1 :
                    // /Users/green/dev/Rtf.g:86:2: control !
                {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_control_in_unknown631);
                    control73=control();
                    state._fsp--;

                }
                break;
                case 2 :
                    // /Users/green/dev/Rtf.g:87:2: '{' ! STAR ! word ^ ( entity )* '}' !
                {
                    root_0 = (Object)adaptor.nil();


                    char_literal74=(Token)match(input,85,FOLLOW_85_in_unknown637);
                    STAR75=(Token)match(input,STAR,FOLLOW_STAR_in_unknown640);
                    pushFollow(FOLLOW_word_in_unknown643);
                    word76=word();
                    state._fsp--;

                    root_0 = (Object)adaptor.becomeRoot(word76.getTree(), root_0);
                    // /Users/green/dev/Rtf.g:87:19: ( entity )*
                    loop18:
                    while (true) {
                        int alt18=2;
                        int LA18_0 = input.LA(1);
                        if ( ((LA18_0 >= ANSI && LA18_0 <= ANSICPG)||LA18_0==B||(LA18_0 >= BULLET && LA18_0 <= CLOSEBRACE)||LA18_0==CONTROL||(LA18_0 >= DEFF && LA18_0 <= DEFTAB)||(LA18_0 >= EMDASH && LA18_0 <= FBIDI)||(LA18_0 >= FDECOR && LA18_0 <= FNIL)||(LA18_0 >= FROMAN && LA18_0 <= GENERATOR)||LA18_0==HEXCHAR||LA18_0==I||(LA18_0 >= INTBL && LA18_0 <= MAC)||LA18_0==NBSP||LA18_0==OPENBRACE||(LA18_0 >= PAR && LA18_0 <= PNSTART)||(LA18_0 >= QC && LA18_0 <= RDBLQUOTE)||(LA18_0 >= ROW && LA18_0 <= RQUOTE)||LA18_0==SLASH||(LA18_0 >= TAB && LA18_0 <= TEXT)||LA18_0==UC||LA18_0==85) ) {
                            alt18=1;
                        }

                        switch (alt18) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:87:19: entity
                            {
                                pushFollow(FOLLOW_entity_in_unknown646);
                                entity77=entity();
                                state._fsp--;

                                adaptor.addChild(root_0, entity77.getTree());

                            }
                            break;

                            default :
                                break loop18;
                        }
                    }

                    char_literal78=(Token)match(input,86,FOLLOW_86_in_unknown649);
                }
                break;
                case 3 :
                    // /Users/green/dev/Rtf.g:88:2: '{' ! STAR ! CONTROL ! ( NUMBER !)? ( entity !)* '}' !
                {
                    root_0 = (Object)adaptor.nil();


                    char_literal79=(Token)match(input,85,FOLLOW_85_in_unknown655);
                    STAR80=(Token)match(input,STAR,FOLLOW_STAR_in_unknown658);
                    CONTROL81=(Token)match(input,CONTROL,FOLLOW_CONTROL_in_unknown661);
                    // /Users/green/dev/Rtf.g:88:22: ( NUMBER !)?
                    int alt19=2;
                    int LA19_0 = input.LA(1);
                    if ( (LA19_0==NUMBER) ) {
                        alt19=1;
                    }
                    switch (alt19) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:88:23: NUMBER !
                        {
                            NUMBER82=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_unknown665);
                        }
                        break;

                    }

                    // /Users/green/dev/Rtf.g:88:33: ( entity !)*
                    loop20:
                    while (true) {
                        int alt20=2;
                        int LA20_0 = input.LA(1);
                        if ( ((LA20_0 >= ANSI && LA20_0 <= ANSICPG)||LA20_0==B||(LA20_0 >= BULLET && LA20_0 <= CLOSEBRACE)||LA20_0==CONTROL||(LA20_0 >= DEFF && LA20_0 <= DEFTAB)||(LA20_0 >= EMDASH && LA20_0 <= FBIDI)||(LA20_0 >= FDECOR && LA20_0 <= FNIL)||(LA20_0 >= FROMAN && LA20_0 <= GENERATOR)||LA20_0==HEXCHAR||LA20_0==I||(LA20_0 >= INTBL && LA20_0 <= MAC)||LA20_0==NBSP||LA20_0==OPENBRACE||(LA20_0 >= PAR && LA20_0 <= PNSTART)||(LA20_0 >= QC && LA20_0 <= RDBLQUOTE)||(LA20_0 >= ROW && LA20_0 <= RQUOTE)||LA20_0==SLASH||(LA20_0 >= TAB && LA20_0 <= TEXT)||LA20_0==UC||LA20_0==85) ) {
                            alt20=1;
                        }

                        switch (alt20) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:88:34: entity !
                            {
                                pushFollow(FOLLOW_entity_in_unknown671);
                                entity83=entity();
                                state._fsp--;

                            }
                            break;

                            default :
                                break loop20;
                        }
                    }

                    char_literal84=(Token)match(input,86,FOLLOW_86_in_unknown676);
                }
                break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unknown"


    public static class control_return extends ParserRuleReturnScope {
        Object tree;
        @Override
        public Object getTree() { return tree; }
    };


    // $ANTLR start "control"
    // /Users/green/dev/Rtf.g:90:1: control : CONTROL ( NUMBER )? ;
    public final RtfParser.control_return control() throws RecognitionException {
        RtfParser.control_return retval = new RtfParser.control_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token CONTROL85=null;
        Token NUMBER86=null;

        Object CONTROL85_tree=null;
        Object NUMBER86_tree=null;

        try {
            // /Users/green/dev/Rtf.g:90:8: ( CONTROL ( NUMBER )? )
            // /Users/green/dev/Rtf.g:90:10: CONTROL ( NUMBER )?
            {
                root_0 = (Object)adaptor.nil();


                CONTROL85=(Token)match(input,CONTROL,FOLLOW_CONTROL_in_control686);
                CONTROL85_tree = (Object)adaptor.create(CONTROL85);
                adaptor.addChild(root_0, CONTROL85_tree);

                // /Users/green/dev/Rtf.g:90:18: ( NUMBER )?
                int alt22=2;
                int LA22_0 = input.LA(1);
                if ( (LA22_0==NUMBER) ) {
                    alt22=1;
                }
                switch (alt22) {
                    case 1 :
                        // /Users/green/dev/Rtf.g:90:18: NUMBER
                    {
                        NUMBER86=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_control688);
                        NUMBER86_tree = (Object)adaptor.create(NUMBER86);
                        adaptor.addChild(root_0, NUMBER86_tree);

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
            retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
        }
        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "control"

    // Delegated rules



    public static final BitSet FOLLOW_85_in_rtf38 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_RTF_in_rtf41 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_rtf44 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_entity_in_rtf48 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_86_in_rtf51 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_entity62 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_entity_in_entity64 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_86_in_entity67 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_entity81 = new BitSet(new long[]{0x080042010000A040L,0x0000000000012042L});
    public static final BitSet FOLLOW_compound_in_entity84 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_entity87 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unknown_in_entity93 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_text_in_entity98 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_word_in_entity103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_word154 = new BitSet(new long[]{0x0200000000000002L});
    public static final BitSet FOLLOW_NUMBER_in_word325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_fontfamily_in_word330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_fonttbl_in_compound378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colortbl_in_compound382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stylesheet_in_compound386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_info_in_compound390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AUTHOR_in_compound395 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_TEXT_in_compound398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TITLE_in_compound403 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_TEXT_in_compound406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPERATOR_in_compound411 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_TEXT_in_compound414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CREATIM_in_compound419 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_time_in_compound422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REVTIM_in_compound427 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_time_in_compound430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRINTIM_in_compound435 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_time_in_compound438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HEADER_in_compound443 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_entity_in_compound446 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_YR_in_time454 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time456 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_MO_in_time458 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time460 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_DY_in_time462 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time464 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_HR_in_time466 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time468 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_MIN_in_time470 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time472 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000400L});
    public static final BitSet FOLLOW_SEC_in_time475 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_time477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FONTTBL_in_fonttbl488 = new BitSet(new long[]{0x0000000000800000L,0x0000000000200000L});
    public static final BitSet FOLLOW_fontinfo_in_fonttbl493 = new BitSet(new long[]{0x0000000000800002L,0x0000000000200000L});
    public static final BitSet FOLLOW_85_in_fonttbl497 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_fontinfo_in_fonttbl501 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_fonttbl503 = new BitSet(new long[]{0x0000000000800002L,0x0000000000200000L});
    public static final BitSet FOLLOW_F_in_fontinfo521 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_fontinfo525 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_fontfamily_in_fontinfo529 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_FCHARSET_in_fontinfo533 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_fontinfo535 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_FPRQ_in_fontinfo539 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_fontinfo541 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_unknown_in_fontinfo545 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_text_in_fontinfo549 = new BitSet(new long[]{0x04800876AE605202L,0x0000000000208800L});
    public static final BitSet FOLLOW_COLORTBL_in_colortbl569 = new BitSet(new long[]{0x0000010000000102L,0x0000000000008020L});
    public static final BitSet FOLLOW_RED_in_colortbl574 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl576 = new BitSet(new long[]{0x0000010000000100L,0x0000000000008000L});
    public static final BitSet FOLLOW_GREEN_in_colortbl581 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl583 = new BitSet(new long[]{0x0000000000000100L,0x0000000000008000L});
    public static final BitSet FOLLOW_BLUE_in_colortbl588 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl590 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_TEXT_in_colortbl594 = new BitSet(new long[]{0x0000010000000102L,0x0000000000008020L});
    public static final BitSet FOLLOW_STYLESHEET_in_stylesheet604 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_entity_in_stylesheet607 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_INFO_in_info617 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_entity_in_info620 = new BitSet(new long[]{0xE49FA8FCFBEF5EB2L,0x000000000024C99DL});
    public static final BitSet FOLLOW_control_in_unknown631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_unknown637 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_STAR_in_unknown640 = new BitSet(new long[]{0xE01FA0FCFB8F0CB0L,0x000000000004419DL});
    public static final BitSet FOLLOW_word_in_unknown643 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_entity_in_unknown646 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_86_in_unknown649 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_unknown655 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_STAR_in_unknown658 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_CONTROL_in_unknown661 = new BitSet(new long[]{0xE69FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_NUMBER_in_unknown665 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_entity_in_unknown671 = new BitSet(new long[]{0xE49FA8FCFBEF5EB0L,0x000000000064C99DL});
    public static final BitSet FOLLOW_86_in_unknown676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONTROL_in_control686 = new BitSet(new long[]{0x0200000000000002L});
    public static final BitSet FOLLOW_NUMBER_in_control688 = new BitSet(new long[]{0x0000000000000002L});
}
