package ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf;


import and.awt.Color;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Convert extends TreeParser {
    public static final String[] tokenNames = new String[] {
            "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ANSI", "ANSICPG", "AUTHOR", "B",
            "BLUE", "BULLET", "CELL", "CF", "CLOSEBRACE", "COLORTBL", "CONTROL", "CREATIM",
            "DEFF", "DEFLANG", "DEFLANGFE", "DEFTAB", "DY", "EMDASH", "ENDASH", "F",
            "FALT", "FBIDI", "FCHARSET", "FDECOR", "FI", "FMODERN", "FNAME", "FNIL",
            "FONTTBL", "FPRQ", "FROMAN", "FS", "FSCRIPT", "FSWISS", "FTECH", "GENERATOR",
            "GREEN", "HEADER", "HEX", "HEXCHAR", "HR", "I", "INFO", "INTBL", "LANG",
            "LDBLQUOTE", "LI", "LINE", "MAC", "MIN", "MO", "NBSP", "NEWLINE", "NUMBER",
            "OPENBRACE", "OPERATOR", "OTHER", "PAR", "PARD", "PLAIN", "PNSTART", "PRINTIM",
            "QC", "QJ", "RDBLQUOTE", "RED", "REVTIM", "ROW", "RQUOTE", "RTF", "SEC",
            "SLASH", "STAR", "STYLESHEET", "TAB", "TEXT", "TITLE", "TREE", "UC", "WS",
            "YR", "'{'", "'}'"
    };
    public static final int EOF=-1;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int ANSI=4;
    public static final int ANSICPG=5;
    public static final int AUTHOR=6;
    public static final int B=7;
    public static final int BLUE=8;
    public static final int BULLET=9;
    public static final int CELL=10;
    public static final int CF=11;
    public static final int CLOSEBRACE=12;
    public static final int COLORTBL=13;
    public static final int CONTROL=14;
    public static final int CREATIM=15;
    public static final int DEFF=16;
    public static final int DEFLANG=17;
    public static final int DEFLANGFE=18;
    public static final int DEFTAB=19;
    public static final int DY=20;
    public static final int EMDASH=21;
    public static final int ENDASH=22;
    public static final int F=23;
    public static final int FALT=24;
    public static final int FBIDI=25;
    public static final int FCHARSET=26;
    public static final int FDECOR=27;
    public static final int FI=28;
    public static final int FMODERN=29;
    public static final int FNAME=30;
    public static final int FNIL=31;
    public static final int FONTTBL=32;
    public static final int FPRQ=33;
    public static final int FROMAN=34;
    public static final int FS=35;
    public static final int FSCRIPT=36;
    public static final int FSWISS=37;
    public static final int FTECH=38;
    public static final int GENERATOR=39;
    public static final int GREEN=40;
    public static final int HEADER=41;
    public static final int HEX=42;
    public static final int HEXCHAR=43;
    public static final int HR=44;
    public static final int I=45;
    public static final int INFO=46;
    public static final int INTBL=47;
    public static final int LANG=48;
    public static final int LDBLQUOTE=49;
    public static final int LI=50;
    public static final int LINE=51;
    public static final int MAC=52;
    public static final int MIN=53;
    public static final int MO=54;
    public static final int NBSP=55;
    public static final int NEWLINE=56;
    public static final int NUMBER=57;
    public static final int OPENBRACE=58;
    public static final int OPERATOR=59;
    public static final int OTHER=60;
    public static final int PAR=61;
    public static final int PARD=62;
    public static final int PLAIN=63;
    public static final int PNSTART=64;
    public static final int PRINTIM=65;
    public static final int QC=66;
    public static final int QJ=67;
    public static final int RDBLQUOTE=68;
    public static final int RED=69;
    public static final int REVTIM=70;
    public static final int ROW=71;
    public static final int RQUOTE=72;
    public static final int RTF=73;
    public static final int SEC=74;
    public static final int SLASH=75;
    public static final int STAR=76;
    public static final int STYLESHEET=77;
    public static final int TAB=78;
    public static final int TEXT=79;
    public static final int TITLE=80;
    public static final int TREE=81;
    public static final int UC=82;
    public static final int WS=83;
    public static final int YR=84;

    // delegates
    public TreeParser[] getDelegates() {
        return new TreeParser[] {};
    }

    // delegators


    public Convert(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public Convert(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    @Override public String[] getTokenNames() { return Convert.tokenNames; }
    @Override public String getGrammarFileName() { return "/assets/Convert.g"; }


    Engine engine;

    public Convert(TreeNodeStream input, Engine engine) {
        this(input);
        this.engine = engine;
    }



    // $ANTLR start "rtf"
    // /assets/Convert.g:21:1: rtf : ^( RTF NUMBER header body ) ;
    public final void rtf() throws RecognitionException {
        try {
            // /assets/Convert.g:21:4: ( ^( RTF NUMBER header body ) )
            // /assets/Convert.g:21:6: ^( RTF NUMBER header body )
            {
                engine.start();
                match(input,RTF,FOLLOW_RTF_in_rtf45);
                match(input, Token.DOWN, null);
                match(input,NUMBER,FOLLOW_NUMBER_in_rtf47);
                pushFollow(FOLLOW_header_in_rtf49);
                header();
                state._fsp--;

                pushFollow(FOLLOW_body_in_rtf51);
                body();
                state._fsp--;

                match(input, Token.UP, null);

                engine.end();
            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "rtf"



    // $ANTLR start "entity"
    // /assets/Convert.g:23:1: entity : . ;
    public final void entity() throws RecognitionException {
        try {
            // /assets/Convert.g:23:7: ( . )
            // /assets/Convert.g:23:9: .
            {
                matchAny(input);
            }

        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "entity"



    // $ANTLR start "hword"
    // /assets/Convert.g:25:1: hword : ( DEFF NUMBER | ANSI | ANSICPG NUMBER | MAC | DEFLANG NUMBER | DEFLANGFE NUMBER | DEFTAB NUMBER | UC NUMBER );
    public final void hword() throws RecognitionException {
        CommonTree NUMBER1=null;
        CommonTree NUMBER2=null;

        try {
            // /assets/Convert.g:25:6: ( DEFF NUMBER | ANSI | ANSICPG NUMBER | MAC | DEFLANG NUMBER | DEFLANGFE NUMBER | DEFTAB NUMBER | UC NUMBER )
            int alt1=8;
            switch ( input.LA(1) ) {
                case DEFF:
                {
                    alt1=1;
                }
                break;
                case ANSI:
                {
                    alt1=2;
                }
                break;
                case ANSICPG:
                {
                    alt1=3;
                }
                break;
                case MAC:
                {
                    alt1=4;
                }
                break;
                case DEFLANG:
                {
                    alt1=5;
                }
                break;
                case DEFLANGFE:
                {
                    alt1=6;
                }
                break;
                case DEFTAB:
                {
                    alt1=7;
                }
                break;
                case UC:
                {
                    alt1=8;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 1, 0, input);
                    throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // /assets/Convert.g:26:2: DEFF NUMBER
                {
                    match(input,DEFF,FOLLOW_DEFF_in_hword73);
                    NUMBER1=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_hword75);
                    engine.deff((NUMBER1!=null?NUMBER1.getText():null));
                }
                break;
                case 2 :
                    // /assets/Convert.g:27:2: ANSI
                {
                    match(input,ANSI,FOLLOW_ANSI_in_hword82);
                }
                break;
                case 3 :
                    // /assets/Convert.g:28:2: ANSICPG NUMBER
                {
                    match(input,ANSICPG,FOLLOW_ANSICPG_in_hword87);
                    NUMBER2=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_hword89);
                    engine.ansicpg(Integer.parseInt((NUMBER2!=null?NUMBER2.getText():null)));
                }
                break;
                case 4 :
                    // /assets/Convert.g:29:2: MAC
                {
                    match(input,MAC,FOLLOW_MAC_in_hword96);
                    engine.mac();
                }
                break;
                case 5 :
                    // /assets/Convert.g:30:2: DEFLANG NUMBER
                {
                    match(input,DEFLANG,FOLLOW_DEFLANG_in_hword103);
                    match(input,NUMBER,FOLLOW_NUMBER_in_hword105);
                }
                break;
                case 6 :
                    // /assets/Convert.g:31:2: DEFLANGFE NUMBER
                {
                    match(input,DEFLANGFE,FOLLOW_DEFLANGFE_in_hword110);
                    match(input,NUMBER,FOLLOW_NUMBER_in_hword112);
                }
                break;
                case 7 :
                    // /assets/Convert.g:32:2: DEFTAB NUMBER
                {
                    match(input,DEFTAB,FOLLOW_DEFTAB_in_hword117);
                    match(input,NUMBER,FOLLOW_NUMBER_in_hword119);
                }
                break;
                case 8 :
                    // /assets/Convert.g:33:2: UC NUMBER
                {
                    match(input,UC,FOLLOW_UC_in_hword124);
                    match(input,NUMBER,FOLLOW_NUMBER_in_hword126);
                }
                break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "hword"



    // $ANTLR start "hentity"
    // /assets/Convert.g:34:1: hentity : ( hword | ^( ( STYLESHEET | GENERATOR | HEADER ) ( entity )* ) | fonttbl | colortbl | info );
    public final void hentity() throws RecognitionException {
        try {
            // /assets/Convert.g:34:8: ( hword | ^( ( STYLESHEET | GENERATOR | HEADER ) ( entity )* ) | fonttbl | colortbl | info )
            int alt3=5;
            switch ( input.LA(1) ) {
                case ANSI:
                case ANSICPG:
                case DEFF:
                case DEFLANG:
                case DEFLANGFE:
                case DEFTAB:
                case MAC:
                case UC:
                {
                    alt3=1;
                }
                break;
                case GENERATOR:
                case HEADER:
                case STYLESHEET:
                {
                    alt3=2;
                }
                break;
                case FONTTBL:
                {
                    alt3=3;
                }
                break;
                case COLORTBL:
                {
                    alt3=4;
                }
                break;
                case INFO:
                {
                    alt3=5;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 3, 0, input);
                    throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // /assets/Convert.g:34:10: hword
                {
                    pushFollow(FOLLOW_hword_in_hentity133);
                    hword();
                    state._fsp--;

                }
                break;
                case 2 :
                    // /assets/Convert.g:34:18: ^( ( STYLESHEET | GENERATOR | HEADER ) ( entity )* )
                {
                    if ( input.LA(1)==GENERATOR||input.LA(1)==HEADER||input.LA(1)==STYLESHEET ) {
                        input.consume();
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }
                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null);
                        // /assets/Convert.g:34:54: ( entity )*
                        loop2:
                        while (true) {
                            int alt2=2;
                            int LA2_0 = input.LA(1);
                            if ( ((LA2_0 >= ANSI && LA2_0 <= 86)) ) {
                                alt2=1;
                            }

                            switch (alt2) {
                                case 1 :
                                    // /assets/Convert.g:34:54: entity
                                {
                                    pushFollow(FOLLOW_entity_in_hentity150);
                                    entity();
                                    state._fsp--;

                                }
                                break;

                                default :
                                    break loop2;
                            }
                        }

                        match(input, Token.UP, null);
                    }

                }
                break;
                case 3 :
                    // /assets/Convert.g:34:65: fonttbl
                {
                    pushFollow(FOLLOW_fonttbl_in_hentity156);
                    fonttbl();
                    state._fsp--;

                }
                break;
                case 4 :
                    // /assets/Convert.g:34:75: colortbl
                {
                    pushFollow(FOLLOW_colortbl_in_hentity160);
                    colortbl();
                    state._fsp--;

                }
                break;
                case 5 :
                    // /assets/Convert.g:34:86: info
                {
                    pushFollow(FOLLOW_info_in_hentity164);
                    info();
                    state._fsp--;

                }
                break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "hentity"



    // $ANTLR start "fonttbl"
    // /assets/Convert.g:36:1: fonttbl : ^( FONTTBL ( fontdesc )* ) ;
    public final void fonttbl() throws RecognitionException {
        try {
            // /assets/Convert.g:36:8: ( ^( FONTTBL ( fontdesc )* ) )
            // /assets/Convert.g:36:10: ^( FONTTBL ( fontdesc )* )
            {
                match(input,FONTTBL,FOLLOW_FONTTBL_in_fonttbl173);
                if ( input.LA(1)==Token.DOWN ) {
                    match(input, Token.DOWN, null);
                    // /assets/Convert.g:36:20: ( fontdesc )*
                    loop4:
                    while (true) {
                        int alt4=2;
                        int LA4_0 = input.LA(1);
                        if ( (LA4_0==F) ) {
                            alt4=1;
                        }

                        switch (alt4) {
                            case 1 :
                                // /assets/Convert.g:36:20: fontdesc
                            {
                                pushFollow(FOLLOW_fontdesc_in_fonttbl175);
                                fontdesc();
                                state._fsp--;

                            }
                            break;

                            default :
                                break loop4;
                        }
                    }

                    match(input, Token.UP, null);
                }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "fonttbl"



    // $ANTLR start "fontdesc"
    // /assets/Convert.g:37:1: fontdesc : ^( F NUMBER text ) ;
    public final void fontdesc() throws RecognitionException {
        CommonTree NUMBER3=null;
        String text4 =null;

        try {
            // /assets/Convert.g:37:9: ( ^( F NUMBER text ) )
            // /assets/Convert.g:37:11: ^( F NUMBER text )
            {
                match(input,F,FOLLOW_F_in_fontdesc185);
                match(input, Token.DOWN, null);
                NUMBER3=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_fontdesc187);
                pushFollow(FOLLOW_text_in_fontdesc189);
                text4=text();
                state._fsp--;

                match(input, Token.UP, null);

                engine.font((NUMBER3!=null?NUMBER3.getText():null), new Engine.Font(text4.substring(0, text4.length() - 1)));
            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "fontdesc"



    // $ANTLR start "info"
    // /assets/Convert.g:39:1: info : ^( INFO ( ^( TITLE title= TEXT ) | entity )* ) ;
    public final void info() throws RecognitionException {
        CommonTree title=null;

        try {
            // /assets/Convert.g:39:5: ( ^( INFO ( ^( TITLE title= TEXT ) | entity )* ) )
            // /assets/Convert.g:39:7: ^( INFO ( ^( TITLE title= TEXT ) | entity )* )
            {
                match(input,INFO,FOLLOW_INFO_in_info201);
                if ( input.LA(1)==Token.DOWN ) {
                    match(input, Token.DOWN, null);
                    // /assets/Convert.g:39:14: ( ^( TITLE title= TEXT ) | entity )*
                    loop5:
                    while (true) {
                        int alt5=3;
                        int LA5_0 = input.LA(1);
                        if ( (LA5_0==TITLE) ) {
                            int LA5_2 = input.LA(2);
                            if ( (LA5_2==DOWN) ) {
                                int LA5_4 = input.LA(3);
                                if ( (LA5_4==TEXT) ) {
                                    int LA5_5 = input.LA(4);
                                    if ( (LA5_5==UP) ) {
                                        alt5=1;
                                    }
                                    else if ( ((LA5_5 >= ANSI && LA5_5 <= 86)) ) {
                                        alt5=2;
                                    }

                                }
                                else if ( ((LA5_4 >= ANSI && LA5_4 <= TAB)||(LA5_4 >= TITLE && LA5_4 <= 86)) ) {
                                    alt5=2;
                                }

                            }
                            else if ( ((LA5_2 >= UP && LA5_2 <= 86)) ) {
                                alt5=2;
                            }

                        }
                        else if ( ((LA5_0 >= ANSI && LA5_0 <= TEXT)||(LA5_0 >= TREE && LA5_0 <= 86)) ) {
                            alt5=2;
                        }

                        switch (alt5) {
                            case 1 :
                                // /assets/Convert.g:39:15: ^( TITLE title= TEXT )
                            {
                                match(input,TITLE,FOLLOW_TITLE_in_info205);
                                match(input, Token.DOWN, null);
                                title=(CommonTree)match(input,TEXT,FOLLOW_TEXT_in_info209);
                                engine.title((title!=null?title.getText():null));
                                match(input, Token.UP, null);

                            }
                            break;
                            case 2 :
                                // /assets/Convert.g:39:69: entity
                            {
                                pushFollow(FOLLOW_entity_in_info217);
                                entity();
                                state._fsp--;

                            }
                            break;

                            default :
                                break loop5;
                        }
                    }

                    match(input, Token.UP, null);
                }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "info"



    // $ANTLR start "colortbl"
    // /assets/Convert.g:41:1: colortbl : ^( COLORTBL ( ( RED r= NUMBER )? ( GREEN g= NUMBER )? ( BLUE b= NUMBER )? TEXT )+ ) ;
    public final void colortbl() throws RecognitionException {
        CommonTree r=null;
        CommonTree g=null;
        CommonTree b=null;

        try {
            // /assets/Convert.g:41:9: ( ^( COLORTBL ( ( RED r= NUMBER )? ( GREEN g= NUMBER )? ( BLUE b= NUMBER )? TEXT )+ ) )
            // /assets/Convert.g:41:11: ^( COLORTBL ( ( RED r= NUMBER )? ( GREEN g= NUMBER )? ( BLUE b= NUMBER )? TEXT )+ )
            {
                match(input,COLORTBL,FOLLOW_COLORTBL_in_colortbl229);

                int red = 0, green = 0, blue = 0;

                match(input, Token.DOWN, null);
                // /assets/Convert.g:44:2: ( ( RED r= NUMBER )? ( GREEN g= NUMBER )? ( BLUE b= NUMBER )? TEXT )+
                int cnt9=0;
                loop9:
                while (true) {
                    int alt9=2;
                    int LA9_0 = input.LA(1);
                    if ( (LA9_0==BLUE||LA9_0==GREEN||LA9_0==RED||LA9_0==TEXT) ) {
                        alt9=1;
                    }

                    switch (alt9) {
                        case 1 :
                            // /assets/Convert.g:44:2: ( RED r= NUMBER )? ( GREEN g= NUMBER )? ( BLUE b= NUMBER )? TEXT
                        {
                            // /assets/Convert.g:44:2: ( RED r= NUMBER )?
                            int alt6=2;
                            int LA6_0 = input.LA(1);
                            if ( (LA6_0==RED) ) {
                                alt6=1;
                            }
                            switch (alt6) {
                                case 1 :
                                    // /assets/Convert.g:44:3: RED r= NUMBER
                                {
                                    match(input,RED,FOLLOW_RED_in_colortbl235);
                                    r=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl239);
                                    red = Integer.parseInt((r!=null?r.getText():null));
                                }
                                break;

                            }

                            // /assets/Convert.g:45:2: ( GREEN g= NUMBER )?
                            int alt7=2;
                            int LA7_0 = input.LA(1);
                            if ( (LA7_0==GREEN) ) {
                                alt7=1;
                            }
                            switch (alt7) {
                                case 1 :
                                    // /assets/Convert.g:45:3: GREEN g= NUMBER
                                {
                                    match(input,GREEN,FOLLOW_GREEN_in_colortbl248);
                                    g=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl252);
                                    green = Integer.parseInt((g!=null?g.getText():null));
                                }
                                break;

                            }

                            // /assets/Convert.g:46:2: ( BLUE b= NUMBER )?
                            int alt8=2;
                            int LA8_0 = input.LA(1);
                            if ( (LA8_0==BLUE) ) {
                                alt8=1;
                            }
                            switch (alt8) {
                                case 1 :
                                    // /assets/Convert.g:46:3: BLUE b= NUMBER
                                {
                                    match(input,BLUE,FOLLOW_BLUE_in_colortbl261);
                                    b=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_colortbl265);
                                    blue = Integer.parseInt((b!=null?b.getText():null));
                                }
                                break;

                            }

                            match(input,TEXT,FOLLOW_TEXT_in_colortbl272);

                            engine.color(new Color(red, green, blue));

                        }
                        break;

                        default :
                            if ( cnt9 >= 1 ) break loop9;
                            EarlyExitException eee = new EarlyExitException(9, input);
                            throw eee;
                    }
                    cnt9++;
                }

                match(input, Token.UP, null);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "colortbl"



    // $ANTLR start "text"
    // /assets/Convert.g:50:1: text returns [String value] : (a= ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE ) )+ ;
    public final String text() throws RecognitionException {
        String value = null;


        CommonTree a=null;

        try {
            // /assets/Convert.g:50:29: ( (a= ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE ) )+ )
            // /assets/Convert.g:50:31: (a= ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE ) )+
            {

                StringBuffer result = new StringBuffer();
                // /assets/Convert.g:52:2: (a= ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE ) )+
                int cnt10=0;
                loop10:
                while (true) {
                    int alt10=2;
                    int LA10_0 = input.LA(1);
                    if ( (LA10_0==BULLET||LA10_0==CLOSEBRACE||(LA10_0 >= EMDASH && LA10_0 <= ENDASH)||LA10_0==HEXCHAR||LA10_0==NBSP||LA10_0==OPENBRACE||LA10_0==SLASH||LA10_0==TEXT) ) {
                        alt10=1;
                    }

                    switch (alt10) {
                        case 1 :
                            // /assets/Convert.g:52:3: a= ( TEXT | NBSP | HEXCHAR | EMDASH | ENDASH | BULLET | SLASH | OPENBRACE | CLOSEBRACE )
                        {
                            a=(CommonTree)input.LT(1);
                            if ( input.LA(1)==BULLET||input.LA(1)==CLOSEBRACE||(input.LA(1) >= EMDASH && input.LA(1) <= ENDASH)||input.LA(1)==HEXCHAR||input.LA(1)==NBSP||input.LA(1)==OPENBRACE||input.LA(1)==SLASH||input.LA(1)==TEXT ) {
                                input.consume();
                                state.errorRecovery=false;
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                throw mse;
                            }

                            result.append((a!=null?a.getText():null));
                        }
                        break;

                        default :
                            if ( cnt10 >= 1 ) break loop10;
                            EarlyExitException eee = new EarlyExitException(10, input);
                            throw eee;
                    }
                    cnt10++;
                }


                value = result.toString();
            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
        return value;
    }
    // $ANTLR end "text"



    // $ANTLR start "header"
    // /assets/Convert.g:56:1: header : ( hentity )* ;
    public final void header() throws RecognitionException {
        try {
            // /assets/Convert.g:56:7: ( ( hentity )* )
            // /assets/Convert.g:56:9: ( hentity )*
            {
                // /assets/Convert.g:56:9: ( hentity )*
                loop11:
                while (true) {
                    int alt11=2;
                    int LA11_0 = input.LA(1);
                    if ( ((LA11_0 >= ANSI && LA11_0 <= ANSICPG)||LA11_0==COLORTBL||(LA11_0 >= DEFF && LA11_0 <= DEFTAB)||LA11_0==FONTTBL||LA11_0==GENERATOR||LA11_0==HEADER||LA11_0==INFO||LA11_0==MAC||LA11_0==STYLESHEET||LA11_0==UC) ) {
                        alt11=1;
                    }

                    switch (alt11) {
                        case 1 :
                            // /assets/Convert.g:56:9: hentity
                        {
                            pushFollow(FOLLOW_hentity_in_header346);
                            hentity();
                            state._fsp--;

                        }
                        break;

                        default :
                            break loop11;
                    }
                }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "header"



    // $ANTLR start "bstart"
    // /assets/Convert.g:58:1: bstart : ( INTBL | CELL | ROW | TEXT | LDBLQUOTE | RDBLQUOTE | TAB | LINE | NBSP | HEXCHAR | QC | QJ | LI NUMBER | CF NUMBER | BULLET | SLASH | OPENBRACE | CLOSEBRACE | PAR | PARD | FS NUMBER | F NUMBER | I NUMBER | I | B | FI NUMBER | PLAIN | EMDASH | ENDASH | B NUMBER | RQUOTE | LANG NUMBER | ^( TREE ( bentity )* ) );
    public final void bstart() throws RecognitionException {
        CommonTree TEXT5=null;
        CommonTree HEXCHAR6=null;
        CommonTree NUMBER7=null;
        CommonTree NUMBER8=null;
        CommonTree NUMBER9=null;
        CommonTree NUMBER10=null;
        CommonTree NUMBER11=null;

        try {
            // /assets/Convert.g:58:7: ( INTBL | CELL | ROW | TEXT | LDBLQUOTE | RDBLQUOTE | TAB | LINE | NBSP | HEXCHAR | QC | QJ | LI NUMBER | CF NUMBER | BULLET | SLASH | OPENBRACE | CLOSEBRACE | PAR | PARD | FS NUMBER | F NUMBER | I NUMBER | I | B | FI NUMBER | PLAIN | EMDASH | ENDASH | B NUMBER | RQUOTE | LANG NUMBER | ^( TREE ( bentity )* ) )
            int alt13=33;
            switch ( input.LA(1) ) {
                case INTBL:
                {
                    alt13=1;
                }
                break;
                case CELL:
                {
                    alt13=2;
                }
                break;
                case ROW:
                {
                    alt13=3;
                }
                break;
                case TEXT:
                {
                    alt13=4;
                }
                break;
                case LDBLQUOTE:
                {
                    alt13=5;
                }
                break;
                case RDBLQUOTE:
                {
                    alt13=6;
                }
                break;
                case TAB:
                {
                    alt13=7;
                }
                break;
                case LINE:
                {
                    alt13=8;
                }
                break;
                case NBSP:
                {
                    alt13=9;
                }
                break;
                case HEXCHAR:
                {
                    alt13=10;
                }
                break;
                case QC:
                {
                    alt13=11;
                }
                break;
                case QJ:
                {
                    alt13=12;
                }
                break;
                case LI:
                {
                    alt13=13;
                }
                break;
                case CF:
                {
                    alt13=14;
                }
                break;
                case BULLET:
                {
                    alt13=15;
                }
                break;
                case SLASH:
                {
                    alt13=16;
                }
                break;
                case OPENBRACE:
                {
                    alt13=17;
                }
                break;
                case CLOSEBRACE:
                {
                    alt13=18;
                }
                break;
                case PAR:
                {
                    alt13=19;
                }
                break;
                case PARD:
                {
                    alt13=20;
                }
                break;
                case FS:
                {
                    alt13=21;
                }
                break;
                case F:
                {
                    alt13=22;
                }
                break;
                case I:
                {
                    int LA13_23 = input.LA(2);
                    if ( (LA13_23==NUMBER) ) {
                        alt13=23;
                    }
                    else if ( (LA13_23==UP||LA13_23==B||(LA13_23 >= BULLET && LA13_23 <= CLOSEBRACE)||(LA13_23 >= EMDASH && LA13_23 <= F)||LA13_23==FI||LA13_23==FS||LA13_23==HEXCHAR||LA13_23==I||(LA13_23 >= INTBL && LA13_23 <= LINE)||LA13_23==NBSP||LA13_23==OPENBRACE||(LA13_23 >= PAR && LA13_23 <= PLAIN)||(LA13_23 >= QC && LA13_23 <= RDBLQUOTE)||(LA13_23 >= ROW && LA13_23 <= RQUOTE)||LA13_23==SLASH||(LA13_23 >= TAB && LA13_23 <= TEXT)||LA13_23==TREE) ) {
                        alt13=24;
                    }

                    else {
                        int nvaeMark = input.mark();
                        try {
                            input.consume();
                            NoViableAltException nvae =
                                    new NoViableAltException("", 13, 23, input);
                            throw nvae;
                        } finally {
                            input.rewind(nvaeMark);
                        }
                    }

                }
                break;
                case B:
                {
                    int LA13_24 = input.LA(2);
                    if ( (LA13_24==NUMBER) ) {
                        alt13=30;
                    }
                    else if ( (LA13_24==UP||LA13_24==B||(LA13_24 >= BULLET && LA13_24 <= CLOSEBRACE)||(LA13_24 >= EMDASH && LA13_24 <= F)||LA13_24==FI||LA13_24==FS||LA13_24==HEXCHAR||LA13_24==I||(LA13_24 >= INTBL && LA13_24 <= LINE)||LA13_24==NBSP||LA13_24==OPENBRACE||(LA13_24 >= PAR && LA13_24 <= PLAIN)||(LA13_24 >= QC && LA13_24 <= RDBLQUOTE)||(LA13_24 >= ROW && LA13_24 <= RQUOTE)||LA13_24==SLASH||(LA13_24 >= TAB && LA13_24 <= TEXT)||LA13_24==TREE) ) {
                        alt13=25;
                    }

                    else {
                        int nvaeMark = input.mark();
                        try {
                            input.consume();
                            NoViableAltException nvae =
                                    new NoViableAltException("", 13, 24, input);
                            throw nvae;
                        } finally {
                            input.rewind(nvaeMark);
                        }
                    }

                }
                break;
                case FI:
                {
                    alt13=26;
                }
                break;
                case PLAIN:
                {
                    alt13=27;
                }
                break;
                case EMDASH:
                {
                    alt13=28;
                }
                break;
                case ENDASH:
                {
                    alt13=29;
                }
                break;
                case RQUOTE:
                {
                    alt13=31;
                }
                break;
                case LANG:
                {
                    alt13=32;
                }
                break;
                case TREE:
                {
                    alt13=33;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 13, 0, input);
                    throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // /assets/Convert.g:59:2: INTBL
                {
                    match(input,INTBL,FOLLOW_INTBL_in_bstart357);
                    engine.intbl();
                }
                break;
                case 2 :
                    // /assets/Convert.g:60:2: CELL
                {
                    match(input,CELL,FOLLOW_CELL_in_bstart364);
                    engine.cell();
                }
                break;
                case 3 :
                    // /assets/Convert.g:61:2: ROW
                {
                    match(input,ROW,FOLLOW_ROW_in_bstart371);
                    engine.row();
                }
                break;
                case 4 :
                    // /assets/Convert.g:62:2: TEXT
                {
                    TEXT5=(CommonTree)match(input,TEXT,FOLLOW_TEXT_in_bstart378);
                    engine.text((TEXT5!=null?TEXT5.getText():null));
                }
                break;
                case 5 :
                    // /assets/Convert.g:63:2: LDBLQUOTE
                {
                    match(input,LDBLQUOTE,FOLLOW_LDBLQUOTE_in_bstart386);
                    engine.outText("\u201c");
                }
                break;
                case 6 :
                    // /assets/Convert.g:64:2: RDBLQUOTE
                {
                    match(input,RDBLQUOTE,FOLLOW_RDBLQUOTE_in_bstart393);
                    engine.outText("\u201d");
                }
                break;
                case 7 :
                    // /assets/Convert.g:65:2: TAB
                {
                    match(input,TAB,FOLLOW_TAB_in_bstart400);
                    engine.tab();
                }
                break;
                case 8 :
                    // /assets/Convert.g:66:2: LINE
                {
                    match(input,LINE,FOLLOW_LINE_in_bstart407);
                    engine.line();
                }
                break;
                case 9 :
                    // /assets/Convert.g:67:2: NBSP
                {
                    match(input,NBSP,FOLLOW_NBSP_in_bstart415);
                    engine.outText("\u00a0");
                }
                break;
                case 10 :
                    // /assets/Convert.g:68:2: HEXCHAR
                {
                    HEXCHAR6=(CommonTree)match(input,HEXCHAR,FOLLOW_HEXCHAR_in_bstart423);

                    int code = Integer.parseInt((HEXCHAR6!=null?HEXCHAR6.getText():null).substring(2), 16);
                    engine.charCode(code);

                }
                break;
                case 11 :
                    // /assets/Convert.g:72:2: QC
                {
                    match(input,QC,FOLLOW_QC_in_bstart430);
                    engine.qc();
                }
                break;
                case 12 :
                    // /assets/Convert.g:73:2: QJ
                {
                    match(input,QJ,FOLLOW_QJ_in_bstart437);
                    engine.qj();
                }
                break;
                case 13 :
                    // /assets/Convert.g:74:2: LI NUMBER
                {
                    match(input,LI,FOLLOW_LI_in_bstart444);
                    NUMBER7=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_bstart446);
                    engine.li(Integer.parseInt((NUMBER7!=null?NUMBER7.getText():null)));
                }
                break;
                case 14 :
                    // /assets/Convert.g:75:2: CF NUMBER
                {
                    match(input,CF,FOLLOW_CF_in_bstart453);
                    NUMBER8=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_bstart455);
                    engine.cf(Integer.parseInt((NUMBER8!=null?NUMBER8.getText():null)));
                }
                break;
                case 15 :
                    // /assets/Convert.g:76:2: BULLET
                {
                    match(input,BULLET,FOLLOW_BULLET_in_bstart462);
                    engine.outText("\u2022");
                }
                break;
                case 16 :
                    // /assets/Convert.g:77:2: SLASH
                {
                    match(input,SLASH,FOLLOW_SLASH_in_bstart469);
                    engine.outText("\\");
                }
                break;
                case 17 :
                    // /assets/Convert.g:78:2: OPENBRACE
                {
                    match(input,OPENBRACE,FOLLOW_OPENBRACE_in_bstart477);
                    engine.outText("{");
                }
                break;
                case 18 :
                    // /assets/Convert.g:79:2: CLOSEBRACE
                {
                    match(input,CLOSEBRACE,FOLLOW_CLOSEBRACE_in_bstart485);
                    engine.outText("}");
                }
                break;
                case 19 :
                    // /assets/Convert.g:80:2: PAR
                {
                    match(input,PAR,FOLLOW_PAR_in_bstart492);
                    engine.par();
                }
                break;
                case 20 :
                    // /assets/Convert.g:81:2: PARD
                {
                    match(input,PARD,FOLLOW_PARD_in_bstart500);
                    engine.pard();
                }
                break;
                case 21 :
                    // /assets/Convert.g:82:2: FS NUMBER
                {
                    match(input,FS,FOLLOW_FS_in_bstart508);
                    NUMBER9=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_bstart510);
                    engine.fs(Integer.parseInt((NUMBER9!=null?NUMBER9.getText():null)));
                }
                break;
                case 22 :
                    // /assets/Convert.g:83:2: F NUMBER
                {
                    match(input,F,FOLLOW_F_in_bstart518);
                    NUMBER10=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_bstart520);
                    engine.f((NUMBER10!=null?NUMBER10.getText():null));
                }
                break;
                case 23 :
                    // /assets/Convert.g:84:2: I NUMBER
                {
                    match(input,I,FOLLOW_I_in_bstart527);
                    match(input,NUMBER,FOLLOW_NUMBER_in_bstart529);
                    engine.i(false);
                }
                break;
                case 24 :
                    // /assets/Convert.g:85:2: I
                {
                    match(input,I,FOLLOW_I_in_bstart537);
                    engine.i(true);
                }
                break;
                case 25 :
                    // /assets/Convert.g:86:2: B
                {
                    match(input,B,FOLLOW_B_in_bstart544);
                    engine.b(true);
                }
                break;
                case 26 :
                    // /assets/Convert.g:87:2: FI NUMBER
                {
                    match(input,FI,FOLLOW_FI_in_bstart551);
                    NUMBER11=(CommonTree)match(input,NUMBER,FOLLOW_NUMBER_in_bstart553);
                    engine.fi(Integer.parseInt((NUMBER11!=null?NUMBER11.getText():null)));
                }
                break;
                case 27 :
                    // /assets/Convert.g:88:2: PLAIN
                {
                    match(input,PLAIN,FOLLOW_PLAIN_in_bstart560);
                    engine.plain();
                }
                break;
                case 28 :
                    // /assets/Convert.g:89:2: EMDASH
                {
                    match(input,EMDASH,FOLLOW_EMDASH_in_bstart567);
                    engine.emdash();
                }
                break;
                case 29 :
                    // /assets/Convert.g:90:2: ENDASH
                {
                    match(input,ENDASH,FOLLOW_ENDASH_in_bstart575);
                    engine.endash();
                }
                break;
                case 30 :
                    // /assets/Convert.g:91:2: B NUMBER
                {
                    match(input,B,FOLLOW_B_in_bstart583);
                    match(input,NUMBER,FOLLOW_NUMBER_in_bstart585);
                    engine.b(false);
                }
                break;
                case 31 :
                    // /assets/Convert.g:92:2: RQUOTE
                {
                    match(input,RQUOTE,FOLLOW_RQUOTE_in_bstart593);
                    engine.rquote();
                }
                break;
                case 32 :
                    // /assets/Convert.g:93:2: LANG NUMBER
                {
                    match(input,LANG,FOLLOW_LANG_in_bstart600);
                    match(input,NUMBER,FOLLOW_NUMBER_in_bstart602);
                }
                break;
                case 33 :
                    // /assets/Convert.g:94:2: ^( TREE ( bentity )* )
                {
                    match(input,TREE,FOLLOW_TREE_in_bstart609);
                    engine.push();
                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null);
                        // /assets/Convert.g:94:28: ( bentity )*
                        loop12:
                        while (true) {
                            int alt12=2;
                            int LA12_0 = input.LA(1);
                            if ( (LA12_0==B||(LA12_0 >= BULLET && LA12_0 <= CLOSEBRACE)||(LA12_0 >= EMDASH && LA12_0 <= F)||LA12_0==FI||LA12_0==FS||LA12_0==HEXCHAR||LA12_0==I||(LA12_0 >= INTBL && LA12_0 <= LINE)||LA12_0==NBSP||LA12_0==OPENBRACE||(LA12_0 >= PAR && LA12_0 <= PLAIN)||(LA12_0 >= QC && LA12_0 <= RDBLQUOTE)||(LA12_0 >= ROW && LA12_0 <= RQUOTE)||LA12_0==SLASH||(LA12_0 >= TAB && LA12_0 <= TEXT)||LA12_0==TREE) ) {
                                alt12=1;
                            }

                            switch (alt12) {
                                case 1 :
                                    // /assets/Convert.g:94:28: bentity
                                {
                                    pushFollow(FOLLOW_bentity_in_bstart613);
                                    bentity();
                                    state._fsp--;

                                }
                                break;

                                default :
                                    break loop12;
                            }
                        }

                        engine.pop();
                        match(input, Token.UP, null);
                    }

                }
                break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "bstart"



    // $ANTLR start "bentity"
    // /assets/Convert.g:95:1: bentity : bstart ;
    public final void bentity() throws RecognitionException {
        try {
            // /assets/Convert.g:95:8: ( bstart )
            // /assets/Convert.g:95:10: bstart
            {
                pushFollow(FOLLOW_bstart_in_bentity625);
                bstart();
                state._fsp--;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "bentity"



    // $ANTLR start "body"
    // /assets/Convert.g:96:1: body : bstart ( bentity )* ;
    public final void body() throws RecognitionException {
        try {
            // /assets/Convert.g:96:5: ( bstart ( bentity )* )
            // /assets/Convert.g:96:7: bstart ( bentity )*
            {
                engine.body();
                pushFollow(FOLLOW_bstart_in_body634);
                bstart();
                state._fsp--;

                // /assets/Convert.g:96:33: ( bentity )*
                loop14:
                while (true) {
                    int alt14=2;
                    int LA14_0 = input.LA(1);
                    if ( (LA14_0==B||(LA14_0 >= BULLET && LA14_0 <= CLOSEBRACE)||(LA14_0 >= EMDASH && LA14_0 <= F)||LA14_0==FI||LA14_0==FS||LA14_0==HEXCHAR||LA14_0==I||(LA14_0 >= INTBL && LA14_0 <= LINE)||LA14_0==NBSP||LA14_0==OPENBRACE||(LA14_0 >= PAR && LA14_0 <= PLAIN)||(LA14_0 >= QC && LA14_0 <= RDBLQUOTE)||(LA14_0 >= ROW && LA14_0 <= RQUOTE)||LA14_0==SLASH||(LA14_0 >= TAB && LA14_0 <= TEXT)||LA14_0==TREE) ) {
                        alt14=1;
                    }

                    switch (alt14) {
                        case 1 :
                            // /assets/Convert.g:96:33: bentity
                        {
                            pushFollow(FOLLOW_bentity_in_body636);
                            bentity();
                            state._fsp--;

                        }
                        break;

                        default :
                            break loop14;
                    }
                }

                engine.endbody();
            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "body"

    // Delegated rules



    public static final BitSet FOLLOW_RTF_in_rtf45 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NUMBER_in_rtf47 = new BitSet(new long[]{0xE49FEA8910EF3EB0L,0x000000000006E99CL});
    public static final BitSet FOLLOW_header_in_rtf49 = new BitSet(new long[]{0xE48FA80810E01E80L,0x000000000002C99CL});
    public static final BitSet FOLLOW_body_in_rtf51 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DEFF_in_hword73 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword75 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ANSI_in_hword82 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ANSICPG_in_hword87 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MAC_in_hword96 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEFLANG_in_hword103 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEFLANGFE_in_hword110 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEFTAB_in_hword117 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_UC_in_hword124 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_hword126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_hword_in_hentity133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_hentity138 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_entity_in_hentity150 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF8L,0x00000000007FFFFFL});
    public static final BitSet FOLLOW_fonttbl_in_hentity156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colortbl_in_hentity160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_info_in_hentity164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FONTTBL_in_fonttbl173 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_fontdesc_in_fonttbl175 = new BitSet(new long[]{0x0000000000800008L});
    public static final BitSet FOLLOW_F_in_fontdesc185 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NUMBER_in_fontdesc187 = new BitSet(new long[]{0x0480080000601200L,0x0000000000008800L});
    public static final BitSet FOLLOW_text_in_fontdesc189 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_INFO_in_info201 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_TITLE_in_info205 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_TEXT_in_info209 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_entity_in_info217 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF8L,0x00000000007FFFFFL});
    public static final BitSet FOLLOW_COLORTBL_in_colortbl229 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_RED_in_colortbl235 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl239 = new BitSet(new long[]{0x0000010000000100L,0x0000000000008000L});
    public static final BitSet FOLLOW_GREEN_in_colortbl248 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl252 = new BitSet(new long[]{0x0000000000000100L,0x0000000000008000L});
    public static final BitSet FOLLOW_BLUE_in_colortbl261 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_colortbl265 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_TEXT_in_colortbl272 = new BitSet(new long[]{0x0000010000000108L,0x0000000000008020L});
    public static final BitSet FOLLOW_set_in_text297 = new BitSet(new long[]{0x0480080000601202L,0x0000000000008800L});
    public static final BitSet FOLLOW_hentity_in_header346 = new BitSet(new long[]{0x00104281000F2032L,0x0000000000042000L});
    public static final BitSet FOLLOW_INTBL_in_bstart357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CELL_in_bstart364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ROW_in_bstart371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_bstart378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LDBLQUOTE_in_bstart386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RDBLQUOTE_in_bstart393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TAB_in_bstart400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LINE_in_bstart407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NBSP_in_bstart415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HEXCHAR_in_bstart423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QC_in_bstart430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QJ_in_bstart437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LI_in_bstart444 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CF_in_bstart453 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BULLET_in_bstart462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SLASH_in_bstart469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPENBRACE_in_bstart477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CLOSEBRACE_in_bstart485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PAR_in_bstart492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PARD_in_bstart500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FS_in_bstart508 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_F_in_bstart518 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_I_in_bstart527 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_I_in_bstart537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_B_in_bstart544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FI_in_bstart551 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLAIN_in_bstart560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EMDASH_in_bstart567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ENDASH_in_bstart575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_B_in_bstart583 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RQUOTE_in_bstart593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LANG_in_bstart600 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_NUMBER_in_bstart602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TREE_in_bstart609 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_bentity_in_bstart613 = new BitSet(new long[]{0xE48FA80810E01E88L,0x000000000002C99CL});
    public static final BitSet FOLLOW_bstart_in_bentity625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_bstart_in_body634 = new BitSet(new long[]{0xE48FA80810E01E82L,0x000000000002C99CL});
    public static final BitSet FOLLOW_bentity_in_body636 = new BitSet(new long[]{0xE48FA80810E01E82L,0x000000000002C99CL});
}

