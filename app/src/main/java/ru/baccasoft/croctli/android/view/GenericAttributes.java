package ru.baccasoft.croctli.android.view;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.StringScalarWithTitle;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by usermane on 05.11.2014.
 */
public class GenericAttributes extends LinearLayout {
    @SuppressWarnings("unused")
    private static final String TAG = GenericAttributes.class.getSimpleName();
    /**
     *
     * @param context
     * @param items вида [заголовок:значение]
     *              или иначе говоря [отображать слева, отображать справа]
     */
    public GenericAttributes(Context context, HashMap<String, String> items) {
        super(context);

        setOrientation(VERTICAL);

//        LinearLayout root = new LinearLayout(context);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        Log.d(TAG, "creating views for generic attributes");
        for(Map.Entry<String, String> i : items.entrySet()) {
            Log.d(TAG, "creating view for generic attribute: [" + i.getKey() + "], [" + i.getValue() + "]");
            StringScalarWithTitle view = new StringScalarWithTitle(context, i.getKey(), i.getValue(), true, null);
            view.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            addView(view);
        }
    }
}
