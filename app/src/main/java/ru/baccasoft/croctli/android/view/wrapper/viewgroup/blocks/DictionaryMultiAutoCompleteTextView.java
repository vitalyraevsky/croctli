package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.editor.ArrayPropertyView;

import java.util.ArrayList;
import java.util.List;

/**
 *  Отображает поле для ввода множества значений. При вводе значения выпадает список для автодополнения.
 *
 */
public class DictionaryMultiAutoCompleteTextView extends
        MultiAutoCompleteTextView {

    private Context mContext;
    private Tokenizer myTokenizer;
    private TextWatcher textWatcher;

    public DictionaryMultiAutoCompleteTextView(Context context) {
        super(context);
        mContext = context;
//        setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_light));
    }

    public void initAdapter(List<ArrayPropertyView.ItemHolder> items) {
        ArrayAdapter adapter = makeOwnAdapter(items);
        setAdapter(adapter);
    }

    //TODO: переписать свой токинайзер
    public void initTokenizer() {

        myTokenizer = new Tokenizer() {
            @Override
            public int findTokenStart(CharSequence text, int cursor) {
                return 0;
            }

            @Override
            public int findTokenEnd(CharSequence text, int cursor) {
                return 0;
            }

            @Override
            public CharSequence terminateToken(CharSequence text) {
                return null;
            }
        };


        setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

    }

    public void initTextWatcher() {
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        this.addTextChangedListener(textWatcher);
    }

    //TODO: переписать кастомный адаптер
    private ArrayAdapter makeOwnAdapter(List<ArrayPropertyView.ItemHolder> items) {
        List<String> tmpList = new ArrayList<String>();
        for(ArrayPropertyView.ItemHolder i : items) {
            tmpList.add(i.name);
            Log.d("", "item: " + i.name);
        }
        Log.d("", "size: " + tmpList.size());

        String[] itemsConverted = new String[tmpList.size()];
        tmpList.toArray(itemsConverted);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_dropdown_item, itemsConverted);

//        ArrayAdapter<ArrayPropertyView.ItemHolder> adapterNew = new ArrayAdapter<ArrayPropertyView.ItemHolder>
        ItemAdapter adapterNew = new ItemAdapter(mContext, R.layout.array_property_dropdown_item, items);

        return adapterNew;
    }

    private class ItemAdapter extends ArrayAdapter<ArrayPropertyView.ItemHolder> {
        private List<ArrayPropertyView.ItemHolder> items;
        private int layoutResourceId;

        public ItemAdapter(Context context, int viewResourceId, List<ArrayPropertyView.ItemHolder> items) {
            super(context, viewResourceId);
            this.items = items;
            this.layoutResourceId = viewResourceId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View v = convertView;

            if(v == null) {
                LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(layoutResourceId, null);

                holder = new ViewHolder();
                holder.id = (TextView) v.findViewById(R.id.id_field);
                holder.text = (TextView) v.findViewById(R.id.text_field);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            holder.id.setText(items.get(position).id);
            holder.text.setText(items.get(position).name);
            Log.d("", items.get(position).name);

            return v;
        }

        private class ViewHolder {
            TextView id;
            TextView text;
        }
    }
}












