package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Classifier;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Связи с вложенными классами:
 * Classifier <- List<ClassifierItem>
 *    ClassifierItem <- List<ItemField>
 */

public class ClassifierDAO extends BaseDaoImpl<Classifier, String> {
    private static final String TAG = ClassifierDAO.class.getSimpleName();

    public ClassifierDAO(ConnectionSource connectionSource, Class<Classifier> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(Classifier data) throws SQLException {
        int ret = super.create(data);

        // by server design с каждым Classifier вместо List<ClassifierItem> приходит null
        // все ClassifierItem'ы, связанные с конкретным Classifier'ом, кладутся
        // на этапе синхронизации в Classifier.itemsCollection
        // так что здесь уже можем их запрашивать

        if(data.itemsCollection == null) {
            data.itemsCollection = (data.getItems().getItem() == null)
                    ? new ArrayList<ClassifierItem>()
                    : new ArrayList<ClassifierItem>(data.getItems().getItem());
        }

        // с каждым Classifier связан List<ClassifierItem>
        for(ClassifierItem classifierItem : data.itemsCollection) {
            classifierItem.setClassifier(data);
            App.getDatabaseHelper().getClassifierItemDAO().create(classifierItem);
        }
        return ret;
    }

    public List<Classifier> getById(String classifierTypeId) throws SQLException {
        QueryBuilder<Classifier, String> q = queryBuilder();
        q.where().eq("id", classifierTypeId);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<Classifier> preparedQuery = q.prepare();

        return query(preparedQuery);
    }
}
