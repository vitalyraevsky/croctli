package ru.baccasoft.croctli.android.filters;

import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.SpecialCondition;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.IExpression;
import ru.baccasoft.croctli.android.specialcondition.parser.ParseException;
import ru.baccasoft.croctli.android.specialcondition.parser.Parser;

public class SpecialConditionFilter implements ITaskFilter {

    @SuppressWarnings("unused")
    private static final String TAG = SpecialConditionFilter.class.getSimpleName();

    private final IExpression<Boolean> expression;

    public SpecialConditionFilter(SpecialCondition condition) {
        try {
            Log.w(TAG, condition.getExpression());
            Parser parser = new Parser(condition.getExpression());
            this.expression = parser.parse();
        } catch( ParseException e ) {
            String message = "Error parsing "+condition.getExpression();
            TableUtils.createLogMessage(message, e);
            throw new RuntimeException(message, e);
        }
    }

    @Override
    public boolean isAvailable(Task task) {
        return expression.compute(task);
    }
}
