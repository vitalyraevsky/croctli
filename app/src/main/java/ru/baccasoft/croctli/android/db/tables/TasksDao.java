package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import org.joda.time.DateTime;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Action;
import ru.baccasoft.croctli.android.gen.core.ActionBehavior;
import ru.baccasoft.croctli.android.gen.core.ArrayOfAction;
import ru.baccasoft.croctli.android.gen.core.ArrayOfConflictData;
import ru.baccasoft.croctli.android.gen.core.ArrayOfTaskActor;
import ru.baccasoft.croctli.android.gen.core.ConflictData;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.gen.core.TaskActor;
import ru.baccasoft.croctli.android.utils.DateTimeWrapper;

public class TasksDao extends BaseDaoImpl<Task, String> {
    private static final String TAG = TasksDao.class.getSimpleName();

    public TasksDao(ConnectionSource connectionSource, Class<Task> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(Task data) throws SQLException {

        // перекладываем даты. см. описание IDateField
        data.setChangeDateTime(toNullableDateTime(data.getChangeDateString()));
        data.setCreateDateTime(toNullableDateTime(data.getCreateDateString()));
        data.setDeadlineDateTime(toNullableDateTime(data.getDeadlineDateString()));

        int ret = super.create(data);

        if (data.getAvailableActions() == null) {
//            data.availableActionsCollection = new ArrayList<Action>();
        } else {
            for (Action action : data.getAvailableActions().getItem()) {

                action.setTask(data);
                App.getDatabaseHelper().getActionDAO().create(action);

                if(action.getBehaviors() == null) {
//                    action.behaviorsCollection = new ArrayList<ActionBehavior>();
                } else {
                    for(ActionBehavior actionBehavior : action.getBehaviors().getItem()) {

                        actionBehavior.setAction(action);
                        App.getDatabaseHelper().getActionBehaviorDAO().create(actionBehavior);

//                        action.behaviorsCollection = new ArrayList<ActionBehavior>(action.getBehaviors().getItem());
                    }
                }
            }
//            data.availableActionsCollection = new ArrayList<Action>(data.getAvailableActions().getItem());
//            Log.d(TAG, "action list size collection: " + data.availableActionsCollection.size());
        }

        // перекладываем исполнителей
        if(data.getExecutants() == null) {
            data.executantsCollection = new ArrayList<TaskActor>();
        } else {
            for(TaskActor taskActor : data.getExecutants().getItem()) {
                taskActor.setTask(data);
                App.getDatabaseHelper().getTaskActorDAO().create(taskActor);
//                data.executantsCollection = new ArrayList<TaskActor>(data.getExecutants().getItem());
            }
        }
        // перекладываем конфликты
        if (data.getConflictDataCollection() == null || data.getConflictDataCollection().isEmpty())
            data.setConflictDataCollection(data.getConflict().getItem());
        for(ConflictData conflict : data.getConflictDataCollection()) {
            conflict.setTask(data);
            App.getDatabaseHelper().getConflictDataDao().create(conflict);
        }
        return ret;
    }

    private static DateTime toNullableDateTime( String value ) {
        DateTimeWrapper dateTimeWrapper = new DateTimeWrapper(value);
        if( dateTimeWrapper.isValidDateSet()) {
            return new DateTime(dateTimeWrapper.smartGetTime());
        } else {
            return null;
        }
    }

    /**
     * Получить список идентификаторов всех задач, содержащихся в базе
     *
     * @return
     * @throws SQLException
     */
    public List<String> getAllIds() throws SQLException {
        List<String> ids = new ArrayList<String>();

        QueryBuilder<Task, String> q = queryBuilder();
        q.selectColumns("id");

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<Task> preparedQuery = q.prepare();
        List<Task> tasks = query(preparedQuery);

        for(Task t : tasks)
            ids.add(t.getId());

        return ids;
    }

    /**
     * Получить список идентификаторов задач, помеченных локально для удаления
     *
     * @return
     * @throws SQLException
     */
    public List<String> getDeletedTaskIds() throws SQLException {
        List<String> ids = new ArrayList<String>();

        QueryBuilder<Task, String> q = queryBuilder();
        q.selectColumns("id");

        q.where().eq("is_deleted", true);

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<Task> preparedQuery = q.prepare();
        List<Task> tasks = query(preparedQuery);

        for(Task t : tasks)
            ids.add(t.getId());

        return ids;
    }

    /**
     * Получить все задачи, измененные после конкретной даты.<br>
     * Обычно - с даты последней синхронизации.
     *
     * @param dateFrom дата в формате DateTime(Timezone.UTC)
     * @return
     * @throws SQLException
     */
    public List<Task> getModifiedTasks(DateTime dateFrom) throws SQLException {
        QueryBuilder<Task, String> q = queryBuilder();
        q.where().ge("change_date_time", dateFrom); // сравнение '>='

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<Task> preparedQuery = q.prepare();
        List<Task> tasks = query(preparedQuery);
        for(Task t:tasks) {
            ArrayOfTaskActor arrayOfTaskActor = new ArrayOfTaskActor();
            List<TaskActor> taskActors = new ArrayList<TaskActor>(t.executantsCollection);
            arrayOfTaskActor.setItem(taskActors);

            t.setExecutants(arrayOfTaskActor);

            ArrayOfAction arrayOfAction = new ArrayOfAction();
            List<Action> actions = new ArrayList<Action>(t.getAvailableActionsCollection());
            arrayOfAction.setItem(actions);

            t.setAvailableActions(arrayOfAction);

            ArrayOfConflictData arrayOfConflictData = new ArrayOfConflictData();
            List<ConflictData> conflictDatas = new ArrayList<ConflictData>(t.getConflictDataCollection());
            arrayOfConflictData.setItem(conflictDatas);

            t.setConflict(arrayOfConflictData);
        }

        return tasks;
    }
}