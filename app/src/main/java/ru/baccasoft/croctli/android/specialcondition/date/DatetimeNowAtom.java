package ru.baccasoft.croctli.android.specialcondition.date;

import ru.baccasoft.croctli.android.gen.core.Task;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DatetimeNowAtom implements IDateOperand {
	
	public static final String NAME = "datetime.now.date";
	
	// НЕПОТОКОБЕЗОПАСНО!
	private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	/*package*/ static int millisToInt( long millis ) {
		return Integer.valueOf( format.format(new Date( millis ) ) );
	}

	@Override
	public Integer compute(Task task) {
		return millisToInt(System.currentTimeMillis());
	}

}
