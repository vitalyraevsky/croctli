package ru.baccasoft.croctli.android.view.wrapper;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.SystemUser;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Создает вьюшку для EntityProperty, у которого флаг isSystemUser == true.
 *
 * Творческий копипаст из {@link DisplayMode1Wrapper}.
 */
public class SystemUserWrapper implements IWrapper {
    private boolean readOnly;
    private Context context;

    private static final String TAG = SystemUserWrapper.class.getSimpleName();

    public SystemUserWrapper(Context context) {
        this.context = context;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        if(this.readOnly) {
            return makeReadView(p);
        } else {
            return makeEditableView(p);
        }
    }

    private View makeReadView(EntityProperty p) {
        String userId = p.getValue();
        SystemUser user = TableUtils.getSystemUserById(userId);
        String displayValue = user.getShortName();
        return ViewUtils.makeStringScalarWithTitle(context,p.getDisplayGroup(), displayValue, true, p.getId());
    }

    private View makeEditableView(EntityProperty p) {
        List<SystemUser> allUsers = TableUtils.getAllUsers();
        String value = Preload.getValueFromClassifier(p);
        // вида [код:значение]
        Map<String, String> items = new HashMap<String, String>();
        for(SystemUser aUser : allUsers) {
            items.put(aUser.getId(), aUser.getShortName());
        }
        LinearLayout view = ViewUtils.makeEditableViewWithTitle(context, p.getDisplayGroup(), items, p.getId(), value, p);
        return view;
    }

    private void validate(EntityProperty p) {
        if(!p.isIsSystemUserType()) {
            Log.w(TAG, ReflectionToStringBuilder.toString(p));
            throw new IllegalStateException( "EntityProperty must reference system user: "+p.getId());
        }
    }
}
