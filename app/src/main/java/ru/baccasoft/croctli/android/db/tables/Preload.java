package ru.baccasoft.croctli.android.db.tables;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Classifier;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.util.CrocUtils;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Created by dima on 19.03.15.
 */
public class Preload {

    private static List<Map.Entry<Map.Entry<String,String>,Classifier>> listClassifierTemp;
    private static List<Map.Entry<String,String>>                       listClassifierKeysTemp;
    private static List<String> listClassifierItems;
    private static List<String> listClassifierItemsKeys;

    private static List<List<ClassifierItem>>   listClassifierItemsByClassifierId;
    private static List<String>                 listClassifierItemsByClassifierIdKeys;
    private static List<String>                 listValueFromClassifier;
    private static List<String>                 listValueFromClassifierKeys;

    private static List<Map.Entry<Map.Entry<String,String>,List<ClassifierItem>>> selectedItems;
    private static List<Map.Entry<String,String>> selectedItemsKeys;

    private static List<ClassifierItem> allItems;
    private static List<String> usedParentId;

    public static enum loading{
        full,
        half
    }

    /**
     * Делаем загрузку из базы отдельно
     * @return
     */
    public static void loadClassifiers(EntityProperty p){
        loadClassifier(p);
        loadClassifierItemByCode(p);
        loadClassifierItemsByClassifierId(p);
    }

    public static void loadClassifierItemByCode(EntityProperty p){
        String classifierCode = p.getValue();
        if (listClassifierItems == null) {
            listClassifierItems = new ArrayList<String>();
            listClassifierItemsKeys = new ArrayList<String>();
        }
        if (listClassifierItemsKeys.contains(classifierCode))
            return;
        if(ViewUtils.isNotEmpty(classifierCode)) {
            ClassifierItem classifierItem = TableUtils.getClassifierItemByCode(p.getClassifierTypeId(), classifierCode);
            if(classifierItem != null) {
                listClassifierItems.add(classifierItem.getDisplayValue());
                listClassifierItemsKeys.add(classifierCode);
            }
        }
    }

    public static void loadClassifierItemsByClassifierId(EntityProperty p){
        String value = p.getValue();
        if (listValueFromClassifier == null){
            listValueFromClassifierKeys = new ArrayList<String>();
            listValueFromClassifier     = new ArrayList<String>();
        }
        if (!listValueFromClassifierKeys.contains(value)){
            listValueFromClassifierKeys.add(value);
            value = CrocUtils.getValueFromClassifier(p);
            listValueFromClassifier.add(value);
        }
    }

    public static void loadSelectedItems(EntityProperty p){
        String itemParentId = p.getEntityPropertyId();
        String itemTypeId = p.getId();
        if (selectedItems == null) {
            selectedItems = new ArrayList<Map.Entry<Map.Entry<String, String>, List<ClassifierItem>>>();
            selectedItemsKeys = new ArrayList<Map.Entry<String, String>>();
        }
        AbstractMap.SimpleEntry<String,String> itemKey = new AbstractMap.SimpleEntry<String, String>(itemParentId,itemTypeId);
        if (selectedItemsKeys.contains(itemKey))
            return;
        List<ClassifierItem> newSelected = TableUtils.getClassifierItemsByCodes(p.getClassifierTypeId(), p.getValue());
        selectedItems.add(new AbstractMap.SimpleEntry<Map.Entry<String, String>, List<ClassifierItem>>(itemKey,newSelected));
        selectedItemsKeys.add(itemKey);
    }

    public static List<ClassifierItem> getSelectedItems(EntityProperty p){
        String itemParentId = p.getEntityPropertyId();
        String itemTypeId = p.getId();
        AbstractMap.SimpleEntry<String,String> itemKey = new AbstractMap.SimpleEntry<String, String>(itemParentId,itemTypeId);
        int i = selectedItemsKeys.indexOf(itemKey);
        if (i<0)
            return null;
        return selectedItems.get(i).getValue();
    }

    public static List<ClassifierItem> getClassifierItemsByClassiferId(EntityProperty p){
        String classifierTypeId = p.getClassifierTypeId();
        int i = listClassifierItemsByClassifierIdKeys.indexOf(classifierTypeId);
        List<ClassifierItem> l = listClassifierItemsByClassifierId.get(i);
        return l;
    }

    public static String getValueFromClassifier(EntityProperty p){
        String key = p.getValue();
        int i = listValueFromClassifierKeys.indexOf(key);
        if (i<0)
            return "";
        String s = listValueFromClassifier.get(i);
        return s;
    }

    public static void loadClassifier(EntityProperty p){
        String classifierParentId = p.getEntityPropertyId();
        String classifierId = p.getId();
        String classifierTypeId = p.getClassifierTypeId();

        if (listClassifierTemp == null) {
            listClassifierKeysTemp = new ArrayList<Map.Entry<String, String>>();
            listClassifierTemp = new ArrayList<Map.Entry<Map.Entry<String, String>, Classifier>>();
        }
        if (listClassifierItemsByClassifierId == null) {
            listClassifierItemsByClassifierId = new ArrayList<List<ClassifierItem>>();
            listClassifierItemsByClassifierIdKeys = new ArrayList<String>();
        }
        Classifier newClassifier = TableUtils.getClassifierById(classifierTypeId);
        if (!listClassifierItemsByClassifierIdKeys.contains(classifierTypeId)) {
            Collection<ClassifierItem> classifierItems = newClassifier.itemsCollection;
            listClassifierItemsByClassifierId.add(new ArrayList(classifierItems));
            listClassifierItemsByClassifierIdKeys.add(classifierTypeId);
        }

        AbstractMap.SimpleEntry<String,String> classifierKey = new AbstractMap.SimpleEntry<String,String>(classifierParentId,classifierId);

        if (listClassifierKeysTemp.contains(classifierKey)){
            return;
        }
        if(newClassifier == null)
            throw new IllegalStateException("cant find Classifier for EntityProperty with id:\'" +
                    p.getId() + "\'");

        listClassifierTemp.add(new AbstractMap.SimpleEntry<Map.Entry<String, String>, Classifier>(classifierKey, newClassifier));
        listClassifierKeysTemp.add(classifierKey);
    }

    public static void addToClassifier(List<EntityProperty> origProps){
        if (listClassifierTemp == null) {
            listClassifierKeysTemp = new ArrayList<Map.Entry<String, String>>();
            listClassifierTemp = new ArrayList<Map.Entry<Map.Entry<String, String>, Classifier>>();
        }
        for (EntityProperty p: origProps) {
            AbstractMap.SimpleEntry<String, String> classifierKey = new AbstractMap.SimpleEntry<String, String>(p.getEntityPropertyId(), p.getId());
            if (listClassifierKeysTemp.contains(classifierKey)){
                continue;
            }
            Classifier newClassifier = TableUtils.getClassifierById(p.getClassifierTypeId());
            if(newClassifier == null)
                continue;

            listClassifierTemp.add(new AbstractMap.SimpleEntry<Map.Entry<String, String>, Classifier>(classifierKey, newClassifier));
            listClassifierKeysTemp.add(classifierKey);
        }

    }

    public static Classifier getClassifier(EntityProperty p){
        AbstractMap.SimpleEntry<String,String> classifierKey = new AbstractMap.SimpleEntry<String, String>(p.getEntityPropertyId(),p.getId());
        int posKey = listClassifierKeysTemp.indexOf(classifierKey);
        if (posKey < 0)
            return null;
        return listClassifierTemp.get(posKey).getValue();
    }

    public static String getClassifierDisplayValue(String key){
        int pos = listClassifierItemsKeys.indexOf(key);
        if (pos < 0)
            return "";
        return listClassifierItems.get(pos);
    }

    public static void clearListClassifier(loading type){
        if(allItems != null)
            allItems.clear();
        if(usedParentId != null)
            usedParentId.clear();

        if (selectedItems != null)
            selectedItems.clear();
        if (selectedItemsKeys != null)
            selectedItemsKeys.clear();


        if (listClassifierKeysTemp != null)
            listClassifierKeysTemp.clear();
        if (listClassifierTemp != null)
            listClassifierTemp.clear();

        if (listClassifierItems != null)
            listClassifierItems.clear();
        if (listClassifierItemsKeys != null)
            listClassifierItemsKeys.clear();

        if (listValueFromClassifier != null)
            listValueFromClassifier.clear();
        if (listValueFromClassifierKeys != null)
            listValueFromClassifierKeys.clear();
    }

    public static void loadBase(String taskId, loading type){
        clearListClassifier(type);
        EntityProperty rootEp = TableUtils.getRootEpForErrands(taskId);
        if(rootEp == null) return;
        List<EntityProperty> childEps = TableUtils.getEntityPropertyChildsById(rootEp.getId());
        for (EntityProperty props:childEps){
            List<EntityProperty> cps = TableUtils.getEntityPropertyChildsById(props.getId());
            for (EntityProperty p: cps){
                startLoadingBaseByChildsId(p);
            }
        }
    }

    public static void startLoadingBaseByChildsId(EntityProperty p){
        if(!p.isIsVisible())
            return;
        if(p.isIsArrayProperty() ) {
            if(ViewUtils.isNotEmpty(p.getClassifierTypeId())) {
                loadClassifierItemsByClassifierId(p);
                loadSelectedItems(p);
            }
        }
        if(ViewUtils.isNotEmpty(p.getClassifierTypeId()) && !p.isIsArrayProperty()) {
            loadClassifiers(p);
        }
    }
}
