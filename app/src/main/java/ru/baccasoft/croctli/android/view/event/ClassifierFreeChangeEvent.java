package ru.baccasoft.croctli.android.view.event;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.validator.BaseValidator;
import ru.baccasoft.croctli.android.view.validator.NotNullValidator;

public class ClassifierFreeChangeEvent implements IChangeEvent {
    String entityPropertyId;
    String value;
    Context context;
    private final List<BaseValidator> validators;

    /**
     *
     * @param value - либо произвольное значение, либо значение из справочника (НЕ код значения)
     * @param entityPropertyId
     */
    public ClassifierFreeChangeEvent(String value, String entityPropertyId, Context context) {
        this.entityPropertyId = entityPropertyId;
        this.value = value;
        this.context = context;
        validators = new ArrayList<BaseValidator>(0);

        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        validators.add(new NotNullValidator(this.context, ep, value));
    }

    @Override
    public void save() {
        String valueToSave;

        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        if(ep == null)//TODO из-за реализации удаления резолюций, приложение тут падает, т.к. обращается к айдишнику, который мы удалили - пока закрыл так (не вникал сильно в Event)
            return;
        List<ClassifierItem> classifierItems = TableUtils.getClassifierItemsByClassifierId(ep.getClassifierTypeId());
        Map<String, ClassifierItem> valueClassifierItemMap = new HashMap<String, ClassifierItem>();

        for(ClassifierItem ci : classifierItems) {
            valueClassifierItemMap.put(ci.getDisplayValue(), ci);
        }
        if(valueClassifierItemMap.containsKey(this.value)) {
            valueToSave = valueClassifierItemMap.get(this.value).getCode();
        } else {
            valueToSave = this.value;
        }

        ep.setValue(valueToSave);
        TableUtils.updateEntityProperty(ep, true);
    }

    @Override
    public String getKey() {
        return entityPropertyId;
    }

    @Override
    public boolean isNotEmpty() {
        if(value.isEmpty()){
            return false;
        }
            return true;
    }

    @Override
    public List<BaseValidator> getValidators() {
        return validators;
    }
}
