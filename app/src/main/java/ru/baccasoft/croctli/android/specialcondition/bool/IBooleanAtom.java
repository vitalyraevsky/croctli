package ru.baccasoft.croctli.android.specialcondition.bool;

import ru.baccasoft.croctli.android.specialcondition.IExpression;

public interface IBooleanAtom extends IExpression<Boolean>, IBooleanAtomCreator {
	
}
