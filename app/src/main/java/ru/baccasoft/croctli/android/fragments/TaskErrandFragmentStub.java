package ru.baccasoft.croctli.android.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.dao.Errands;
import ru.baccasoft.croctli.android.db.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * На этом фрагменте будет показываться массив из Предварительных поручений.
 *
 */

public class TaskErrandFragmentStub extends Fragment {
    @SuppressWarnings("unused")
    private static final String TAG = TaskErrandFragmentStub.class.getSimpleName();

    private String taskId;

//    private List<EntityProperty> tmpEntityProperties = new ArrayList<EntityProperty>();
    private List<Errands> tmpErrands = new ArrayList<Errands>();


    public static TaskErrandFragmentStub newInstance(String taskId) {
        TaskErrandFragmentStub f = new TaskErrandFragmentStub();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if(args == null || !args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY)) {
            String message = "taskId cannot be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        } else {
            taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);
        }

        tmpErrands.add(new Errands());
        tmpErrands.add(new Errands());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_errand_fragment, container, false);
//        View view = inflater.inflate(R.layout.task_errand_fragment_tmp, container, false);
//        TODO: не мокать данные, получать нормально


        return view;
    }
}
