package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.StringScalarEditText;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.StringScalarTextView;

/**
 * Created by developer on 05.11.14.
 */
public class StringScalarWithTitle extends LinearLayout {

    private TextView titleView;
    private TextView contentView;

    public StringScalarWithTitle(Context context, String title, String text, boolean isReadonly,
                                 String entityPropertyId) {
        super(context);

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        titleView = ViewUtils.makeTitleTextView(context, title, null);

        if(isReadonly) {
            contentView = new StringScalarTextView(context, text);
        } else {
            contentView = new StringScalarEditText(context, text, entityPropertyId);
        }

        contentView.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        addView(titleView);
        addView(contentView);
    }
}
