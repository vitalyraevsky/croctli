package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.ClassifierAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * GD, GM
 *
 //6.2.14	Обновление справочников (ядро → клиент)
 // При синхронизации объектов Classifier не заполняется вложенная коллекция ClassifierItem.
 // Выгрузку соответствующих ClassifierItem-ов клиент делает самостоятельно
 // черех метод /api/classifieritem/deeplistbyclassifieritem (см. 7.2.15.2 Deeplistbyclassifier),
 // получая измененные с определенного момента времени элементы справочника.
 // Удаленные (подлежащие удалению на клиенте) элементы при этом отмечены флагом isdeleted=true.
 // Подлежащие обновлению и созданию отмечены флагом isdeleted=false.
 *
 */
public class ClassifierSync implements ISync {

    /**
     * Запрашиваются справочники в виде ClassifierSet.
     * Из него забирается List<Classifier>.
     * Для каждого Classifier'а запрашиваем массив ClassifierItem'ов
     * и кладем внутрь соответствующего Classifier'а.
     *  @param dateFrom
     * @param dateTo
     */
    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        ClassifierAPI classifierAPI = new ClassifierAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<ClassifierSet, Classifier>();

        @SuppressWarnings("unchecked")
        List<Classifier> classifiers = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, classifierAPI);

        for(Classifier c : classifiers) {
            List<ClassifierItem> classifierItems;
            classifierItems = getAllClassifierItems(c.getId(), 0, SYNC_MAX_COUNT, dateFrom);
            c.itemsCollection = new ArrayList<ClassifierItem>(classifierItems);

            TableUtils.createOrUpdateClassifier(c);
        }
    }

    /**
     * Получения связанных ClassifierItem'ов по отдельному rest-вызову
     * @return
     */
    private List<ClassifierItem> getAllClassifierItems(String classifierId, int start, int count, RestApiDate dateFrom) {
        List<ClassifierItem> allClassifierItems = new ArrayList<ClassifierItem>();

        final String url = "/api/classifieritem/deeplistbyclassifier/" + classifierId + "/" +
                String.valueOf(count) + "/" + String.valueOf(start) + "/" + dateFrom;
        final boolean needAuth = true;

        ClassifierItemSet classifierItemSet = RestClient.fillJsonClass(ClassifierItemSet.class, url, needAuth);
        if (classifierItemSet.getArrayOf() == null)
            return allClassifierItems;
        List<ClassifierItem> classifierItems = classifierItemSet.getArrayOf().getItem();

        if(classifierItems == null)
            return allClassifierItems;

        allClassifierItems.addAll(classifierItems);

        int returnedCount = classifierItems.size();
        if(returnedCount == count) {
            allClassifierItems.addAll(getAllClassifierItems(classifierId, start + count + 1, count, dateFrom));
        }

        return allClassifierItems;
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        ClassifierAPI classifierAPI = new ClassifierAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> classifierDeleteIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, classifierAPI);

        TableUtils.deleteClassifiers(classifierDeleteIds);
    }

//    @Override
//    public void getList() {
//        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
//    }
}
