package ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class RtfLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int ANSI=4;
    public static final int ANSICPG=5;
    public static final int AUTHOR=6;
    public static final int B=7;
    public static final int BLUE=8;
    public static final int BULLET=9;
    public static final int CELL=10;
    public static final int CF=11;
    public static final int CLOSEBRACE=12;
    public static final int COLORTBL=13;
    public static final int CONTROL=14;
    public static final int CREATIM=15;
    public static final int DEFF=16;
    public static final int DEFLANG=17;
    public static final int DEFLANGFE=18;
    public static final int DEFTAB=19;
    public static final int DY=20;
    public static final int EMDASH=21;
    public static final int ENDASH=22;
    public static final int F=23;
    public static final int FALT=24;
    public static final int FBIDI=25;
    public static final int FCHARSET=26;
    public static final int FDECOR=27;
    public static final int FI=28;
    public static final int FMODERN=29;
    public static final int FNAME=30;
    public static final int FNIL=31;
    public static final int FONTTBL=32;
    public static final int FPRQ=33;
    public static final int FROMAN=34;
    public static final int FS=35;
    public static final int FSCRIPT=36;
    public static final int FSWISS=37;
    public static final int FTECH=38;
    public static final int GENERATOR=39;
    public static final int GREEN=40;
    public static final int HEADER=41;
    public static final int HEX=42;
    public static final int HEXCHAR=43;
    public static final int HR=44;
    public static final int I=45;
    public static final int INFO=46;
    public static final int INTBL=47;
    public static final int LANG=48;
    public static final int LDBLQUOTE=49;
    public static final int LI=50;
    public static final int LINE=51;
    public static final int MAC=52;
    public static final int MIN=53;
    public static final int MO=54;
    public static final int NBSP=55;
    public static final int NEWLINE=56;
    public static final int NUMBER=57;
    public static final int OPENBRACE=58;
    public static final int OPERATOR=59;
    public static final int OTHER=60;
    public static final int PAR=61;
    public static final int PARD=62;
    public static final int PLAIN=63;
    public static final int PNSTART=64;
    public static final int PRINTIM=65;
    public static final int QC=66;
    public static final int QJ=67;
    public static final int RDBLQUOTE=68;
    public static final int RED=69;
    public static final int REVTIM=70;
    public static final int ROW=71;
    public static final int RQUOTE=72;
    public static final int RTF=73;
    public static final int SEC=74;
    public static final int SLASH=75;
    public static final int STAR=76;
    public static final int STYLESHEET=77;
    public static final int TAB=78;
    public static final int TEXT=79;
    public static final int TITLE=80;
    public static final int TREE=81;
    public static final int UC=82;
    public static final int WS=83;
    public static final int YR=84;

    boolean afterControl = false;


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public RtfLexer() {}
    public RtfLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public RtfLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    @Override public String getGrammarFileName() { return "/Users/green/dev/Rtf.g"; }

    // $ANTLR start "T__85"
    public final void mT__85() throws RecognitionException {
        try {
            int _type = T__85;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:6:7: ( '{' )
            // /Users/green/dev/Rtf.g:6:9: '{'
            {
                match('{');
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__85"

    // $ANTLR start "T__86"
    public final void mT__86() throws RecognitionException {
        try {
            int _type = T__86;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:7:7: ( '}' )
            // /Users/green/dev/Rtf.g:7:9: '}'
            {
                match('}');
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__86"

    // $ANTLR start "SLASH"
    public final void mSLASH() throws RecognitionException {
        try {
            int _type = SLASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:92:6: ( '\\\\\\\\' )
            // /Users/green/dev/Rtf.g:92:8: '\\\\\\\\'
            {
                match("\\\\");

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "SLASH"

    // $ANTLR start "STAR"
    public final void mSTAR() throws RecognitionException {
        try {
            int _type = STAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:93:5: ( '\\\\*' )
            // /Users/green/dev/Rtf.g:93:7: '\\\\*'
            {
                match("\\*");

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "STAR"

    // $ANTLR start "OPENBRACE"
    public final void mOPENBRACE() throws RecognitionException {
        try {
            int _type = OPENBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:94:10: ( '\\\\{' )
            // /Users/green/dev/Rtf.g:94:12: '\\\\{'
            {
                match("\\{");

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OPENBRACE"

    // $ANTLR start "CLOSEBRACE"
    public final void mCLOSEBRACE() throws RecognitionException {
        try {
            int _type = CLOSEBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:95:11: ( '\\\\}' )
            // /Users/green/dev/Rtf.g:95:13: '\\\\}'
            {
                match("\\}");

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CLOSEBRACE"

    // $ANTLR start "NBSP"
    public final void mNBSP() throws RecognitionException {
        try {
            int _type = NBSP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:96:5: ( '\\\\~' )
            // /Users/green/dev/Rtf.g:96:7: '\\\\~'
            {
                match("\\~");

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "NBSP"

    // $ANTLR start "OTHER"
    public final void mOTHER() throws RecognitionException {
        try {
            int _type = OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:97:6: ( '\\\\' ~ ( '\\n' | '\\r' | '\\\\' | '\\'' | '*' | '~' | '{' | '}' | 'a' .. 'z' | 'A' .. 'Z' ) )
            // /Users/green/dev/Rtf.g:97:8: '\\\\' ~ ( '\\n' | '\\r' | '\\\\' | '\\'' | '*' | '~' | '{' | '}' | 'a' .. 'z' | 'A' .. 'Z' )
            {
                match('\\');
                if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= ')')||(input.LA(1) >= '+' && input.LA(1) <= '@')||input.LA(1)=='['||(input.LA(1) >= ']' && input.LA(1) <= '`')||input.LA(1)=='|'||(input.LA(1) >= '\u007F' && input.LA(1) <= '\uFFFF') ) {
                    input.consume();
                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null,input);
                    recover(mse);
                    throw mse;
                }
                skip();
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OTHER"

    // $ANTLR start "PRINTIM"
    public final void mPRINTIM() throws RecognitionException {
        try {
            int _type = PRINTIM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:100:8: ( '\\\\printim' )
            // /Users/green/dev/Rtf.g:100:10: '\\\\printim'
            {
                match("\\printim");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "PRINTIM"

    // $ANTLR start "CELL"
    public final void mCELL() throws RecognitionException {
        try {
            int _type = CELL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:101:5: ( '\\\\cell' )
            // /Users/green/dev/Rtf.g:101:7: '\\\\cell'
            {
                match("\\cell");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CELL"

    // $ANTLR start "ROW"
    public final void mROW() throws RecognitionException {
        try {
            int _type = ROW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:102:4: ( '\\\\row' )
            // /Users/green/dev/Rtf.g:102:6: '\\\\row'
            {
                match("\\row");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ROW"

    // $ANTLR start "INTBL"
    public final void mINTBL() throws RecognitionException {
        try {
            int _type = INTBL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:103:6: ( '\\\\intbl' )
            // /Users/green/dev/Rtf.g:103:8: '\\\\intbl'
            {
                match("\\intbl");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "INTBL"

    // $ANTLR start "LDBLQUOTE"
    public final void mLDBLQUOTE() throws RecognitionException {
        try {
            int _type = LDBLQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:104:10: ( '\\\\ldblquote' )
            // /Users/green/dev/Rtf.g:104:12: '\\\\ldblquote'
            {
                match("\\ldblquote");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LDBLQUOTE"

    // $ANTLR start "RDBLQUOTE"
    public final void mRDBLQUOTE() throws RecognitionException {
        try {
            int _type = RDBLQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:105:10: ( '\\\\rdblquote' )
            // /Users/green/dev/Rtf.g:105:12: '\\\\rdblquote'
            {
                match("\\rdblquote");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "RDBLQUOTE"

    // $ANTLR start "TITLE"
    public final void mTITLE() throws RecognitionException {
        try {
            int _type = TITLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:106:6: ( '\\\\title' )
            // /Users/green/dev/Rtf.g:106:8: '\\\\title'
            {
                match("\\title");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "TITLE"

    // $ANTLR start "TAB"
    public final void mTAB() throws RecognitionException {
        try {
            int _type = TAB;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:107:4: ( '\\\\tab' )
            // /Users/green/dev/Rtf.g:107:6: '\\\\tab'
            {
                match("\\tab");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "TAB"

    // $ANTLR start "ANSI"
    public final void mANSI() throws RecognitionException {
        try {
            int _type = ANSI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:108:5: ( '\\\\ansi' )
            // /Users/green/dev/Rtf.g:108:7: '\\\\ansi'
            {
                match("\\ansi");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ANSI"

    // $ANTLR start "ANSICPG"
    public final void mANSICPG() throws RecognitionException {
        try {
            int _type = ANSICPG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:109:8: ( '\\\\ansicpg' )
            // /Users/green/dev/Rtf.g:109:10: '\\\\ansicpg'
            {
                match("\\ansicpg");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ANSICPG"

    // $ANTLR start "AUTHOR"
    public final void mAUTHOR() throws RecognitionException {
        try {
            int _type = AUTHOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:110:7: ( '\\\\author' )
            // /Users/green/dev/Rtf.g:110:9: '\\\\author'
            {
                match("\\author");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "AUTHOR"

    // $ANTLR start "B"
    public final void mB() throws RecognitionException {
        try {
            int _type = B;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:111:2: ( '\\\\b' )
            // /Users/green/dev/Rtf.g:111:4: '\\\\b'
            {
                match("\\b");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "B"

    // $ANTLR start "BLUE"
    public final void mBLUE() throws RecognitionException {
        try {
            int _type = BLUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:112:5: ( '\\\\blue' )
            // /Users/green/dev/Rtf.g:112:7: '\\\\blue'
            {
                match("\\blue");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "BLUE"

    // $ANTLR start "BULLET"
    public final void mBULLET() throws RecognitionException {
        try {
            int _type = BULLET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:113:7: ( '\\\\bullet' )
            // /Users/green/dev/Rtf.g:113:9: '\\\\bullet'
            {
                match("\\bullet");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "BULLET"

    // $ANTLR start "CF"
    public final void mCF() throws RecognitionException {
        try {
            int _type = CF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:114:3: ( '\\\\cf' )
            // /Users/green/dev/Rtf.g:114:5: '\\\\cf'
            {
                match("\\cf");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CF"

    // $ANTLR start "COLORTBL"
    public final void mCOLORTBL() throws RecognitionException {
        try {
            int _type = COLORTBL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:115:9: ( '\\\\colortbl' )
            // /Users/green/dev/Rtf.g:115:11: '\\\\colortbl'
            {
                match("\\colortbl");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "COLORTBL"

    // $ANTLR start "CREATIM"
    public final void mCREATIM() throws RecognitionException {
        try {
            int _type = CREATIM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:116:8: ( '\\\\creatim' )
            // /Users/green/dev/Rtf.g:116:10: '\\\\creatim'
            {
                match("\\creatim");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CREATIM"

    // $ANTLR start "DEFF"
    public final void mDEFF() throws RecognitionException {
        try {
            int _type = DEFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:117:5: ( '\\\\deff' )
            // /Users/green/dev/Rtf.g:117:7: '\\\\deff'
            {
                match("\\deff");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DEFF"

    // $ANTLR start "DEFLANG"
    public final void mDEFLANG() throws RecognitionException {
        try {
            int _type = DEFLANG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:118:8: ( '\\\\deflang' )
            // /Users/green/dev/Rtf.g:118:10: '\\\\deflang'
            {
                match("\\deflang");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DEFLANG"

    // $ANTLR start "DEFLANGFE"
    public final void mDEFLANGFE() throws RecognitionException {
        try {
            int _type = DEFLANGFE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:119:10: ( '\\\\deflangfe' )
            // /Users/green/dev/Rtf.g:119:12: '\\\\deflangfe'
            {
                match("\\deflangfe");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DEFLANGFE"

    // $ANTLR start "DEFTAB"
    public final void mDEFTAB() throws RecognitionException {
        try {
            int _type = DEFTAB;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:120:7: ( '\\\\deftab' )
            // /Users/green/dev/Rtf.g:120:9: '\\\\deftab'
            {
                match("\\deftab");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DEFTAB"

    // $ANTLR start "DY"
    public final void mDY() throws RecognitionException {
        try {
            int _type = DY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:121:3: ( '\\\\dy' )
            // /Users/green/dev/Rtf.g:121:5: '\\\\dy'
            {
                match("\\dy");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DY"

    // $ANTLR start "EMDASH"
    public final void mEMDASH() throws RecognitionException {
        try {
            int _type = EMDASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:122:7: ( '\\\\emdash' )
            // /Users/green/dev/Rtf.g:122:9: '\\\\emdash'
            {
                match("\\emdash");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "EMDASH"

    // $ANTLR start "ENDASH"
    public final void mENDASH() throws RecognitionException {
        try {
            int _type = ENDASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:123:7: ( '\\\\endash' )
            // /Users/green/dev/Rtf.g:123:9: '\\\\endash'
            {
                match("\\endash");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ENDASH"

    // $ANTLR start "F"
    public final void mF() throws RecognitionException {
        try {
            int _type = F;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:124:2: ( '\\\\f' )
            // /Users/green/dev/Rtf.g:124:4: '\\\\f'
            {
                match("\\f");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "F"

    // $ANTLR start "FALT"
    public final void mFALT() throws RecognitionException {
        try {
            int _type = FALT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:125:5: ( '\\\\falt' )
            // /Users/green/dev/Rtf.g:125:7: '\\\\falt'
            {
                match("\\falt");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FALT"

    // $ANTLR start "FBIDI"
    public final void mFBIDI() throws RecognitionException {
        try {
            int _type = FBIDI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:126:6: ( '\\\\fbidi' )
            // /Users/green/dev/Rtf.g:126:8: '\\\\fbidi'
            {
                match("\\fbidi");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FBIDI"

    // $ANTLR start "FCHARSET"
    public final void mFCHARSET() throws RecognitionException {
        try {
            int _type = FCHARSET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:127:9: ( '\\\\fcharset' )
            // /Users/green/dev/Rtf.g:127:11: '\\\\fcharset'
            {
                match("\\fcharset");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FCHARSET"

    // $ANTLR start "FDECOR"
    public final void mFDECOR() throws RecognitionException {
        try {
            int _type = FDECOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:128:7: ( '\\\\fdecor' )
            // /Users/green/dev/Rtf.g:128:9: '\\\\fdecor'
            {
                match("\\fdecor");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FDECOR"

    // $ANTLR start "FI"
    public final void mFI() throws RecognitionException {
        try {
            int _type = FI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:129:3: ( '\\\\fi' )
            // /Users/green/dev/Rtf.g:129:5: '\\\\fi'
            {
                match("\\fi");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FI"

    // $ANTLR start "FMODERN"
    public final void mFMODERN() throws RecognitionException {
        try {
            int _type = FMODERN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:130:8: ( '\\\\fmodern' )
            // /Users/green/dev/Rtf.g:130:10: '\\\\fmodern'
            {
                match("\\fmodern");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FMODERN"

    // $ANTLR start "FNAME"
    public final void mFNAME() throws RecognitionException {
        try {
            int _type = FNAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:131:6: ( '\\\\fname' )
            // /Users/green/dev/Rtf.g:131:8: '\\\\fname'
            {
                match("\\fname");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FNAME"

    // $ANTLR start "FNIL"
    public final void mFNIL() throws RecognitionException {
        try {
            int _type = FNIL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:132:5: ( '\\\\fnil' )
            // /Users/green/dev/Rtf.g:132:7: '\\\\fnil'
            {
                match("\\fnil");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FNIL"

    // $ANTLR start "FONTTBL"
    public final void mFONTTBL() throws RecognitionException {
        try {
            int _type = FONTTBL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:133:8: ( '\\\\fonttbl' )
            // /Users/green/dev/Rtf.g:133:10: '\\\\fonttbl'
            {
                match("\\fonttbl");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FONTTBL"

    // $ANTLR start "FPRQ"
    public final void mFPRQ() throws RecognitionException {
        try {
            int _type = FPRQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:134:5: ( '\\\\fprq' )
            // /Users/green/dev/Rtf.g:134:7: '\\\\fprq'
            {
                match("\\fprq");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FPRQ"

    // $ANTLR start "FROMAN"
    public final void mFROMAN() throws RecognitionException {
        try {
            int _type = FROMAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:135:7: ( '\\\\froman' )
            // /Users/green/dev/Rtf.g:135:9: '\\\\froman'
            {
                match("\\froman");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FROMAN"

    // $ANTLR start "FS"
    public final void mFS() throws RecognitionException {
        try {
            int _type = FS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:136:3: ( '\\\\fs' )
            // /Users/green/dev/Rtf.g:136:5: '\\\\fs'
            {
                match("\\fs");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FS"

    // $ANTLR start "FSCRIPT"
    public final void mFSCRIPT() throws RecognitionException {
        try {
            int _type = FSCRIPT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:137:8: ( '\\\\fscript' )
            // /Users/green/dev/Rtf.g:137:10: '\\\\fscript'
            {
                match("\\fscript");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FSCRIPT"

    // $ANTLR start "FSWISS"
    public final void mFSWISS() throws RecognitionException {
        try {
            int _type = FSWISS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:138:7: ( '\\\\fswiss' )
            // /Users/green/dev/Rtf.g:138:9: '\\\\fswiss'
            {
                match("\\fswiss");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FSWISS"

    // $ANTLR start "FTECH"
    public final void mFTECH() throws RecognitionException {
        try {
            int _type = FTECH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:139:6: ( '\\\\ftech' )
            // /Users/green/dev/Rtf.g:139:8: '\\\\ftech'
            {
                match("\\ftech");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FTECH"

    // $ANTLR start "GENERATOR"
    public final void mGENERATOR() throws RecognitionException {
        try {
            int _type = GENERATOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:140:10: ( '\\\\generator' )
            // /Users/green/dev/Rtf.g:140:12: '\\\\generator'
            {
                match("\\generator");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "GENERATOR"

    // $ANTLR start "GREEN"
    public final void mGREEN() throws RecognitionException {
        try {
            int _type = GREEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:141:6: ( '\\\\green' )
            // /Users/green/dev/Rtf.g:141:8: '\\\\green'
            {
                match("\\green");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "GREEN"

    // $ANTLR start "HEADER"
    public final void mHEADER() throws RecognitionException {
        try {
            int _type = HEADER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:142:7: ( '\\\\header' )
            // /Users/green/dev/Rtf.g:142:9: '\\\\header'
            {
                match("\\header");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "HEADER"

    // $ANTLR start "HR"
    public final void mHR() throws RecognitionException {
        try {
            int _type = HR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:143:3: ( '\\\\hr' )
            // /Users/green/dev/Rtf.g:143:5: '\\\\hr'
            {
                match("\\hr");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "HR"

    // $ANTLR start "I"
    public final void mI() throws RecognitionException {
        try {
            int _type = I;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:144:2: ( '\\\\i' )
            // /Users/green/dev/Rtf.g:144:4: '\\\\i'
            {
                match("\\i");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "I"

    // $ANTLR start "INFO"
    public final void mINFO() throws RecognitionException {
        try {
            int _type = INFO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:145:5: ( '\\\\info' )
            // /Users/green/dev/Rtf.g:145:7: '\\\\info'
            {
                match("\\info");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "INFO"

    // $ANTLR start "LANG"
    public final void mLANG() throws RecognitionException {
        try {
            int _type = LANG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:146:5: ( '\\\\lang' )
            // /Users/green/dev/Rtf.g:146:7: '\\\\lang'
            {
                match("\\lang");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LANG"

    // $ANTLR start "LI"
    public final void mLI() throws RecognitionException {
        try {
            int _type = LI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:147:3: ( '\\\\li' )
            // /Users/green/dev/Rtf.g:147:5: '\\\\li'
            {
                match("\\li");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LI"

    // $ANTLR start "LINE"
    public final void mLINE() throws RecognitionException {
        try {
            int _type = LINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:149:5: ( '\\\\line' )
            // /Users/green/dev/Rtf.g:149:7: '\\\\line'
            {
                match("\\line");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LINE"

    // $ANTLR start "MIN"
    public final void mMIN() throws RecognitionException {
        try {
            int _type = MIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:150:4: ( '\\\\min' )
            // /Users/green/dev/Rtf.g:150:6: '\\\\min'
            {
                match("\\min");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "MIN"

    // $ANTLR start "MO"
    public final void mMO() throws RecognitionException {
        try {
            int _type = MO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:151:3: ( '\\\\mo' )
            // /Users/green/dev/Rtf.g:151:5: '\\\\mo'
            {
                match("\\mo");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "MO"

    // $ANTLR start "OPERATOR"
    public final void mOPERATOR() throws RecognitionException {
        try {
            int _type = OPERATOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:152:9: ( '\\\\operator' )
            // /Users/green/dev/Rtf.g:152:11: '\\\\operator'
            {
                match("\\operator");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OPERATOR"

    // $ANTLR start "PAR"
    public final void mPAR() throws RecognitionException {
        try {
            int _type = PAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:153:4: ( ( '\\\\par' | '\\\\\\n' | '\\\\\\r' ) )
            // /Users/green/dev/Rtf.g:153:6: ( '\\\\par' | '\\\\\\n' | '\\\\\\r' )
            {
                // /Users/green/dev/Rtf.g:153:6: ( '\\\\par' | '\\\\\\n' | '\\\\\\r' )
                int alt1=3;
                int LA1_0 = input.LA(1);
                if ( (LA1_0=='\\') ) {
                    switch ( input.LA(2) ) {
                        case 'p':
                        {
                            alt1=1;
                        }
                        break;
                        case '\n':
                        {
                            alt1=2;
                        }
                        break;
                        case '\r':
                        {
                            alt1=3;
                        }
                        break;
                        default:
                            int nvaeMark = input.mark();
                            try {
                                input.consume();
                                NoViableAltException nvae =
                                        new NoViableAltException("", 1, 1, input);
                                throw nvae;
                            } finally {
                                input.rewind(nvaeMark);
                            }
                    }
                }

                else {
                    NoViableAltException nvae =
                            new NoViableAltException("", 1, 0, input);
                    throw nvae;
                }

                switch (alt1) {
                    case 1 :
                        // /Users/green/dev/Rtf.g:153:7: '\\\\par'
                    {
                        match("\\par");

                    }
                    break;
                    case 2 :
                        // /Users/green/dev/Rtf.g:153:17: '\\\\\\n'
                    {
                        match("\\\n");

                    }
                    break;
                    case 3 :
                        // /Users/green/dev/Rtf.g:153:26: '\\\\\\r'
                    {
                        match("\\\r");

                    }
                    break;

                }

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "PAR"

    // $ANTLR start "PARD"
    public final void mPARD() throws RecognitionException {
        try {
            int _type = PARD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:154:5: ( '\\\\pard' )
            // /Users/green/dev/Rtf.g:154:7: '\\\\pard'
            {
                match("\\pard");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "PARD"

    // $ANTLR start "PLAIN"
    public final void mPLAIN() throws RecognitionException {
        try {
            int _type = PLAIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:155:6: ( '\\\\plain' )
            // /Users/green/dev/Rtf.g:155:8: '\\\\plain'
            {
                match("\\plain");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "PLAIN"

    // $ANTLR start "PNSTART"
    public final void mPNSTART() throws RecognitionException {
        try {
            int _type = PNSTART;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:156:8: ( '\\\\pnstart' )
            // /Users/green/dev/Rtf.g:156:10: '\\\\pnstart'
            {
                match("\\pnstart");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "PNSTART"

    // $ANTLR start "QC"
    public final void mQC() throws RecognitionException {
        try {
            int _type = QC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:157:3: ( '\\\\qc' )
            // /Users/green/dev/Rtf.g:157:5: '\\\\qc'
            {
                match("\\qc");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "QC"

    // $ANTLR start "QJ"
    public final void mQJ() throws RecognitionException {
        try {
            int _type = QJ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:158:3: ( '\\\\qj' )
            // /Users/green/dev/Rtf.g:158:5: '\\\\qj'
            {
                match("\\qj");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "QJ"

    // $ANTLR start "RED"
    public final void mRED() throws RecognitionException {
        try {
            int _type = RED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:159:4: ( '\\\\red' )
            // /Users/green/dev/Rtf.g:159:6: '\\\\red'
            {
                match("\\red");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "RED"

    // $ANTLR start "REVTIM"
    public final void mREVTIM() throws RecognitionException {
        try {
            int _type = REVTIM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:160:7: ( '\\\\revtim' )
            // /Users/green/dev/Rtf.g:160:9: '\\\\revtim'
            {
                match("\\revtim");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "REVTIM"

    // $ANTLR start "RQUOTE"
    public final void mRQUOTE() throws RecognitionException {
        try {
            int _type = RQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:161:7: ( '\\\\rquote' )
            // /Users/green/dev/Rtf.g:161:9: '\\\\rquote'
            {
                match("\\rquote");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "RQUOTE"

    // $ANTLR start "RTF"
    public final void mRTF() throws RecognitionException {
        try {
            int _type = RTF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:162:4: ( '\\\\rtf' )
            // /Users/green/dev/Rtf.g:162:6: '\\\\rtf'
            {
                match("\\rtf");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "RTF"

    // $ANTLR start "SEC"
    public final void mSEC() throws RecognitionException {
        try {
            int _type = SEC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:163:4: ( '\\\\sec' )
            // /Users/green/dev/Rtf.g:163:6: '\\\\sec'
            {
                match("\\sec");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "SEC"

    // $ANTLR start "STYLESHEET"
    public final void mSTYLESHEET() throws RecognitionException {
        try {
            int _type = STYLESHEET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:164:11: ( '\\\\stylesheet' )
            // /Users/green/dev/Rtf.g:164:13: '\\\\stylesheet'
            {
                match("\\stylesheet");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "STYLESHEET"

    // $ANTLR start "UC"
    public final void mUC() throws RecognitionException {
        try {
            int _type = UC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:165:3: ( '\\\\uc' )
            // /Users/green/dev/Rtf.g:165:5: '\\\\uc'
            {
                match("\\uc");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "UC"

    // $ANTLR start "YR"
    public final void mYR() throws RecognitionException {
        try {
            int _type = YR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:166:3: ( '\\\\yr' )
            // /Users/green/dev/Rtf.g:166:5: '\\\\yr'
            {
                match("\\yr");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "YR"

    // $ANTLR start "MAC"
    public final void mMAC() throws RecognitionException {
        try {
            int _type = MAC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:167:4: ( '\\\\mac' )
            // /Users/green/dev/Rtf.g:167:6: '\\\\mac'
            {
                match("\\mac");

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "MAC"

    // $ANTLR start "CONTROL"
    public final void mCONTROL() throws RecognitionException {
        try {
            int _type = CONTROL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:170:8: ( '\\\\' ( 'a' .. 'z' | 'A' .. 'Z' )+ )
            // /Users/green/dev/Rtf.g:170:10: '\\\\' ( 'a' .. 'z' | 'A' .. 'Z' )+
            {
                match('\\');
                // /Users/green/dev/Rtf.g:170:15: ( 'a' .. 'z' | 'A' .. 'Z' )+
                int cnt2=0;
                loop2:
                while (true) {
                    int alt2=2;
                    int LA2_0 = input.LA(1);
                    if ( ((LA2_0 >= 'A' && LA2_0 <= 'Z')||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
                        alt2=1;
                    }

                    switch (alt2) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:
                        {
                            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }
                        }
                        break;

                        default :
                            if ( cnt2 >= 1 ) break loop2;
                            EarlyExitException eee = new EarlyExitException(2, input);
                            throw eee;
                    }
                    cnt2++;
                }

                afterControl = true;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CONTROL"

    // $ANTLR start "NUMBER"
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:172:7: ({...}? => ( '-' )? ( '0' .. '9' )+ )
            // /Users/green/dev/Rtf.g:172:9: {...}? => ( '-' )? ( '0' .. '9' )+
            {
                if ( !((afterControl)) ) {
                    throw new FailedPredicateException(input, "NUMBER", "afterControl");
                }
                // /Users/green/dev/Rtf.g:172:28: ( '-' )?
                int alt3=2;
                int LA3_0 = input.LA(1);
                if ( (LA3_0=='-') ) {
                    alt3=1;
                }
                switch (alt3) {
                    case 1 :
                        // /Users/green/dev/Rtf.g:172:28: '-'
                    {
                        match('-');
                    }
                    break;

                }

                // /Users/green/dev/Rtf.g:172:33: ( '0' .. '9' )+
                int cnt4=0;
                loop4:
                while (true) {
                    int alt4=2;
                    int LA4_0 = input.LA(1);
                    if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
                        alt4=1;
                    }

                    switch (alt4) {
                        case 1 :
                            // /Users/green/dev/Rtf.g:
                        {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }
                        }
                        break;

                        default :
                            if ( cnt4 >= 1 ) break loop4;
                            EarlyExitException eee = new EarlyExitException(4, input);
                            throw eee;
                    }
                    cnt4++;
                }

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:173:3: ({...}? => ' ' )
            // /Users/green/dev/Rtf.g:173:5: {...}? => ' '
            {
                if ( !((afterControl)) ) {
                    throw new FailedPredicateException(input, "WS", "afterControl");
                }
                match(' ');
                skip(); afterControl = false;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "NEWLINE"
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:174:8: ( ( '\\n' | '\\r' ) )
            // /Users/green/dev/Rtf.g:174:10: ( '\\n' | '\\r' )
            {
                if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
                    input.consume();
                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null,input);
                    recover(mse);
                    throw mse;
                }
                skip(); afterControl = false;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "NEWLINE"

    // $ANTLR start "HEX"
    public final void mHEX() throws RecognitionException {
        try {
            // /Users/green/dev/Rtf.g:176:13: ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )
            // /Users/green/dev/Rtf.g:
            {
                if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
                    input.consume();
                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null,input);
                    recover(mse);
                    throw mse;
                }
            }

        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "HEX"

    // $ANTLR start "HEXCHAR"
    public final void mHEXCHAR() throws RecognitionException {
        try {
            int _type = HEXCHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:177:8: ( '\\\\' '\\'' HEX HEX )
            // /Users/green/dev/Rtf.g:177:10: '\\\\' '\\'' HEX HEX
            {
                match('\\');
                match('\'');
                mHEX();

                mHEX();

                afterControl = false;
            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "HEXCHAR"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/green/dev/Rtf.g:179:5: ({...}? => (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )+ |~ ( ' ' | '0' .. '9' | '-' | '\\\\' | '{' | '}' | '\\n' | '\\r' ) (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )* )
            int alt7=2;
            int LA7_0 = input.LA(1);
            if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\t')||(LA7_0 >= '\u000B' && LA7_0 <= '\f')||(LA7_0 >= '\u000E' && LA7_0 <= '\u001F')||(LA7_0 >= '!' && LA7_0 <= ',')||(LA7_0 >= '.' && LA7_0 <= '/')||(LA7_0 >= ':' && LA7_0 <= '[')||(LA7_0 >= ']' && LA7_0 <= 'z')||LA7_0=='|'||(LA7_0 >= '~' && LA7_0 <= '\uFFFF')) ) {
                int LA7_1 = input.LA(2);
                if ( ((!afterControl)) ) {
                    alt7=1;
                }
                else if ( (true) ) {
                    alt7=2;
                }

            }
            else if ( (LA7_0==' '||LA7_0=='-'||(LA7_0 >= '0' && LA7_0 <= '9')) && ((!afterControl))) {
                alt7=1;
            }

            switch (alt7) {
                case 1 :
                    // /Users/green/dev/Rtf.g:180:2: {...}? => (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )+
                {
                    if ( !((!afterControl)) ) {
                        throw new FailedPredicateException(input, "TEXT", "!afterControl");
                    }
                    // /Users/green/dev/Rtf.g:180:22: (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )+
                    int cnt5=0;
                    loop5:
                    while (true) {
                        int alt5=2;
                        int LA5_0 = input.LA(1);
                        if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\t')||(LA5_0 >= '\u000B' && LA5_0 <= '\f')||(LA5_0 >= '\u000E' && LA5_0 <= '[')||(LA5_0 >= ']' && LA5_0 <= 'z')||LA5_0=='|'||(LA5_0 >= '~' && LA5_0 <= '\uFFFF')) ) {
                            alt5=1;
                        }

                        switch (alt5) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:
                            {
                                if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= 'z')||input.LA(1)=='|'||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
                                    input.consume();
                                }
                                else {
                                    MismatchedSetException mse = new MismatchedSetException(null,input);
                                    recover(mse);
                                    throw mse;
                                }
                            }
                            break;

                            default :
                                if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee = new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    }

                }
                break;
                case 2 :
                    // /Users/green/dev/Rtf.g:181:2: ~ ( ' ' | '0' .. '9' | '-' | '\\\\' | '{' | '}' | '\\n' | '\\r' ) (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )*
                {
                    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F')||(input.LA(1) >= '!' && input.LA(1) <= ',')||(input.LA(1) >= '.' && input.LA(1) <= '/')||(input.LA(1) >= ':' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= 'z')||input.LA(1)=='|'||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }
                    // /Users/green/dev/Rtf.g:181:59: (~ ( '\\\\' | '{' | '}' | '\\n' | '\\r' ) )*
                    loop6:
                    while (true) {
                        int alt6=2;
                        int LA6_0 = input.LA(1);
                        if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\t')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '[')||(LA6_0 >= ']' && LA6_0 <= 'z')||LA6_0=='|'||(LA6_0 >= '~' && LA6_0 <= '\uFFFF')) ) {
                            alt6=1;
                        }

                        switch (alt6) {
                            case 1 :
                                // /Users/green/dev/Rtf.g:
                            {
                                if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= 'z')||input.LA(1)=='|'||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
                                    input.consume();
                                }
                                else {
                                    MismatchedSetException mse = new MismatchedSetException(null,input);
                                    recover(mse);
                                    throw mse;
                                }
                            }
                            break;

                            default :
                                break loop6;
                        }
                    }

                    afterControl = false;
                }
                break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    @Override
    public void mTokens() throws RecognitionException {
        // /Users/green/dev/Rtf.g:1:8: ( T__85 | T__86 | SLASH | STAR | OPENBRACE | CLOSEBRACE | NBSP | OTHER | PRINTIM | CELL | ROW | INTBL | LDBLQUOTE | RDBLQUOTE | TITLE | TAB | ANSI | ANSICPG | AUTHOR | B | BLUE | BULLET | CF | COLORTBL | CREATIM | DEFF | DEFLANG | DEFLANGFE | DEFTAB | DY | EMDASH | ENDASH | F | FALT | FBIDI | FCHARSET | FDECOR | FI | FMODERN | FNAME | FNIL | FONTTBL | FPRQ | FROMAN | FS | FSCRIPT | FSWISS | FTECH | GENERATOR | GREEN | HEADER | HR | I | INFO | LANG | LI | LINE | MIN | MO | OPERATOR | PAR | PARD | PLAIN | PNSTART | QC | QJ | RED | REVTIM | RQUOTE | RTF | SEC | STYLESHEET | UC | YR | MAC | CONTROL | NUMBER | WS | NEWLINE | HEXCHAR | TEXT )
        int alt8=81;
        alt8 = dfa8.predict(input);
        switch (alt8) {
            case 1 :
                // /Users/green/dev/Rtf.g:1:10: T__85
            {
                mT__85();

            }
            break;
            case 2 :
                // /Users/green/dev/Rtf.g:1:16: T__86
            {
                mT__86();

            }
            break;
            case 3 :
                // /Users/green/dev/Rtf.g:1:22: SLASH
            {
                mSLASH();

            }
            break;
            case 4 :
                // /Users/green/dev/Rtf.g:1:28: STAR
            {
                mSTAR();

            }
            break;
            case 5 :
                // /Users/green/dev/Rtf.g:1:33: OPENBRACE
            {
                mOPENBRACE();

            }
            break;
            case 6 :
                // /Users/green/dev/Rtf.g:1:43: CLOSEBRACE
            {
                mCLOSEBRACE();

            }
            break;
            case 7 :
                // /Users/green/dev/Rtf.g:1:54: NBSP
            {
                mNBSP();

            }
            break;
            case 8 :
                // /Users/green/dev/Rtf.g:1:59: OTHER
            {
                mOTHER();

            }
            break;
            case 9 :
                // /Users/green/dev/Rtf.g:1:65: PRINTIM
            {
                mPRINTIM();

            }
            break;
            case 10 :
                // /Users/green/dev/Rtf.g:1:73: CELL
            {
                mCELL();

            }
            break;
            case 11 :
                // /Users/green/dev/Rtf.g:1:78: ROW
            {
                mROW();

            }
            break;
            case 12 :
                // /Users/green/dev/Rtf.g:1:82: INTBL
            {
                mINTBL();

            }
            break;
            case 13 :
                // /Users/green/dev/Rtf.g:1:88: LDBLQUOTE
            {
                mLDBLQUOTE();

            }
            break;
            case 14 :
                // /Users/green/dev/Rtf.g:1:98: RDBLQUOTE
            {
                mRDBLQUOTE();

            }
            break;
            case 15 :
                // /Users/green/dev/Rtf.g:1:108: TITLE
            {
                mTITLE();

            }
            break;
            case 16 :
                // /Users/green/dev/Rtf.g:1:114: TAB
            {
                mTAB();

            }
            break;
            case 17 :
                // /Users/green/dev/Rtf.g:1:118: ANSI
            {
                mANSI();

            }
            break;
            case 18 :
                // /Users/green/dev/Rtf.g:1:123: ANSICPG
            {
                mANSICPG();

            }
            break;
            case 19 :
                // /Users/green/dev/Rtf.g:1:131: AUTHOR
            {
                mAUTHOR();

            }
            break;
            case 20 :
                // /Users/green/dev/Rtf.g:1:138: B
            {
                mB();

            }
            break;
            case 21 :
                // /Users/green/dev/Rtf.g:1:140: BLUE
            {
                mBLUE();

            }
            break;
            case 22 :
                // /Users/green/dev/Rtf.g:1:145: BULLET
            {
                mBULLET();

            }
            break;
            case 23 :
                // /Users/green/dev/Rtf.g:1:152: CF
            {
                mCF();

            }
            break;
            case 24 :
                // /Users/green/dev/Rtf.g:1:155: COLORTBL
            {
                mCOLORTBL();

            }
            break;
            case 25 :
                // /Users/green/dev/Rtf.g:1:164: CREATIM
            {
                mCREATIM();

            }
            break;
            case 26 :
                // /Users/green/dev/Rtf.g:1:172: DEFF
            {
                mDEFF();

            }
            break;
            case 27 :
                // /Users/green/dev/Rtf.g:1:177: DEFLANG
            {
                mDEFLANG();

            }
            break;
            case 28 :
                // /Users/green/dev/Rtf.g:1:185: DEFLANGFE
            {
                mDEFLANGFE();

            }
            break;
            case 29 :
                // /Users/green/dev/Rtf.g:1:195: DEFTAB
            {
                mDEFTAB();

            }
            break;
            case 30 :
                // /Users/green/dev/Rtf.g:1:202: DY
            {
                mDY();

            }
            break;
            case 31 :
                // /Users/green/dev/Rtf.g:1:205: EMDASH
            {
                mEMDASH();

            }
            break;
            case 32 :
                // /Users/green/dev/Rtf.g:1:212: ENDASH
            {
                mENDASH();

            }
            break;
            case 33 :
                // /Users/green/dev/Rtf.g:1:219: F
            {
                mF();

            }
            break;
            case 34 :
                // /Users/green/dev/Rtf.g:1:221: FALT
            {
                mFALT();

            }
            break;
            case 35 :
                // /Users/green/dev/Rtf.g:1:226: FBIDI
            {
                mFBIDI();

            }
            break;
            case 36 :
                // /Users/green/dev/Rtf.g:1:232: FCHARSET
            {
                mFCHARSET();

            }
            break;
            case 37 :
                // /Users/green/dev/Rtf.g:1:241: FDECOR
            {
                mFDECOR();

            }
            break;
            case 38 :
                // /Users/green/dev/Rtf.g:1:248: FI
            {
                mFI();

            }
            break;
            case 39 :
                // /Users/green/dev/Rtf.g:1:251: FMODERN
            {
                mFMODERN();

            }
            break;
            case 40 :
                // /Users/green/dev/Rtf.g:1:259: FNAME
            {
                mFNAME();

            }
            break;
            case 41 :
                // /Users/green/dev/Rtf.g:1:265: FNIL
            {
                mFNIL();

            }
            break;
            case 42 :
                // /Users/green/dev/Rtf.g:1:270: FONTTBL
            {
                mFONTTBL();

            }
            break;
            case 43 :
                // /Users/green/dev/Rtf.g:1:278: FPRQ
            {
                mFPRQ();

            }
            break;
            case 44 :
                // /Users/green/dev/Rtf.g:1:283: FROMAN
            {
                mFROMAN();

            }
            break;
            case 45 :
                // /Users/green/dev/Rtf.g:1:290: FS
            {
                mFS();

            }
            break;
            case 46 :
                // /Users/green/dev/Rtf.g:1:293: FSCRIPT
            {
                mFSCRIPT();

            }
            break;
            case 47 :
                // /Users/green/dev/Rtf.g:1:301: FSWISS
            {
                mFSWISS();

            }
            break;
            case 48 :
                // /Users/green/dev/Rtf.g:1:308: FTECH
            {
                mFTECH();

            }
            break;
            case 49 :
                // /Users/green/dev/Rtf.g:1:314: GENERATOR
            {
                mGENERATOR();

            }
            break;
            case 50 :
                // /Users/green/dev/Rtf.g:1:324: GREEN
            {
                mGREEN();

            }
            break;
            case 51 :
                // /Users/green/dev/Rtf.g:1:330: HEADER
            {
                mHEADER();

            }
            break;
            case 52 :
                // /Users/green/dev/Rtf.g:1:337: HR
            {
                mHR();

            }
            break;
            case 53 :
                // /Users/green/dev/Rtf.g:1:340: I
            {
                mI();

            }
            break;
            case 54 :
                // /Users/green/dev/Rtf.g:1:342: INFO
            {
                mINFO();

            }
            break;
            case 55 :
                // /Users/green/dev/Rtf.g:1:347: LANG
            {
                mLANG();

            }
            break;
            case 56 :
                // /Users/green/dev/Rtf.g:1:352: LI
            {
                mLI();

            }
            break;
            case 57 :
                // /Users/green/dev/Rtf.g:1:355: LINE
            {
                mLINE();

            }
            break;
            case 58 :
                // /Users/green/dev/Rtf.g:1:360: MIN
            {
                mMIN();

            }
            break;
            case 59 :
                // /Users/green/dev/Rtf.g:1:364: MO
            {
                mMO();

            }
            break;
            case 60 :
                // /Users/green/dev/Rtf.g:1:367: OPERATOR
            {
                mOPERATOR();

            }
            break;
            case 61 :
                // /Users/green/dev/Rtf.g:1:376: PAR
            {
                mPAR();

            }
            break;
            case 62 :
                // /Users/green/dev/Rtf.g:1:380: PARD
            {
                mPARD();

            }
            break;
            case 63 :
                // /Users/green/dev/Rtf.g:1:385: PLAIN
            {
                mPLAIN();

            }
            break;
            case 64 :
                // /Users/green/dev/Rtf.g:1:391: PNSTART
            {
                mPNSTART();

            }
            break;
            case 65 :
                // /Users/green/dev/Rtf.g:1:399: QC
            {
                mQC();

            }
            break;
            case 66 :
                // /Users/green/dev/Rtf.g:1:402: QJ
            {
                mQJ();

            }
            break;
            case 67 :
                // /Users/green/dev/Rtf.g:1:405: RED
            {
                mRED();

            }
            break;
            case 68 :
                // /Users/green/dev/Rtf.g:1:409: REVTIM
            {
                mREVTIM();

            }
            break;
            case 69 :
                // /Users/green/dev/Rtf.g:1:416: RQUOTE
            {
                mRQUOTE();

            }
            break;
            case 70 :
                // /Users/green/dev/Rtf.g:1:423: RTF
            {
                mRTF();

            }
            break;
            case 71 :
                // /Users/green/dev/Rtf.g:1:427: SEC
            {
                mSEC();

            }
            break;
            case 72 :
                // /Users/green/dev/Rtf.g:1:431: STYLESHEET
            {
                mSTYLESHEET();

            }
            break;
            case 73 :
                // /Users/green/dev/Rtf.g:1:442: UC
            {
                mUC();

            }
            break;
            case 74 :
                // /Users/green/dev/Rtf.g:1:445: YR
            {
                mYR();

            }
            break;
            case 75 :
                // /Users/green/dev/Rtf.g:1:448: MAC
            {
                mMAC();

            }
            break;
            case 76 :
                // /Users/green/dev/Rtf.g:1:452: CONTROL
            {
                mCONTROL();

            }
            break;
            case 77 :
                // /Users/green/dev/Rtf.g:1:460: NUMBER
            {
                mNUMBER();

            }
            break;
            case 78 :
                // /Users/green/dev/Rtf.g:1:467: WS
            {
                mWS();

            }
            break;
            case 79 :
                // /Users/green/dev/Rtf.g:1:470: NEWLINE
            {
                mNEWLINE();

            }
            break;
            case 80 :
                // /Users/green/dev/Rtf.g:1:478: HEXCHAR
            {
                mHEXCHAR();

            }
            break;
            case 81 :
                // /Users/green/dev/Rtf.g:1:486: TEXT
            {
                mTEXT();

            }
            break;

        }
    }


    protected DFA8 dfa8 = new DFA8(this);
    static final String DFA8_eotS =
            "\4\uffff\1\45\1\46\1\47\10\uffff\3\44\1\66\3\44\1\100\2\44\1\121\4\44"+
                    "\1\uffff\4\44\5\uffff\5\44\1\147\10\44\1\uffff\2\44\1\165\6\44\1\uffff"+
                    "\1\44\1\175\6\44\1\u0084\5\44\1\u008d\1\44\1\uffff\3\44\1\u0092\1\44\1"+
                    "\u0094\2\44\1\u0097\1\u0098\2\44\1\u009b\1\u009c\2\uffff\1\44\1\36\3\44"+
                    "\1\uffff\2\44\1\u00a4\1\44\1\u00a6\2\44\1\u00a9\5\44\1\uffff\1\44\1\u00b0"+
                    "\5\44\1\uffff\6\44\1\uffff\10\44\1\uffff\4\44\1\uffff\1\u00ca\1\uffff"+
                    "\1\u00cb\1\44\2\uffff\1\u00cd\1\44\2\uffff\1\44\1\u00d0\2\44\1\u00d3\2"+
                    "\44\1\uffff\1\44\1\uffff\2\44\1\uffff\1\44\1\u00da\1\44\1\u00dc\1\u00dd"+
                    "\1\44\1\uffff\1\u00e0\1\44\1\u00e2\1\44\1\u00e4\4\44\1\u00e9\5\44\1\u00ef"+
                    "\1\44\1\u00f1\7\44\2\uffff\1\44\1\uffff\2\44\1\uffff\1\u00fc\1\44\1\uffff"+
                    "\5\44\1\u0103\1\uffff\1\44\2\uffff\1\u0105\1\44\1\uffff\1\44\1\uffff\1"+
                    "\44\1\uffff\4\44\1\uffff\1\u010d\3\44\1\u0111\1\uffff\1\44\1\uffff\3\44"+
                    "\1\u0116\1\44\1\u0118\4\44\1\uffff\4\44\1\u0121\1\u0122\1\uffff\1\44\1"+
                    "\uffff\1\44\1\u0125\1\u0126\1\44\1\u0128\1\u0129\1\u012a\1\uffff\1\44"+
                    "\1\u012c\1\44\1\uffff\1\44\1\u012f\1\44\1\u0131\1\uffff\1\44\1\uffff\1"+
                    "\u0133\2\44\1\u0136\1\u0137\1\44\1\u0139\1\44\2\uffff\1\44\1\u013c\2\uffff"+
                    "\1\u013e\3\uffff\1\44\1\uffff\1\u0140\1\u0141\1\uffff\1\u0142\1\uffff"+
                    "\1\44\1\uffff\2\44\2\uffff\1\u0146\1\uffff\2\44\1\uffff\1\44\1\uffff\1"+
                    "\u014a\3\uffff\1\44\1\u014c\1\44\1\uffff\1\u014e\1\u014f\1\u0150\1\uffff"+
                    "\1\u0151\1\uffff\1\44\4\uffff\1\u0153\1\uffff";
    static final String DFA8_eofS =
            "\u0154\uffff";
    static final String DFA8_minS =
            "\1\0\2\uffff\1\0\1\60\2\0\10\uffff\1\141\1\145\1\144\1\101\2\141\1\156"+
                    "\1\101\1\145\1\155\1\101\2\145\1\141\1\160\1\uffff\1\143\1\145\1\143\1"+
                    "\162\3\uffff\2\0\1\151\1\162\1\141\1\163\1\154\1\101\1\154\1\145\1\167"+
                    "\1\142\1\144\1\165\2\146\1\uffff\1\142\1\156\1\101\1\164\1\142\1\163\1"+
                    "\164\1\165\1\154\1\uffff\1\146\1\101\2\144\1\154\1\151\1\150\1\145\1\101"+
                    "\1\157\1\141\1\156\1\162\1\157\1\101\1\145\1\uffff\1\156\1\145\1\141\1"+
                    "\101\1\156\1\101\1\143\1\145\2\101\1\143\1\171\2\101\2\uffff\1\156\1\101"+
                    "\1\151\1\164\1\154\1\uffff\1\157\1\141\1\101\1\154\1\101\1\164\1\157\1"+
                    "\101\1\142\1\157\1\154\1\147\1\145\1\uffff\1\154\1\101\1\151\1\150\1\145"+
                    "\1\154\1\146\1\uffff\2\141\1\164\1\144\1\141\1\143\1\uffff\1\144\1\155"+
                    "\1\154\1\164\1\161\1\155\1\162\1\151\1\uffff\1\143\2\145\1\144\1\uffff"+
                    "\1\101\1\uffff\1\101\1\162\2\uffff\1\101\1\154\2\uffff\1\164\1\101\1\156"+
                    "\1\141\1\101\1\162\1\164\1\uffff\1\161\1\uffff\1\151\1\164\1\uffff\1\154"+
                    "\1\101\1\161\2\101\1\145\1\uffff\1\101\1\157\1\101\1\145\1\101\2\141\2"+
                    "\163\1\101\1\151\1\162\1\157\2\145\1\101\1\164\1\101\1\141\1\151\1\163"+
                    "\1\150\1\162\1\156\1\145\2\uffff\1\141\1\uffff\1\145\1\151\1\uffff\1\101"+
                    "\1\162\1\uffff\1\164\1\151\1\165\1\155\1\145\1\101\1\uffff\1\165\2\uffff"+
                    "\1\101\1\160\1\uffff\1\162\1\uffff\1\164\1\uffff\1\156\1\142\2\150\1\uffff"+
                    "\1\101\1\163\2\162\1\101\1\uffff\1\142\1\uffff\1\156\1\160\1\163\1\101"+
                    "\1\141\1\101\1\162\1\164\1\163\1\155\1\uffff\1\164\1\142\1\155\1\157\2"+
                    "\101\1\uffff\1\157\1\uffff\1\147\2\101\1\147\3\101\1\uffff\1\145\1\101"+
                    "\1\156\1\uffff\1\154\1\101\1\164\1\101\1\uffff\1\164\1\uffff\1\101\1\157"+
                    "\1\150\2\101\1\154\1\101\1\164\2\uffff\1\164\1\101\2\uffff\1\101\3\uffff"+
                    "\1\164\1\uffff\2\101\1\uffff\1\101\1\uffff\1\157\1\uffff\1\162\1\145\2"+
                    "\uffff\1\101\1\uffff\2\145\1\uffff\1\145\1\uffff\1\101\3\uffff\1\162\1"+
                    "\101\1\145\1\uffff\3\101\1\uffff\1\101\1\uffff\1\164\4\uffff\1\101\1\uffff";
    static final String DFA8_maxS =
            "\1\uffff\2\uffff\1\uffff\1\71\2\uffff\10\uffff\2\162\1\164\1\172\2\151"+
                    "\1\165\1\172\1\171\1\156\1\172\2\162\1\157\1\160\1\uffff\1\152\1\164\1"+
                    "\143\1\162\3\uffff\2\0\1\151\1\162\1\141\1\163\1\154\1\172\1\154\1\145"+
                    "\1\167\1\142\1\166\1\165\1\146\1\164\1\uffff\1\142\1\156\1\172\1\164\1"+
                    "\142\1\163\1\164\1\165\1\154\1\uffff\1\146\1\172\2\144\1\154\1\151\1\150"+
                    "\1\145\1\172\1\157\1\151\1\156\1\162\1\157\1\172\1\145\1\uffff\1\156\1"+
                    "\145\1\141\1\172\1\156\1\172\1\143\1\145\2\172\1\143\1\171\2\172\2\uffff"+
                    "\1\156\1\172\1\151\1\164\1\154\1\uffff\1\157\1\141\1\172\1\154\1\172\1"+
                    "\164\1\157\1\172\1\142\1\157\1\154\1\147\1\145\1\uffff\1\154\1\172\1\151"+
                    "\1\150\1\145\1\154\1\164\1\uffff\2\141\1\164\1\144\1\141\1\143\1\uffff"+
                    "\1\144\1\155\1\154\1\164\1\161\1\155\1\162\1\151\1\uffff\1\143\2\145\1"+
                    "\144\1\uffff\1\172\1\uffff\1\172\1\162\2\uffff\1\172\1\154\2\uffff\1\164"+
                    "\1\172\1\156\1\141\1\172\1\162\1\164\1\uffff\1\161\1\uffff\1\151\1\164"+
                    "\1\uffff\1\154\1\172\1\161\2\172\1\145\1\uffff\1\172\1\157\1\172\1\145"+
                    "\1\172\2\141\2\163\1\172\1\151\1\162\1\157\2\145\1\172\1\164\1\172\1\141"+
                    "\1\151\1\163\1\150\1\162\1\156\1\145\2\uffff\1\141\1\uffff\1\145\1\151"+
                    "\1\uffff\1\172\1\162\1\uffff\1\164\1\151\1\165\1\155\1\145\1\172\1\uffff"+
                    "\1\165\2\uffff\1\172\1\160\1\uffff\1\162\1\uffff\1\164\1\uffff\1\156\1"+
                    "\142\2\150\1\uffff\1\172\1\163\2\162\1\172\1\uffff\1\142\1\uffff\1\156"+
                    "\1\160\1\163\1\172\1\141\1\172\1\162\1\164\1\163\1\155\1\uffff\1\164\1"+
                    "\142\1\155\1\157\2\172\1\uffff\1\157\1\uffff\1\147\2\172\1\147\3\172\1"+
                    "\uffff\1\145\1\172\1\156\1\uffff\1\154\1\172\1\164\1\172\1\uffff\1\164"+
                    "\1\uffff\1\172\1\157\1\150\2\172\1\154\1\172\1\164\2\uffff\1\164\1\172"+
                    "\2\uffff\1\172\3\uffff\1\164\1\uffff\2\172\1\uffff\1\172\1\uffff\1\157"+
                    "\1\uffff\1\162\1\145\2\uffff\1\172\1\uffff\2\145\1\uffff\1\145\1\uffff"+
                    "\1\172\3\uffff\1\162\1\172\1\145\1\uffff\3\172\1\uffff\1\172\1\uffff\1"+
                    "\164\4\uffff\1\172\1\uffff";
    static final String DFA8_acceptS =
            "\1\uffff\1\1\1\2\4\uffff\1\117\1\121\1\3\1\4\1\5\1\6\1\7\1\10\17\uffff"+
                    "\1\75\4\uffff\1\120\1\114\1\121\20\uffff\1\65\11\uffff\1\24\20\uffff\1"+
                    "\41\16\uffff\1\115\1\116\5\uffff\1\27\15\uffff\1\70\7\uffff\1\36\6\uffff"+
                    "\1\46\10\uffff\1\55\4\uffff\1\64\1\uffff\1\73\2\uffff\1\101\1\102\2\uffff"+
                    "\1\111\1\112\7\uffff\1\13\1\uffff\1\103\2\uffff\1\106\6\uffff\1\20\31"+
                    "\uffff\1\72\1\113\1\uffff\1\107\2\uffff\1\76\2\uffff\1\12\6\uffff\1\66"+
                    "\1\uffff\1\67\1\71\2\uffff\1\21\1\uffff\1\25\1\uffff\1\32\4\uffff\1\42"+
                    "\5\uffff\1\51\1\uffff\1\53\12\uffff\1\77\6\uffff\1\14\1\uffff\1\17\7\uffff"+
                    "\1\43\3\uffff\1\50\4\uffff\1\60\1\uffff\1\62\10\uffff\1\104\1\105\2\uffff"+
                    "\1\23\1\26\1\uffff\1\35\1\37\1\40\1\uffff\1\45\2\uffff\1\54\1\uffff\1"+
                    "\57\1\uffff\1\63\2\uffff\1\11\1\100\1\uffff\1\31\2\uffff\1\22\1\uffff"+
                    "\1\33\1\uffff\1\47\1\52\1\56\3\uffff\1\30\3\uffff\1\44\1\uffff\1\74\1"+
                    "\uffff\1\16\1\15\1\34\1\61\1\uffff\1\110";
    static final String DFA8_specialS =
            "\1\6\2\uffff\1\5\1\4\1\3\1\0\37\uffff\1\2\1\1\u012c\uffff}>";
    static final String[] DFA8_transitionS = {
            "\12\10\1\7\2\10\1\7\22\10\1\6\14\10\1\4\2\10\12\5\42\10\1\3\36\10\1\1"+
                    "\1\10\1\2\uff82\10",
            "",
            "",
            "\12\16\1\36\2\16\1\36\31\16\1\43\2\16\1\12\26\16\32\44\1\16\1\11\4\16"+
                    "\1\25\1\26\1\20\1\27\1\30\1\31\1\32\1\33\1\22\2\44\1\23\1\34\1\44\1\35"+
                    "\1\17\1\37\1\21\1\40\1\24\1\41\3\44\1\42\1\44\1\13\1\16\1\14\1\15\uff81"+
                    "\16",
            "\12\5",
            "\12\45\1\uffff\2\45\1\uffff\42\45\12\5\42\45\1\uffff\36\45\1\uffff\1"+
                    "\45\1\uffff\uff82\45",
            "\12\45\1\uffff\2\45\1\uffff\116\45\1\uffff\36\45\1\uffff\1\45\1\uffff"+
                    "\uff82\45",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\51\12\uffff\1\52\1\uffff\1\53\3\uffff\1\50",
            "\1\54\1\55\10\uffff\1\56\2\uffff\1\57",
            "\1\61\1\62\11\uffff\1\60\1\uffff\1\63\2\uffff\1\64",
            "\32\44\6\uffff\15\44\1\65\14\44",
            "\1\70\2\uffff\1\67\4\uffff\1\71",
            "\1\73\7\uffff\1\72",
            "\1\74\6\uffff\1\75",
            "\32\44\6\uffff\13\44\1\76\10\44\1\77\5\44",
            "\1\101\23\uffff\1\102",
            "\1\103\1\104",
            "\32\44\6\uffff\1\105\1\106\1\107\1\110\4\44\1\111\3\44\1\112\1\113\1"+
                    "\114\1\115\1\44\1\116\1\117\1\120\6\44",
            "\1\122\14\uffff\1\123",
            "\1\124\14\uffff\1\125",
            "\1\130\7\uffff\1\126\5\uffff\1\127",
            "\1\131",
            "",
            "\1\132\6\uffff\1\133",
            "\1\134\16\uffff\1\135",
            "\1\136",
            "\1\137",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\32\44\6\uffff\32\44",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154\21\uffff\1\155",
            "\1\156",
            "\1\157",
            "\1\161\15\uffff\1\160",
            "",
            "\1\162",
            "\1\163",
            "\32\44\6\uffff\15\44\1\164\14\44",
            "\1\166",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\173",
            "",
            "\1\174",
            "\32\44\6\uffff\32\44",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\32\44\6\uffff\32\44",
            "\1\u0085",
            "\1\u0086\7\uffff\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\32\44\6\uffff\2\44\1\u008b\23\44\1\u008c\3\44",
            "\1\u008e",
            "",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\32\44\6\uffff\32\44",
            "\1\u0093",
            "\32\44\6\uffff\32\44",
            "\1\u0095",
            "\1\u0096",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\1\u0099",
            "\1\u009a",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "",
            "",
            "\1\u009d",
            "\32\44\6\uffff\3\44\1\u009e\26\44",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "",
            "\1\u00a2",
            "\1\u00a3",
            "\32\44\6\uffff\32\44",
            "\1\u00a5",
            "\32\44\6\uffff\32\44",
            "\1\u00a7",
            "\1\u00a8",
            "\32\44\6\uffff\32\44",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "",
            "\1\u00af",
            "\32\44\6\uffff\32\44",
            "\1\u00b1",
            "\1\u00b2",
            "\1\u00b3",
            "\1\u00b4",
            "\1\u00b5\5\uffff\1\u00b6\7\uffff\1\u00b7",
            "",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "",
            "\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "",
            "\32\44\6\uffff\32\44",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u00cc",
            "",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u00ce",
            "",
            "",
            "\1\u00cf",
            "\32\44\6\uffff\32\44",
            "\1\u00d1",
            "\1\u00d2",
            "\32\44\6\uffff\32\44",
            "\1\u00d4",
            "\1\u00d5",
            "",
            "\1\u00d6",
            "",
            "\1\u00d7",
            "\1\u00d8",
            "",
            "\1\u00d9",
            "\32\44\6\uffff\32\44",
            "\1\u00db",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\1\u00de",
            "",
            "\32\44\6\uffff\2\44\1\u00df\27\44",
            "\1\u00e1",
            "\32\44\6\uffff\32\44",
            "\1\u00e3",
            "\32\44\6\uffff\32\44",
            "\1\u00e5",
            "\1\u00e6",
            "\1\u00e7",
            "\1\u00e8",
            "\32\44\6\uffff\32\44",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\1\u00ee",
            "\32\44\6\uffff\32\44",
            "\1\u00f0",
            "\32\44\6\uffff\32\44",
            "\1\u00f2",
            "\1\u00f3",
            "\1\u00f4",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\1\u00f8",
            "",
            "",
            "\1\u00f9",
            "",
            "\1\u00fa",
            "\1\u00fb",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u00fd",
            "",
            "\1\u00fe",
            "\1\u00ff",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0104",
            "",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u0106",
            "",
            "\1\u0107",
            "",
            "\1\u0108",
            "",
            "\1\u0109",
            "\1\u010a",
            "\1\u010b",
            "\1\u010c",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u010e",
            "\1\u010f",
            "\1\u0110",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0112",
            "",
            "\1\u0113",
            "\1\u0114",
            "\1\u0115",
            "\32\44\6\uffff\32\44",
            "\1\u0117",
            "\32\44\6\uffff\32\44",
            "\1\u0119",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "",
            "\1\u011d",
            "\1\u011e",
            "\1\u011f",
            "\1\u0120",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0123",
            "",
            "\1\u0124",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\1\u0127",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u012b",
            "\32\44\6\uffff\32\44",
            "\1\u012d",
            "",
            "\1\u012e",
            "\32\44\6\uffff\32\44",
            "\1\u0130",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0132",
            "",
            "\32\44\6\uffff\32\44",
            "\1\u0134",
            "\1\u0135",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\1\u0138",
            "\32\44\6\uffff\32\44",
            "\1\u013a",
            "",
            "",
            "\1\u013b",
            "\32\44\6\uffff\32\44",
            "",
            "",
            "\32\44\6\uffff\5\44\1\u013d\24\44",
            "",
            "",
            "",
            "\1\u013f",
            "",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0143",
            "",
            "\1\u0144",
            "\1\u0145",
            "",
            "",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0147",
            "\1\u0148",
            "",
            "\1\u0149",
            "",
            "\32\44\6\uffff\32\44",
            "",
            "",
            "",
            "\1\u014b",
            "\32\44\6\uffff\32\44",
            "\1\u014d",
            "",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "\32\44\6\uffff\32\44",
            "",
            "\32\44\6\uffff\32\44",
            "",
            "\1\u0152",
            "",
            "",
            "",
            "",
            "\32\44\6\uffff\32\44",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    protected class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        @Override
        public String getDescription() {
            return "1:1: Tokens : ( T__85 | T__86 | SLASH | STAR | OPENBRACE | CLOSEBRACE | NBSP | OTHER | PRINTIM | CELL | ROW | INTBL | LDBLQUOTE | RDBLQUOTE | TITLE | TAB | ANSI | ANSICPG | AUTHOR | B | BLUE | BULLET | CF | COLORTBL | CREATIM | DEFF | DEFLANG | DEFLANGFE | DEFTAB | DY | EMDASH | ENDASH | F | FALT | FBIDI | FCHARSET | FDECOR | FI | FMODERN | FNAME | FNIL | FONTTBL | FPRQ | FROMAN | FS | FSCRIPT | FSWISS | FTECH | GENERATOR | GREEN | HEADER | HR | I | INFO | LANG | LI | LINE | MIN | MO | OPERATOR | PAR | PARD | PLAIN | PNSTART | QC | QJ | RED | REVTIM | RQUOTE | RTF | SEC | STYLESHEET | UC | YR | MAC | CONTROL | NUMBER | WS | NEWLINE | HEXCHAR | TEXT );";
        }
        @Override
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
            int _s = s;
            switch ( s ) {
                case 0 :
                    int LA8_6 = input.LA(1);

                    int index8_6 = input.index();
                    input.rewind();
                    s = -1;
                    if ( ((LA8_6 >= '\u0000' && LA8_6 <= '\t')||(LA8_6 >= '\u000B' && LA8_6 <= '\f')||(LA8_6 >= '\u000E' && LA8_6 <= '[')||(LA8_6 >= ']' && LA8_6 <= 'z')||LA8_6=='|'||(LA8_6 >= '~' && LA8_6 <= '\uFFFF')) && ((!afterControl))) {s = 37;}
                    else s = 39;

                    input.seek(index8_6);
                    if ( s>=0 ) return s;
                    break;

                case 1 :
                    int LA8_39 = input.LA(1);

                    int index8_39 = input.index();
                    input.rewind();
                    s = -1;
                    if ( ((afterControl)) ) {s = 97;}
                    else if ( ((!afterControl)) ) {s = 37;}

                    input.seek(index8_39);
                    if ( s>=0 ) return s;
                    break;

                case 2 :
                    int LA8_38 = input.LA(1);

                    int index8_38 = input.index();
                    input.rewind();
                    s = -1;
                    if ( ((afterControl)) ) {s = 96;}
                    else if ( ((!afterControl)) ) {s = 37;}

                    input.seek(index8_38);
                    if ( s>=0 ) return s;
                    break;

                case 3 :
                    int LA8_5 = input.LA(1);

                    int index8_5 = input.index();
                    input.rewind();
                    s = -1;
                    if ( ((LA8_5 >= '0' && LA8_5 <= '9')) && (((afterControl)||(!afterControl)))) {s = 5;}
                    else if ( ((LA8_5 >= '\u0000' && LA8_5 <= '\t')||(LA8_5 >= '\u000B' && LA8_5 <= '\f')||(LA8_5 >= '\u000E' && LA8_5 <= '/')||(LA8_5 >= ':' && LA8_5 <= '[')||(LA8_5 >= ']' && LA8_5 <= 'z')||LA8_5=='|'||(LA8_5 >= '~' && LA8_5 <= '\uFFFF')) && ((!afterControl))) {s = 37;}
                    else s = 38;

                    input.seek(index8_5);
                    if ( s>=0 ) return s;
                    break;

                case 4 :
                    int LA8_4 = input.LA(1);

                    int index8_4 = input.index();
                    input.rewind();
                    s = -1;
                    if ( ((LA8_4 >= '0' && LA8_4 <= '9')) && (((afterControl)||(!afterControl)))) {s = 5;}
                    else s = 37;

                    input.seek(index8_4);
                    if ( s>=0 ) return s;
                    break;

                case 5 :
                    int LA8_3 = input.LA(1);
                    s = -1;
                    if ( (LA8_3=='\\') ) {s = 9;}
                    else if ( (LA8_3=='*') ) {s = 10;}
                    else if ( (LA8_3=='{') ) {s = 11;}
                    else if ( (LA8_3=='}') ) {s = 12;}
                    else if ( (LA8_3=='~') ) {s = 13;}
                    else if ( ((LA8_3 >= '\u0000' && LA8_3 <= '\t')||(LA8_3 >= '\u000B' && LA8_3 <= '\f')||(LA8_3 >= '\u000E' && LA8_3 <= '&')||(LA8_3 >= '(' && LA8_3 <= ')')||(LA8_3 >= '+' && LA8_3 <= '@')||LA8_3=='['||(LA8_3 >= ']' && LA8_3 <= '`')||LA8_3=='|'||(LA8_3 >= '\u007F' && LA8_3 <= '\uFFFF')) ) {s = 14;}
                    else if ( (LA8_3=='p') ) {s = 15;}
                    else if ( (LA8_3=='c') ) {s = 16;}
                    else if ( (LA8_3=='r') ) {s = 17;}
                    else if ( (LA8_3=='i') ) {s = 18;}
                    else if ( (LA8_3=='l') ) {s = 19;}
                    else if ( (LA8_3=='t') ) {s = 20;}
                    else if ( (LA8_3=='a') ) {s = 21;}
                    else if ( (LA8_3=='b') ) {s = 22;}
                    else if ( (LA8_3=='d') ) {s = 23;}
                    else if ( (LA8_3=='e') ) {s = 24;}
                    else if ( (LA8_3=='f') ) {s = 25;}
                    else if ( (LA8_3=='g') ) {s = 26;}
                    else if ( (LA8_3=='h') ) {s = 27;}
                    else if ( (LA8_3=='m') ) {s = 28;}
                    else if ( (LA8_3=='o') ) {s = 29;}
                    else if ( (LA8_3=='\n'||LA8_3=='\r') ) {s = 30;}
                    else if ( (LA8_3=='q') ) {s = 31;}
                    else if ( (LA8_3=='s') ) {s = 32;}
                    else if ( (LA8_3=='u') ) {s = 33;}
                    else if ( (LA8_3=='y') ) {s = 34;}
                    else if ( (LA8_3=='\'') ) {s = 35;}
                    else if ( ((LA8_3 >= 'A' && LA8_3 <= 'Z')||(LA8_3 >= 'j' && LA8_3 <= 'k')||LA8_3=='n'||(LA8_3 >= 'v' && LA8_3 <= 'x')||LA8_3=='z') ) {s = 36;}
                    if ( s>=0 ) return s;
                    break;

                case 6 :
                    int LA8_0 = input.LA(1);

                    int index8_0 = input.index();
                    input.rewind();
                    s = -1;
                    if ( (LA8_0=='{') ) {s = 1;}
                    else if ( (LA8_0=='}') ) {s = 2;}
                    else if ( (LA8_0=='\\') ) {s = 3;}
                    else if ( (LA8_0=='-') && (((afterControl)||(!afterControl)))) {s = 4;}
                    else if ( ((LA8_0 >= '0' && LA8_0 <= '9')) && (((afterControl)||(!afterControl)))) {s = 5;}
                    else if ( (LA8_0==' ') && (((afterControl)||(!afterControl)))) {s = 6;}
                    else if ( (LA8_0=='\n'||LA8_0=='\r') ) {s = 7;}
                    else if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\t')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\u001F')||(LA8_0 >= '!' && LA8_0 <= ',')||(LA8_0 >= '.' && LA8_0 <= '/')||(LA8_0 >= ':' && LA8_0 <= '[')||(LA8_0 >= ']' && LA8_0 <= 'z')||LA8_0=='|'||(LA8_0 >= '~' && LA8_0 <= '\uFFFF')) ) {s = 8;}

                    input.seek(index8_0);
                    if ( s>=0 ) return s;
                    break;
            }
            NoViableAltException nvae =
                    new NoViableAltException(getDescription(), 8, _s, input);
            error(nvae);
            throw nvae;
        }
    }

}