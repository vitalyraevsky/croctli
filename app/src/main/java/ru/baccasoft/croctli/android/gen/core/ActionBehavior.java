
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for ActionBehavior complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActionBehavior">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsNullable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsReadonly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsVisible" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PropertyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActionBehavior", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "isNullable",
    "isReadonly",
    "isVisible",
    "propertyName"
})
@DatabaseTable(tableName = "action_behavior")
@JsonIgnoreProperties({"action"})
public class ActionBehavior {

    public static final String ACTION_ID_FIELD = "action_foreign_id_field";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = ACTION_ID_FIELD)
    private Action action;

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @DatabaseField(id = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsNullable")
    @XmlElement(name = "IsNullable", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isNullable;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsReadonly")
    @XmlElement(name = "IsReadonly", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isReadonly;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsVisible")
    @XmlElement(name = "IsVisible", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isVisible;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @JsonProperty("PropertyName")
    @XmlElement(name = "PropertyName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String propertyName;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the isNullable property.
     * 
     */
    public boolean isIsNullable() {
        return isNullable;
    }

    /**
     * Sets the value of the isNullable property.
     * 
     */
    public void setIsNullable(boolean value) {
        this.isNullable = value;
    }

    /**
     * Gets the value of the isReadonly property.
     * 
     */
    public boolean isIsReadonly() {
        return isReadonly;
    }

    /**
     * Sets the value of the isReadonly property.
     * 
     */
    public void setIsReadonly(boolean value) {
        this.isReadonly = value;
    }

    /**
     * Gets the value of the isVisible property.
     * 
     */
    public boolean isIsVisible() {
        return isVisible;
    }

    /**
     * Sets the value of the isVisible property.
     * 
     */
    public void setIsVisible(boolean value) {
        this.isVisible = value;
    }

    /**
     * Gets the value of the propertyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the value of the propertyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyName(String value) {
        this.propertyName = value;
    }

}
