package ru.baccasoft.croctli.android.view.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.ItemField;
import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.event.ClassifierSimpleChangeEvent;
import ru.baccasoft.croctli.android.view.event.ScalarChangeEvent;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.ClassifierDisplayMode1;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.ClassifierFree;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.ClassifierSimple;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateScalar;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateTimeScalar;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test.ClassifierArrayNew2;

/**
 * Created by developer on 12.11.14.
 */
public class ViewUtils {
    @SuppressWarnings("unused")
    private static final String TAG = ViewUtils.class.getSimpleName();

    //{{{ Параметра, с которыми надо добавлять каждую вьюшку отдельного поручения в родилельский ViewGroup
    public final static LinearLayout.LayoutParams errandViewLP = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    static { errandViewLP.setMargins(10, 5, 10, 5); }
    //}}}


    public final static String USER_VALUE_LABEL = "#UserValueLabel";
//    public final static List<String> validScalarList = new ArrayList<String>() {{
//        add("string");
//        add("text");
//        add("int");
//        add("float");
//        add("decimal");
//        add("boolean");
//        add("date");
//        add("dateTime");
//    }};

    // кастомные вьюшки могут иметь тег, выставленный через View.setTag(key, object)
    // с этим ключом должен храниться идентификатор соответствуюшего EntityProperty
    public static final int VIEW_ENTITY_PROPERTY_TAG_KEY = R.string.view_entity_property_tag_key;

    public static enum SCALAR_TYPE {
        STRING("string"),
        TEXT("text"),
        INT("int"),
        FLOAT("float"),
        DECIMAL("decimal"),
        BOOLEAN("boolean"),
        DATE("date"),
        DATE_TIME("dateTime");

        private String value;
        SCALAR_TYPE(String value) { this.value = value; }

        private static final Map<String, SCALAR_TYPE> MY_MAP = new HashMap<String, SCALAR_TYPE>();
        static {
            for(SCALAR_TYPE st : values()) {
                MY_MAP.put(st.getValue(), st);
            }
            // FIXME затычка ошибки на стороне сервера
//            MY_MAP.put("datetime", DATE_TIME);
        }
        public String getValue() { return value; }
        public static SCALAR_TYPE forValue(String value) { return MY_MAP.get(value); }


        public static boolean isValid(String type) { return (MY_MAP.containsKey(type)); }
    }

    /**
     * Делает контейнер LinearLayout, с параметрами: <br>
     * orientation=VERTICAL, width=MATCH_PARENT, height=WRAP_CONTENT
     *
     * @param ctx
     * @return
     */
    public static LinearLayout makeVerticalContainer(Context ctx) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.vertical_container, null);
        Log.d(TAG, "Inflate VertivalContainer from View Utils");

        return ll;
    }

    /**
     * Делает контейнер LinearLayout, с параметрами: <br>
     * orientation=HORIZONTAL, width=MATCH_PARENT, height=WRAP_CONTENT
     *
     * @param ctx
     * @return
     */
    public static LinearLayout makeHorizontalContainer(Context ctx) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.horizontal_container, null);
        Log.d(TAG, "Inflate HorizontalContainer from View Utils");

        return ll;
    }

    public static LinearLayout makeHorizontalContainer(Context ctx, Integer gravity) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.horizontal_container, null);
        Log.d(TAG, "Inflate HorizontalContainer + Gravity from View Utils");
        if(gravity != null)
            ll.setGravity(gravity);

        return ll;
    }

    public static TextView makeTitleTextView(Context context, String string, EntityProperty p){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(R.layout.title_text_view, null);

        if(p != null && !p.isIsNullable()){
            String mark = "*";
            SpannableStringBuilder builder = new SpannableStringBuilder();

            builder.append(string).append(":");
            int start = builder.length();
            builder.append(mark);
            int end = builder.length();
            builder.append(" ");


            builder.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new RelativeSizeSpan(1.5f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(builder);
        }else{
            textView.setText(string + ": ");
        }

        Log.d(TAG, "Inflate TitleTextView from ViewUtils: " + string);
        return textView;
    }

    public static TextView makeTextView(Context ctx){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(R.layout.simple_text_view, null);

        Log.d(TAG, "makeTextView");
        return textView;
    }

    public static EditText makeStringScalarEditText(final Context ctx, String text, final String entityPropertyId){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText editText = (EditText) inflater.inflate(R.layout.string_scalar_edit_text, null);

        editText.setText(text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                EventBus.getDefault().post(new ScalarChangeEvent(
                        entityPropertyId, s.toString(), ctx));
            }
        });
        return editText;
    }
    public static TextView makeStringScalarTextView(Context ctx, String text){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(R.layout.string_scalar_text_view, null);
        textView.setText(text);

        return textView;
    }

    public static TextView makeTextScalarTextView(Context ctx, String text){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(R.layout.text_scalar_text_view, null);
        textView.setText(text);

        return textView;
    }
    public static EditText makeTextScalarEditText(Context ctx, String text){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText editText = (EditText) inflater.inflate(R.layout.text_scalar_edit_text, null);
        editText.setText(text);

        return editText;
    }

    public static LinearLayout makeStringScalarWithTitle(Context ctx, String title, String text, boolean isReadonly,String entityPropertyId){
        TextView contentView;
        TextView titleText;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);

        if(isReadonly){
           contentView = makeStringScalarTextView(ctx, text);
        }else{
           contentView = makeStringScalarEditText(ctx, text, entityPropertyId);
        }

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makeStringScalarWithTitle: " + title + " | " + text);

        return linearLayout;
    }
    public static LinearLayout makeTextScalarWithTitle(Context ctx, String title, String text, boolean isReadonly){
        TextView titleText;
        TextView contentView;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);

        if(isReadonly){
            contentView = makeTextScalarTextView(ctx, text);
        }else{
            contentView = makeTextScalarEditText(ctx, text);
        }

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makeTextScalarWithTitle: " + title + " | " + text);
        return linearLayout;
    }

    public static EditText makeIntScalar(Context ctx, String text, boolean isReadonly){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText editText = (EditText) inflater.inflate(R.layout.int_scalar_edit_text, null);

        editText.setText(text);
        editText.setEnabled(!isReadonly);

        Log.d(TAG, "makeIntScalar: " + text);
        return editText;

    }

    public static LinearLayout makeIntScalarWithTitle(Context ctx, String title, String text, boolean isReadonly){
        TextView titleText;
        TextView contentView;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);

        contentView = makeIntScalar(ctx, text, isReadonly);

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makeIntScalarWithTitle: " + title + " | " + text);
        return linearLayout;
    }

    public static EditText makeFloatScalar(Context ctx, String text, boolean isReadonly){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText editText = (EditText) inflater.inflate(R.layout.float_scalar_edit_text, null);

        editText.setText(text);
        editText.setEnabled(!isReadonly);

        Log.d(TAG, "makeFloatScalar: " + text);
        return editText;
    }

    public static LinearLayout makeFloatScalarWithTitle(Context ctx, String title, String text, boolean isReadonly){
        TextView titleText;
        TextView contentView;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);

        contentView = makeFloatScalar(ctx, text, isReadonly);

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);
        Log.d(TAG, "makeFloatScalarWithTitle: " + title + " | " + text );
        return linearLayout;
    }

    public static CheckBox makeBooleanScalar(Context ctx, boolean state, boolean isReadonly){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        CheckBox checkBox = (CheckBox)inflater.inflate(R.layout.boolean_scalar_check_box, null);

        checkBox.setChecked(state);
        checkBox.setEnabled(!isReadonly);

        Log.d(TAG,"makeBooleanScalar");
        return checkBox;
    }

    public static LinearLayout makeBooleanScalarWithTitle(Context ctx, String title, boolean isEnabled, boolean isReadonly){
        TextView titleText;
        CheckBox contentView;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);

        contentView = makeBooleanScalar(ctx, isEnabled, isReadonly);

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makeBooleanScalarWithTitle: " + title);
        return linearLayout;
    }

    public static TextView makeDateScalar(final Activity activity, IDateField date, boolean isReadonly, String entityPropertyId){

        TextView textView = new DateScalar(activity, date, isReadonly, entityPropertyId);

        textView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        Log.d(TAG, "makeDateScalar");
        return textView;
    }

    public static ImageButton makeImageButtonCleanData(final Context ctx, final TextView dateScalar, final String entityPropertyId){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageButton imageButton = (ImageButton)inflater.inflate(R.layout.clean_data_image, null);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //очистить дату в dateScalar
                Log.d(TAG,"makeImageButtonCleanData: CLICK CLEAN IMAGE: " + dateScalar.getText());
                dateScalar.setText("");
                EventBus.getDefault().post(new ScalarChangeEvent(entityPropertyId, "", ctx));
            }
        });
        return imageButton;
    }

    public static LinearLayout makeDateScalarWithTitle(final Activity activity, String title, IDateField date,boolean isReadonly, final String entityPropertyId){
        Context mContext = activity.getApplicationContext();
        TextView titleText;
        TextView contentView;
        ImageButton imageButton;

        LinearLayout linearlayout = makeHorizontalContainer(mContext);

        titleText = makeTitleTextView(mContext, title, null);
        contentView = makeDateScalar(activity, date, isReadonly, entityPropertyId);

        linearlayout.addView(titleText);
        linearlayout.addView(contentView);

        if(!isReadonly){
            imageButton = makeImageButtonCleanData(mContext, contentView, entityPropertyId);
            linearlayout.addView(imageButton);
        }
        Log.d(TAG, "makeDateScalarWithTitle");
        return linearlayout;
    }


    public static TextView makeDateTimeScalar(Activity activity, IDateField date, boolean isReadonly, String entityPropertyId){
        TextView textView;
        textView = new DateTimeScalar(activity, date, isReadonly, entityPropertyId);

        textView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        Log.d(TAG, "makeDateTimeScalar");
        return textView;
    }
    public static LinearLayout makeDateTimeScalarWithTitle(Activity activity, String title, IDateField date,boolean isReadonly, String entityPropertyId){
        Context mContext = activity.getApplicationContext();
        TextView titleText;
        TextView contentView;

        LinearLayout linearLayout = makeHorizontalContainer(mContext);
        titleText = makeTitleTextView(mContext, title, null);
        contentView = makeDateTimeScalar(activity, date, isReadonly, entityPropertyId);

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makeDateTimeScalarWithTitle");
        return linearLayout;
    }

    public static MultiAutoCompleteTextView makeClassifierArrayNew2(List<ClassifierItem> allItems, List<ClassifierItem> initItems,String entityPropertyId, Context ctx){
        MultiAutoCompleteTextView classifierArrayNew2 = new ClassifierArrayNew2(allItems, initItems, entityPropertyId, ctx);

        classifierArrayNew2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        classifierArrayNew2.setGravity(Gravity.CENTER_VERTICAL);

        Log.d(TAG, "makeClassifierArrayNew2");
        return classifierArrayNew2;
    }

    public static ImageButton makeImageButtonAddClassifier(Context ctx,final MultiAutoCompleteTextView multiAutoCompleteTextView){
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageButton imageButton = (ImageButton)inflater.inflate(R.layout.add_classifier_image, null);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ClickOnAddClassifier");
                multiAutoCompleteTextView.showDropDown();
            }
        });
        Log.d(TAG, "makeImageButtonAddClassifier");
        return imageButton;
    }

    public static AutoCompleteTextView makeClassifierFree(Context ctx, Map<String, String> items, String entityPropertyId, String value){
        AutoCompleteTextView classifierFree = new ClassifierFree(ctx, items, entityPropertyId);
        classifierFree.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        classifierFree.setText(value);

        Log.d(TAG, "makeClassifierFree");
        return classifierFree;
    }

    public static AutoCompleteTextView makeClassifierSimple(Context ctx, Map<String, String> items, String entityPropertyId){
        AutoCompleteTextView classifierSimple = new ClassifierSimple(ctx, items, entityPropertyId);
        classifierSimple.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        classifierSimple.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d("", "onFocusChange");
            }
        });

        Log.d(TAG, "makeClassifierSimple");
        return classifierSimple;
    }

    public static LinearLayout makeClassifierSimpleWithTitle(Context ctx, String title, Map<String, String> items, EntityProperty p){
        TextView titleText;
        AutoCompleteTextView classifierSimple;
        LinearLayout linearLayout = makeHorizontalContainer(ctx);

        titleText = makeTitleTextView(ctx, title, null);
        classifierSimple = makeClassifierSimple(ctx, items, p.getId());

        linearLayout.addView(titleText);
        linearLayout.addView(classifierSimple);

        Log.d(TAG,"makeClassifierSimpleWithTitle: " + title);
        return linearLayout;
    }

    public static LinearLayout makemakeClassifierFreeWithTitle(Context ctx, String title, Map<String, String> items, String entityPropertyId, String value, EntityProperty p){
        TextView titleText;
        AutoCompleteTextView contentView;

        LinearLayout linearLayout = makeHorizontalContainer(ctx);
        titleText = makeTitleTextView(ctx, title, p);
        contentView = makeClassifierFree(ctx, items, entityPropertyId, value);

        linearLayout.addView(titleText);
        linearLayout.addView(contentView);

        Log.d(TAG, "makemakeClassifierFreeWithTitle: " + title + " | " + value);
        return linearLayout;
    }

    public static AutoCompleteTextView makeClassifierDisplayMode1(Context ctx, Map<String, String> items, String entityPropertyId, String value){
        ClassifierDisplayMode1 classifierDisplayMode1 = new ClassifierDisplayMode1(ctx, items, entityPropertyId);
        classifierDisplayMode1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        classifierDisplayMode1.setFocusableInTouchMode(false);
        classifierDisplayMode1.setText(value);
        if(value.isEmpty()){
            EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, "", ctx));
        }
        Log.d(TAG, "makeClassifierDisplayMode1");
        return classifierDisplayMode1;
    }

    public static LinearLayout makeEditableViewWithTitle(Context ctx, String title, Map<String, String> items, String entityPropertyId, String value, EntityProperty p){
        TextView titleText;
        AutoCompleteTextView autoCompleteTextView;

        LinearLayout linearLayout = ViewUtils.makeHorizontalContainer(ctx);
        titleText = makeTitleTextView(ctx, title, p);
        autoCompleteTextView = makeClassifierDisplayMode1(ctx, items, entityPropertyId, value);

        linearLayout.addView(titleText);
        linearLayout.addView(autoCompleteTextView);

        Log.d(TAG,"makemakeEditableViewWithTitle: ");
        return linearLayout;
    }

    public static boolean isEmpty(String s) {
        return (s == null || s.trim().equals(""));
    }
    public static boolean isNotEmpty(String s) { return !isEmpty(s); }
    public static <T> boolean isEmpty(List<T> l) {
        return (l == null || l.size() == 0);
    }
    public static <T> boolean isNotEmpty(List<T> l) {
        return !isEmpty(l);
    }

     /**
     * Проверяет, следует ли вьюшку со свойством отображать в режиме для чтения.
     * Учитывает глобальный флаг globalReadOnly, который имеет приоритет над
     * флагом isReadOnly самого свойства.
     *
     * @param p свойство EntityProperty, для которого нужно сделать вьюшку
     * @param readOnly глобальный флаг. если установлен, имеет приоритет над флагом из EntityProperty
     * @return
     */
    public static boolean isReadOnly(EntityProperty p, Boolean readOnly) {
        if(readOnly != null)
            return readOnly;
        return p.isIsReadonly();
    }

    public static TreeMap<String, Map<String, String>> getValuesForClassifierSimple(EntityProperty p) {
        String value = p.getValue();

        TreeMap<String, Map<String, String>> ret = new TreeMap<String, Map<String, String>>();
        Map<String, String> items = new HashMap<String, String>();

        List<ClassifierItem> classifierItems = TableUtils.getClassifierItemsByClassifierId(p.getClassifierTypeId());

        for(ClassifierItem ci : classifierItems) {
            items.put(ci.getCode(), ci.getDisplayValue());

            if(ci.getCode().equals(value)) {
                value = ci.getDisplayValue();
            }
        }

        ret.put(value, items);

        return ret;
    }

    /**
     * Получить массив строк, которые которые будут предлагаться в выпадающем списке справочника
     * при редактировании
     *
     * @param p
     * @return
     */
    public static TreeMap<String, List<String>> getClassifierViewItemList(EntityProperty p) {
        final String selectedValue;
        final List<String> itemsListForView = new ArrayList<String>();

        // Если атрибут ClassifierDisplayField заполнен, источником данных будет массив Classifier.ClassifierItem.ItemField.Value,
        // где Classifier.id = EntityProperty. СlassifierId AND ClassifierItem.Code = EntityProperty. ClassifierDisplayField
        if(isNotEmpty(p.getClassifierDisplayField())) {
            List<ItemField> itemFields = TableUtils.getItemFieldsForDisplayMode0(p);
            for(ItemField i : itemFields) {
                itemsListForView.add(i.getValue());
            }
            // Выбранное значение классификатора в этом случае хранится как ItemField.Name
            if(isNotEmpty(p.getValue())) {
                selectedValue = TableUtils.getItemFieldByName(p.getValue()).getValue();
            } else {
                selectedValue = "";
            }
            // По умолчанию источником данных для списка являются элементы классификатора (ClassifierItem.DisplayValue),
            // id которого передан в поле СlassifierId.
        } else {
            List<ClassifierItem> classifierItems = TableUtils
                    .getClassifierItemsByClassifierId(p.getClassifierTypeId());
            for(ClassifierItem ci : classifierItems) {
                itemsListForView.add(ci.getDisplayValue());
            }
            // Выбранное значение классификатора в этом случае хранится как ClassifierItem.Code
            if(isNotEmpty(p.getValue())) {
                Log.d("", "[getClassifierViewItemList] saved code: \'" + p.getValue() + "\'");

                ClassifierItem classifierItem = TableUtils.getClassifierItemByCode(p.getClassifierTypeId(), p.getValue());

                if(classifierItem == null) {
                    if(p.isIsClassifierFree()) {
                        // значит произвольное значение, введенное в поле справочника со свободным вводом
                        // было сохранено в EntityProperty.value
                        selectedValue = p.getValue();
                    } else {
                        // иначе нет объяснения, почему не получили соответствующего ClassifierItem
                        throw new IllegalStateException("cant find classifierItem with code:\'" + p.getValue() + "\'");
                    }
                } else {
                    selectedValue = classifierItem.getDisplayValue();
                }
            } else {
                selectedValue = "";
            }
        }
        Log.d("", "[getClassifierViewItemList] selected value: \'" + selectedValue + "\'");

        TreeMap<String, List<String>> ret = new TreeMap<String, List<String>>();
        ret.put(selectedValue, itemsListForView);

        return ret;
    }

    //{{{
    /**
     * Все слова, обрамленные символами @, воспринимаются как имена дочерних элементов объектного свойства
     * и заменяются на их значения, если такие атрибуты найдены.
     *
     * @param expr строка с патернами @name@ (или без)
     * @param parentEntityPropertyId идентификатор родительского EntityProperty, среди потомков которого нужно искать
     *                               значения, которые будут подставлены вместо патернов @name@
     * @return
     */
    public static String textFromPattern(String expr, String parentEntityPropertyId) {
        // В значении атрибута ClassifierDisplayField  все слова, обрамленные символами @, воспринимаются как имена
        // дочерних элементов объектного свойства и заменяются на их значения, если такие атрибуты найдены.
        // Остальной текст остается как есть.
        String result = expr;
        List<String> names = parsePattern(expr);
        List<String> processedNames = new ArrayList<String>();

        if(names.size() == 0)
            return expr;

        List<EntityProperty> props = TableUtils.getEntityPropertyChildsById(parentEntityPropertyId);
        // удобный мап. ключ = поле name
        Map<String, EntityProperty> mapProp = new HashMap<String, EntityProperty>();
        for(EntityProperty p : props) {
            String name = p.getName().trim();

            // отбрасываем EntityProperty, у которых есть потомки, ибо сложные они объекты
            if(TableUtils.getEntityPropertyChildsById(p.getId()).size() > 0)
                continue;

            mapProp.put(name, p);
        }

        // храним пары [EntityProperty.name | EntityProperty.value]
        Map<String, String> replaceMap = new HashMap<String, String>();
        for(String n : names) {
            if(mapProp.containsKey(n)) {
                EntityProperty p = mapProp.get(n);
                String processedName = p.getName();

                String replaceValue = p.getValue(); //его будем подставлять вместо шаблонов @name@

                // проверка, может ли значение поля 'name' быть кодом из справочника
                if(isNotEmpty(p.getClassifierTypeId()) && !replaceValue.isEmpty()) {
                    ClassifierItem ci = TableUtils.getClassifierItemByCode(p.getClassifierTypeId(), replaceValue);

                    if (ci != null)
                        replaceValue = ci.getDisplayValue();
                }

                replaceMap.put(processedName, replaceValue);
                processedNames.add(processedName);
            }
        }

        names.removeAll(processedNames);
        if(names.size() > 0)
            throw new IllegalStateException("cant find child EntityProperties with fields \"name\":"+ names);

        for(Map.Entry<String, String> r : replaceMap.entrySet())
            result = result.replace("@"+r.getKey()+"@", r.getValue());

        return result;
    }

    /**
     * Распарсить строку (обычно заголовок экспандера),
     * выдернуть все слова, обрамленные символами @@
     *
     * @param expr выражение, которое нужно распарсить
     * @return список выдернутых слов
     */
    private static List<String> parsePattern(String expr) {
        List<String> ret = new ArrayList<String>();

        Pattern p = Pattern.compile("(@\\w+@)");
        Matcher m = p.matcher(expr);

        while (m.find()) {
            ret.add(m.group().replace("@", ""));
        }

        return ret;
    }
    //}}}

    //{{{
    /**
     * Рекурсивно создает набор EP для нового массива объектных свойств на основе шаблона.
     *
     * Прим.: перед первым вызовом нужно не забыть поменять у корневого шаблона EP
     * поле orderInArray (чтобы небыло равно -1)
     *
     * @param templateEp
     * @param date
     * @return список EP.id, для созданных на основе макета EP
     */
    public static List<String> createEpsBasedOnTemplate(EntityProperty templateEp, String origTemplateId, String date) {
        List<String> newProps = new ArrayList<String>();

        // добавляем корень
        String rootEpNewId = makeUUIDforEP();
        templateEp.setId(rootEpNewId);

        Log.d(TAG, "in db creating EP with id:\'" + templateEp.getId() + "\'");
        TableUtils.insertNewEntityProperty(templateEp);
        newProps.add(templateEp.getId());

        //берем всех потомков и создаем рекурсивно копии на основе шаблона
        List<EntityProperty> origProps = TableUtils.getEntityPropertyChildsById(origTemplateId);
        for(EntityProperty p: origProps) {
            String origChildId = p.getId();
            String newChildId = makeUUIDforEP();
            p.setId(newChildId);
            p.setEntityPropertyId(rootEpNewId);

//            Log.d(TAG, "in db creating EP with id:\'" + p.getId() + "\'");
//            TableUtils.insertNewEntityProperty(p);
//            newProps.add(p.getId());

            List<String> childProps = createEpsBasedOnTemplate(p, origChildId, date);
            newProps.addAll(childProps);
        }
        Preload.addToClassifier(origProps);
        return newProps;
    }

    /**
     * Получаем айдишники детей по айдишнику
     * @param templateId
     * @param date
     * @return
     */
    public static List<String> getChildsEpsBasedOnTemplate (String templateId, String date){
        List<String> childsList = new ArrayList<String>();
        childsList.add(templateId);
        //берем всех потомков
        List<EntityProperty> props = TableUtils.getEntityPropertyChildsById(templateId);
        for(EntityProperty p: props) {
            String childId = p.getId();
            childsList.add(childId);
        }
        return childsList;
    }

    public static String makeUUIDforEP() {
        String uuid = UUID.randomUUID().toString();
        EntityProperty ep = TableUtils.getEntityPropertyById(uuid);
        if(ep != null)
            return makeUUIDforEP();

        return uuid;
    }
    //}}}

    public static void setTextSize(TextView textView, int dimenResId, Context context) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(dimenResId));
    }

    /**
     * Меняет визуальное отображение вьюшки, применяя некоторые параметры.
     * Нужен для форматирования рандомной вьюшки, представляющей отдельное поручение,
     * чтобы потом добавить эту вьюшку в окно редактирования поручения
     * или в блок отображения поручений.
     *
     * @param view
     * @param resources
     */
    public static void makeErrandViewPretty(ViewGroup view, Resources resources) {
        view.setBackgroundColor(resources.getColor(R.color.text_FFFFFF));
        view.setPadding(10, 10, 10, 10);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if( activity.getCurrentFocus() != null )
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

}
