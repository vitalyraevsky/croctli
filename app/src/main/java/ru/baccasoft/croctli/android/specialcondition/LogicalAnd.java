package ru.baccasoft.croctli.android.specialcondition;

import ru.baccasoft.croctli.android.gen.core.Task;

import java.util.LinkedList;
import java.util.List;

public class LogicalAnd implements IExpression<Boolean>{
	private List<IExpression<Boolean>> operands = new LinkedList<IExpression<Boolean>>();
	
	public void append( IExpression<Boolean> e ) {
		operands.add(e);
	}

	@Override
	public Boolean compute(Task task) {
		for( IExpression<Boolean> operand: operands ) {
			if( !operand.compute(task)) {
				return false;
			}
		}
		return true;
	}
}
