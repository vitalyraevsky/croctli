package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.app.Activity;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;

import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateScalar;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateTimeScalar;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.ScalarArrayContainer;

import static ru.baccasoft.croctli.android.view.util.ViewUtils.SCALAR_TYPE;


/**
 * Вьюшка для отображения плоского массива дат.<br>
 * Создает как для scalarType=date, так и для scalarType=dateTime.<br>
 * соответствующие вьюшки.<br>
 * Визуально - горизонтальный контейнер, в левой части заголовок,
 * в правой - подряд горизонтально даты.<br>
 */

public class DateArrayWithTitle extends LinearLayout {
    private Activity mActivity;
    private Context mContext;

    private LinearLayout arrayView;

    private Map<String, IDateField> params;
    private boolean readOnly;


    /**
     *
     * @param activity
     * @param scalarType дата или дата-время
     * @param title
     * @param params пары entityPropertyId:дата
     */
    public DateArrayWithTitle(Activity activity, SCALAR_TYPE scalarType, String title,
                              Map<String, IDateField> params, boolean readOnly) {
        super(activity.getApplicationContext());
        validate(scalarType);

        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.params = params;
        this.readOnly = readOnly;

        initThis();

        TextView titleView = ViewUtils.makeTitleTextView(mContext, title, null);
        arrayView = new ScalarArrayContainer(mContext);
        populateArrayView(scalarType);

        addView(titleView);
        addView(arrayView);
    }

    private void populateArrayView(SCALAR_TYPE scalarType) {
        if(scalarType == SCALAR_TYPE.DATE) {
            for(Map.Entry<String, IDateField> e : this.params.entrySet()) {
                DateScalar dateScalar = new DateScalar(mActivity, e.getValue(), this.readOnly, e.getKey());
                arrayView.addView(dateScalar);
            }
        } else if(scalarType == SCALAR_TYPE.DATE_TIME) {
            for(Map.Entry<String, IDateField> e : this.params.entrySet()) {
                DateTimeScalar dateTimeScalar = new DateTimeScalar(mActivity, e.getValue(), this.readOnly, e.getKey());
                arrayView.addView(dateTimeScalar);
            }
        }
    }

    private void validate(SCALAR_TYPE scalarType) {
        if(scalarType == null)
            throw new IllegalStateException("viewType cant be null. must be date or dateTime.");
        if(scalarType != SCALAR_TYPE.DATE && scalarType != SCALAR_TYPE.DATE_TIME)
            throw new IllegalStateException("viewType must be date or dateTime but we got:\'" +
                scalarType.getValue());
        if(params == null)
            throw new IllegalStateException("\"params\" cant be null. pass at least empty map");
    }

    private void initThis() {
        setOrientation(HORIZONTAL);
    }
}
