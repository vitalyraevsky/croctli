package ru.baccasoft.croctli.android.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.db.TableUtils;

/**
 * Created by developer on 30.10.14.
 */
@Deprecated
public class TaskErrandEditorDialogFragment extends DialogFragment {
    @SuppressWarnings("unused")
    private static final String TAG = TaskErrandEditorDialogFragment.class.getSimpleName();

    private String taskId;

    public static TaskErrandEditorDialogFragment newInstance(String taskId) {
        TaskErrandEditorDialogFragment f = new TaskErrandEditorDialogFragment();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null || !args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY)) {
            String message = "taskId cannot be null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        } else {
            taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.task_errand_editor_dialog_fragment, container, false);
        return inflater.inflate(R.layout.task_errand_editor_list_item, container, false);
    }
}
