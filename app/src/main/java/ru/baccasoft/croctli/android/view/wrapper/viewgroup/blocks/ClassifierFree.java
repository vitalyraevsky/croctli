package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.event.ClassifierFreeChangeEvent;
import ru.baccasoft.croctli.android.view.event.EmptyFieldEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Created by developer on 06.11.14.
 */
public class ClassifierFree extends AutoCompleteTextView {
    private PassThroughClickListener mPassThroughClickListener;
    private Context mContext;
    Map<String, String> items;
    String entityPropertyId;

    ArrayAdapter adapter;
    ArrayAdapter clearAdapter;
    final int itemResId = android.R.layout.simple_dropdown_item_1line;

    /**
     *
     * @param context
     * @param items пары [code справочника : выводимое значение]
     */
    public ClassifierFree(Context context, Map<String, String> items, final String entityPropertyId) {
        super(context);
        this.items = items;
        this.mContext = context;
        this.entityPropertyId = entityPropertyId;
        clearAdapter = new ArrayAdapter(context, itemResId);

        //validate(); //TODO

        initThis(mContext);

        List<String> values = new ArrayList<String>();
        for(Map.Entry<String, String> e : this.items.entrySet()) {
            values.add(e.getValue());
        }

        adapter = new ArrayAdapter(context, itemResId, values);
        setAdapter(adapter);

        mPassThroughClickListener = new PassThroughClickListener();
        super.setOnClickListener(mPassThroughClickListener);

        EventBus.getDefault().register(this);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        mPassThroughClickListener.mWrapped = listener;
    }

    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер
     */
    private class PassThroughClickListener implements OnClickListener {
        private OnClickListener mWrapped;

        @Override
        public void onClick(View v) {
            onClickImpl();

            if(mWrapped != null)
                mWrapped.onClick(v);
        }
    }

    private void onClickImpl() {
        if(!isPopupShowing())
            showDropDown();
    }

    // настраиваем вьюшку
    private void initThis(Context context) {
        setThreshold(1);
        setMaxLines(5);
        setBackgroundColor(context.getResources().getColor(R.color.text_F2F2F2));
        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
        addTextChangedListener(textWatcher);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //nothing
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //nothing
        }

        @Override
        public void afterTextChanged(Editable s) {
            EventBus.getDefault().post(new ClassifierFreeChangeEvent(s.toString(), entityPropertyId, mContext));
        }
    };

    public void onEvent(EmptyFieldEvent event){
        if(getText().toString().isEmpty()) {
            setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().
                    getDrawable(R.drawable.ic_error), null);
        }else{
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }
}
