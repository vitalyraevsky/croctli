
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for TaskUserUI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskUserUI">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WasRead" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFavourite" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Notice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskUserUI", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "taskId",
    "wasRead",
    "isFavourite",
    "notice"
})
@DatabaseTable(tableName = "task_user_ui")
public class TaskUserUI {

    @JsonProperty("TaskId")
    @XmlElement(name = "TaskId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "task_id", dataType = DataType.STRING, id = true)
    protected String taskId;

    @JsonProperty("WasRead")
    @XmlElement(name = "WasRead", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "was_read", dataType = DataType.BOOLEAN)
    protected boolean wasRead;

    @JsonProperty("IsFavourite")
    @XmlElement(name = "IsFavourite", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "is_favorite", dataType = DataType.BOOLEAN)
    protected boolean isFavourite;

    @JsonProperty("Notice")
    @XmlElement(name = "Notice", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "notice", dataType = DataType.STRING)
    protected String notice;

    /**
     * Gets the value of the taskId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Sets the value of the taskId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * Gets the value of the wasRead property.
     * 
     */
    public boolean isWasRead() {
        return wasRead;
    }

    /**
     * Sets the value of the wasRead property.
     * 
     */
    public void setWasRead(boolean value) {
        this.wasRead = value;
    }

    /**
     * Gets the value of the isFavourite property.
     * 
     */
    public boolean isIsFavourite() {
        return isFavourite;
    }

    /**
     * Sets the value of the isFavourite property.
     * 
     */
    public void setIsFavourite(boolean value) {
        this.isFavourite = value;
    }

    /**
     * Gets the value of the notice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotice() {
        return notice;
    }

    /**
     * Sets the value of the notice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotice(String value) {
        this.notice = value;
    }

}
