package ru.baccasoft.croctli.android.fragments.documents.converters.utils;


import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DocFileConversionHandler implements PicturesManager {

    private static final String TAG = DocFileConversionHandler.class.getSimpleName();

    private Activity activity;
    private String targetUri;
    private String imageDirPath;

    public DocFileConversionHandler(String imageDirPath, String targetUri, Activity activity) {
        this.activity = activity;
        this.targetUri = targetUri;
        this.imageDirPath = imageDirPath;
    }

    @Override
    public String savePicture( byte[] content, PictureType pictureType, String suggestedName, float widthInches, float heightInches ) {

        Log.d(TAG,"savePicture : " + suggestedName);
        String uri = null;
        // To create directory:
        File folder = activity.getDir(imageDirPath, Context.MODE_WORLD_READABLE);

        try {
            uri = storeImage(content , folder, suggestedName);
        } catch (Docx4JException e) {
            Log.d(TAG, "Docx4JException", e );
        }

        return uri;
    }

    protected String storeImage( byte[] bytes, File folder, String filename) throws Docx4JException {
        String uri = null;
        File imageFile = new File(folder, filename);
        FileOutputStream out = null;
        Log.i(TAG, "Writing: " + imageFile.getAbsolutePath());
        if (imageFile.exists()) {
            Log.w(TAG,"Overwriting (!) existing file!");
        }
        try {
            out = new FileOutputStream(imageFile);
            out.write(bytes);
            // return the uri
            uri = imageFile.getName();//setupImageUri(imageFile);
            Log.i(TAG,"Wrote @src='" + uri);
        } catch (IOException ioe) {
            throw new Docx4JException("Exception storing '" + filename + "', " + ioe.toString(), ioe);
        } finally {
            try {
                out.close();
            } catch (IOException ioe) {
                Log.e(TAG,"fail to close FileOutputStream",ioe);
            }
        }
        return uri;
    }

}
