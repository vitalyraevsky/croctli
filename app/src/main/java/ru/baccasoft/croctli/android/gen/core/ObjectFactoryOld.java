//
//package ru.baccasoft.croctli.android.gen.core;
//
//import ae.javax.xml.bind.annotation.XmlRegistry;
//
//
///**
// * This object contains factory methods for each
// * Java content interface and Java element interface
// * generated in the ru.baccasoft.croctli.android.gen.generated package.
// * <p>An ObjectFactory allows you to programatically
// * construct new instances of the Java representation
// * for XML content. The Java representation of XML
// * content can consist of schema derived interfaces
// * and classes representing the binding of schema
// * type definitions, element declarations and model
// * groups.  Factory methods for each of these are
// * provided in this class.
// *
// */
//@XmlRegistry
//public class ObjectFactoryOld {
//
//
//    /**
//     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.baccasoft.croctli.android.gen.generated
//     *
//     */
//    public ObjectFactoryOld() {
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfFavoriteValue }
//     *
//     */
//    public ArrayOfFavoriteValue createArrayOfFavoriteValue() {
//        return new ArrayOfFavoriteValue();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfUserNSI }
//     *
//     */
//    public ArrayOfUserNSI createArrayOfUserNSI() {
//        return new ArrayOfUserNSI();
//    }
//
//    /**
//     * Create an instance of {@link ViewCustomizationSet }
//     *
//     */
//    public ViewCustomizationSet createViewCustomizationSet() {
//        return new ViewCustomizationSet();
//    }
//
//    /**
//     * Create an instance of {@link TaskStateInSystem }
//     *
//     */
//    public TaskStateInSystem createTaskStateInSystem() {
//        return new TaskStateInSystem();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfBoolean }
//     *
//     */
//    public ArrayOfBoolean createArrayOfBoolean() {
//        return new ArrayOfBoolean();
//    }
//
//    /**
//     * Create an instance of {@link RibbonSettingSet }
//     *
//     */
//    public RibbonSettingSet createRibbonSettingSet() {
//        return new RibbonSettingSet();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTab }
//     *
//     */
//    public ArrayOfTab createArrayOfTab() {
//        return new ArrayOfTab();
//    }
//
//    /**
//     * Create an instance of {@link TypeActivity }
//     *
//     */
//    public TypeActivity createTypeActivity() {
//        return new TypeActivity();
//    }
//
//    /**
//     * Create an instance of {@link ProcessTypeSet }
//     *
//     */
//    public ProcessTypeSet createProcessTypeSet() {
//        return new ProcessTypeSet();
//    }
//
//    /**
//     * Create an instance of {@link UserNSI }
//     *
//     */
//    public UserNSI createUserNSI() {
//        return new UserNSI();
//    }
//
//    /**
//     * Create an instance of {@link DeepProcessType }
//     *
//     */
//    public DeepProcessType createDeepProcessType() {
//        return new DeepProcessType();
//    }
//
//    /**
//     * Create an instance of {@link TaskPackage }
//     *
//     */
//    public TaskPackage createTaskPackage() {
//        return new TaskPackage();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTaskUserUI }
//     *
//     */
//    public ArrayOfTaskUserUI createArrayOfTaskUserUI() {
//        return new ArrayOfTaskUserUI();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfDeepEntityProperty }
//     *
//     */
//    public ArrayOfDeepEntityProperty createArrayOfDeepEntityProperty() {
//        return new ArrayOfDeepEntityProperty();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfOperationResponse }
//     *
//     */
//    public ArrayOfOperationResponse createArrayOfOperationResponse() {
//        return new ArrayOfOperationResponse();
//    }
//
//    /**
//     * Create an instance of {@link ConditionOperatorSet }
//     *
//     */
//    public ConditionOperatorSet createConditionOperatorSet() {
//        return new ConditionOperatorSet();
//    }
//
//    /**
//     * Create an instance of {@link UIForm }
//     *
//     */
//    public UIForm createUIForm() {
//        return new UIForm();
//    }
//
//    /**
//     * Create an instance of {@link UserActivity }
//     *
//     */
//    public UserActivity createUserActivity() {
//        return new UserActivity();
//    }
//
//    /**
//     * Create an instance of {@link Classifier }
//     *
//     */
//    public Classifier createClassifier() {
//        return new Classifier();
//    }
//
//    /**
//     * Create an instance of {@link FavoriteUserProcess }
//     *
//     */
//    public FavoriteUserProcess createFavoriteUserProcess() {
//        return new FavoriteUserProcess();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfDelegat }
//     *
//     */
//    public ArrayOfDelegat createArrayOfDelegat() {
//        return new ArrayOfDelegat();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfFavoriteUserProcess }
//     *
//     */
//    public ArrayOfFavoriteUserProcess createArrayOfFavoriteUserProcess() {
//        return new ArrayOfFavoriteUserProcess();
//    }
//
//    /**
//     * Create an instance of {@link EntityProperty }
//     *
//     */
//    public EntityProperty createEntityProperty() {
//        return new EntityProperty();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTypeActivity }
//     *
//     */
//    public ArrayOfTypeActivity createArrayOfTypeActivity() {
//        return new ArrayOfTypeActivity();
//    }
//
//    /**
//     * Create an instance of {@link ProcessType }
//     *
//     */
//    public ProcessType createProcessType() {
//        return new ProcessType();
//    }
//
//    /**
//     * Create an instance of {@link ConditionOperator }
//     *
//     */
//    public ConditionOperator createConditionOperator() {
//        return new ConditionOperator();
//    }
//
//    /**
//     * Create an instance of {@link StringSet }
//     *
//     */
//    public StringSet createStringSet() {
//        return new StringSet();
//    }
//
//    /**
//     * Create an instance of {@link TaskUserUI }
//     *
//     */
//    public TaskUserUI createTaskUserUI() {
//        return new TaskUserUI();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfRibbonSetting }
//     *
//     */
//    public ArrayOfRibbonSetting createArrayOfRibbonSetting() {
//        return new ArrayOfRibbonSetting();
//    }
//
//    /**
//     * Create an instance of {@link Delegat }
//     *
//     */
//    public Delegat createDelegat() {
//        return new Delegat();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTask }
//     *
//     */
//    public ArrayOfTask createArrayOfTask() {
//        return new ArrayOfTask();
//    }
//
//    /**
//     * Create an instance of {@link ProcessSet }
//     *
//     */
//    public ProcessSet createProcessSet() {
//        return new ProcessSet();
//    }
//
//    /**
//     * Create an instance of {@link SpecialConditionSet }
//     *
//     */
//    public SpecialConditionSet createSpecialConditionSet() {
//        return new SpecialConditionSet();
//    }
//
//    /**
//     * Create an instance of {@link TaskStateInSystemSet }
//     *
//     */
//    public TaskStateInSystemSet createTaskStateInSystemSet() {
//        return new TaskStateInSystemSet();
//    }
//
//    /**
//     * Create an instance of {@link UserInSystem }
//     *
//     */
//    public UserInSystem createUserInSystem() {
//        return new UserInSystem();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfSystemUser }
//     *
//     */
//    public ArrayOfSystemUser createArrayOfSystemUser() {
//        return new ArrayOfSystemUser();
//    }
//
//    /**
//     * Create an instance of {@link Tab }
//     *
//     */
//    public Tab createTab() {
//        return new Tab();
//    }
//
//    /**
//     * Create an instance of {@link LogMessage }
//     *
//     */
//    public LogMessage createLogMessage() {
//        return new LogMessage();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfConflictData }
//     *
//     */
//    public ArrayOfConflictData createArrayOfConflictData() {
//        return new ArrayOfConflictData();
//    }
//
//    /**
//     * Create an instance of {@link TabSet }
//     *
//     */
//    public TabSet createTabSet() {
//        return new TabSet();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfAction }
//     *
//     */
//    public ArrayOfAction createArrayOfAction() {
//        return new ArrayOfAction();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfProcessType }
//     *
//     */
//    public ArrayOfProcessType createArrayOfProcessType() {
//        return new ArrayOfProcessType();
//    }
//
//    /**
//     * Create an instance of {@link SetOfAll }
//     *
//     */
//    public SetOfAll createSetOfAll() {
//        return new SetOfAll();
//    }
//
//    /**
//     * Create an instance of {@link SourceSystem }
//     *
//     */
//    public SourceSystem createSourceSystem() {
//        return new SourceSystem();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfActionBehavior }
//     *
//     */
//    public ArrayOfActionBehavior createArrayOfActionBehavior() {
//        return new ArrayOfActionBehavior();
//    }
//
//    /**
//     * Create an instance of {@link SystemUserSet }
//     *
//     */
//    public SystemUserSet createSystemUserSet() {
//        return new SystemUserSet();
//    }
//
//    /**
//     * Create an instance of {@link Task }
//     *
//     */
//    public Task createTask() {
//        return new Task();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfFilterCondition }
//     *
//     */
//    public ArrayOfFilterCondition createArrayOfFilterCondition() {
//        return new ArrayOfFilterCondition();
//    }
//
//    /**
//     * Create an instance of {@link Adapter }
//     *
//     */
//    public Adapter createAdapter() {
//        return new Adapter();
//    }
//
//    /**
//     * Create an instance of {@link Process }
//     *
//     */
//    public Process createProcess() {
//        return new Process();
//    }
//
//    /**
//     * Create an instance of {@link Attachment }
//     *
//     */
//    public Attachment createAttachment() {
//        return new Attachment();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfClassifier }
//     *
//     */
//    public ArrayOfClassifier createArrayOfClassifier() {
//        return new ArrayOfClassifier();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfSourceSystem }
//     *
//     */
//    public ArrayOfSourceSystem createArrayOfSourceSystem() {
//        return new ArrayOfSourceSystem();
//    }
//
//    /**
//     * Create an instance of {@link AdapterSet }
//     *
//     */
//    public AdapterSet createAdapterSet() {
//        return new AdapterSet();
//    }
//
//    /**
//     * Create an instance of {@link SpecialCondition }
//     *
//     */
//    public SpecialCondition createSpecialCondition() {
//        return new SpecialCondition();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfSpecialCondition }
//     *
//     */
//    public ArrayOfSpecialCondition createArrayOfSpecialCondition() {
//        return new ArrayOfSpecialCondition();
//    }
//
//    /**
//     * Create an instance of {@link ClassifierItemSet }
//     *
//     */
//    public ClassifierItemSet createClassifierItemSet() {
//        return new ClassifierItemSet();
//    }
//
//    /**
//     * Create an instance of {@link MetaAuth }
//     *
//     */
//    public MetaAuth createMetaAuth() {
//        return new MetaAuth();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfComment }
//     *
//     */
//    public ArrayOfComment createArrayOfComment() {
//        return new ArrayOfComment();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfEntityProperty }
//     *
//     */
//    public ArrayOfEntityProperty createArrayOfEntityProperty() {
//        return new ArrayOfEntityProperty();
//    }
//
//    /**
//     * Create an instance of {@link SystemUser }
//     *
//     */
//    public SystemUser createSystemUser() {
//        return new SystemUser();
//    }
//
//    /**
//     * Create an instance of {@link HandmadeDelegate }
//     *
//     */
//    public HandmadeDelegate createHandmadeDelegate() {
//        return new HandmadeDelegate();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfHandmadeDelegate }
//     *
//     */
//    public ArrayOfHandmadeDelegate createArrayOfHandmadeDelegate() {
//        return new ArrayOfHandmadeDelegate();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfAdapter }
//     *
//     */
//    public ArrayOfAdapter createArrayOfAdapter() {
//        return new ArrayOfAdapter();
//    }
//
//    /**
//     * Create an instance of {@link ClassifierItem }
//     *
//     */
//    public ClassifierItem createClassifierItem() {
//        return new ClassifierItem();
//    }
//
//    /**
//     * Create an instance of {@link FilterCondition }
//     *
//     */
//    public FilterCondition createFilterCondition() {
//        return new FilterCondition();
//    }
//
//    /**
//     * Create an instance of {@link Device }
//     *
//     */
//    public Device createDevice() {
//        return new Device();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfUIForm }
//     *
//     */
//    public ArrayOfUIForm createArrayOfUIForm() {
//        return new ArrayOfUIForm();
//    }
//
//    /**
//     * Create an instance of {@link ActionBehavior }
//     *
//     */
//    public ActionBehavior createActionBehavior() {
//        return new ActionBehavior();
//    }
//
//    /**
//     * Create an instance of {@link DeepProcess }
//     *
//     */
//    public DeepProcess createDeepProcess() {
//        return new DeepProcess();
//    }
//
//    /**
//     * Create an instance of {@link Action }
//     *
//     */
//    public Action createAction() {
//        return new Action();
//    }
//
//    /**
//     * Create an instance of {@link FavoriteValue }
//     *
//     */
//    public FavoriteValue createFavoriteValue() {
//        return new FavoriteValue();
//    }
//
//    /**
//     * Create an instance of {@link ClassifierSet }
//     *
//     */
//    public ClassifierSet createClassifierSet() {
//        return new ClassifierSet();
//    }
//
//    /**
//     * Create an instance of {@link CommentSet }
//     *
//     */
//    public CommentSet createCommentSet() {
//        return new CommentSet();
//    }
//
//    /**
//     * Create an instance of {@link ViewCustomization }
//     *
//     */
//    public ViewCustomization createViewCustomization() {
//        return new ViewCustomization();
//    }
//
//    /**
//     * Create an instance of {@link RibbonSettings }
//     *
//     */
//    public RibbonSettings createRibbonSettings() {
//        return new RibbonSettings();
//    }
//
//    /**
//     * Create an instance of {@link SourceSystemSet }
//     *
//     */
//    public SourceSystemSet createSourceSystemSet() {
//        return new SourceSystemSet();
//    }
//
//    /**
//     * Create an instance of {@link EntityPropertySet }
//     *
//     */
//    public EntityPropertySet createEntityPropertySet() {
//        return new EntityPropertySet();
//    }
//
//    /**
//     * Create an instance of {@link AppVersion }
//     *
//     */
//    public AppVersion createAppVersion() {
//        return new AppVersion();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfConditionOperator }
//     *
//     */
//    public ArrayOfConditionOperator createArrayOfConditionOperator() {
//        return new ArrayOfConditionOperator();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTaskStateInSystem }
//     *
//     */
//    public ArrayOfTaskStateInSystem createArrayOfTaskStateInSystem() {
//        return new ArrayOfTaskStateInSystem();
//    }
//
//    /**
//     * Create an instance of {@link HandmadeDelegateSet }
//     *
//     */
//    public HandmadeDelegateSet createHandmadeDelegateSet() {
//        return new HandmadeDelegateSet();
//    }
//
//    /**
//     * Create an instance of {@link TaskUserUISet }
//     *
//     */
//    public TaskUserUISet createTaskUserUISet() {
//        return new TaskUserUISet();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfLogMessage }
//     *
//     */
//    public ArrayOfLogMessage createArrayOfLogMessage() {
//        return new ArrayOfLogMessage();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfUserActivity }
//     *
//     */
//    public ArrayOfUserActivity createArrayOfUserActivity() {
//        return new ArrayOfUserActivity();
//    }
//
//    /**
//     * Create an instance of {@link TaskActor }
//     *
//     */
//    public TaskActor createTaskActor() {
//        return new TaskActor();
//    }
//
//    /**
//     * Create an instance of {@link Comment }
//     *
//     */
//    public Comment createComment() {
//        return new Comment();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfUserInSystem }
//     *
//     */
//    public ArrayOfUserInSystem createArrayOfUserInSystem() {
//        return new ArrayOfUserInSystem();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfTaskActor }
//     *
//     */
//    public ArrayOfTaskActor createArrayOfTaskActor() {
//        return new ArrayOfTaskActor();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfProcess }
//     *
//     */
//    public ArrayOfProcess createArrayOfProcess() {
//        return new ArrayOfProcess();
//    }
//
//    /**
//     * Create an instance of {@link DeepTask }
//     *
//     */
//    public DeepTask createDeepTask() {
//        return new DeepTask();
//    }
//
//    /**
//     * Create an instance of {@link LogMessageSet }
//     *
//     */
//    public LogMessageSet createLogMessageSet() {
//        return new LogMessageSet();
//    }
//
//    /**
//     * Create an instance of {@link ConflictData }
//     *
//     */
//    public ConflictData createConflictData() {
//        return new ConflictData();
//    }
//
//    /**
//     * Create an instance of {@link ItemField }
//     *
//     */
//    public ItemField createItemField() {
//        return new ItemField();
//    }
//
//    /**
//     * Create an instance of {@link TaskSet }
//     *
//     */
//    public TaskSet createTaskSet() {
//        return new TaskSet();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfViewCustomization }
//     *
//     */
//    public ArrayOfViewCustomization createArrayOfViewCustomization() {
//        return new ArrayOfViewCustomization();
//    }
//
//    /**
//     * Create an instance of {@link Filter }
//     *
//     */
//    public Filter createFilter() {
//        return new Filter();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfItemField }
//     *
//     */
//    public ArrayOfItemField createArrayOfItemField() {
//        return new ArrayOfItemField();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfString }
//     *
//     */
//    public ArrayOfString createArrayOfString() {
//        return new ArrayOfString();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfClassifierItem }
//     *
//     */
//    public ArrayOfClassifierItem createArrayOfClassifierItem() {
//        return new ArrayOfClassifierItem();
//    }
//
//    /**
//     * Create an instance of {@link OperationResponse }
//     *
//     */
//    public OperationResponse createOperationResponse() {
//        return new OperationResponse();
//    }
//
//    /**
//     * Create an instance of {@link DeepEntityProperty }
//     *
//     */
//    public DeepEntityProperty createDeepEntityProperty() {
//        return new DeepEntityProperty();
//    }
//
//}
