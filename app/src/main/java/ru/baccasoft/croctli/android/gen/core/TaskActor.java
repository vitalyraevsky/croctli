
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for TaskActor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskActor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsComlete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsRevoked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SystemUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DelegateUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskActor", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "isComlete",
    "isRevoked",
    "systemUserId",
    "delegateUserId"
})
@DatabaseTable(tableName = "task_actor")
@JsonIgnoreProperties({"task"})
public class TaskActor {

    public static final String TASK_ID_FIELD = "task_foreign_id_field";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = TASK_ID_FIELD)
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @DatabaseField(id = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsComlete")
    @XmlElement(name = "IsComlete", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isComlete;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("IsRevoked")
    @XmlElement(name = "IsRevoked", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean isRevoked;

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("SystemUserId")
    @XmlElement(name = "SystemUserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String systemUserId;

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("DelegateUserId")
    @XmlElement(name = "DelegateUserId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String delegateUserId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the isComlete property.
     * 
     */
    public boolean isIsComlete() {
        return isComlete;
    }

    /**
     * Sets the value of the isComlete property.
     * 
     */
    public void setIsComlete(boolean value) {
        this.isComlete = value;
    }

    /**
     * Gets the value of the isRevoked property.
     * 
     */
    public boolean isIsRevoked() {
        return isRevoked;
    }

    /**
     * Sets the value of the isRevoked property.
     * 
     */
    public void setIsRevoked(boolean value) {
        this.isRevoked = value;
    }

    /**
     * Gets the value of the systemUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemUserId() {
        return systemUserId;
    }

    /**
     * Sets the value of the systemUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemUserId(String value) {
        this.systemUserId = value;
    }

    /**
     * Gets the value of the delegateUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegateUserId() {
        return delegateUserId;
    }

    /**
     * Sets the value of the delegateUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegateUserId(String value) {
        this.delegateUserId = value;
    }

}
