package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.List;

import ru.baccasoft.croctli.android.utils.DateTimeWrapper;
import ru.baccasoft.croctli.android.utils.DateWrapper;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.IntScalar;

import static ru.baccasoft.croctli.android.view.util.ViewUtils.SCALAR_TYPE;

public class ScalarArrayWithTitle extends LinearLayout {
    private Activity mActivity;
    private Context mContext;

    public ScalarArrayWithTitle(Activity activity, String title, SCALAR_TYPE scalarType,
                            List<String> values, boolean isReadOnly, String entityPropertyId) {
        super(activity.getApplicationContext());
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();

        TextView titleView = ViewUtils.makeTitleTextView(mContext, title, null);

        ViewGroup valuesView = getValuesLayout(scalarType, values, isReadOnly, entityPropertyId);

        addView(titleView);
        addView(valuesView);
    }

    private ViewGroup getValuesLayout(SCALAR_TYPE scalarType, List<String> values,
                                      boolean isReadOnly, String entityPropertyId) {
        LinearLayout ll = ViewUtils.makeHorizontalContainer(mContext);

        switch (scalarType) {
            case STRING:
                for (String s : values) {
                    ll.addView(ViewUtils.makeStringScalarEditText(mContext, s, entityPropertyId));
                }
                break;
            case TEXT:
                for (String s : values) {
                    ll.addView(ViewUtils.makeStringScalarEditText(mContext, s, entityPropertyId));
                }
                break;
            case INT:
                for (String s : values) {
                    ll.addView(new IntScalar(mContext, s, isReadOnly));
                }
                break;
            case FLOAT:
                for (String s : values) {
                    ll.addView(ViewUtils.makeFloatScalar(mContext, s, isReadOnly));
                }
                break;
            case DECIMAL:
                for (String s : values) {
                    ll.addView(ViewUtils.makeFloatScalar(mContext, s, isReadOnly));
                }
                break;
            case BOOLEAN:
                for (String s : values) {
                    ll.addView(ViewUtils.makeFloatScalar(mContext, s, isReadOnly));
                }
                break;
            case DATE:
                for (String s : values) {
                    DateWrapper dateTime = new DateWrapper(s);
                    ll.addView(ViewUtils.makeDateScalar(mActivity, dateTime, isReadOnly, entityPropertyId));
                }
                break;
            case DATE_TIME:
                for (String s : values) {
                    DateTimeWrapper dateTime = new DateTimeWrapper(s);
                    ll.addView(ViewUtils.makeDateTimeScalar(mActivity, dateTime, isReadOnly, entityPropertyId));
                }
                break;
            default:
                throw new IllegalArgumentException("scalar type:\'" + scalarType.toString() + " not supported");

        }
        return ll;
    }



}
