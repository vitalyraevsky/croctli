package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfSourceSystem;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.SourceSystemSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 * GD, GM
 */
public class SourceSystemAPI implements IRestApiCall<SourceSystemSet, ArrayOfSourceSystem, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = SourceSystemAPI.class.getSimpleName();

    @Override
    public SourceSystemSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/SourceSystem/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = false;

        return RestClient.fillJsonClass(SourceSystemSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/SourceSystem/Delete/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        final boolean needAuth = false;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfSourceSystem objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
