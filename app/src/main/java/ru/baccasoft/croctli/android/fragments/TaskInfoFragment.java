package ru.baccasoft.croctli.android.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.SQLException;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.TaskBrowseActivity;
import ru.baccasoft.croctli.android.adapters.TasksAdapter;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.view.event.SetFavoriteEvent;
import ru.baccasoft.croctli.android.view.util.CrocUtils;

/**
 *
 10 дек. 2014 г., в 14:26, Fetisov Alexey <AFetisov@croc.ru> написал(а):
 Подписал убираем, делаем как сейчас в IOS
 Краткое содержание, документ, организация и срочность.
 */

public class TaskInfoFragment extends Fragment {
    @SuppressWarnings("unused")
    private static final String TAG = TaskInfoFragment.class.getSimpleName();

    private String taskId;
    private String firstTaskName;
    private String firstTaskDate;
    private Task task = null;


    private TextView taskCreateDateView, taskNameView, descriptionView,
            documentView, organisationView, urgencyView;
    private ImageView favoritesIcon;

    public static TaskInfoFragment newInstance(String taskId) {
        TaskInfoFragment f = new TaskInfoFragment();

        Bundle args = new Bundle();
        args.putString(TaskBrowseActivity.TASK_ID_ARGS_KEY, taskId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null || !args.containsKey(TaskBrowseActivity.TASK_ID_ARGS_KEY))
            throw new IllegalArgumentException("taskId cannot be null");

        taskId = args.getString(TaskBrowseActivity.TASK_ID_ARGS_KEY);

        try {
            task = App.getDatabaseHelper().getTasksDao().queryForId(taskId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_info_fragment, container, false);

        taskCreateDateView = (TextView) view.findViewById(R.id.task_create_date);
        taskNameView = (TextView) view.findViewById(R.id.task_name);
        descriptionView = (TextView) view.findViewById(R.id.description);
        documentView = (TextView) view.findViewById(R.id.document);
        organisationView = (TextView) view.findViewById(R.id.organisation);
        urgencyView = (TextView) view.findViewById(R.id.urgency);
        favoritesIcon = (ImageView) view.findViewById(R.id.favorites_icon);


        firstTaskName = CrocUtils.getValueByAttributeCode("TaskName", task);
        firstTaskDate = CrocUtils.getValueByAttributeCode("TaskCreateDate", task);
        taskNameView.setText(firstTaskName);
        taskCreateDateView.setText(firstTaskDate);
        task = null;

        new syncTask().execute(taskId);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                TaskEditorDialogFragment dialog = TaskEditorDialogFragment.newInstance(taskId);
                dialog.show(fm, "task_info_fragment");
            }
        });

        return view;
    }

    class syncTask extends AsyncTask<String, Void, syncTask.TaskInfoHolder> {
        @Override
        protected TaskInfoHolder doInBackground(String... params) {
            try {
                final String taskId = params[0];
                TaskInfoHolder taskInfo = new TaskInfoHolder();

                task = App.getDatabaseHelper().getTasksDao().queryForId(taskId);

                taskInfo.task = task;
                taskInfo.createDate = CrocUtils.getValueByAttributeCode("TaskCreateDate", task);
                taskInfo.description = CrocUtils.getValueByAttributeCode("ProcessTypeDescription", task);
                taskInfo.taskName = CrocUtils.getValueByAttributeCode("TaskName", task);
                taskInfo.document = CrocUtils.getValueByAttributeCode("ProcessName", task);

                //FIXME: сильно неуверен, что именно это значение нужно показывать
                String taskOrganization;
                EntityProperty ep = TableUtils.findEpByDisplayGroup(taskId, "Организация");
                if (ep == null || ep.getValue() == null) {
                    taskOrganization = "";
                } else {
                    taskOrganization = ep.getValue();
                }
                taskInfo.organisation = taskOrganization;

                //FIXME: сильно неуверен, что отсюда нужно брать значения
                String taskUrgency;
                ep = TableUtils.findEpByDisplayGroup(taskId, "Срочность");
                if (ep == null || ep.getValue() == null) {
                    taskUrgency = "";
                } else {
                    taskUrgency = ep.getValue();
                }
                taskInfo.urgency = taskUrgency;

                return taskInfo;
            } catch (SQLException e) {
                String message = "cant read from task db";
                TableUtils.createLogMessage(message, e);
                Log.e(TAG, message, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(final TaskInfoHolder taskInfo) {

            taskCreateDateView.setText(taskInfo.createDate);
            taskNameView.setText(taskInfo.taskName);
            descriptionView.setText(taskInfo.description);
            documentView.setText(taskInfo.document);

            organisationView.setText(taskInfo.organisation);
            urgencyView.setText(taskInfo.urgency);

            favoritesIcon.setVisibility(View.VISIBLE);
            TasksAdapter.updateFavoriteFlag(favoritesIcon, taskInfo.task);
            favoritesIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    taskInfo.task.setIsFavourite(!taskInfo.task.isIsFavourite());
                    TasksAdapter.updateFavoriteFlag(favoritesIcon, taskInfo.task);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                App.getDatabaseHelper().getTasksDao().update(taskInfo.task);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    Log.d(TAG, "FAVORITE FLAG NOW ID: " + taskInfo.task.isIsFavourite());
                }
            });
        }

        public class TaskInfoHolder {
            public String createDate;
            public String taskName;
            public String description;
            public String document;
            public String organisation;
            public String urgency;

            public Task task;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    public void onEvent(SetFavoriteEvent event) throws SQLException {
        Log.d(TAG,"get event");
        task = App.getDatabaseHelper().getTasksDao().queryForId(taskId);
        TasksAdapter.updateFavoriteFlag(favoritesIcon, task);
    }
}
