
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Device complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Device">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UseAPNS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="APNSId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Device", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "useAPNS",
    "apnsId",
    "language"
})
public class Device {

    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected String id;
    @XmlElement(name = "UseAPNS", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean useAPNS;
    @XmlElement(name = "APNSId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected String apnsId;
    @XmlElement(name = "Language", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected String language;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the useAPNS property.
     * 
     */
    public boolean isUseAPNS() {
        return useAPNS;
    }

    /**
     * Sets the value of the useAPNS property.
     * 
     */
    public void setUseAPNS(boolean value) {
        this.useAPNS = value;
    }

    /**
     * Gets the value of the apnsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPNSId() {
        return apnsId;
    }

    /**
     * Sets the value of the apnsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPNSId(String value) {
        this.apnsId = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
