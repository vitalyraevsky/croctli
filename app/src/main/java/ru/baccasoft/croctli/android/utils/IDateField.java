package ru.baccasoft.croctli.android.utils;

import java.util.Date;

/**
 * Много колдунства с датами и временами.
 *
 * Исторически сложилось, что в проекте даты могут быть представлены в трех видах (если не считать XMLGregorianCalendar)
 *
 * Лирическое отступление: важно понимать, что каждый сгенерированный на основе xsd-схемы класс служит двум задачам:
 * для маппинга json-данных на объект и маппинга объекта в базу.
 *
 * <b>XMLGregorianCalendar</b> и <b>Строковая дата</b> (StringDate)
 * Это касается хранения приходящих json-данных в объектах.
 *
 * Данные типы даты используются в классах, сгенерированных на основе полученной от Крока схемы (xsd).
 * К сожалению, из-за сложностей интегрирования адресного пространства javax.xml.bind.annotation
 * в андроид-проекты, данные этого типа пришлось мапить в переменные типа String.
 * Т.е. в каждом сгенерированном классе в дополнение к каждому полю типа XMLGregorianCalendar
 * есть дублирующее поле с суффиксом 'String', в которое и кладутся приходящие в json данные о дате.
 * Например:
 *
 * @XmlElement(name = "ChangeDate", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
 * @XmlSchemaType(name = "dateTime")
 * protected XMLGregorianCalendar changeDate;   // <-- это сгенерированное поле, никакой роли не играющее.
 *                                              //        осталось, чтобы меньше ручной работы делать
 * @JsonProperty("ChangeDate")
 * protected String changeDateString;           // <-- это созданное вручную поле, специально анотированное,
 *                                              //      чтобы соответствующая дата из json-данных автоматически
 *                                              //      в него ложилась средствами Jackson'а
 * Пример даты:
 * 2014-09-10T23:28:30.997Z
 *
 *
 * <b>DateTime</b>
 * Это касается хранения объектов с данными в локальной БД (далее просто "БД")
 * Работа с БД организованна через orm 'OrmLite'. Мапинг объектов в БД осуществляется анотациями.
 * Пример:
 *
 * @DatabaseField(columnName = "change_date", dataType = DataType.STRING)
 * protected String serverTimeString;
 *
 * Прежде все даты сохранялись в БД в том же виде, что и хранились в классе, т.е. строкой (см. выше пример с changeDateString)
 * Из-за сложностей реализации поиска/сравнения в БД по дате, хранящейся в БД виде String,
 * начался переход на использование формата DateTime
 *
 * Для этого в каждый сгенерированный на основе схемы класс для каждого поля с датой добавляется еще одно поле типа DateTime
 * @DatabaseField(columnName = "change_date_time", dataType = DataType.DATE_TIME)
 * protected DateTime changeDateTime;
 *
 * Таким образом в соответствующей таблице будет создано дополнительное поле "change_date_time",
 * куда будет ложиться эта дата при мапинге инстанса класса в БД.
 * Однако в инстансе класса это поле не заполняется автоматически при натягивании json-данных на объект.
 * Поэтому при создании/обновлении данных в БД необходимо явно помещать в это поле значение даты
 * в переопределенных методах create()/update() в соответствующем классе [имя_объекта]DAO (например, AdapterDAO).
 * Пример:
 *
 * public class TasksDao extends BaseDaoImpl<Task, String> {
 * ...
 *  @Override
 *  public int create(Task data) throws SQLException {
 *          data.setChangeDateTime(new DateTime(data.getChangeDateString(), DateTimeZone.UTC));
 *          int ret = super.create(data);
 *          ...
 *          return ret;
 *  }
 * }
 *
 * Формат дат и времени
 * ====================
 *
 * Формат хранения и обмена с сервером даты (ScalarType = date) YYYY-MM-DD, даты со временем (ScalarType = dateTime) YYYY-MM-DDTHH:MM(:SS(.mmm))(zzz).
 *
 * Для визуализации используется формат в соответствии с региональными настройками ОС пользователя.
 *
 * Если атрибут со ScalarType = date передан в более точном формате, то лишнее игнорируется.
 *
 * Если значение не соответствует формату, то атрибут отображается как ScalarType = string.
 *
 * Перед отображением dateTime-полей их необходимо перевести в часовой пояс клиента. Поля с типом date в часовой пояс клиента не переводятся.
 */
public interface IDateField {
    String toGuiString();
    String toRestString();
    Date smartGetTime();
    boolean isValidDateSet();
}
