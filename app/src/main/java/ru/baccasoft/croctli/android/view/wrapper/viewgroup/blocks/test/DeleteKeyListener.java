package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;

import android.text.Editable;
import android.text.InputType;
import android.text.method.BaseKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

/**
 * Нужен только для того, чтобы при выборе исполнителей отключить ввод любых символов,
 * кроме удаления.
 */

public class DeleteKeyListener extends BaseKeyListener {
    @SuppressWarnings("unused")
    private static final String TAG = DeleteKeyListener.class.getSimpleName();

    @Override
    public int getInputType() {
        return InputType.TYPE_CLASS_TEXT;
    }

    @Override
    public boolean onKeyUp(View view, Editable content, int keyCode, KeyEvent event) {
        return super.onKeyUp(view, content, keyCode, event);
    }

    @Override
    public boolean onKeyDown(View view, Editable content, int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown");
        boolean handled;
        switch (keyCode) {
            case KeyEvent.KEYCODE_DEL:
                handled = backspace(view, content, keyCode, event);
                break;
            case KeyEvent.KEYCODE_FORWARD_DEL:
                handled = forwardDelete(view, content, keyCode, event);
                break;
            default:
                handled = false;
                break;
        }

        if (handled) {
            adjustMetaAfterKeypress(content);
        }

        return false;
    }
}
