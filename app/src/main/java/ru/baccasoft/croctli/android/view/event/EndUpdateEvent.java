package ru.baccasoft.croctli.android.view.event;

/**
 * Created by 123 on 03.06.2015.
 */
public class EndUpdateEvent {
    private boolean result;

    public EndUpdateEvent(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }
}
