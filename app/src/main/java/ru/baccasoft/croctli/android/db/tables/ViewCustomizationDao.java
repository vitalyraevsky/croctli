package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ViewCustomization;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ViewCustomizationDao extends BaseDaoImpl<ViewCustomization, String> {
    @SuppressWarnings("unused")
    private static final String TAG = ViewCustomizationDao.class.getSimpleName();

    public ViewCustomizationDao(ConnectionSource connectionSource, Class<ViewCustomization> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<ViewCustomization> getMenuItems() throws SQLException {
        QueryBuilder<ViewCustomization, String> q = queryBuilder();

        // название поля зашито во ViewCustomization
        q.where().eq("displayInSort", Boolean.TRUE);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ViewCustomization> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    public List<String> getDisplayInRibbonKeys() throws SQLException {
        QueryBuilder<ViewCustomization, String> q = queryBuilder();

        // название поля зашито во ViewCustomization
        q.where().eq("displayInRibbon", Boolean.TRUE);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ViewCustomization> preparedQuery = q.prepare();

        List<ViewCustomization> tmpList = query(preparedQuery);
        List<String> ret = new ArrayList<String>();

        for (ViewCustomization v : tmpList)
            ret.add(v.getKeyName());

        return ret;
    }

    public List<String> getDisplayInTaskArea() throws SQLException {
        QueryBuilder<ViewCustomization, String> q = queryBuilder();

        // название поля зашито во ViewCustomization
        q.where().eq("displayInTaskArea", Boolean.TRUE);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ViewCustomization> preparedQuery = q.prepare();

        List<ViewCustomization> tmpList = query(preparedQuery);
        List<String> ret = new ArrayList<String>();

        for (ViewCustomization v : tmpList)
            ret.add(v.getKeyName());

        return ret;
    }

    public List<String> getDisplayNameInTaskArea() throws SQLException {
        QueryBuilder<ViewCustomization, String> q = queryBuilder();

        // название поля зашито во ViewCustomization
        q.where().eq("displayNameInTaskArea", Boolean.TRUE);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<ViewCustomization> preparedQuery = q.prepare();

        List<ViewCustomization> tmpList = query(preparedQuery);
        List<String> ret = new ArrayList<String>();

        for (ViewCustomization v : tmpList)
            ret.add(v.getKeyName());

        return ret;
    }
}
