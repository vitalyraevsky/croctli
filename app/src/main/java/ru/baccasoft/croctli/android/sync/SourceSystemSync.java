package ru.baccasoft.croctli.android.sync;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.SourceSystem;
import ru.baccasoft.croctli.android.gen.core.SourceSystemSet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.SourceSystemAPI;

import java.util.List;


public class SourceSystemSync implements ISync {

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        SourceSystemAPI sourceSystemAPI = new SourceSystemAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<SourceSystemSet, SourceSystem>();

        @SuppressWarnings("unchecked")
        List<SourceSystem> sourceSystems = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, sourceSystemAPI);

        for(SourceSystem ss : sourceSystems) {
            TableUtils.createOrUpdateSourceSystem(ss);
        }
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        SourceSystemAPI sourceSystemAPI = new SourceSystemAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> sourceSystemIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, sourceSystemAPI);

        TableUtils.deleteSourceSystem(sourceSystemIds);
    }
}
