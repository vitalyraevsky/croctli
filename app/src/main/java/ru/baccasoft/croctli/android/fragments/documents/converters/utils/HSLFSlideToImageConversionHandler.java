package ru.baccasoft.croctli.android.fragments.documents.converters.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;

import net.pbdavey.awt.Graphics2D;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import and.awt.Dimension;
import and.awt.geom.AffineTransform;
import and.org.apache.poi.hslf.model.Slide;


public class HSLFSlideToImageConversionHandler {

    private static final String TAG = HSLFSlideToImageConversionHandler.class.getSimpleName();

    private static final double ZOOM_FACTOR = 2;

    private String imageDirPath;
    private Activity activity;
    private Handler handler;

    public HSLFSlideToImageConversionHandler(String imageDirPath, Activity activity, Handler handler) {
        this.imageDirPath = imageDirPath;
        this.activity = activity;
        this.handler = handler;
    }

    public String convertSlideToImage(Slide slide,String suggestedName, Dimension pgsize) {

        AffineTransform at = new AffineTransform();
        at.setToScale( ZOOM_FACTOR , ZOOM_FACTOR );

        Bitmap bmp = Bitmap.createBitmap((int) pgsize.getWidth(),
                (int) pgsize.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmp);
        Paint paint = new Paint();
        paint.setColor(android.graphics.Color.WHITE);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        canvas.drawPaint(paint);

        final Graphics2D graphics2d = new Graphics2D(canvas);
        final AtomicBoolean isCanceled = new AtomicBoolean(false);
        slide.draw(graphics2d,isCanceled,handler,0);

        File folder = activity.getDir(imageDirPath, Context.MODE_WORLD_READABLE);
        File slideImage = new File( folder, suggestedName+".png" );
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(slideImage);

            bmp.compress( Bitmap.CompressFormat.PNG, 100, out );
            out.close();
            return slideImage.getPath();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "fail to save slide img : " + suggestedName, e);
        } catch (IOException e) {
            Log.e(TAG,"fail to save slide img : " + suggestedName,e);
        } finally {
            try {
                out.close();
            } catch (IOException ioe) {
                Log.e(TAG,"fail to close FileOutputStream",ioe);
            }
        }

        return null;
    }

}
