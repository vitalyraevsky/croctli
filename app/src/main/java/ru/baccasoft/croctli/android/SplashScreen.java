package ru.baccasoft.croctli.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import ru.baccasoft.croctli.android.updater.FirstSyncStepOneTask;
import ru.baccasoft.croctli.android.updater.StepOneResult;


public class SplashScreen extends Activity {
    @SuppressWarnings("unused")
    private static final String TAG = SplashScreen.class.getSimpleName();

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_new);
        progressBar = (ProgressBar) findViewById(R.id.splash_progress_bar);

        new MyFirstSyncStepOne().execute();
    }

    private class MyFirstSyncStepOne extends FirstSyncStepOneTask {
        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            progressBar.setProgress(progress);
        }

        @Override
        protected void onPostExecute(StepOneResult result) {

            if( result.status == StepOneResult.FIRST_SYNC_STATUS.SUCCESS ||  result.status == StepOneResult.FIRST_SYNC_STATUS.SKIP ){

                App.setStepOneResult(result);

                Intent intent = new Intent(SplashScreen.this, TasksActivity.class);
                startActivity(intent);
                finish();

            } else {

                new AlertDialog.Builder(SplashScreen.this)
                        .setMessage(R.string.no_connection)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();
                            }
                        })
                        .show();


            }


        }
    }
}
