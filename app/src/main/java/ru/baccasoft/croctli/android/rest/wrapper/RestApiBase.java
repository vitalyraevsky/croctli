package ru.baccasoft.croctli.android.rest.wrapper;

/**
 * Начало базовой имплементации интерфейса
 */
public abstract class RestApiBase<T> {
    private String apiUrlSuf;
    private boolean needAuth;

    Class<T> type;
    T t;

    protected RestApiBase(Class<T> type, String apiUrlSuf, boolean needAuth) {
        this.type = type;
        this.apiUrlSuf = apiUrlSuf;
        this.needAuth = needAuth;
    }

//    public T list() {
//        final T data = RestClient.fillJsonClass(t, apiUrlSuf, needAuth);
//        // TODO: переделать на сохранение long'а вместо строки для времени
//        final String serverDate = tabSet.getServerTimeString();
//        for (Tab t : tabSet.getArrayOf().getItem()) {
//            t.setServerTime(serverDate);
//        }
//        return tabSet;
//    }

}
