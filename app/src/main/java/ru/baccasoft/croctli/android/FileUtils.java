package ru.baccasoft.croctli.android;


import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;

public final class FileUtils {


    /**
     * Копировать файл в дерикторию кэша и вернуть объект File на кэшированный файл
     * @param cxt - Контектс
     * @param path - Путь откуда взять файл для кэширования
     * @param newName - Имя нового файла
     * @param update - Перезаписать кэшированный файл
     * @return
     * @throws IOException
     */
    public static File cacheFile(Context cxt, String path, String newName , boolean update ) throws IOException {

        File cached = new File( cxt.getCacheDir() + File.separator + newName );

        if( cached.exists() ){
            if(update){
                cached.delete();
            } else {
                return cached;
            }
        }

        File source = new File(path);

        copyFile( source, cached );

        return cached;
    }

    /**
     * Скопировать файл из source в dest
     * @param source
     * @param dest
     * @throws IOException
     */
    public static void copyFile(File source, File dest) throws IOException {
        FileChannel sourceChannel = new FileInputStream(source).getChannel();
        try {
            FileChannel destChannel = new FileOutputStream(dest).getChannel();
            try {
                destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
            } finally {
                destChannel.close();
            }
        } finally {
            sourceChannel.close();
        }
    }

    /**
     * Записать строку в файл
     * @param filePath
     * @param fileName
     * @param content
     * @return
     * @throws IOException
     */
    public static File writeStringToFile( File filePath, String fileName,String content ) throws IOException {

        File file = new File( filePath, fileName );

        if(file.exists())
            return file;

        BufferedWriter bfw = new BufferedWriter( new FileWriter( file, false) );
        bfw.write(content);
        bfw.close();

        return file;
    }

    /**
     * Читаем фаил в строчку
     * @param file
     * @return
     * @throws IOException
     */
    public static String readFile( String file ) throws IOException {

        BufferedReader reader = new BufferedReader( new FileReader(file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        return stringBuilder.toString();
    }
}
