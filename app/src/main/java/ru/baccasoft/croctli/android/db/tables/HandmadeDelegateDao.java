package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.HandmadeDelegate;

import java.sql.SQLException;

public class HandmadeDelegateDao extends BaseDaoImpl<HandmadeDelegate, String> {
    public HandmadeDelegateDao(ConnectionSource connectionSource, Class<HandmadeDelegate> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
