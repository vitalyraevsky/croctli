package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfUIForm;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class UIFormAPI implements IRestApiCall<ArrayOfUIForm, Void, Void, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = UIFormAPI.class.getSimpleName();

    /**
     * Прим.: особый вызов, плюет на входные параметры
     * @param dateFrom игнорит
     * @param dateTo игнорит
     * @param max игнорит
     */
    @Override
    public ArrayOfUIForm modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = "/UIForm/ListAll";
        final boolean needAuth = false;

        return RestClient.fillJsonClass(ArrayOfUIForm.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
