package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ConflictData;
import ru.baccasoft.croctli.android.gen.core.Task;

import java.sql.SQLException;
import java.util.List;

public class ConflictDataDao extends BaseDaoImpl<ConflictData, Integer> {
    public ConflictDataDao(ConnectionSource connectionSource, Class<ConflictData> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<ConflictData> getByTask(Task task) throws SQLException {
        QueryBuilder<ConflictData, Integer> q = queryBuilder();
        q.where().eq(ConflictData.TASK_ID_FIELD, task);
        return query(q.prepare());
    }
}
