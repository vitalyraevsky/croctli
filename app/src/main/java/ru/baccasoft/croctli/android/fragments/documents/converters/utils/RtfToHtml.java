package ru.baccasoft.croctli.android.fragments.documents.converters.utils;

import android.util.Log;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Writer;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf.Convert;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf.DomEngine;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf.Engine;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf.RtfLexer;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.rtf.RtfParser;

/**
 * Rtf converter cloned from github project https://github.com/andriy-gerasika/rtf2xml
 */
public class RtfToHtml {

    private static final String TAG = RtfToHtml.class.getSimpleName();

    private RtfLexer lexer;
    private Writer out;

    private RtfToHtml(RtfLexer lexer, Writer out){
        this.lexer = lexer;
        this.out = out;
    }

    public static RtfToHtml create(String path, Writer out) throws IOException {
        return create(new ANTLRFileStream(path), out);
    }

    public static RtfToHtml create(ANTLRFileStream antlrFileStream, Writer out) {
        RtfLexer lexer = new RtfLexer(antlrFileStream);
        return create(lexer,out);
    }

    public static RtfToHtml create(RtfLexer lexer, Writer out) {
        return new RtfToHtml(lexer,out);
    }

    public void print(){
        CommonTokenStream stream  = new CommonTokenStream(lexer);

        RtfParser parser = new RtfParser(stream);
        try {

            CommonTree tree = (CommonTree) parser.rtf().getTree();
            CommonTreeNodeStream s = new CommonTreeNodeStream(tree);

            DomEngine engine = new DomEngine();

            Convert convert = new Convert(s, engine);
            convert.rtf();

            DOMSource domSource = new DOMSource(engine.getDocument());

            StreamResult result = new StreamResult(out);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform( domSource, result );


        } catch (RecognitionException e) {
            Log.e(TAG,"RecognitionException exception",e);
        } catch (Exception e) {
            Log.e(TAG, "Dom exception", e);
        }


    }
}
