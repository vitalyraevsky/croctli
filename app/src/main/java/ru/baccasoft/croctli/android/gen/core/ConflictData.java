
package ru.baccasoft.croctli.android.gen.core;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConflictData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConflictData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConflictSignal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ConflictMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConflictData", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "conflictSignal",
    "conflictMessage"
})
@DatabaseTable(tableName = "conflict_data")
public class ConflictData implements Serializable {
    public static final String TASK_ID_FIELD = "fk_task";

//    @DatabaseField(id = true, generatedId = true, columnName = "id", dataType = DataType.INTEGER)
//    protected int id;

    @XmlElement(name = "ConflictSignal", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    @DatabaseField(columnName = "conflict_signal", dataType = DataType.INTEGER)
    protected int conflictSignal;

    @XmlElement(name = "ConflictMessage", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "conflict_message", dataType = DataType.STRING)
    protected String conflictMessage;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = TASK_ID_FIELD)
    protected Task task;

    /**
     * Gets the value of the conflictSignal property.
     * 
     */
    public int getConflictSignal() {
        return conflictSignal;
    }

    /**
     * Sets the value of the conflictSignal property.
     * 
     */
    public void setConflictSignal(int value) {
        this.conflictSignal = value;
    }

    /**
     * Gets the value of the conflictMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConflictMessage() {
        return conflictMessage;
    }

    /**
     * Sets the value of the conflictMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConflictMessage(String value) {
        this.conflictMessage = value;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
