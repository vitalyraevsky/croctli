package ru.baccasoft.croctli.android;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.fragments.DocContentFragment;
import ru.baccasoft.croctli.android.fragments.TaskErrandFragment;
import ru.baccasoft.croctli.android.fragments.TaskInfoFragment;
import ru.baccasoft.croctli.android.gen.core.Action;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.Tab;
import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.rest.Utils;
import ru.baccasoft.croctli.android.view.event.UpdateYourselfEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Экран отображения информации о задаче (Task).
 * Содержит три блока, каждый из которых представлен фрагментом.
 * (лево-верх) Информация по задаче
 * (лево-низ) Поручения
 * (право) *пока не реализовано*
 *
 * И полосу меню с кнопками перехода между задачами
 * и кнопками доступных действий по задаче
 *
 */
public class TaskBrowseActivity extends SherlockActivity {
    @SuppressWarnings("unused")
    private static final String TAG = TaskBrowseActivity.class.getSimpleName();

    public static final String TASK_ID_ARGS_KEY = "TASK_ID_ARGS_KEY";
    public static final String TAB_ID_ARGS_KEY = "TAB_ID_ARGS_KEY";

    private String taskId;
    private String tabId;
    private Task task = null;
    private int currentTaskIndex;
    private boolean taskFavoriteFlag;

    private List<Task> currentTabTasks;

    // набор пар "menuItemId : actionId". включает все отображаемые на данный момент пункты меню, связанные с действиями
    private Map<Integer, String> actionIdsMap = new HashMap<Integer, String>();
    private boolean backEnable = false;
    private boolean nextEnable = false;
    private boolean bothEnable = false;

    private ProgressBar progressBar;

    public AsyncTask asyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_browse_test);
        getSupportActionBar().setTitle(R.string.App_Title);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.icon_home));

        Bundle args = getIntent().getExtras();
        if (args != null && args.containsKey(TASK_ID_ARGS_KEY)) {

            taskId = args.getString(TASK_ID_ARGS_KEY);
            tabId = args.getString(TAB_ID_ARGS_KEY);
            try {
                task = App.getDatabaseHelper().getTasksDao().queryForId(taskId);
            } catch (SQLException e) {
                Log.e(TAG, null, e);
            }

            taskFavoriteFlag = task.isIsFavourite();
            checkTaskIfWasRead(task);
            initTasksFromTab(tabId);
        }
        if (task == null) {
            String message = "task == null";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }

        Log.d(TAG, "taskId: \'" + taskId + "\'");

        progressBar = (ProgressBar) findViewById(R.id.task_errand_progress);

//        LinearLayout docView = (LinearLayout) findViewById(R.id.doc_content);
//        docView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick");
//                FragmentManager fm = getFragmentManager();
//                TaskErrandEditorDialogFragment errandEditor = new TaskErrandEditorDialogFragment();
//                errandEditor.show(fm, "SOME_TAG");
//            }
//        });
        new PreloadTask().execute();
        setupFragments();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();

        Log.d(TAG, "menu item id:" + itemId + ", itemTitle=" + item.getTitle());

        //FIXME: хардкод значений
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                break;
            case 2001:  // undo
                task.setPerformedActionId(null);
                TableUtils.updateTask(task);

                final String messageText1 = Utils.getStringResourceByName("VisualTask_TaskCancelled", getApplicationContext());
                Toast.makeText(getApplicationContext(), messageText1, Toast.LENGTH_LONG);
                //TODO:
                //После отмены действия по задаче в списке последних действий пользователя появляется надпись с кодом VisualTask_TaskCancelled.
                break;
            case 2002:  //delete
                task.setIsDeleted(true);
                TableUtils.updateTask(task);
                break;

            case R.id.next_task:
                goToNextTask();
                break;

            case R.id.prev_task:
                goToPreviousTask();
                break;

            default:
                // обрабатываем действия по задаче
                if(actionIdsMap.containsKey(itemId)) {
                    final String actionId = actionIdsMap.get(itemId);
                    final Action action = TableUtils.getActionById(actionId);
                    final boolean isWithoutComplete = action.isWithoutComplete();

                    if (isWithoutComplete) {    // незавершающие действия
                        AlertDialog dialog = makeUserMessageDialog(action);
                        if(dialog != null)
                            dialog.show();
                    } else {    // завершающие действия
                        //[MZavgorodniy:] В реализации для ФДА функционал удаленных не используется.
                        // После выполнения завершающего действия задача должна сразу пропадать из списка задач
                        // (сразу закрываться), а у пользователя отображаться список активных задач из текущей папки.
                        task.setPerformedActionId(actionId);
                        TableUtils.updateTask(task);
                        finish();
                        //TODO:
                        //ФС: После выполнения задачи в списке последних действий пользователя появляется надпись с кодом VisualTask_TaskCompleted.
                    }
                }
                break;
            }

        supportInvalidateOptionsMenu();     //перерисовать меню

        return super.onOptionsItemSelected(item);
    }

    /**
     * Создает диалоговое окно по ФС:<br>
     * Выводится сообщение Action.UserMessage, если оно не пустое и кнопкой UiService_ShowDialog_CloseButton
     *
     * @param action
     * @return может вернуть null, если userMessage пустое
     */
    private AlertDialog makeUserMessageDialog(Action action) {
        final String buttonTextResName = "UiService_ShowDialog_CloseButton";
        final String buttonText = Utils.getStringResourceByName(buttonTextResName, getApplicationContext());
        String userMessage = action.getUserMessage();
        if(ViewUtils.isEmpty(userMessage))
            return null;

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage(userMessage)
                .setCancelable(false)
                .setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = alertBuilder.create();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.task_item_menu, menu);

        addActionsInMenu(menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem itemNext = menu.findItem(R.id.next_task);
        MenuItem itemBack = menu.findItem(R.id.prev_task);

        if(bothEnable){
            itemNext.setEnabled(false);
            itemNext.getIcon().setAlpha(130);
            itemBack.setEnabled(false);
            itemBack.getIcon().setAlpha(130);
            return super.onPrepareOptionsMenu(menu);
        }else {
            itemNext.setEnabled(true);
            itemNext.getIcon().setAlpha(255);
            itemBack.setEnabled(true);
            itemBack.getIcon().setAlpha(255);
        }

        if(backEnable){
            itemBack.setEnabled(false);
            itemBack.getIcon().setAlpha(130);
        }else{
            itemBack.setEnabled(true);
            itemBack.getIcon().setAlpha(255);
        }

        if(nextEnable){
            itemNext.setEnabled(false);
            itemNext.getIcon().setAlpha(130);
        }else{
            itemNext.setEnabled(true);
            itemNext.getIcon().setAlpha(255);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"RESUME IN TaskBrowse Activity");
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"PAUSE IN TaskBrowse Activity");
        //аналогичная ситуация из метода setupFragments(), только вылет
        // мог произойти в момет перехода на прошлую Activity в момент работы asynctask
        if(asyncTask != null){
            asyncTask.cancel(true);
        }

        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"STOP IN TaskBrowse");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"RESTART IN TaskBrowse");
    }



    private void initTasksFromTab(String currentTabId) {
        try {
            Tab currentTab = App.getDatabaseHelper().getTabDao().queryForId(currentTabId);
            currentTabTasks = TasksActivity.getTasksOnCurrentTab(); //TasksActivity.getTasksFromTab(currentTab.getTabFilter());
            for(Task t: currentTabTasks){
                if(t.getId().equals(task.getId())){
                    currentTaskIndex = currentTabTasks.indexOf(t);
                }
            }

            if(currentTabTasks.size() == 1){
                bothEnable = true;
            }else if(currentTaskIndex == 0){
                backEnable = true;
                nextEnable = false;
            }else if(currentTaskIndex == currentTabTasks.size()-1 && currentTabTasks.size() > 0){
                backEnable = false;
                nextEnable = true;
            }
            Log.d(TAG, ">>>> total amount of tasks at tab with id: " + tabId + " is: " + currentTabTasks.size());
        } catch (SQLException sqe) {
            String message = "Cannot read from db tab with id: " + currentTabId;
            TableUtils.createLogMessage(message, sqe);
            Log.e(TAG, message, sqe);
        }
    }

    private void goToNextTask() {
        Log.d(TAG, ">>>> at go to next");
        changeTask(currentTaskIndex + 1);
        if(backEnable){
            backEnable = false;
            invalidateOptionsMenu();
        }
        if(currentTaskIndex == currentTabTasks.size()-1){
            nextEnable = true;
            invalidateOptionsMenu();
        }
    }

    private void goToPreviousTask() {
        Log.d(TAG, ">>>> at go to previous");
        changeTask(currentTaskIndex - 1);
        if(currentTaskIndex == 0){
            backEnable = true;
            invalidateOptionsMenu();
        }

        if(currentTaskIndex < currentTabTasks.size()-1){
            nextEnable = false;
            invalidateOptionsMenu();
        }
    }

    private void changeTask(int newPositionAtTasksList) {
        if ((newPositionAtTasksList < 0) || (newPositionAtTasksList >= currentTabTasks.size())) {
            return;
        }

        currentTaskIndex = newPositionAtTasksList;
        task = currentTabTasks.get(currentTaskIndex);
        //обновляем taskid тоже, без него не обновляется информация в левом верхнем углу.
        taskId = task.getId();
        Log.d(TAG, ">>>> current task index is: " + currentTaskIndex);
        Log.d(TAG, ">>>> current task id is: " + task.getId());
        Log.d(TAG, ">>>> total amount of tasks at tab with id: " + tabId + " is: " + currentTabTasks.size());

        checkTaskIfWasRead(task);
        setupFragments();
    }

    private void checkTaskIfWasRead(Task task){
        try {
            if (!task.isWasRead()) {
                task.setWasRead(true);
                App.getDatabaseHelper().getTasksDao().update(task);
            }
        } catch (SQLException sqe) {
            String message = "Cannot change state of task with id: " + task.getId() + " at database";
            TableUtils.createLogMessage(message, sqe);
            Log.e(TAG, message, sqe);
        }
    }

    /**
     * Добавляем в меню нужные кнопки.<br>
     * Каждой кнопке присваивается свой id. <br>
     * Если поле task.actionId не заполнено, выводим все доступные действия для задачи.<br>
     * Отдельно в Map сохраняются пары [menuItemId:actionId], чтобы связать конкретную кнопку с отдельным действием.<br>
     * Если поле task.actionId заполнено, выводим одну кнопку. <br>
     * По нажатию на нее либо происходит отмена выполненного действия, либо задача помечается на удаление.
     * В этом случае в Map[menuItemId:actionId] значение actionId==null <br>
     *
     * @param menu
     */
    private void addActionsInMenu(Menu menu) {
        Integer itemId = 1000;
        Integer undoItemId = 2001;
        Integer deleteItemId = 2002;

        final String actionId = task.getPerformedActionId();

        actionIdsMap.clear();

        if(ViewUtils.isEmpty(actionId)) {   // выводим доступные действия
            for (Action action : task.getAvailableActionsCollection()) {
                actionIdsMap.put(itemId, action.getId());
                MenuItem menuAction = menu.add(Menu.NONE, itemId, Menu.NONE, action.getLocalName());
                menuAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                itemId++;
            }
        } else {    // или добавляем одну кнопку для отмены выбранного действия
            final Action performedAction = TableUtils.getActionById(actionId);
            String buttonText;

            boolean wasSynchronized = false;
            final boolean isWithoutComplete = performedAction.isWithoutComplete();


//            wasSynchronized = wasSynchronized(task);

            if(!wasSynchronized) {
                if (isWithoutComplete) {    // блокируем все кнопки действий
                    for (Action action : task.getAvailableActionsCollection()) {
                        actionIdsMap.put(itemId, action.getId());
                        MenuItem menuAction = menu.add(Menu.NONE, itemId, Menu.NONE, action.getLocalName());
                        menuAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                        menuAction.setEnabled(false);
                        itemId++;
                    }
                } else {    // кнопка отмены
                    buttonText = Utils.getStringResourceByName("VisualTask_UndoAction", this.getApplicationContext())
                            + ": " + performedAction.getLocalName();
                    actionIdsMap.put(undoItemId, null);
                    MenuItem menuAction = menu.add(Menu.NONE, undoItemId, Menu.NONE, buttonText);
                    menuAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                }
            } else {    // кнопка удаления
                buttonText = Utils.getStringResourceByName("VisualTask_DeleteAction", this.getApplicationContext())
                        + ": " + performedAction.getLocalName();
                actionIdsMap.put(deleteItemId, null);
                MenuItem menuAction = menu.add(Menu.NONE, deleteItemId, Menu.NONE, buttonText);
                menuAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
        }
    }

    private void setupFragments() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        TaskInfoFragment taskInfoFragment = TaskInfoFragment.newInstance(taskId);
        DocContentFragment docContentFragment = new DocContentFragment();

        ft.replace(R.id.task_info_fragment_holder, taskInfoFragment)
          .replace(R.id.doc_content, docContentFragment)
          .commit();

       //при перелистывании задач кнопками "<" ">" происходт вылет,
       // так как переизбыток asynctask'ов, приводит к обращению к ресурсам,
       // которые заняты предыдущим потоком.
       //данный прием помогает прервать прошлый экземпляр asynctask
       if(asyncTask != null){
            asyncTask.cancel(true);
       }
        asyncTask = new updateErrandFragmentTask().execute(taskId);
    }

    public Task getActiveTask() {
        return task;
    }

    /**
     * Обновляет содержимое фрагмента с резолюциями (который слева, read-only)
     * На вход принимает taskId
     */
    private class updateErrandFragmentTask extends AsyncTask<String, Void, String> {
//        private boolean hasErrandsRoot = false; // связаны ли с задачей какие-либо резолюции
//        private boolean hasErrandsEPs = false;  // есть ли связанное(-ые) EP, содержащие резолюции
//        private boolean hasErrandsTemplateEP = false;   // если шаблонные EP для создания резолюций

        private String titleString;
        @Override
        protected String doInBackground(String... params) {
            Log.d("Oleg_log", "updateErrandFragmentTask");
            String taskId = params[0];
            // получаем свойство, указывающее на дочерние резолюции
            EntityProperty rootEp = TableUtils.getRootEpForErrands(taskId);

            if(rootEp != null && rootEp.getDisplayGroup() != null) {
                titleString = rootEp.getDisplayGroup();
            } else {
                titleString = "";
            }
            return titleString;
        }

        @Override
        protected void onPostExecute(String title) {
            Log.d("Oleg_log", "updateErrandFragmentTask_POST");
            if(isCancelled()){
                return;
            }
            progressBar.setVisibility(View.GONE);
            TaskErrandFragment taskErrandFragment = TaskErrandFragment.newInstance(taskId, title);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.task_errand_fragment_holder, taskErrandFragment)
                .commit();
        }
    }

    public void onEvent(UpdateYourselfEvent event) {
        setupFragments();
    }

    public class PreloadTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            Preload.loadBase(taskId, Preload.loading.full);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //setupFragments();
        }
    }

}

