package ru.baccasoft.croctli.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.db.tables.*;
import ru.baccasoft.croctli.android.gen.core.*;
import ru.baccasoft.croctli.android.gen.core.Process;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DATABASE_NAME ="ru.baccasoft.croctli.android.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 2;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private MetaAuthDao metaAuthDao = null;
    private StringJsonDAO stringJsonsDao = null;
    private TabDAO tabDao = null;
    private TasksDao tasksDao = null;
    private FilterDao filterDao = null;
    private FilterConditionDao filterConditionDao = null;
    private ConditionOperatorDao conditionOperatorDao = null;
    private SpecialConditionDao specialConditionDao = null;
    private TaskStateInSystemDao taskStateInSystemDao = null;
    private ProcessDao processDao = null;
    private ProcessTypeDao processTypeDao = null;
    private ViewCustomizationDao viewCustomizationDao = null;
    private SourceSystemDao sourceSystemDao = null;
    private SystemUserDao systemUserDao = null;
    private UserInSystemDao userInSystemDao = null;
    private UserNsiDao userNsiDao = null;
    private AttachmentDAO attachmentDAO = null;
    private EntityPropertyDAO entityPropertyDAO = null;
    private ActionBehaviorDAO actionBehaviorDAO = null;
    private ActionDAO actionDAO = null;
    private TaskActorDAO taskActorDAO = null;
    private ClassifierDAO classifierDAO = null;
    private ClassifierItemDAO classifierItemDAO = null;
    private ItemFieldDAO itemFieldDAO = null;
    private ConflictDataDao conflictDataDao = null;
    private CommentsDao commentsDao = null;
    private AdapterDao adapterDao = null;
    private TaskUserUiDao taskUserUiDao = null;
    private HandmadeDelegateDao handmadeDelegateDao = null;
    private RibbonSettingsDao ribbonSettingsDao = null;
    private LogMessageDao logMessageDao = null;
    private LogMessageCategoryDao logMessageCategoryDao = null;

    public List<Class> entities = new ArrayList<Class>() { {
        add(StringJson.class);
        add(MetaAuth.class);
        add(Tab.class);
        add(ActionBehavior.class);
        add(Action.class);
        add(TaskActor.class);
        add(Task.class);
        add(Filter.class);
        add(FilterCondition.class);
        add(ConditionOperator.class);
        add(SpecialCondition.class);
        add(TaskStateInSystem.class);
        add(ViewCustomization.class);
        add(Process.class);
        add(ProcessType.class);
        add(SourceSystem.class);
        add(SystemUser.class);
        add(UserInSystem.class);
        add(UserNSI.class);
        add(Attachment.class);
        add(EntityProperty.class);
        add(Classifier.class);
        add(ClassifierItem.class);
        add(ItemField.class);
        add(ConflictData.class);
        add(Comment.class);
        add(Adapter.class);
        add(TaskUserUI.class);
        add(HandmadeDelegate.class);
        add(RibbonSettings.class);
        add(LogMessage.class);
        add(LogMessageCategory.class);
    }};

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME/* + "_" + App.prefs.getUserLogin()*/, null, DATABASE_VERSION);
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try
        {
            for (Class entity : entities) {
                Log.d(TAG, "trying to create table for class:\'" + entity.getSimpleName() + "\'");
                TableUtils.createTable(connectionSource, entity);
            }
            Log.d(TAG, "db created");
        }
        catch (SQLException e) {
            String message = "error creating DB";

            ru.baccasoft.croctli.android.db.TableUtils.createLogMessage(message, e);
            Log.e(TAG, message, e);

            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){
        try {
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            for (Class entity : entities)
                TableUtils.dropTable(connectionSource, entity, true);
            onCreate(db, connectionSource);
        } catch (Exception e) {
            String message = "error upgrading db "+DATABASE_NAME+" from ver "+oldVer;
            ru.baccasoft.croctli.android.db.TableUtils.createLogMessage(message, e);
            Log.e(TAG, message, e);

            throw new RuntimeException(e);
        }
    }

    //выполняется при закрытии приложения
    @Override
    public void close(){
        super.close();
    }

    public MetaAuthDao getMetaAuthDao() throws SQLException {
        if(metaAuthDao == null){
            metaAuthDao = new MetaAuthDao(getConnectionSource(), MetaAuth.class);
        }
        return metaAuthDao;
    }

    public StringJsonDAO getArrayOfStrings() throws SQLException {
        if(stringJsonsDao == null)
            stringJsonsDao = new StringJsonDAO(getConnectionSource(), StringJson.class);
        return stringJsonsDao;
    }

    public TabDAO getTabDao() throws SQLException {
        if(tabDao == null)
            tabDao = new TabDAO(getConnectionSource(), Tab.class);
        return tabDao;
    }

    public TasksDao getTasksDao() throws SQLException {
        if (tasksDao == null)
            tasksDao = new TasksDao(getConnectionSource(), Task.class);
        return tasksDao;
    }

    public FilterDao getFilterDao() throws SQLException {
        if (filterDao == null)
            filterDao = new FilterDao(getConnectionSource(), Filter.class);
        return filterDao;
    }

    public FilterConditionDao getFilterConditionDao() throws SQLException {
        if (filterConditionDao == null)
            filterConditionDao = new FilterConditionDao(getConnectionSource(), FilterCondition.class);
        return filterConditionDao;
    }

    public ConditionOperatorDao getConditionOperatorDao() throws SQLException {
        if (conditionOperatorDao == null)
            conditionOperatorDao = new ConditionOperatorDao(getConnectionSource(), ConditionOperator.class);
        return conditionOperatorDao;
    }

    public SpecialConditionDao getSpecialConditionDao() throws SQLException {
        if (specialConditionDao == null)
            specialConditionDao = new SpecialConditionDao(getConnectionSource(), SpecialCondition.class);
        return specialConditionDao;
    }

    public TaskStateInSystemDao getTaskStateInSystemDao() throws SQLException {
        if (taskStateInSystemDao == null)
            taskStateInSystemDao = new TaskStateInSystemDao(getConnectionSource(), TaskStateInSystem.class);
        return taskStateInSystemDao;
    }

    public ViewCustomizationDao getViewCustomizationDao() throws SQLException {
        if(viewCustomizationDao == null)
            viewCustomizationDao = new ViewCustomizationDao(getConnectionSource(), ViewCustomization.class);
        return viewCustomizationDao;
    }

    public ProcessDao getProcessDao() throws SQLException {
        if (processDao == null)
            processDao = new ProcessDao(getConnectionSource(), Process.class);
        return processDao;
    }

    public ProcessTypeDao getProcessTypeDao() throws SQLException {
        if (processTypeDao == null)
            processTypeDao = new ProcessTypeDao(getConnectionSource(), ProcessType.class);
        return processTypeDao;
    }

    public SourceSystemDao getSourceSystemDao() throws SQLException {
        if (sourceSystemDao == null)
            sourceSystemDao = new SourceSystemDao(getConnectionSource(), SourceSystem.class);
        return sourceSystemDao;
    }

    public SystemUserDao getSystemUserDao() throws SQLException {
        if (systemUserDao == null)
            systemUserDao = new SystemUserDao(getConnectionSource(), SystemUser.class);
        return systemUserDao;
    }

    public UserInSystemDao getUserInSystemDao() throws SQLException {
        if (userInSystemDao == null)
            userInSystemDao = new UserInSystemDao(getConnectionSource(), UserInSystem.class);
        return userInSystemDao;
    }

    public UserNsiDao getUserNsiDao() throws SQLException {
        if (userNsiDao == null)
            userNsiDao = new UserNsiDao(getConnectionSource(), UserNSI.class);
        return userNsiDao;
    }

    public AttachmentDAO getAttachmentDAO() throws SQLException {
        if (attachmentDAO == null)
            attachmentDAO = new AttachmentDAO(getConnectionSource(), Attachment.class);
        return attachmentDAO;
    }

    public EntityPropertyDAO getEntityPropertyDAO() throws SQLException {
        if (entityPropertyDAO == null)
            entityPropertyDAO = new EntityPropertyDAO(getConnectionSource(), EntityProperty.class);
        return entityPropertyDAO;
    }

    public ActionBehaviorDAO getActionBehaviorDAO() throws SQLException {
        if (actionBehaviorDAO == null)
            actionBehaviorDAO = new ActionBehaviorDAO(getConnectionSource(), ActionBehavior.class);
        return actionBehaviorDAO;
    }

    public ActionDAO getActionDAO() throws SQLException {
        if(actionDAO == null)
            actionDAO = new ActionDAO(getConnectionSource(), Action.class);
        return actionDAO;
    }

    public TaskActorDAO getTaskActorDAO() throws SQLException {
        if(taskActorDAO == null)
            taskActorDAO = new TaskActorDAO(getConnectionSource(), TaskActor.class);
        return taskActorDAO;
    }

    public ClassifierDAO getClassifierDAO() throws SQLException {
        if(classifierDAO == null)
            classifierDAO = new ClassifierDAO(getConnectionSource(), Classifier.class);
        return classifierDAO;
    }

    public ClassifierItemDAO getClassifierItemDAO() throws SQLException {
        if(classifierItemDAO == null)
            classifierItemDAO = new ClassifierItemDAO(getConnectionSource(), ClassifierItem.class);
        return classifierItemDAO;
    }

    public ItemFieldDAO getItemFieldDAO() throws SQLException {
        if(itemFieldDAO == null)
            itemFieldDAO = new ItemFieldDAO(getConnectionSource(), ItemField.class);
        return itemFieldDAO;
    }

    public ConflictDataDao getConflictDataDao() throws SQLException {
        if(conflictDataDao == null)
            conflictDataDao = new ConflictDataDao(getConnectionSource(), ConflictData.class);
        return conflictDataDao;
    }

    public CommentsDao getCommentsDao() throws SQLException {
        if(commentsDao == null)
            commentsDao = new CommentsDao(getConnectionSource(), Comment.class);
        return commentsDao;
    }

    public AdapterDao getAdapterDao() throws SQLException {
        if(adapterDao == null)
            adapterDao = new AdapterDao(getConnectionSource(), Adapter.class);
        return adapterDao;
    }

    public TaskUserUiDao getTaskUserUiDao() throws SQLException {
        if(taskUserUiDao == null)
            taskUserUiDao = new TaskUserUiDao(getConnectionSource(), TaskUserUI.class);
        return taskUserUiDao;
    }

    public HandmadeDelegateDao getHandmadeDelegateDao() throws SQLException {
        if(handmadeDelegateDao == null)
            handmadeDelegateDao = new HandmadeDelegateDao(getConnectionSource(), HandmadeDelegate.class);
        return handmadeDelegateDao;
    }

    public RibbonSettingsDao getRibbonSettingsDao() throws SQLException {
        if(ribbonSettingsDao == null)
            ribbonSettingsDao = new RibbonSettingsDao(getConnectionSource(), RibbonSettings.class);
        return ribbonSettingsDao;
    }

    public LogMessageDao getLogMessageDao() throws SQLException {
        if(logMessageDao == null)
            logMessageDao = new LogMessageDao(getConnectionSource(), LogMessage.class);
        return logMessageDao;
    }

    public LogMessageCategoryDao getLogMessageCategoryDao() throws SQLException {
        if(logMessageCategoryDao == null)
            logMessageCategoryDao = new LogMessageCategoryDao(getConnectionSource(), LogMessageCategory.class);
        return logMessageCategoryDao;
    }
}
