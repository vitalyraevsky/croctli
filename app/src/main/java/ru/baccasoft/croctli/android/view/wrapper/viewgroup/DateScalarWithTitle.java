package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.utils.IDateField;
import ru.baccasoft.croctli.android.view.event.ScalarChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.DateScalar;

public class DateScalarWithTitle extends LinearLayout {
    @SuppressWarnings("unused")
    private static final String TAG = DateScalarWithTitle.class.getSimpleName();

    public DateScalarWithTitle(final Activity activity, String title, IDateField date,
                               boolean isReadonly, final String entityPropertyId) {
        super(activity.getApplicationContext());
        Context mContext = activity.getApplicationContext();

        TextView titleView = ViewUtils.makeTitleTextView(mContext, title, null);

        final DateScalar dateScalar = new DateScalar(activity, date, isReadonly, entityPropertyId);
        dateScalar.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        //Кнопка "Очистить дату"
        ImageButton resetDate = null;
        if (!isReadonly) {
            resetDate = new ImageButton(mContext);
            resetDate.setImageResource(android.R.drawable.ic_notification_clear_all);
            resetDate.setBackgroundResource(R.drawable.fab_button);
            resetDate.setPadding(10,0,0,0);
                    resetDate.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //очистить дату в dateScalar
                            dateScalar.setText("");
                            EventBus.getDefault().post(new ScalarChangeEvent(
                                    entityPropertyId, "", activity.getApplicationContext()));
                        }
                    });
            resetDate.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            ));
        }
        //!Кнопка "Очистить дату"

        addView(titleView);
        addView(dateScalar);
        if(resetDate != null)
            addView(resetDate);
    }
}
