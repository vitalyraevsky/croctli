package ru.baccasoft.croctli.android;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.updater.LogMessageSyncTask;
import ru.baccasoft.croctli.android.updater.SyncByRequestTask;
import ru.baccasoft.croctli.android.view.event.EndUpdateEvent;
import ru.baccasoft.croctli.android.view.event.StartUpdateEvent;
import ru.baccasoft.croctli.android.view.event.UpdateTasksEvent;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends Service {
    private static final String ACTION_START = "action_start";
    private static final String SYNC_PERIOD = "sync_period";
    private static final String ACTION_START_BY_REQUEST = "action_start_by_request";
    private static final String ACTION_START_SCHEDULE = "action_start_schedule";
    private String action = null;
    private int syncPeriod;
    private Timer timer = new Timer();
    private TimerTask doAsynchronousTask;
    private final Handler handler = new Handler();
    private AsyncTask localSyncByRequest;
    private AsyncTask logMessageSyncTask;

    private static String TAG = MyIntentService.class.getSimpleName();

    public MyIntentService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "MyIntentService OnCreate");
        //EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "MyIntentService OnStart");
        if(intent != null){
            action = intent.getAction();
            if (ACTION_START.equals(action)) {
                syncPeriod = intent.getIntExtra(SYNC_PERIOD, 1);
                if (syncPeriod > 0) {
                    Log.d(TAG, "syncPeriod > 0");
                    doAsynchronousTask = timerTask();
                    if(timer == null) {
                        timer = new Timer();
                    }else{
                        timer.cancel();
                        timer = new Timer();
                    }
                    timer.schedule(doAsynchronousTask, syncPeriod, syncPeriod);

                }else{
                    Log.d(TAG, "syncPeriod = 0");
                    if(timer != null) {
                        Log.d(TAG, "Stop timer tasks");
                        timer.cancel();
                        timer = null;
                    }

                }
            }else if(ACTION_START_BY_REQUEST.equals(action)){
                Log.d(TAG,"ACTION_START_BY_REQUEST");
                if(timer != null) {
                    Log.d(TAG, "Stop timer tasks");
                    timer.cancel();
                    timer = null;
                }

                Log.d(TAG, "start update");
                localSyncByRequest = new LocalSyncByRequest().execute();
                logMessageSyncTask = new LogMessageSyncTask().execute();


            }
        }
        return START_STICKY;
    }

    private TimerTask timerTask() {
        TimerTask AsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d(TAG, "post Run Sync");
                            localSyncByRequest = new LocalSyncByRequest().execute();
                            logMessageSyncTask = new LogMessageSyncTask().execute();
                        } catch (Exception e) {
                            Log.e(TAG, "error during regular sync", e);
                        }
                    }
                });
            };
        };
        return AsynchronousTask;
    }

    private class LocalSyncByRequest extends SyncByRequestTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "send StartUpdateEvent");
            EventBus.getDefault().post(new StartUpdateEvent());
        }

        @Override
        protected Void doInBackground(RestApiDate... params) {
            Void v = super.doInBackground(params);
            //update();
            EventBus.getDefault().post(new UpdateTasksEvent());
            return v;
        }

        @Override
        protected void onPostExecute(Void param) {
            super.onPostExecute(param);
            if(timer == null && syncPeriod > 0){
                Log.d(TAG,"Timer start after cancel with " + syncPeriod/60000 + " min period");
                timer = new Timer();
                doAsynchronousTask = timerTask();
                timer.schedule(doAsynchronousTask, syncPeriod, syncPeriod);
            }

            Log.d(TAG, "send EndUpdateEvent");
            EventBus.getDefault().postSticky(new EndUpdateEvent(resultSuccess));

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "OnDestroyService");
        if(timer != null) {
            Log.d(TAG, "Stop timer tasks");
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);
    }
}
