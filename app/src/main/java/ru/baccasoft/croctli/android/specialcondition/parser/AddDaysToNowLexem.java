package ru.baccasoft.croctli.android.specialcondition.parser;

public class AddDaysToNowLexem extends SimpleLexem {
	
	public final int days; 

	public AddDaysToNowLexem(LexerSource lexerSource, int days ) {
		super(LexemType.DateAtom, lexerSource, AddDaysToNowLexemRecognizer.HEAD+days+AddDaysToNowLexemRecognizer.TAIL);
		this.days = days;
	}
	
}
