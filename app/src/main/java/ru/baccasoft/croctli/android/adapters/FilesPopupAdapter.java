package ru.baccasoft.croctli.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.dao.PopupFileInfo;

public class FilesPopupAdapter extends BaseAdapter {

    Activity mActivity;
    Context mContext;
    List<PopupFileInfo> mData;
    public static String TAG = "FilesPopupAdapter";

    private final static int viewItemRes = R.layout.popup_file_item;

    public FilesPopupAdapter(List<PopupFileInfo> data, Activity activity) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mData = new LinkedList<PopupFileInfo>(data);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder h;

        if(v == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            v = inflater.inflate(viewItemRes, parent, false);

            h = new ViewHolder();
            h.id = (TextView) v.findViewById(R.id.file_id);
            h.icon = (ImageView) v.findViewById(R.id.icon);
            h.name = (TextView) v.findViewById(R.id.name);
            h.size = (TextView) v.findViewById(R.id.size);

            v.setTag(h);
        } else {
            h =  (ViewHolder) v.getTag();
        }

        PopupFileInfo object = mData.get(position);

        if(object != null) {
            h.id.setText(object.id);
            h.icon.setImageResource(object.typeRes);
            h.name.setText(getFormatName(object.name));
            h.size.setText(readableSize(object.size));
        }

        return v;
    }

    private class ViewHolder {
        TextView id;
        ImageView icon;
        TextView name;
        TextView size;
    }

    /**
     * Приводит размер файла в байтах в читаемый формат.
     * 1 кб = 1024 байта
     *
     * @return human-readable отображение размера
     */
    public static String readableSize(int bytes) {
            int unit = 1024;
            if (bytes < unit) return bytes + " Б";
            int exp = (int) (Math.log(bytes) / Math.log(unit));
            char pre = "КМГТП".charAt(exp-1);
            return String.format("%.1f %sБ", bytes / Math.pow(unit, exp), pre);
    }

    public static String getFormatName(String s){
        StringBuilder stringBuilder = new StringBuilder();
        Log.d(TAG, s);

        if(s.length() <= 30){
            return s;
        }else if(s.length() < 60){
            stringBuilder.append(s.substring(0, 29)).append("\n").append(s.substring(29, 59));
            Log.d(TAG,s.substring(0, 29) + ": " + s.substring(29, 59));
        }else if(s.length() >= 60){
            stringBuilder.append(s.substring(0, 29)).append("\n").append(s.substring(29, 59)).append("...");
            Log.d(TAG, s.substring(0, 29) + ": " + s.substring(29, 59));
        }
        return stringBuilder.toString();
    }
}
