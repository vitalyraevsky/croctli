package ru.baccasoft.croctli.android.updater;

import android.os.AsyncTask;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.TypeActivity;
import ru.baccasoft.croctli.android.gen.core.UserActivity;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.UserActivityAPI;
import ru.baccasoft.croctli.android.sync.AdapterSync;
import ru.baccasoft.croctli.android.sync.ClassifierSync;
import ru.baccasoft.croctli.android.sync.ConditionOperatorSync;
import ru.baccasoft.croctli.android.sync.EntityPropertySync;
import ru.baccasoft.croctli.android.sync.HandmadeDelegateSync;
import ru.baccasoft.croctli.android.sync.ProcessSync;
import ru.baccasoft.croctli.android.sync.ProcessTypeSync;
import ru.baccasoft.croctli.android.sync.SourceSystemSync;
import ru.baccasoft.croctli.android.sync.SpecialConditionSync;
import ru.baccasoft.croctli.android.sync.SystemUserSync;
import ru.baccasoft.croctli.android.sync.TaskStateInSystemSync;
import ru.baccasoft.croctli.android.sync.TaskSync;
import ru.baccasoft.croctli.android.sync.TaskUserUISync;
import ru.baccasoft.croctli.android.sync.ViewCustomizationSync;
import ru.baccasoft.croctli.android.updater.attachment.AttachmentSyncTask;

/*
        Порядок синхронизации:
        sourcesystem                GD, GM          Список систем – источников
        adapter                     GD, GM          Список адаптеров
        systemuser                  GD, GM          Список пользователей системы
        taskstateinsystem           GD, GM          Специальный справочник состояний задачи
        classifier                  GD, GM          Справочники и классификаторы
        processtype                 GD, GM          Список типов процессов
        viewcustomization           GD, GM          Справочник кастомизации UI
        process                     SD, SM, GD, GM  Список процессов
        api/task/listid                             Список id задач
        task                        SD, SM, GM      Список задач
        taskUserUI                  SM, GM          Визуальное представление задачи
        entityproperty              SD, SM, GD, GM  Список свойств задач
        handmadedelegate
        /api/conditionoperator/list SD, SM, GD, GM  Список объектов ручного назначения задач
        specialcondition            GD, GM          Справочник специальных условий фильтров системных закладок
        /api/conditionoperator/list
        conditionoperator           GD, GM          Справочник операндов условий фильтров
        tab                         SD, SM, GD, GM  Перечень системных и пользовательских закладок
        ribbonsetting               SD, SM, GD, GM  Список настроек ленты текущего пользователя
        logmessage

        SourceSystem
        Adapter
        ProcessType
        Classifier
        ConditionOperator
        SpecialCondition
        TaskStateInSystem
        SystemUser
        UIForm
        ViewCustomization

*/
public class SyncByRequestTask extends AsyncTask<RestApiDate, Void, Void> {

    private static final String TAG = SyncByRequestTask.class.getSimpleName();
    protected Boolean resultSuccess = true;

    @Override
    protected Void doInBackground(RestApiDate... params) {

        final long lastSyncDateMillis = App.prefs.getLastSyncDate();

        if(lastSyncDateMillis == 0) {
            return null;
        }

        try {
            UserActivityAPI userActivityAPI = new UserActivityAPI();
            UserActivity userActivity = userActivityAPI.modifyGet(null, null, 0);

            DateTime serverDate = RestApiDate.parseDateTimeParsable(userActivity.getServerTimeString());
            DateTime activeDate = RestApiDate.parseDateTimeParsable(userActivity.getActiveTimeString());

            List<TypeActivity> types = userActivity.getTypes().getItem();

            final RestApiDate dateFrom = new RestApiDate(new DateTime(lastSyncDateMillis));
            final RestApiDate dateTo = new RestApiDate(serverDate);
            //final RestDate dateTo = new RestDate(new DateTime(System.currentTimeMillis()), false);

            Log.d(TAG, "[SYNC] starting first sync for period: [" + dateFrom + " | " + dateTo + "]");

            Log.d(TAG, "server time: " + userActivity.getServerTimeString());
            Log.d(TAG, "active time: " + userActivity.getActiveTimeString());
            Log.d(TAG, "type syze: " + types.size());

            Map<String, RestApiDate> typeNameToActiveDate = new HashMap<String, RestApiDate>();

            for (TypeActivity type : types) {
                Log.d(TAG, "type name: " + type.getTypeName() + " active time: " + type.getActiveTimeString());
                typeNameToActiveDate.put(type.getTypeName(), new RestApiDate(type.getActiveTimeString(), false));
            }

            RestApiDate typeActiveDate = null;

            //sourcesystem
            typeActiveDate = typeNameToActiveDate.get("SourceSystem");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, "sync SourceSystem");
                SourceSystemSync sourceSystemSync = new SourceSystemSync();
                sourceSystemSync.byRequest(dateFrom, dateTo);
                //            sourceSystemSync.atFirst(dateFrom, dateTo);
            }

            //adapter
            typeActiveDate = typeNameToActiveDate.get("Adapter");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, " sync Adapter");
                AdapterSync adapterSync = new AdapterSync();
                adapterSync.byRequest(dateFrom, dateTo);
            }

            //systemuser
            typeActiveDate = typeNameToActiveDate.get("SystemUser");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, "sync SystemUser");
                SystemUserSync systemUser = new SystemUserSync();
                systemUser.byRequest(dateFrom, dateTo);
                //          systemUser.atFirst(dateFrom, dateTo);
            }

            //taskstateinsystem
            typeActiveDate = typeNameToActiveDate.get("TaskStateInSystem");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, "sync TaskStateInSystem");
                TaskStateInSystemSync taskStateInSystemSync = new TaskStateInSystemSync();
                taskStateInSystemSync.byRequest(dateFrom, dateTo);
                //            taskStateInSystemSync.atFirst(dateFrom, dateTo);
            }

            //classifier
            typeActiveDate = typeNameToActiveDate.get("Classifier");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG," sync Classifier");
                ClassifierSync classifierSync = new ClassifierSync();
                classifierSync.byRequest(dateFrom, dateTo);
            }

            //processtype
            typeActiveDate = typeNameToActiveDate.get("ProcessType");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, "sync ProcessType");
                ProcessTypeSync processTypeSync = new ProcessTypeSync();
                processTypeSync.byRequest(dateFrom, dateTo);
                //          processTypeSync.atFirst(dateFrom, dateTo);
            }

            //viewcustomization
            typeActiveDate = typeNameToActiveDate.get("ViewCustomization");
            if (typeActiveDate != null && !typeActiveDate.toDateTime().isBefore(dateFrom.getMillis())) {
                Log.d(TAG, "sync ViewCustomization");
                ViewCustomizationSync viewCustomizationSync = new ViewCustomizationSync();
                //            viewCustomizationSync.atFirst(dateFrom, dateTo);
                viewCustomizationSync.byRequest(dateFrom, dateTo);
            }

            //process
            if(activeDate.isAfter(lastSyncDateMillis)) {
                Log.d(TAG, "sync process");
                ProcessSync processSync = new ProcessSync();
                processSync.byRequest(dateFrom, dateTo);
                //        processSync.atFirst(dateFrom, dateTo);
            }


            // api/task/listid
            if(activeDate.isAfter(lastSyncDateMillis)) {
                Log.d(TAG, "sync api/task/listid");
                TaskSync taskSync = new TaskSync();
                taskSync.byRequest(dateFrom, dateTo);
                //        taskSync.atFirst(dateFrom, dateTo);
            }


            //task

            //taskUserUI
            //TODO: маппинг
            if(activeDate.isAfter(lastSyncDateMillis)) {
                Log.d(TAG, "sync taskUserUI");
                TaskUserUISync taskUserUISync = new TaskUserUISync();
                taskUserUISync.byRequest(dateFrom, dateTo);
            }


            //entityproperty
            if(activeDate.isAfter(lastSyncDateMillis)) {
                Log.d(TAG, "sync entityproperty");
                EntityPropertySync entityPropertySync = new EntityPropertySync();
                entityPropertySync.byRequest(dateFrom, dateTo);
            }

            //handmadedelegate
            //TODO: маппинг
            HandmadeDelegateSync handmadeDelegateSync = new HandmadeDelegateSync();
            //handmadeDelegateSync.byRequest(dateFrom, dateTo);

            // api/specialcondition/list
            SpecialConditionSync specialConditionSync = new SpecialConditionSync();
            //specialConditionSync.byRequest(dateFrom, dateTo);

            // api/conditionoperator/list
            ConditionOperatorSync conditionOperatorSync = new ConditionOperatorSync();
            //conditionOperatorSync.byRequest(dateFrom, dateTo);

            //tab

            //ribbonsetting
            //todo: маппинг

            // сохраняем новую дату последней синхронизации
            App.prefs.setLastSyncDate(dateTo.toDateTime().getMillis());

        } catch (Throwable th) {    // по ФС при любой ошибке во время синхронизации показываем "грусный смайлик"
            resultSuccess = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void param) {
        new AttachmentSyncTask().execute();
    }
}
