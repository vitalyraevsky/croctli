package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.MetaAuth;
import ru.baccasoft.croctli.android.gen.core.StringJson;

import java.sql.SQLException;

public class MetaAuthDao extends BaseDaoImpl<MetaAuth, Integer> {
    public MetaAuthDao(ConnectionSource connectionSource, Class<MetaAuth> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(MetaAuth data) throws SQLException {
        int ret = super.create(data);
        for(String s : data.getMethods().getItem()) {
            final StringJson sj = new StringJson(data, s);
            ret += App.getDatabaseHelper().getArrayOfStrings().create(sj);
        }
        return ret;
    }
}
