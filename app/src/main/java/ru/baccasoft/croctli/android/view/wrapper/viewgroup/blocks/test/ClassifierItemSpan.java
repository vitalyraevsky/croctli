package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;

import android.text.style.UnderlineSpan;

public class ClassifierItemSpan extends UnderlineSpan {
    private String classifierItemCode;

    public ClassifierItemSpan(String classifierItemCode) {
        this.classifierItemCode = classifierItemCode;

    }

    public String getClassifierItemCode() {
        return classifierItemCode;
    }

    //    public ClassifierItemSpan(String classifierItemCode) {
//        super(R.color.text_2A2A2A);
//        setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        this.setTextColor(getResources().getColor(R.color.text_2A2A2A));
////        this.setTextSize(getResources().getDimension(R.dimen.px_16));
//
//        this.classifierItemCode = classifierItemCode;
//    }
}
