package ru.baccasoft.croctli.android.rest.wrapper;

import org.apache.http.HttpResponse;
import ru.baccasoft.croctli.android.gen.core.ArrayOfLogMessage;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class LogMessageAPI implements IRestApiCall<Void, ArrayOfLogMessage, Void, Boolean> {

    public static final String URI = "/api/logmessage/log";

    @Override
    public Void modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return statusCode из пришедшего от сервера ответа
     */
    @Override
    public Boolean modifyPost(RestApiDate dateLastSync, ArrayOfLogMessage objects) {
        boolean needAuth = true;

        String jsonData = Utils.getAsJSON(objects);

        HttpResponse response = RestClient.callSomeRestApi(URI, needAuth, jsonData);

        boolean success = response != null;
        return success;
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException();
    }
}
