
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * <p>Java class for AppVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AppVersion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MajorVersion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MinorVersion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BuildNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RevisionNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppVersion", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "majorVersion",
    "minorVersion",
    "buildNumber",
    "revisionNumber"
})

@DatabaseTable(tableName = "app_version")
public class AppVersion {

    @DatabaseField(dataType = DataType.INTEGER)
    @JsonProperty("MajorVersion")
    @XmlElement(name = "MajorVersion", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int majorVersion;

    @DatabaseField(dataType = DataType.INTEGER)
    @JsonProperty("MinorVersion")
    @XmlElement(name = "MinorVersion", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int minorVersion;

    @DatabaseField(dataType = DataType.INTEGER)
    @JsonProperty("BuildNumber")
    @XmlElement(name = "BuildNumber", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int buildNumber;

    @DatabaseField(dataType = DataType.INTEGER)
    @JsonProperty("RevisionNumber")
    @XmlElement(name = "RevisionNumber", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected int revisionNumber;

    /**
     * Gets the value of the majorVersion property.
     * 
     */
    public int getMajorVersion() {
        return majorVersion;
    }

    /**
     * Sets the value of the majorVersion property.
     * 
     */
    public void setMajorVersion(int value) {
        this.majorVersion = value;
    }

    /**
     * Gets the value of the minorVersion property.
     * 
     */
    public int getMinorVersion() {
        return minorVersion;
    }

    /**
     * Sets the value of the minorVersion property.
     * 
     */
    public void setMinorVersion(int value) {
        this.minorVersion = value;
    }

    /**
     * Gets the value of the buildNumber property.
     * 
     */
    public int getBuildNumber() {
        return buildNumber;
    }

    /**
     * Sets the value of the buildNumber property.
     * 
     */
    public void setBuildNumber(int value) {
        this.buildNumber = value;
    }

    /**
     * Gets the value of the revisionNumber property.
     * 
     */
    public int getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Sets the value of the revisionNumber property.
     * 
     */
    public void setRevisionNumber(int value) {
        this.revisionNumber = value;
    }

}
