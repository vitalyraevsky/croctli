package ru.baccasoft.croctli.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;


public class Preferences {

    @SuppressWarnings("unused")
    private static final String TAG = Preferences.class.getSimpleName();

    private static final String PREFS_NAME = "mainprefs";

    private SharedPreferences prefs;

    private enum PKEYS {
        AUTH_SERVER_URL("AUTH_SERVER_URL"),
        AUTH_TGT_KEY ("AUTH_TGT_KEY"),
        AUTH_TGT_URL ("AUTH_TGT_URL"),
        UNIQUE_DEVICE_ID ("UNIQUE_DEVICE_ID"),
        LAST_SYNC_DATE("LAST_SYNC_DATE"),
        USER_LOGIN("USER_LOGIN"),
        LAST_USER("LAST_USER");

        private final String value;
        PKEYS(String s) { value = s; }
    }


    public Preferences(Context ctx, String userLogin) {
        prefs = ctx.getSharedPreferences(PREFS_NAME/*+"_"+userLogin*/, Context.MODE_PRIVATE);
        Log.d(TAG, "Preferences new");
        setUserLogin(userLogin);
    }


    public void setTGTkey(String value) {
        prefs.edit().putString(PKEYS.AUTH_TGT_KEY.value, value)
                .apply();
    }
    public String getTGTkey() {
        return prefs.getString(PKEYS.AUTH_TGT_KEY.value, null);
    }

    public void setTGTurl(String value) {
        prefs.edit().putString(PKEYS.AUTH_TGT_URL.value, value)
                .apply();
    }
    public String getTGTurl() {
        return prefs.getString(PKEYS.AUTH_TGT_URL.value, null);
    }

    public void setAuthServer(String value) {
        prefs.edit().putString(PKEYS.AUTH_SERVER_URL.value, null)
                .apply();
    }
    public String getAuthServer() {
        return prefs.getString(PKEYS.AUTH_SERVER_URL.value, null);
    }

    public void setDeviceId(String value) {
        prefs.edit().putString(PKEYS.UNIQUE_DEVICE_ID.value, value).apply();
    }

    public String getDeviceId() {
        return prefs.getString(PKEYS.UNIQUE_DEVICE_ID.value, null);
    }

    public void setLastSyncDate(long value) {
        prefs.edit().putLong(PKEYS.LAST_SYNC_DATE.value, value).apply();
    }

    public long getLastSyncDate() {
        return prefs.getLong(PKEYS.LAST_SYNC_DATE.value, 0);
    }

    public void setUserLogin(String login) {
        prefs.edit().putString(PKEYS.USER_LOGIN.value , login).apply();
    }

    public String getUserLogin() {
        return prefs.getString(PKEYS.USER_LOGIN.value , null);
    }

    public void setLastUser(String value){
        prefs.edit().putString(PKEYS.LAST_USER.value, value).apply();
    }

    public String getLastUser(){
        return prefs.getString(PKEYS.LAST_USER.value, null);
    }
}
