
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.List;


/**
 * <p>Java class for UserInSystem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserInSystem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NativeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NativeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SourceSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NSI" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfUserNSI"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserInSystem", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "nativeId",
    "nativeName",
    "sourceSystemId",
    "nsi"
})
@DatabaseTable(tableName = "user_in_system")
public class UserInSystem {

    public static final String SYSTEM_USER_ID_FIELD = "fk_system_user";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = SYSTEM_USER_ID_FIELD)
    private SystemUser systemUser;

    public SystemUser getSystemUser() { return systemUser; }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }


    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(id = true, columnName = "id", dataType = DataType.STRING)
    protected String id;

    @JsonProperty("NativeId")
    @XmlElement(name = "NativeId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "native_id", dataType = DataType.STRING)
    protected String nativeId;

    @JsonProperty("NativeName")
    @XmlElement(name = "NativeName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "native_name", dataType = DataType.STRING)
    protected String nativeName;

    @JsonProperty("SourceSystemId")
    @XmlElement(name = "SourceSystemId", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    @DatabaseField(columnName = "source_system_id", dataType = DataType.STRING)
    protected String sourceSystemId;

    @JsonProperty("NSI")
    @XmlElement(name = "NSI", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfUserNSI nsi;

    @ForeignCollectionField
    protected Collection<UserNSI> nsiList;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the nativeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeId() {
        return nativeId;
    }

    /**
     * Sets the value of the nativeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeId(String value) {
        this.nativeId = value;
    }

    /**
     * Gets the value of the nativeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeName() {
        return nativeName;
    }

    /**
     * Sets the value of the nativeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeName(String value) {
        this.nativeName = value;
    }

    /**
     * Gets the value of the sourceSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSystemId() {
        return sourceSystemId;
    }

    /**
     * Sets the value of the sourceSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSystemId(String value) {
        this.sourceSystemId = value;
    }

    /**
     * Gets the value of the nsi property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUserNSI }
     *     
     */
    public ArrayOfUserNSI getNSI() {
        return nsi;
    }

    /**
     * Sets the value of the nsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUserNSI }
     *     
     */
    public void setNSI(ArrayOfUserNSI value) {
        this.nsi = value;
    }

    public Collection<UserNSI> getNsiList() {
        return nsiList;
    }

    public void setNsiList(List<UserNSI> nsiList) {
        this.nsiList = nsiList;
    }
}
