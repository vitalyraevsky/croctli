package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.SystemUser;
import ru.baccasoft.croctli.android.gen.core.TaskActor;
import ru.baccasoft.croctli.android.gen.core.UserInSystem;
import ru.baccasoft.croctli.android.gen.core.UserNSI;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SystemUserDao extends BaseDaoImpl<SystemUser, String> {
    @SuppressWarnings("unused")
    private static final String TAG = SystemUserDao.class.getSimpleName();

    public SystemUserDao(ConnectionSource connectionSource, Class<SystemUser> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(SystemUser data) throws SQLException {
        int ret = super.create(data);

        List<UserInSystem> userInSystemList = data.getUsers().getItem();
        if(userInSystemList == null)
            userInSystemList = new ArrayList<UserInSystem>();
        data.userInSystemCollection = new ArrayList<UserInSystem>(userInSystemList);

        for(UserInSystem uis : data.userInSystemCollection) {
            uis.setSystemUser(data);
            ret += App.getDatabaseHelper().getUserInSystemDao().create(uis);
        }

        return ret;
    }

    public List<SystemUser> getByIdForList(List<String> systemUserIds) throws SQLException {
        List<SystemUser> systemUsers = new ArrayList<SystemUser>();

        for(String id : systemUserIds) {
            getById(id);
        }

        return systemUsers;
    }

    public SystemUser getById(String systemUserId) throws SQLException {
        QueryBuilder<SystemUser, String> q = queryBuilder();
        q.where().eq("id", systemUserId);

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<SystemUser> preparedQuery = q.prepare();
        List<SystemUser> tmpList = query(preparedQuery);
        return tmpList.get(0);
    }

    public List<SystemUser> getAllUsers() throws SQLException {
        QueryBuilder<SystemUser, String> q = queryBuilder();
        q.orderBy("short_name", true);

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<SystemUser> preparedQuery = q.prepare();
        return query(preparedQuery);
    }

    public SystemUser getBylogin(String login) throws SQLException {
        QueryBuilder<SystemUser, String> q = queryBuilder();
        q.where().eq("name", login);

        Log.d(TAG, q.prepareStatementString());

        PreparedQuery<SystemUser> preparedQuery = q.prepare();
        List<SystemUser> tmpList = query(preparedQuery);
        return tmpList.get(0);
    }

    public List<SystemUser> getByTaskActorList(List<TaskActor> taskActors) throws SQLException {
        List<SystemUser> systemUsers = new ArrayList<SystemUser>();

        for(TaskActor taskActor : taskActors) {
            QueryBuilder<SystemUser, String> q = queryBuilder();
            q.where().eq("id", taskActor.getSystemUserId());

            Log.d(TAG, q.prepareStatementString());

            PreparedQuery<SystemUser> preparedQuery = q.prepare();
            List<SystemUser> tmpList = query(preparedQuery);

            systemUsers.addAll(tmpList);
        }
        return systemUsers;
    }

}
