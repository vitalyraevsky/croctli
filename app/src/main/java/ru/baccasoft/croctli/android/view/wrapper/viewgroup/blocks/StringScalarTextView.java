package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class StringScalarTextView extends TextView {

    public StringScalarTextView(Context context, String text) {
        super(context);
        setMaxLines(1);
        setText(text);

        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
    }
}
