package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.TextScalarEditText;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.TextScalarTextView;

/**
 * Created by developer on 06.11.14.
 */
public class TextScalarWithTitle extends LinearLayout {
    public TextScalarWithTitle(Context context, String title, String text, boolean isReadonly) {
        super(context);

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView titleView = ViewUtils.makeTitleTextView(context, title, null);
        addView(titleView);

        if (isReadonly) {
            TextScalarTextView textShowScalar = new TextScalarTextView(context, text);
            textShowScalar.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            addView(textShowScalar);
        } else {
            TextScalarEditText textEditScalar = new TextScalarEditText(context, text);
            textEditScalar.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            addView(textEditScalar);
        }
    }
}
