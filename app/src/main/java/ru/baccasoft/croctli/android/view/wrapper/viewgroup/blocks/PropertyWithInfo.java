package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ClassifierPopup;

import java.util.Map;

/**
 * 2.8.2.1	Справочные свойства с DisplayMode = 1 и пользователи
 * Отрисовывается в виде подчеркнутого текста (<- по нашему макету, в оригинале было в виде кнопки).
 * По клику открывается попап с дополнительной информацией.
 *
 */

public class PropertyWithInfo extends TextView {

    private ClassifierPopup mPopup;

    private Context mContext;
//    private LayoutInflater inflater;

    public PropertyWithInfo(Context context, String text,
                            final String popupTitle, final Map<String, String> popupItems) {
        super(context);

        mContext = context;
//        inflater = LayoutInflater.from(context);

        initThis(text);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // говорим всем заинтересованным попапам, что им пора свалить с экрана
//                EventBus.getDefault().post(new DismissPopupEvent());

                if(mPopup == null)
                    mPopup = new ClassifierPopup(mContext, popupTitle, popupItems);

                mPopup.showUnder(v);
            }
        });
    }

    private void initThis(String text) {
        this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        this.setTextColor(getResources().getColor(R.color.text_2A2A2A));
        this.setTextSize(getResources().getDimension(R.dimen.px_16));

        this.setFocusable(true);

        this.setText(text);
    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//    }
}
