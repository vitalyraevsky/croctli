package ru.baccasoft.croctli.android.rest.wrapper;



import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.UserActivity;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;

public class UserActivityAPI implements IRestApiCall<UserActivity, Void, Void, Void> {

    /**
     * Прим.: особый вызов, плюет на входные параметры
     * @param dateFrom игнорит
     * @param dateTo игнорит
     * @param max игнорит
     */
    @Override
    public UserActivity modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String api = "/server/activity";
        final boolean needAuth = true;

        return RestClient.fillJsonClass(UserActivity.class, api, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, Void objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
