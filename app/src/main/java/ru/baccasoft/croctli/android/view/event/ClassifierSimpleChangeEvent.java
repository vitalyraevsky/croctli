package ru.baccasoft.croctli.android.view.event;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.db.tables.Preload;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.validator.BaseValidator;
import ru.baccasoft.croctli.android.view.validator.NotNullValidator;

public class ClassifierSimpleChangeEvent implements IChangeEvent {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierSimpleChangeEvent.class.getSimpleName();

    private final String entityPropertyId;
    private final String value;
    private final Context context;
    private final List<BaseValidator> validators;

    public ClassifierSimpleChangeEvent(String entityPropertyId, String value, Context context) {
        this.entityPropertyId = entityPropertyId;
        this.value = value;
        this.context = context;
        validators = new ArrayList<BaseValidator>(0);

        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        validators.add(new NotNullValidator(this.context, ep, value));
    }

    @Override
    public void save() {
        Log.d(TAG, "trying to save value=\'" + this.value + "\' to Ep with id=\'" + entityPropertyId + "\'");

        EntityProperty ep = TableUtils.getEntityPropertyById(entityPropertyId);
        List<ClassifierItem> classifierItems = Preload.getClassifierItemsByClassiferId(ep);
        Map<String, ClassifierItem> valueClassifierItemMap = new HashMap<String, ClassifierItem>();
        for(ClassifierItem ci : classifierItems) {
            valueClassifierItemMap.put(ci.getDisplayValue(), ci);
        }

        String valueToSave;
        if(valueClassifierItemMap.containsKey(this.value)) {
            valueToSave = valueClassifierItemMap.get(this.value).getCode();
        } else {
            valueToSave = "";
        }

        Log.d(TAG, "saving value=\'" + valueToSave + "\' to Ep with id=\'" + entityPropertyId + "\'");

        ep.setValue(valueToSave);
        TableUtils.updateEntityProperty(ep, true);
    }

    @Override
    public String getKey() {
        return entityPropertyId;
    }

    @Override
    public boolean isNotEmpty() {
        if(value.isEmpty()){
            return false;
        }
            return true;

    }

    @Override
    public List<BaseValidator> getValidators() {
        return validators;
    }

}
