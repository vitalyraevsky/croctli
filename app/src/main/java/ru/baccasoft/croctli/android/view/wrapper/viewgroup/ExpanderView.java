package ru.baccasoft.croctli.android.view.wrapper.viewgroup;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.view.event.AddObjectArrayElement;
import ru.baccasoft.croctli.android.view.event.RemoveObjectArrayElement;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Created by developer on 10.11.14.
 */
public class ExpanderView extends LinearLayout {
    private Context mContext;

    private ViewGroup titleView;
    private TextView tText;
    private ImageView tStateIcon;
    private ImageView tActionIcon;
    private LinearLayout contentView;

    private boolean isTopExpander;  // является ли экспандер четного уровня вложенности
    private boolean isReadOnly;
    private int nestLevel;

    /**
     * Экспандер с заголовком и областью содержимого (LinearLayout contentView).
     * Клик по заголовку открывает/закрывает экспандер.
     * В экспандер можно добавить дополнительные вьюшки методом ExpanderView.addSubView(View childView).
     * Добавленные вьюшки будут помещены в contentView, т.е. будут иметь отступы слева.
     *
     * @param context
     * @param title
     * @param nestLevel уровень вложенности экспандера в другие экспандеры.<br>
     *                  для экспандера первого уровня == 1.<br>
     *                  для следующего 2 и т.д.<br>
     *                  влияет на раскраску заголовка экспандера<br>
     *                  должен быть не меньше 1
     * @param readOnly нужно ли в заголовке добавлять кнопку редактирования вложенного контента (true - не нужно)
     * @param entityPropertyId идентификатор EP, который является корнем для корнем сложного массивного свойства.
     *                         1. среди его потомков может быть один с полем OrderInArray==-1
     *                         2. остальные потомки являются сложными свойствами, которые будут отображаться
     *                         каждое в своем экспандере внутри этого экспандера
     *
     */
    public ExpanderView(Context context, String title, int nestLevel, boolean readOnly, String entityPropertyId) {
        super(context);

        if(nestLevel < 1)
            throw new IllegalArgumentException("nesting level can't be less than 1");

        this.mContext = context;
        this.isReadOnly = readOnly;
        this.nestLevel = nestLevel;
        this.isTopExpander = !(nestLevel % 2 == 0);
        setTag(ViewUtils.VIEW_ENTITY_PROPERTY_TAG_KEY, entityPropertyId);

        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        initTitleView(context, title, nestLevel);
        initContentView(context);

        addView(titleView);
        addView(contentView);

        // в самый низ добавляем кнопку добавления массива
        if(!readOnly && isTopExpander) {
            initAddActionButton(context, entityPropertyId);
        }
    }

    private void initAddActionButton(Context context, final String entityPropertyId) {
        //fixme: внимание, грязно. вынести работу с базой отсюда
        // если нет шаблона для добавления, кнопку не показываем
        try {
            TableUtils.getEPObjectArrayAddingTemplate(entityPropertyId);
            // в текущей реализации getEPObjectArrayAddingTemplate
            // если не нашли шаблона, кидает исключение
        } catch (IllegalStateException ex) {
            return;
        }

        LinearLayout addActionLL = ViewUtils.makeHorizontalContainer(context);
        ImageView addActionButton = new ImageView(context);
        addActionButton.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        addActionButton.setImageResource(R.drawable.plus);

        // добавляем новый массив объектов в экспандер
        addActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new AddObjectArrayElement(entityPropertyId, ExpanderView.this, nestLevel));
            }
        });

        addActionLL.addView(addActionButton);
        addView(addActionLL);
    }

    /**
     * Задаем параметры области экспандера, в которую будут добавляться
     * вложенные в экспандер элементы
     */
    private void initContentView(Context context) {
        contentView = new LinearLayout(context);
        contentView.setOrientation(VERTICAL);
        contentView.setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        contentView.setPadding(20, 0, 0, 0);
    }

    /**
     * Создаем заголовок экспандера. Цвет заголовка и отображение текста зависит от
     * уровня вложенности экспандера (уровень = во сколько экспандеров он вложен +1)
     */
    private void initTitleView(Context context, String title, int nestLevel) {
        //сам заголовок экспандера
        titleView = ViewUtils.makeHorizontalContainer(context, Gravity.CENTER_VERTICAL);
        ((LinearLayout) titleView).setOrientation(LinearLayout.HORIZONTAL);

        titleView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentView.getVisibility() == VISIBLE) {
                    contentView.setVisibility(GONE);
                    changeExpanderStateIcon(false);
                    return;
                }
                contentView.setVisibility(VISIBLE);
                changeExpanderStateIcon(true);
            }
        });

        // иконка со стрелочкой
        tStateIcon = new ImageView(context);
        LayoutParams llpStateIcon = new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        llpStateIcon.gravity = Gravity.CENTER_VERTICAL;
        tStateIcon.setLayoutParams(llpStateIcon);
        tStateIcon.setPadding(4,0,4,0);
        changeExpanderStateIcon(true);

        // текст заголовка
        tText = new TextView(context);
        LayoutParams llpText = new LayoutParams(
                0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tText.setLayoutParams(llpText);
//        tText.setLayoutParams(new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        Log.d("", "title text:" + title);
        tText.setText(title);

        // иконка с действием (удалить или изменить)
        tActionIcon = new ImageView(context);
        tActionIcon.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        changeActionIcon();

        tActionIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isReadOnly) { return; }
                Log.d("", "removing expander from view");
                // убрать с экрана
                EventBus.getDefault().post(new RemoveObjectArrayElement(
                        (String) getTag(ViewUtils.VIEW_ENTITY_PROPERTY_TAG_KEY), ExpanderView.this));
            }
        });

        titleView.addView(tStateIcon);
        titleView.addView(tText);
        titleView.addView(tActionIcon);

        // изменяем цвет заголовка в зависимости от уровня вложенности экспандера
        // (экспандер может находится внутри других экспандеров)
        if(!isTopExpander) { // рисуем эспандер второго уровня
            titleView.setBackgroundColor(getResources().getColor(R.color.text_C2C1C1));
            tText.setTextColor(getResources().getColor(R.color.text_444444));
        } else {    // экспандер первого уровня
            titleView.setBackgroundColor(getResources().getColor(R.color.text_FFFFFF));
            tText.setTextColor(getResources().getColor(R.color.text_777777));
            tText.setTypeface(null, Typeface.BOLD);
        }
    }

    private void changeActionIcon() {
        Log.d("", "changing expander action icon state");
        if(!isReadOnly) {
            if (!isTopExpander) {// иконка удаления для отдельного массива объектов
                tActionIcon.setImageResource(R.drawable.cross);
            }
        }
    }

    private void changeExpanderStateIcon(boolean expanded) {
        if(expanded) {
            tStateIcon.setImageResource(R.drawable.expander_icon_opened);  //стрелка вниз
        } else {
            tStateIcon.setImageResource(R.drawable.expander_icon_closed);  //стрелка вправо
        }
    }

    @Deprecated
    public LinearLayout getContentView() {
        return contentView;
    }

    /**
     * Добавить вьюшку в конец блока с содержимым.
     *
     * @param child дочерняя вьюшка
     * @param isTemplate создана ли вьюшка на основе шаблона (EP с полем orderInArray==-1)
     *                   true -
     */
    public void addSubView(View child, boolean isTemplate) {
//        super.addView(child);
        contentView.addView(child);
    }
}
