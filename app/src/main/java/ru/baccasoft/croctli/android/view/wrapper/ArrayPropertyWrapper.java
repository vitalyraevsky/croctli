package ru.baccasoft.croctli.android.view.wrapper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.view.SpecialAttributesWrapper;
import ru.baccasoft.croctli.android.view.util.ViewUtils;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.ExpanderView;
import ru.baccasoft.croctli.android.view.wrapper.viewgroup.StringScalarWithTitle;

import java.util.List;

/**
 * Сюда приходит EntityProperty с флагом isArrayProperty= true;
 *
 * Это может быть
 *      либо массивное сложное свойство
 *      либо массивное справочное свойство
 *      либо массивное необъектное свойство (string, date etc)
 *
 */
// TODO: выбранные значения массива сохраняются в EntityProperty.value через разделитель
// ТОЛЬКО для необъектных типов свойств, вроде "string", "date" и т.д.
// для "date" и "dateTime" все значения приходят правильно сформированными - п. 2.8.3 Формат дат и времени.
// иначе будем падать.
// все значения разделены символом @
// если в значении встречается символ @, он экранируется


public class ArrayPropertyWrapper implements IWrapper {
    private Activity mActivity;
    private Context mContext;
    private boolean readOnly;
    private int currentNestLevel;


    public ArrayPropertyWrapper(Activity activity, int currentNestLevel) {
        this.mActivity = activity;
        this.mContext = this.mActivity.getApplicationContext();
        this.currentNestLevel = currentNestLevel;
    }

    @Override
    public View getLayout(EntityProperty p, Boolean globalReadOnly) {
        validate(p);
        this.readOnly = ViewUtils.isReadOnly(p, globalReadOnly);

        // для обычных массивов скаляров
        if(ViewUtils.isNotEmpty(p.getScalarType())) {
            return getScalarArray(p, this.readOnly);
        }

        // массив из значений справочника
        if(ViewUtils.isNotEmpty(p.getClassifierTypeId())) {
            //TODO:
        }

        // для помороченных массивов делаем экспандер
        String expanderTitle = p.getDisplayGroup();
        final int newNestLevel = currentNestLevel + 1;
        expanderTitle = ViewUtils.textFromPattern(expanderTitle, p.getId());
        Log.d("", "expander title:\'"+expanderTitle+"\'");
        ExpanderView expanderView = new ExpanderView(mContext, expanderTitle, newNestLevel, this.readOnly, p.getId());

        List<EntityProperty> childProps = TableUtils.getEntityPropertyChildsById(p.getId());
        Log.d("", " for EP id:\'"+p.getId()+"\' have " + childProps.size() + " childs");
        if(ViewUtils.isEmpty(childProps)) {
            return expanderView;
        }

        SpecialAttributesWrapper attributesWrapper = new SpecialAttributesWrapper(
                childProps, mActivity, newNestLevel, this.readOnly);

        View childView = attributesWrapper.getLayout();
        expanderView.addSubView(childView, false);

        return expanderView;
    }

    private View getScalarArray(EntityProperty p, boolean readOnly) {
        String scalarType = p.getScalarType();
        String content = restoreContent(p.getValue());
        //TODO: возвращать не скаляр, а вьюшку для массивных справочных свойств
        // с DisplayMode=0 (Название: дата, дата, дата)
        StringScalarWithTitle view = new StringScalarWithTitle(
                mContext, p.getDisplayGroup(), content, readOnly, p.getId());
        return view;
    }

    /**
     * Распарсить данные, сохраненные в строку:
     * все значения разделены символом @
     * @param value
     * @return
     */
    private String restoreContent(String value) {
        //TODO: парсить данные, разделенные символом @
        return value;


    }

    private void validate(EntityProperty p) {
        if(!p.isIsArrayProperty())
            throw new IllegalStateException("field isArrayProperty must be true. EntityPropertyId=\'" + p.getId());

    }


}
