package ru.baccasoft.croctli.android.filters;

import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.ConditionOperator;
import ru.baccasoft.croctli.android.gen.core.FilterCondition;
import ru.baccasoft.croctli.android.gen.core.Task;

public class TaskFilterFactory {
    @SuppressWarnings("unused")
    private static final String TAG = TaskFilterFactory.class.getSimpleName();

    public ITaskFilter createFilter(final FilterCondition condition, ConditionOperator operator) {
        if (operator.getCode().equals("Equals")) {
            if (condition.getEntityPropertyName().equals("TaskStatus")) {
                return new ITaskFilter() {
                    @Override
                    public boolean isAvailable(Task task) {
                        return task.getTaskStateId().equals(condition.getValue());
                    }
                };
            }
        }

        String message = "impossible to create filter for this condition and predicate";
        TableUtils.createLogMessage(message, "");
        Log.e(TAG, message);

        throw new UnsupportedOperationException(message);
    }
}
