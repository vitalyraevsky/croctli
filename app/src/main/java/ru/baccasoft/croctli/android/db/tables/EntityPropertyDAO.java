package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import org.joda.time.DateTime;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Attachment;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EntityPropertyDAO extends BaseDaoImpl<EntityProperty, String> {
    @SuppressWarnings("unused")
    private static final String TAG = EntityPropertyDAO.class.getSimpleName();

    public EntityPropertyDAO(ConnectionSource connectionSource, Class<EntityProperty> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(EntityProperty data) throws SQLException {
        int ret = super.create(data);

        if(data.getBinaryValue() != null) {
            Attachment attachment = data.getBinaryValue();
            attachment.setEntityProperty(data);

            ret += App.getDatabaseHelper().getAttachmentDAO().create(attachment);
        }
        return ret;
    }

    /**
     * Получить список EntityProperty для конкретной задачи по taskId.
     * Записи отсортированы по полю displayOrder по возрастанию.
     */
    public List<EntityProperty> getByTaskId(String taskId) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.orderBy("displayOrder", true).where().eq("taskId", taskId);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    /**
     * Получить список EntityProperty для конкретной задачи по taskId.
     * Записи отсортированы по полю displayOrder по возрастанию.
     */
    public List<Attachment> getAttachmentsByTaskId(String taskId) throws SQLException {

        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.orderBy("displayOrder", true).where().eq("taskId", taskId).and().isNotNull("binaryvalue_id");

        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        List<EntityProperty> entities = query(preparedQuery);
        List<String> ids = new ArrayList<String>();
        for( EntityProperty ep :entities ){
            ids.add(ep.getBinaryValue().getId());
        }

        return App.getDatabaseHelper().getAttachmentDAO().getAttachemntsByIds(ids);
    }

    /**
     * Получить список дочерних EntityProperty.<br>
     * Т.е. тех, у которых поле entityPropertyId совпадает с переданным идентификатором родителя.<br>
     * Записи отсортированы по полю displayOrder по возрастанию.
     *
     * @param propId - идентификатор родительского EntityProperty
     * @return список всех потомков, за исключением тех, у которых
     * поле orderInArray != -1 (исключенные EntityProperty являются шаблоном и не отображаются, см. ФС андроид)
     */
    public List<EntityProperty> getChildsById(String propId) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.orderBy("displayOrder", true).where().eq("entityPropertyId", propId)
                .and().ne("orderInArray", -1);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    /**
     * Получить шаблон для добавления новых элементов в массив объектов
     *
     * @param propId идентификатор родителя (поле entityPropertyId у дочерних свойств)
     * @return список всех потомков, у которых
     * поле orderInArray == -1 (полученный EP является шаблоном для добавления новых элементов)
     */
    public List<EntityProperty> getTemplateChildById(String propId) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.orderBy("displayOrder", true).where().eq("entityPropertyId", propId)
                .and().eq("orderInArray", -1);
        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

//    public List<EntityProperty> getOrganisationOfTask(String taskId) throws SQLException {
//        final String key = "Организация";
//
//        QueryBuilder<EntityProperty, String> q = queryBuilder();
//        q.where().eq("taskId", taskId)
//            .and().eq("displayGroup", key);
////        Log.d(TAG, q.prepareStatementString());
//        PreparedQuery<EntityProperty> preparedQuery = q.prepare();
//
//        return query(preparedQuery);
//    }

//    public List<EntityProperty> getUrgencyOfTask(String taskId) throws SQLException {
//        final String key = "Срочность";
//
//        QueryBuilder<EntityProperty, String> q = queryBuilder();
//        q.where().eq("taskId", taskId)
//                .and().eq("displayGroup", key);
////        Log.d(TAG, q.prepareStatementString());
//        PreparedQuery<EntityProperty> preparedQuery = q.prepare();
//
//        return query(preparedQuery);
//    }

    public List<EntityProperty> getErrands(String taskId) throws SQLException {
        final String key = "instructions"; // todo: имя брать из ресурсов

        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.where().eq("taskId", taskId)
                .and().eq("name", key);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    public List<EntityProperty> getModifiedEntityProperties() throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.where().eq("was_changed", Boolean.TRUE);
        //Log.d(TAG, q.prepareStatementString());

        PreparedQuery<EntityProperty> preparedQuery = q.prepare();
        List<EntityProperty> entityProperties = query(preparedQuery);
        return entityProperties;
    }

    public List<EntityProperty> getByDisplayGroup(String taskId, String displayGroup) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.where().eq("taskId", taskId)
                .and().eq("displayGroup", displayGroup);
//        Log.d(TAG, q.prepareStatementString());
        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        return query(preparedQuery);
    }

    public List<String> getIdsByTaskId(String id) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.distinct().selectColumns("id").where().eq("taskId", id);

        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        List<EntityProperty> tmp = query(preparedQuery);
        List<String> result = new ArrayList<String>(tmp.size());
        for(EntityProperty p : tmp) {
            result.add(p.getId());
        }

        return result;
    }

    public List<String> getChildsIdsByEpId(String epId) throws SQLException {
        QueryBuilder<EntityProperty, String> q = queryBuilder();
        q.distinct().selectColumns("id").where().eq("entityPropertyId", epId);

        PreparedQuery<EntityProperty> preparedQuery = q.prepare();

        List<EntityProperty> tmp = query(preparedQuery);
        List<String> result = new ArrayList<String>(tmp.size());
        for(EntityProperty p : tmp) {
            result.add(p.getId());
        }

        return result;
    }
}
