package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.event.ClassifierSimpleChangeEvent;
import ru.baccasoft.croctli.android.view.event.EmptyFieldEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

//import android.view.KeyEvent;

/**
 * Created by developer on 06.11.14.
 */
public class ClassifierDisplayMode1 extends AutoCompleteTextView {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierDisplayMode1.class.getSimpleName();

//    private PassThroughFocusChangeListener mPassThroughFocusChangeListener;
    private PassThroughClickListener mPassThroughClickListener;
    //private PassThroughKeyListener mPassThroughKeyListener;

    Context mContext;
    Map<String, String> items;
    final String entityPropertyId;

    ArrayAdapter adapter;
    ArrayAdapter clearAdapter;
    final int itemResId = android.R.layout.simple_dropdown_item_1line;
    final Validator validator = new Validator();

    /**
     *
     * @param context
     * @param items пары [ключ : выводимое значение]
     */
    public ClassifierDisplayMode1(final Context context, final Map<String, String> items, final String entityPropertyId) {
        super(context);
        this.items = items;
        this.mContext = context;
        this.entityPropertyId = entityPropertyId;

        clearAdapter = new ArrayAdapter(context, itemResId);

        final List<String> values = new ArrayList<String>();
        for(Map.Entry<String, String> e : this.items.entrySet()) {
            values.add(e.getValue());
        }

        adapter = new ArrayAdapter(context, itemResId, values);
        setAdapter(adapter);

        mPassThroughClickListener = new PassThroughClickListener();
        super.setOnClickListener(mPassThroughClickListener);

        //mPassThroughKeyListener = new PassThroughKeyListener();
        //super.setOnKeyListener(mPassThroughKeyListener);

        initThis();
        EventBus.getDefault().register(this);

        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedValue = values.get(position);

                Log.d(TAG, "item selected: \'" + selectedValue + "\'");
                EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, selectedValue, mContext));
            }
        });
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        mPassThroughClickListener.mWrapped = listener;
    }

    /*@Override
    public void setOnKeyListener(OnKeyListener l) {
        mPassThroughKeyListener.mWrapped = l;
    }*/

    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер.
     * Мы же отслеживаем backspace и удаляем всю строку
     *
     */
    /*private class PassThroughKeyListener implements OnKeyListener {
        private OnKeyListener mWrapped;

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            onKeyImpl(keyCode, event);

            if(mWrapped != null)
                mWrapped.onKey(v, keyCode, event);

            return false;
        }
    }

    private void onKeyImpl(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP) {
            // очищаем ввод
            onClear();
            EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, ""));
        }
    }*/

    /*@Override
    public void onFilterComplete(int count) {
        super.onFilterComplete(count);

        if (count == 1) { // подставляем предлагаемое значение и скрываем попап
            setListSelection(0);
            performCompletion();

//            EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, getText().toString()));
        } else if (count == items.size()) {
            showDropDown();
        }
    }*/

    /**
     * Обертка, чтобы пользователь класса мог устанавливать свой листнер
     */
    private class PassThroughClickListener implements OnClickListener {
        private OnClickListener mWrapped;

        @Override
        public void onClick(View v) {
            onClickImpl();

            if(mWrapped != null)
                mWrapped.onClick(v);
        }
    }

    private void onClickImpl() {
        if(!TextUtils.isEmpty(getText().toString())){
            performFiltering("", -1);
            EventBus.getDefault().post(new ClassifierSimpleChangeEvent(entityPropertyId, "", mContext));
        }
        if(!isPopupShowing())
            showDropDown();
    }

    /**
     * Проверяет, что введено значение только из переданного списка.
     * В качестве исправления неверного ввода отдает пустую строку.
     */
    private class Validator implements AutoCompleteTextView.Validator {

        @Override
        public boolean isValid(CharSequence text) {
            return items.containsValue(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            return "";
        }
    }

    /*private void onClear() {
        setText("");
        performFiltering("", -1);
    }*/

    private void initThis() {
        setThreshold(1);
        setMaxLines(1);
        setInputType(InputType.TYPE_NULL);
        setTextColor(getResources().getColor(R.color.text_2A2A2A));
        setBackgroundColor(mContext.getResources().getColor(R.color.text_F2F2F2));
        ViewUtils.setTextSize(this, R.dimen.px_16, mContext);
    }

    public void onEvent(EmptyFieldEvent event){
        if(getText().toString().isEmpty()) {
            setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().
                    getDrawable(R.drawable.ic_error), null);
        }else{
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }
}
