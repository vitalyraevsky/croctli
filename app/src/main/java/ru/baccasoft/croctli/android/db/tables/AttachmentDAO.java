package ru.baccasoft.croctli.android.db.tables;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.Attachment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AttachmentDAO extends BaseDaoImpl<Attachment, String> {


    private static final String TAG = AttachmentDAO.class.getSimpleName();

    public AttachmentDAO(ConnectionSource connectionSource, Class<Attachment> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Attachment> getNotDownloaded() throws SQLException {
        QueryBuilder<Attachment, String> q = queryBuilder();
        q.orderBy( "name" , true );
        PreparedQuery<Attachment> preparedQuery = q.prepare();
        return query(preparedQuery);

    }

    //TODO: Надо выбрать сразу по айдишникам
    public List<Attachment> getAttachemntsByIds(List<String> ids) throws SQLException {

        Log.d( TAG, "ids size : " + ids.size() );
        List<Attachment> attachments = new ArrayList<Attachment>();

        for(String id : ids){
//            Log.d(TAG,"attachments id " + id);
            Attachment at = queryForId(id.trim());
            if( at!= null ) {
                Log.d(TAG,"attachments id " + id + " is not null");
                attachments.add(at);
            } else{
                Log.d(TAG,"attachments id " + id + " is null");
            }
        }

        Log.d( TAG, "attachments size : " + attachments.size() );
        return attachments;
//        QueryBuilder<Attachment, String> q = queryBuilder();
//        q.where().in( "id", ids );
//
//        PreparedQuery<Attachment> preparedQuery = q.prepare();
//
//        return query(preparedQuery);
//        return queryForAll();
    }


}
