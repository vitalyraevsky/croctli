package ru.baccasoft.croctli.android.specialcondition.predicate;

import ru.baccasoft.croctli.android.gen.core.Task;
import ru.baccasoft.croctli.android.specialcondition.bool.IBooleanAtom;

public class BooleanFalse implements IBooleanAtom {
	
	public static final String ATOM_NAME = "false";

	@Override
	public Boolean compute(Task task) {
		return false;
	}

	@Override
	public IBooleanAtom create() {
		return new BooleanFalse();
	}

	@Override
	public String getAtomName() {
		return ATOM_NAME;
	}
}
