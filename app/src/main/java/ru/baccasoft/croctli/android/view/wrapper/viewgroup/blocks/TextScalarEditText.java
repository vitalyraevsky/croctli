package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.widget.EditText;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

/**
 * Для скаляров типа "text" в режиме редактирования
 *
 */
public class TextScalarEditText extends EditText {
    public TextScalarEditText(Context context, String text) {
        super(context);

        setText(text);

        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
    }
}
