package ru.baccasoft.croctli.android.view.event;

/**
 * Отсылать после того, как вьюшка с массивом объектов была добавлена
 * в экспандер.
 * Поскольку EP.id будет являтся идентификатором шаблона,
 * при получении этого события нужно этот идентификатор сменить.
 *
 */
public class ObjectArrayElementAdded {
    private String oldEntityPropertyId;
    private String newEntityPropertyId;

    public ObjectArrayElementAdded(String oldEntityPropertyId, String newEntityPropertyId) {
        this.oldEntityPropertyId = oldEntityPropertyId;
        this.newEntityPropertyId = newEntityPropertyId;
    }

    public String getOldEntityPropertyId() {
        return oldEntityPropertyId;
    }

    public String getNewEntityPropertyId() {
        return newEntityPropertyId;
    }
}
