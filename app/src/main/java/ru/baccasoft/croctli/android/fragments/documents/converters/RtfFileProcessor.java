package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.util.Log;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.RtfToHtml;

import ae.javax.xml.bind.JAXBException;


public class RtfFileProcessor implements ConvertationProcessor {

    private String TAG = RtfFileProcessor.class.getSimpleName();

    private RtfToHtml rtfToHtml;
    private Writer writer;

    @Override
    public void init(String officeFileName) throws Exception {
        writer = new StringWriter();
        rtfToHtml = RtfToHtml.create( officeFileName, writer );
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException, IOException {
        rtfToHtml.print();
        String html = writer != null ? writer.toString() : null ;
        writer.close();
        Log.d(TAG,"html : " + html);
        return html;
    }
}
