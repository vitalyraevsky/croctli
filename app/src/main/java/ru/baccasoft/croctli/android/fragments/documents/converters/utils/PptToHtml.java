package ru.baccasoft.croctli.android.fragments.documents.converters.utils;

import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Formatter;

import and.awt.Dimension;
import and.org.apache.poi.hslf.model.Picture;
import and.org.apache.poi.hslf.model.Shape;
import and.org.apache.poi.hslf.model.Slide;
import and.org.apache.poi.hslf.usermodel.PictureData;
import and.org.apache.poi.hslf.usermodel.SlideShow;


public class PptToHtml {

    private static final String TAG = PptToHtml.class.getSimpleName();

    private HSLFSlideToImageConversionHandler imageConversionHandler;
    private SlideShow slideShow;
    private Writer output;
    private Formatter out;

    private PptToHtml(SlideShow slideShow, Writer output) {
        this.slideShow = slideShow;
        this.output = output;
    }

    public static PptToHtml create(String path, Writer out) throws IOException {
        return create(new FileInputStream(path), out);
    }

    private static PptToHtml create(InputStream in, Writer out) {
        try {
            SlideShow slideShow = new SlideShow(in);
            return create(slideShow, out);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot create workbook from stream", e);
        }
    }

    private static PptToHtml create(SlideShow slideShow, Writer out) {
        return new PptToHtml(slideShow, out);
    }

    private void ensureOut() {
        if (out == null)
            out = new Formatter(output);
    }

    public void printSlides() throws IOException {

        ensureOut();

        out.format("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>%n");
        out.format("<html>%n");
        out.format("<head>%n");
        out.format("</head>%n");
        out.format("<body>%n");

        print();

        out.format("</body>%n");
        out.format("</html>%n");

        Log.d(TAG, "parsed ppt : " + output.toString());
    }

    private void print() throws IOException {

        PictureData[] picsData = slideShow.getPictureData();
        if(picsData != null) {
            Log.d(TAG, "slideshow picsData size: " + picsData.length);
            for (PictureData pic : picsData) {
                Log.d(TAG, "pic get data : " + pic.getData().length);
            }
        }
        Slide[] slides = slideShow.getSlides();
        for (int i = 0; i < slides.length; i++) {
            Slide slide = slides[i];
            for(Shape shape :slide.getShapes()){
                if(shape instanceof Picture){
                    Picture pic = ((Picture) shape);
                    Log.d(TAG,"pic anme: " +pic.getPictureName() + " data length:"+ pic.getPictureData().getData().length);
                }
            }
            printSlideAsImage( slide,"slide" + (i+1), slideShow.getPageSize() );
        }

    }

    private void printSlideAsImage(Slide slide,String suggestedName, Dimension pgsize) throws IOException {
        String imagePath = imageConversionHandler.convertSlideToImage(slide,suggestedName,pgsize);
        out.format("<img name='" + suggestedName + ".png' src='" + imagePath + " ' align='middle'/>%n");
        out.format("</br>%n");
        out.format("</br>%n");
    }

    public void setImageConversionHandler(HSLFSlideToImageConversionHandler imageConversionHandler) {
        this.imageConversionHandler = imageConversionHandler;
    }
}
