package ru.baccasoft.croctli.android.fragments.documents;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import ru.baccasoft.croctli.android.db.TableUtils;

public abstract class AttachmentViewFragment extends Fragment {

    private static final String TAG = AttachmentViewFragment.class.getSimpleName();

    public static final String ARG_ATTACH_PATH = "attach_file_path";
    public static final String ARG_ATTACH_NAME = "attach_file_name";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if( getArguments() == null || getAttachmentPath() == null || getAttachmentName() ==null ) {
            String message = ARG_ATTACH_PATH + " and " + ARG_ATTACH_NAME + " must not be empty";
            TableUtils.createLogMessage(message, "");
            Log.e(TAG, message);
        }

        Log.d(TAG,"attachmentPath : " + getAttachmentPath());
        Log.d(TAG,"attachmentName : " + getAttachmentName());
    }

    public String getAttachmentPath() {
        return getArguments().getString(ARG_ATTACH_PATH);
    }

    public String getAttachmentName() {
        return getArguments().getString(ARG_ATTACH_NAME);
    }

}
