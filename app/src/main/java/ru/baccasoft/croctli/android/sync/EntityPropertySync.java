package ru.baccasoft.croctli.android.sync;

import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.db.TableUtils;
import ru.baccasoft.croctli.android.gen.core.Attachment;
import ru.baccasoft.croctli.android.gen.core.EntityProperty;
import ru.baccasoft.croctli.android.gen.core.EntityPropertySet;
import ru.baccasoft.croctli.android.gen.core.ArrayOfEntityProperty;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestApiRecursiveCall;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.wrapper.EntityPropertyAPI;

/**
 * SD, SM, GD, GM
 */
public class EntityPropertySync implements ISync {
    @SuppressWarnings("unused")
    private static final String TAG = EntityPropertySync.class.getSimpleName();
    private List<Attachment> oldAttachments;

    @Override
    public void atFirst(RestApiDate dateFrom, RestApiDate dateTo) {
        gm(dateFrom, dateTo);
    }

    @Override
    public void byRequest(RestApiDate dateFrom, RestApiDate dateTo) {
        //TODO: нет алгоритма отправки
//        sd(dateFrom, dateTo);
//        sm(dateFrom, dateTo);
        gd(dateFrom, dateTo);
        gm(dateFrom, dateTo);

        //1. Локальные измененные проперти
        final List<EntityProperty> localModifiedEntity = TableUtils.getChangedEntityPeoperties();
        EntityPropertyAPI entityPropertyAPI = new EntityPropertyAPI();
        if (localModifiedEntity.size() > 0) {
            ArrayOfEntityProperty arrayOfEntityProperty = new ArrayOfEntityProperty();
            arrayOfEntityProperty.setItem(localModifiedEntity);
            entityPropertyAPI.modifyPost(dateFrom, arrayOfEntityProperty);
        }
    }

    private void gm(RestApiDate dateFrom, RestApiDate dateTo) {
        EntityPropertyAPI entityPropertyAPI = new EntityPropertyAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<EntityPropertySet, EntityProperty>();

        try {
            oldAttachments = App.getDatabaseHelper().getAttachmentDAO().getNotDownloaded();
            Log.d("TAG", "Size: " + oldAttachments.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        @SuppressWarnings("unchecked")
        List<EntityProperty> entityProperties = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, entityPropertyAPI);

        if(oldAttachments.isEmpty()){
            for(EntityProperty ep : entityProperties) {
                TableUtils.createOrUpdateEntityProperty(ep);
            }
        }else{
            for(EntityProperty ep : entityProperties) {
                if(ep.getBinaryValue() != null){
                    for(Attachment at: oldAttachments){
                        if(at.getName().equals(ep.getBinaryValue().getName())
                                && at.getCheckSum().compareTo(ep.getBinaryValue().getCheckSum()) != 0){
                            at.setLocalLocation(null);
                        }
                    }
                }
                TableUtils.createOrUpdateEntityProperty(ep);
            }
        }

        /*for(EntityProperty ep : entityProperties) {
            TableUtils.createOrUpdateEntityProperty(ep);
        }*/
    }

    private void gd(RestApiDate dateFrom, RestApiDate dateTo) {
        EntityPropertyAPI entityPropertyAPI = new EntityPropertyAPI();
        RestApiRecursiveCall recursiveCall = new RestApiRecursiveCall<StringSet, String>();

        @SuppressWarnings("unchecked")
        List<String> epIds = recursiveCall.getRestApiSetAsList(dateFrom, dateTo, SYNC_MAX_COUNT, entityPropertyAPI);

        TableUtils.deleteEntityProperties(epIds);
    }
}
