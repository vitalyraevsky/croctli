package ru.baccasoft.croctli.android.rest.wrapper;

import org.apache.http.HttpResponse;

import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.ArrayOfEntityProperty;
import ru.baccasoft.croctli.android.gen.core.ArrayOfOperationResponse;
import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.EntityPropertySet;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class EntityPropertyAPI implements IRestApiCall<EntityPropertySet,ArrayOfEntityProperty,ArrayOfString,Void> {
    @SuppressWarnings("unused")
    private static final String TAG = EntityPropertyAPI.class.getSimpleName();

    @Override
    public EntityPropertySet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final long lastSyncDateMillss = App.prefs.getLastSyncDate();
        String url;
        //Ставим флаг нулевой синхронизации,
        //ориентируясь на дату последней синхронизации, при первой авторизации пользователя она == 0
        if(lastSyncDateMillss == 0){
            url = "/EntityProperty/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max) + "/1";
        }else {
            url = "/EntityProperty/Modify/" + dateFrom + "/" + dateTo + "/" + String.valueOf(max);
        }
        final boolean needAuth = true;

        return RestClient.fillJsonClass(EntityPropertySet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfEntityProperty objects) {
        final String api = "/EntityProperty/Modify/" + dateLastSync;
        boolean needAuth = true;
        String jsonData = Utils.getAsJSON(objects);

        HttpResponse rPost = RestClient.callSomeRestApi(api, needAuth, jsonData);
        final String rPostAnswer = Utils.toString(rPost);

        RestClient.fillJsonClass(ArrayOfOperationResponse.class, rPostAnswer);
        return null;
    }
}
