package ru.baccasoft.croctli.android.specialcondition.parser;

public class AddDaysToNowLexemRecognizer implements ILexemRecognizer {

	public static final String HEAD = "datetime.now.adddays(";
	public static final String TAIL = ").date";

	@Override
	public boolean isApplicable(LexerSource source) {
		return source.getUnparsedTail().startsWith(AddDaysToNowLexemRecognizer.HEAD);
	}

	@Override
	public ILexem parse(LexerSource source) throws ParseException {
		String sourceString = source.getUnparsedTail();
		
		int startIdx = HEAD.length();
		int endIdx = sourceString.indexOf(TAIL);
		if( endIdx <= 0 ) {
			throw new ParseException( source, "expected DateTime.Now.AddDays(X).Date");
		}
		
		String numberStr = sourceString.substring(startIdx, endIdx);
		if( !numberStr.matches("[0-9]+") ) {
			throw new ParseException( source, "expected integer inside DateTime.Now.AddDays(X).Date");
		}
		
		ILexem result = new AddDaysToNowLexem(source, Integer.valueOf(numberStr));
		source.moveAhead(endIdx + TAIL.length());
		return result;
	}


}
