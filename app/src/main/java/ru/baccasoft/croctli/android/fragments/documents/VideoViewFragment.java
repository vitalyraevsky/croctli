package ru.baccasoft.croctli.android.fragments.documents;


import android.content.Intent;
import android.net.Uri;

public class VideoViewFragment extends MediaFileViewFragment {

    @Override
    protected void openFile(String path) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        Uri data = Uri.parse(path);
        intent.setDataAndType(data, "video/*");
        startActivity(intent);
    }
}
