package ru.baccasoft.croctli.android.gen.core;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by developer on 15.10.14.
 */
@DatabaseTable(tableName = "array_of_string")
public class StringJson {
    public static final String METAAUTH_ID = "metaauth_id";

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(foreign = true, foreignAutoCreate = true, columnName = METAAUTH_ID)
    public MetaAuth metaAuth;

    @DatabaseField(dataType = DataType.STRING)
    public String item;

    public StringJson() { }

    public StringJson(MetaAuth metaAuth, String item) {
        this.metaAuth = metaAuth;
        this.item = item;
    }
}
