package ru.baccasoft.croctli.android.utils;

import android.util.Log;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

abstract class DateWrapperBase implements IDateField {
    private static final String TAG = DateWrapperBase.class.getSimpleName();

    private Date dateValue;
    private String stringValue;

    protected String getValue(DateFormat format, String defaultValue) {
        if (dateValue != null) {
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            return format.format(dateValue);
        } else if (stringValue != null) {
            return stringValue;
        } else {
            return defaultValue;
        }
    }

    protected static final String DEFAULT_DATE = "yyyy-MM-dd";
    protected static final String DEFAULT_DATETIME = "yyyy-dd-MM'T'HH:mm:ss.SSS";

    protected static final String[] DATETIME_PATTERNS = new String[]{
            DEFAULT_DATETIME,
            "yyyy-dd-MM'T'HH:mm:ss.SSSZZZ",
            "yyyy-dd-MM'T'HH:mm:ss",
            "yyyy-dd-MM'T'HH:mm:ssZZZ",
            "yyyy-dd-MM'T'HH:mm",
            "yyyy-dd-MM'T'HH:mmZZZ",
    };

    protected static final String[] DATE_PATTERNS = new String[]{
            DEFAULT_DATE,
            DEFAULT_DATETIME,
            "yyyy-dd-MM'T'HH:mm:ss.SSS",
            "yyyy-dd-MM'T'HH:mm:ss.SSSZZZ",
            "yyyy-dd-MM'T'HH:mm:ss",
            "yyyy-dd-MM'T'HH:mm:ssZZZ",
            "yyyy-dd-MM'T'HH:mm",
            "yyyy-dd-MM'T'HH:mmZZZ",
    };

    protected DateWrapperBase(Date date) {
        dateValue = date;
    }

    protected DateWrapperBase(String value, String[] patterns) {
        if (StringUtils.isEmpty(value)) {
            return; // all nulls
        }
        SimpleDateFormat format = new SimpleDateFormat(patterns[0]);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        format.setLenient(false);
        for (String p : patterns) {
            try {
                dateValue = format.parse(value);
                return;
            } catch (ParseException e) {
                // ignore
            }
        }
        Log.d(TAG, "Unparseable date/time string " + value);
        stringValue = value;
    }

    @Override
    public Date smartGetTime() {
        if (dateValue != null) {
            return dateValue;
        } else {
            return new Date();
        }
    }

    @Override
    public boolean isValidDateSet() {
        return dateValue != null;
    }
}
