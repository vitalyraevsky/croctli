package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.view.event.ScalarChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

public class StringScalarEditText extends EditText {

    public StringScalarEditText(final Context context, String text, final String entityPropertyId) {
        super(context);
        setMaxLines(1);
        setText(text);
        setEnabled(true);

        setTextColor(context.getResources().getColor(R.color.text_2A2A2A));
        ViewUtils.setTextSize(this, R.dimen.px_16, context);

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                EventBus.getDefault().post(new ScalarChangeEvent(
                                entityPropertyId, s.toString(), context));
            }
        });
    }
}
