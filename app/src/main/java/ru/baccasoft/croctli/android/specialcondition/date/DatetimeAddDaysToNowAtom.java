package ru.baccasoft.croctli.android.specialcondition.date;

import ru.baccasoft.croctli.android.gen.core.Task;

public class DatetimeAddDaysToNowAtom implements IDateOperand {
	
	private final long addMillis;
	
	public DatetimeAddDaysToNowAtom( int days ) {
		addMillis = days * 24*3600*1000;
	}
	
	@Override
	public Integer compute(Task task) {
		return DatetimeNowAtom.millisToInt(addMillis + System.currentTimeMillis());
	}

}
