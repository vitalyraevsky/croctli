package ru.baccasoft.croctli.android.view.wrapper.viewgroup.blocks.test;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.MultiAutoCompleteTextView;
import de.greenrobot.event.EventBus;
import ru.baccasoft.croctli.android.R;
import ru.baccasoft.croctli.android.gen.core.ClassifierItem;
import ru.baccasoft.croctli.android.view.event.ClassifierArrayChangeEvent;
import ru.baccasoft.croctli.android.view.util.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.baccasoft.croctli.android.view.event.ClassifierArrayChangeEvent.OPERATION;

/**
 * Массивные справочные свойства с DisplayMode=1
 *
 */
@Deprecated
public class ClassifierArrayNew extends MultiAutoCompleteTextView implements AdapterView.OnItemClickListener, View.OnKeyListener {
    @SuppressWarnings("unused")
    private static final String TAG = ClassifierArrayNew.class.getSimpleName();

    List<String> classifierItemValues;
    Map<String, String> classifierItemsByValue;
    String entityPropertyId;

    Context mContext;

    /**
     * @param allItems все элементы справочника, доступные для выбора
     * @param initItems изначально выбранные элементы справочника
     * @param entityPropertyId
     * @param context
     */
    public ClassifierArrayNew(List<ClassifierItem> allItems, List<ClassifierItem> initItems, String entityPropertyId, Context context) {
        super(context);
        this.entityPropertyId = entityPropertyId;
        this.mContext = context;

        classifierItemValues = new ArrayList<String>();
        classifierItemsByValue = new HashMap<String, String>();
        for(ClassifierItem ci : allItems) {
            classifierItemValues.add(ci.getDisplayValue());
            classifierItemsByValue.put(ci.getDisplayValue(), ci.getCode());
        }

        init(context, initItems);
    }

    private void init(Context context, List<ClassifierItem> initItems) {
        ViewUtils.setTextSize(this, R.dimen.px_16, context);
//        setMaxLines(5);

        setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown();
            }
        });
        setKeyListener(new DeleteKeyListener());
        setOnItemClickListener(this);
//        setOnKeyListener(this);

        ArrayAdapter<String> adp = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_dropdown_item_1line, classifierItemValues);
        setAdapter(adp);
        setInitialState(initItems);


    }

    /**
     * Вставляем начальные значения
     *
     * @param initItems
     */
    private void setInitialState(List<ClassifierItem> initItems) {
        if(initItems.size() == 0)
            return;

        StringBuilder sb = new StringBuilder();
        sb.append(initItems.get(0).getDisplayValue());
        for(int i = 1;i< initItems.size(); i++) {
            sb.append(",")
                .append(initItems.get(i).getDisplayValue());
        }

        Log.d("", "selected items: " + sb);
        placeBubbles(sb.toString());
    }

    //{{{

    private void placeBubblesFromContent() {
        Log.d(TAG, "text: " + getText().toString());
        if(!getText().toString().contains(","))   // check separator in string
            return;

        placeBubbles(getText().toString());
    }

    private void placeBubbles(String text) {
        Log.d(TAG, "placeBubbles");
        SpannableStringBuilder ssb = new SpannableStringBuilder(text);

        String chips[] = text.trim().split(",");
        List<String> selectedItemsCodes = new ArrayList<String>();
        int x = 0;
        for (String c : chips) {
            String displayValue = c.trim();
            Log.d("", "chip: " + c);
            String code = classifierItemsByValue.get(displayValue);
            selectedItemsCodes.add(code);
            ssb.setSpan(new ClassifierItemSpan(code), x, x + c.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            x = x + c.length() + 1;
        }
//        SpannableStringBuilder ssb = new SpannableStringBuilder()
        setText(ssb);
        setTextColor(getResources().getColor(R.color.text_2A2A2A));
        setSelection(getText().length());
        sendSelectedValues(selectedItemsCodes);
    }
    //}}}

    /**
     * Отправляем, какие значения были выбраны
     * @param selectedItemsCodes
     */
    private void sendSelectedValues(List<String> selectedItemsCodes) {
        for(String s :selectedItemsCodes) {
            Log.d("", "sending code:\'"+s+"\'");
            EventBus.getDefault().post(new ClassifierArrayChangeEvent(
                    entityPropertyId, s, OPERATION.ADD));
//            EventBus.getDefault().post(new ValueModifiedEvent(entityPropertyId, s, VIEW_TYPE.CLASSIFIER_ARRAY));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        placeBubblesFromContent();
    }

//    private TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            Log.d(TAG, "beforeTextChanged");
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            Log.d(TAG, "onTextChanged");
//            if (count < 1) { return; }
//            if(s.charAt(start) == ',')
//                placeBubblesFromContent();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//            Log.d(TAG, "afterTextChanged");
//        }
//    };


    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyPreIme");
//        return super.onKeyPreIme(keyCode, event);
        Log.d(TAG, "keyCode:\'" + keyCode + "\'");
        Log.d(TAG, "action:\'" + event.getAction() + "\'");

        return true;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        Log.d(TAG, "onKey");
        if(keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP) {
//            placeBubblesFromContent();
            deleteLastBubbleAndPlaceIt();
        }
        return false;
//        return true;
    }

    /**
     * Удалить последний элемент из списка
     */
    private void deleteLastBubbleAndPlaceIt() {
        final String TAG = ClassifierArrayNew.TAG + "[deleteLastBubbleAndPlaceIt]";
        Log.d(TAG, "orig text: \'" + getText().toString() + "\'");

        String text = getText().toString().trim();
        if(text.length() == 0)
            return;

        String chips[] = text.trim().split(",");
        text = "";

        // убираем последний элемент из списка
        int lastItemId = chips.length - 1;
        for(int i = 0; i < lastItemId; i++)
            text += chips[i].trim() + ",";

//        // удаляем последнюю запятую
//        if(text.length() > 0)
//            text = text.substring(0, text.length() - 1);

        Log.d(TAG, "text without last item: \'" + text + "\'");

        placeBubbles(text);
    }

}
