
package ru.baccasoft.croctli.android.gen.core;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * <p>Java class for Action complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Action">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LocalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Script" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WithoutComplete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Behaviors" type="{http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model}ArrayOfActionBehavior"/>
 *         &lt;element name="UserMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Action", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", propOrder = {
    "id",
    "name",
    "localName",
    "script",
    "withoutComplete",
    "behaviors",
    "userMessage"
})
@DatabaseTable(tableName = "action")
@JsonIgnoreProperties({"task", "behaviorsCollection"})
public class Action {

    public static final String TASK_ID_FIELD = "task_foreign_id_field";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = TASK_ID_FIELD)
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @DatabaseField(id = true, dataType = DataType.STRING)
    @JsonProperty("Id")
    @XmlElement(name = "Id", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String id;

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("Name")
    @XmlElement(name = "Name", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String name;

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("LocalName")
    @XmlElement(name = "LocalName", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String localName;

    @DatabaseField(dataType = DataType.STRING)
    @JsonProperty("Script")
    @XmlElement(name = "Script", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true, nillable = true)
    protected String script;

    @DatabaseField(dataType = DataType.BOOLEAN)
    @JsonProperty("WithoutComplete")
    @XmlElement(name = "WithoutComplete", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model")
    protected boolean withoutComplete;

//---
    @JsonProperty("Behaviors")
    @XmlElement(name = "Behaviors", namespace = "http://www.wli.croc.ru/ISync.v1/Croc.WLI.Core.Model", required = true)
    protected ArrayOfActionBehavior behaviors;

    @ForeignCollectionField(eager = true, maxEagerLevel = 99)
    public Collection<ActionBehavior> behaviorsCollection;
//---

    @JsonProperty("UserMessage")
    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    @XmlElement(name = "UserMessage", required = true, nillable = true)
    protected String userMessage;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the localName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Sets the value of the localName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalName(String value) {
        this.localName = value;
    }

    /**
     * Gets the value of the script property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScript() {
        return script;
    }

    /**
     * Sets the value of the script property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScript(String value) {
        this.script = value;
    }

    /**
     * Gets the value of the withoutComplete property.
     * 
     */
    public boolean isWithoutComplete() {
        return withoutComplete;
    }

    /**
     * Sets the value of the withoutComplete property.
     * 
     */
    public void setWithoutComplete(boolean value) {
        this.withoutComplete = value;
    }

    /**
     * Gets the value of the behaviors property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfActionBehavior }
     *     
     */
    public ArrayOfActionBehavior getBehaviors() {
        return behaviors;
    }

    /**
     * Sets the value of the behaviors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfActionBehavior }
     *     
     */
    public void setBehaviors(ArrayOfActionBehavior value) {
        this.behaviors = value;
    }

    /**
     * Gets the value of the userMessage property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * Sets the value of the userMessage property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserMessage(String value) {
        this.userMessage = value;
    }

}
