package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.Adapter;

import java.sql.SQLException;

public class AdapterDao extends BaseDaoImpl<Adapter, String> {
    public AdapterDao(ConnectionSource connectionSource, Class<Adapter> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
