package ru.baccasoft.croctli.android.fragments.documents.converters;


import android.app.Activity;
import android.util.Log;

import org.docx4j.XmlUtils;
import org.docx4j.convert.out.html.HtmlExporterNonXSLT;
import org.docx4j.model.images.ConversionImageHandler;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io.LoadFromZipNG;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.P;

import java.util.ArrayList;
import java.util.List;

import ae.javax.xml.bind.JAXBException;
import ru.baccasoft.croctli.android.fragments.documents.converters.utils.AndroidFileConversionImageHandler;

public class DocXProcessor implements ConvertationProcessor {

    private static final String TAG = DocXProcessor.class.getSimpleName();

    private WordprocessingMLPackage wordMLPackage;

    @Override
    public void init( String officeFileName ) throws Docx4JException {
        Log.d(TAG,"init docx converter with file: " + officeFileName );
        final LoadFromZipNG loader = new LoadFromZipNG();
        wordMLPackage = (WordprocessingMLPackage) loader.get(officeFileName);
    }

    @Override
    public String convertToHtml(String imageDirPath, String targetUri, Activity activity) throws JAXBException {

        // создаем обработчик картинок
        ConversionImageHandler conversionImageHandler = new AndroidFileConversionImageHandler(imageDirPath,targetUri,false,activity);

        List<Object> objs = wordMLPackage.getMainDocumentPart().getContent();
        List<Object> wraped = wrapRootTables(objs);

        wordMLPackage.getMainDocumentPart().getContent().clear();
        wordMLPackage.getMainDocumentPart().getContent().addAll(wraped);

        // в некоторых документах не получается получить StyleTree
        // это грязный хак с проверкой, если мы не можем получить StyleTree то сбрасываем настройки по умолчанию для стилей
        if( isNoAbleToGetStyleTree(wordMLPackage) ){
                wordMLPackage.getMainDocumentPart().getStyleDefinitionsPart().unmarshalDefaultStyles();
        }

        HtmlExporterNonXSLT withoutXSLT = new HtmlExporterNonXSLT(wordMLPackage, conversionImageHandler );

        return XmlUtils.w3CDomNodeToString(withoutXSLT.export());
    }

    private List<Object> wrapRootTables(List<Object> objs) {
        List<Object> result = new ArrayList<Object>();
        for(Object o: objs){
            if(o instanceof org.docx4j.wml.Tbl ||
                    o instanceof ae.javax.xml.bind.JAXBElement ){
                P p = wrapTable(o);
                result.add(p);
            } else {
                result.add(o);
            }
        }
        return result;
    }

    private P wrapTable(Object o) {
        org.docx4j.wml.ObjectFactory factory = org.docx4j.jaxb.Context.getWmlObjectFactory();
        org.docx4j.wml.P  p = factory.createP();

        org.docx4j.wml.R  run = factory.createR();
        p.getContent().add(run);
        run.getContent().add(o);
        return p;
    }

    public void logContent(String level,List<Object> objs){

        Log.i(TAG, level + "childs: objs " + String.valueOf(objs == null ? 0 : objs.size()));

        for(Object o : objs){

            Log.i(TAG, "<" + o.getClass().getName()+">");

            if (o instanceof org.docx4j.wml.P) {
                logContent("p",((P)o).getContent());
            } else if (o instanceof org.docx4j.wml.R) {
                logContent("r",((org.docx4j.wml.R)o).getContent());
            } else if (o instanceof org.docx4j.wml.Text) {
            } else if (o instanceof org.docx4j.wml.Tbl) {
                logContent("tbl",((org.docx4j.wml.Tbl)o).getContent());
            } else if (o instanceof org.docx4j.dml.wordprocessingDrawing.Inline
                    || o instanceof org.docx4j.dml.wordprocessingDrawing.Anchor) {
            } else if (o instanceof org.docx4j.dml.CTBlip) {
            } else if (o instanceof org.docx4j.wml.Pict) {
            } else {
            }
            Log.i(TAG, "<" + o.getClass().getName()+"/>");
        }
    }

    public boolean isNoAbleToGetStyleTree(WordprocessingMLPackage pckg) {
        boolean result = true;
        try {
            Log.d(TAG,"check ");
            pckg.getMainDocumentPart().getStyleTree().getCharacterStylesTree();
            pckg.getMainDocumentPart().getStyleTree().getParagraphStylesTree();
            result = false;
        } catch(Exception e){
            Log.e(TAG,"not able to build tree", e );
        }
        return result;
    }
}
