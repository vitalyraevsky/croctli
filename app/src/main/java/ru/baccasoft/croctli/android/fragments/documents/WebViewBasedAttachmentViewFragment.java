package ru.baccasoft.croctli.android.fragments.documents;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.baccasoft.croctli.android.R;

public abstract class WebViewBasedAttachmentViewFragment extends AttachmentViewFragment {

    private static final String TAG = WebViewBasedAttachmentViewFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attach_webview, null );
        WebView webView = (WebView) view.findViewById(R.id.content);
        webView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webView.setHorizontalFadingEdgeEnabled(false);
        webView.setVerticalFadingEdgeEnabled(false);
        webView.getSettings().setJavaScriptEnabled(false);

        // не кэшируем данные, иначе вэбвью будет показывать старые данные по файлу даже если он изменился
        webView.setWebViewClient( new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.clearCache(true);
            }
        });

        loadContent(webView);

        return view;
    }

    public abstract void loadContent(WebView webView);

}
