package ru.baccasoft.croctli.android.rest.wrapper;

import ru.baccasoft.croctli.android.gen.core.ArrayOfString;
import ru.baccasoft.croctli.android.gen.core.ArrayOfTaskStateInSystem;
import ru.baccasoft.croctli.android.gen.core.StringSet;
import ru.baccasoft.croctli.android.gen.core.TaskStateInSystemSet;
import ru.baccasoft.croctli.android.rest.RestClient;
import ru.baccasoft.croctli.android.rest.RestApiDate;
import ru.baccasoft.croctli.android.rest.Utils;

public class TaskStateInSystemAPI implements IRestApiCall<TaskStateInSystemSet, ArrayOfTaskStateInSystem, ArrayOfString, Void> {
    @SuppressWarnings("unused")
    private static final String TAG = TaskStateInSystemAPI.class.getSimpleName();

    private static final String CALL_PREFIX = "TaskStateInSystem";

    @Override
    public TaskStateInSystemSet modifyGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = Utils.getApiString(CALL_PREFIX, Utils.apiCallMethods.MODIFY, dateFrom, dateTo, max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(TaskStateInSystemSet.class, url, needAuth);
    }

    @Override
    public StringSet deleteGet(RestApiDate dateFrom, RestApiDate dateTo, int max) {
        final String url = Utils.getApiString(CALL_PREFIX, Utils.apiCallMethods.DELETE, dateFrom, dateTo, max);
        final boolean needAuth = true;

        return RestClient.fillJsonClass(StringSet.class, url, needAuth);
    }

    @Override
    public void deletePost(RestApiDate dateLastSync, ArrayOfString objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }

    @Override
    public Void modifyPost(RestApiDate dateLastSync, ArrayOfTaskStateInSystem objects) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support this method.");
    }
}
