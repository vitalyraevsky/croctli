package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.gen.core.ConditionOperator;

import java.sql.SQLException;

public class ConditionOperatorDao extends BaseDaoImpl<ConditionOperator, String> {
    public ConditionOperatorDao(ConnectionSource connectionSource, Class<ConditionOperator> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
