package ru.baccasoft.croctli.android.updater;


import ru.baccasoft.croctli.android.rest.RestApiDate;

/**
 * Результат первого шага в первоначальной синхронизации
 * Даты могут быть null, в случае если результатт == FAILED
 */
public class StepOneResult {

    public enum FIRST_SYNC_STATUS {
        SUCCESS , FAILED , SKIP, UNKNOWN
    }

    public FIRST_SYNC_STATUS status = FIRST_SYNC_STATUS.UNKNOWN;
    public RestApiDate dateFrom;
    public RestApiDate dateTo;

}
