package ru.baccasoft.croctli.android.db.tables;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import ru.baccasoft.croctli.android.App;
import ru.baccasoft.croctli.android.gen.core.Filter;
import ru.baccasoft.croctli.android.gen.core.FilterCondition;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FilterDao extends BaseDaoImpl<Filter, String> {
    public FilterDao(ConnectionSource connectionSource, Class dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public int create(Filter filter) throws SQLException {
        String id = extractId(filter);
        int ret;
        if (id == null || !idExists(id)) {
            ret = super.create(filter);
        } else {
            ret = super.update(filter);
        }

        List<FilterCondition> origFilterConditions = filter.getConditions().getItem();
        if(origFilterConditions == null) {
            filter.setFilterConditions(new ArrayList<FilterCondition>(0));
        } else {
            filter.setFilterConditions(new ArrayList<FilterCondition>(origFilterConditions));
        }

        for (FilterCondition condition : filter.getFilterConditions()) {
            condition.setFilter(filter);
            ret += App.getDatabaseHelper().getFilterConditionDao().create(condition);
        }
        return ret;
    }
}
