package ru.baccasoft.croctli.android.rest;

import ru.baccasoft.croctli.android.gen.CrocSet;
import ru.baccasoft.croctli.android.rest.wrapper.IRestApiCall;

import java.util.ArrayList;
import java.util.List;

public class RestApiRecursiveCall<S extends CrocSet, T> {
    /**
     * Дозапрашиваем по serverTime
     */
    public List<T> getRestApiSetAsList(RestApiDate dateFrom, RestApiDate dateTo,
                                        int count, IRestApiCall apiCaller) {
        List<T> list = new ArrayList<T>();

        @SuppressWarnings("unchecked")
        S set = (S) apiCaller.modifyGet(dateFrom, dateTo, count);

        @SuppressWarnings("unchecked")
        List<T> items = set.getItems();

        list.addAll(items);

        RestApiDate lastEntryDate = new RestApiDate(set.getServerTimeString(), false);
        if (lastEntryDate.getMillis() != dateTo.getMillis()) {
            list.addAll(getRestApiSetAsList(lastEntryDate, dateTo, count, apiCaller));
        }

        return list;
    }
}
