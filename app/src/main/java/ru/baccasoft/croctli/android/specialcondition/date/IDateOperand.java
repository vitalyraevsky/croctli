package ru.baccasoft.croctli.android.specialcondition.date;

import ru.baccasoft.croctli.android.specialcondition.IExpression;

/**
 * Для непустых дат compute() возврашает дату в виде числа yyyyMMdd, например 01 ноября 2014 г будет представлено как 20141101.
 * На пустых датах compute() падает!
 */
public interface IDateOperand extends IExpression<Integer> {
}
