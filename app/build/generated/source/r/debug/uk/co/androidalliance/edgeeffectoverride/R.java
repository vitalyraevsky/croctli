/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package uk.co.androidalliance.edgeeffectoverride;

public final class R {
	public static final class attr {
		public static final int edgeeffect_color = 0x7f010006;
	}
	public static final class color {
		public static final int default_edgeeffect_color = 0x7f09000a;
	}
	public static final class drawable {
		public static final int overscroll_edge = 0x7f0200c7;
		public static final int overscroll_glow = 0x7f0200c8;
	}
	public static final class styleable {
		public static final int[] EdgeEffectView = { 0x7f010006 };
		public static final int EdgeEffectView_edgeeffect_color = 0;
	}
}
